
;
;little demo to dump the contents of a B3D file
;

Include "b3dfile.bb"

f_name$=Input$( "B3D file to dump:" )

file=ReadFile( f_name$ )
If Not file RuntimeError "Unable to open b3d file"

b3dSetFile( file )

If b3dReadChunk$()<>"BB3D" RuntimeError "Invalid b3d file"

If b3dReadInt()/100>0 RuntimeError "Invalid b3d file version"

Print "Dumping file to debug log..."

DebugLog "File opened..."

DumpChunks()

b3dExitChunk()

CloseFile file

WaitKey

End

Function DumpChunks( tab$="" )
	tt$=tab$
	tab$=tab$+"  "
	While b3dChunkSize()
		chunk$=b3dReadChunk$()
		DebugLog tt$+"Chunk:"+chunk$+" (size="+b3dChunkSize()+")"
		Select chunk$
		Case "ANIM"
			flags=b3dReadInt()
			n_frames=b3dReadInt()
			fps=b3dReadFloat()
			DebugLog tab$+flags+" "+n_frames+" "+fps
		Case "KEYS"
			flags=b3dReadInt()
			sz=4
			If flags And 1 sz=sz+12
			If flags And 2 sz=sz+12
			If flags And 4 sz=sz+16
			n_keys=b3dChunkSize()/sz
			If n_keys*sz=b3dChunkSize() 
				DebugLog tab$+n_keys+" keys"
				;read all keys in chunk
				While b3dChunkSize()
					frame=b3dReadInt()
					If flags And 1
						key_px#=b3dReadFloat()
						key_py#=b3dReadFloat()
						key_pz#=b3dReadFloat()
					EndIf
					If flags And 2
						key_sx#=b3dReadFloat()
						key_sy#=b3dReadFloat()
						key_sz#=b3dReadFloat()
					EndIf
					If flags And 4
						key_rw#=b3dReadFloat()
						key_rx#=b3dReadFloat()
						key_ry#=b3dReadFloat()
						key_rz#=b3dReadFloat()
					EndIf
				Wend
			Else
				DebugLog tab$+"***** Illegal number of keys *****"
			EndIf
		Case "TEXS"
			While b3dChunkSize()
				name$=b3dReadString$()
				flags=b3dReadInt()
				blend=b3dReadInt()
				x_pos#=b3dReadFloat()
				y_pos#=b3dReadFloat()
				x_scl#=b3dReadFloat()
				y_scl#=b3dReadFloat()
				rot#=b3dReadFloat()
				DebugLog tab$+name$
			Wend
		Case "BRUS"
			n_texs=b3dReadInt()
			;read all brushes in chunk...
			While b3dChunkSize()
				name$=b3dReadString$()
				red#=b3dReadFloat()
				grn#=b3dReadFloat()
				blu#=b3dReadFloat()
				alp#=b3dReadFloat()
				shi#=b3dReadFloat()
				blend=b3dReadInt()
				fx=b3dReadInt()
				For k=0 To n_texs-1
					tex_id=b3dReadInt()
				Next
				DebugLog tab$+name$
			Wend
		Case "VRTS"
			flags=b3dReadInt()
			tc_sets=b3dReadInt()
			tc_size=b3dReadInt()
			sz=12+tc_sets*tc_size*4
			If flags And 1 Then sz=sz+12
			If flags And 2 Then sz=sz+16
			n_verts=b3dChunkSize()/sz
			If n_verts*sz=b3dChunkSize() 
				DebugLog tab$+n_verts+" vertices"
				;read all verts in chunk
				While b3dChunkSize()
					x#=b3dReadFloat()
					y#=b3dReadFloat()
					z#=b3dReadFloat()
					If flags And 1
						nx#=b3dReadFloat()
						ny#=b3dReadFloat()
						nz#=b3dReadFloat()
					EndIf
					If flags And 2
						red#=b3dReadFloat()
						grn#=b3dReadFloat()
						blu#=b3dReadFloat()
						lfa#=b3dReadFloat()
					EndIf
					;read tex coords...
					For j=1 To tc_sets*tc_size
						b3dReadFloat()
					Next
				Wend
			Else
				DebugLog tab$+"***** Illegal number of vertices *****"
			EndIf
		Case "TRIS"
			brush_id=b3dReadInt()
			sz=12
			n_tris=b3dChunkSize()/sz
			If n_tris*sz=b3dChunkSize()
				DebugLog tab$+n_tris+" triangles"
				;read all tris in chunk
				While b3dChunkSize()
					v0=b3dReadInt()
					v1=b3dReadInt()
					v2=b3dReadInt()
				Wend
			Else
				DebugLog tab$+"***** Illegal number of triangles *****"
			EndIf
		Case "MESH"
			brush_id=b3dReadInt()
			DebugLog tab$+"brush="+brush_id
		Case "BONE"
			sz=8
			n_weights=b3dChunkSize()/sz
			If n_weights*sz=b3dChunkSize()
				DebugLog tab$+n_weights+" weights"
				;read all weights
				While b3dChunkSize()
					vertex_id=b3dReadInt()
					weight#=b3dReadFloat()
				Wend
			Else
				DebugLog tab$+"***** Illegal number of bone weights *****"
			EndIf
		Case "NODE"
			name$=b3dReadString$()
			x_pos#=b3dReadFloat()
			y_pos#=b3dReadFloat()
			z_pos#=b3dReadFloat()
			x_scl#=b3dReadFloat()
			y_scl#=b3dReadFloat()
			z_scl#=b3dReadFloat()
			w_rot#=b3dReadFloat()
			x_rot#=b3dReadFloat()
			y_rot#=b3dReadFloat()
			z_rot#=b3dReadFloat()
			DebugLog tab$+"name="+name$
		End Select

		DumpChunks( tab$ )	;dump any subchunks
		b3dExitChunk()		;exit this chunk
		
	Wend
End Function