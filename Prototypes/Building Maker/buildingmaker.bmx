Import "inc/bbtype.bmx"
Import "inc/bbvkey.bmx"

Import sidesign.minib3d	
Include "inc/b3dfile.bmx"





SetGraphicsDriver GLGraphicsDriver(),GRAPHICS_BACKBUFFER|GRAPHICS_DEPTHBUFFER

	
Global Mouse:TMouse = TMouse.Init()
	

Global BaseModel
Global RoofModel
Global DoorModel 
Global BaseModelBase



Global add[50]


Local UpKey:Int
Local DownKey:Int
Local LeftKey:Int
Local RightKey:Int
Local MouseRight:Int
Local SpaceKey:Int

Local KeyW:Int
Local KeyA:Int
Local KeyS:Int
Local KeyD:Int

Global camera:TCamera
Global skydome':CEntity
Global mesh':CEntity
Global texture':CTexture
Global res:Byte
Global Terrain
Global RoofX:Float,RoofY:Float,RoofZ:Float
Global DoorX:Float,DoorY:Float,DoorZ:Float
Global WindowsTex[10],WindowsCnt[10],WindowsX[10],WindowsY[10],WindowsZ[10],WinChild[10]

Global c_surfs
Global WindowCnt:String
Global BaseMesh 


Global KeyX:Float
Global KeyY:Float
Global KeyZ:Float

Global nodeselected:Int
Global RoofPoint':CEntity


Global  MouseXEndPosi:Float
Global  MouseYEndPosi:Float

Global Window:TGadget=CreateWindow("BuildingMaker",0,00,978,704,0,3)
	Global tabTabber0:TGadget=CreateTabber(10,12,245,658,Window)
	AddGadgetItem tabTabber0,"Base", True
	AddGadgetItem tabTabber0,"Roofs", False
	AddGadgetItem tabTabber0,"Windows", False
	AddGadgetItem tabTabber0,"Doors", False
	SetGadgetLayout tabTabber0,1,2,1,2
		Global TabPanel0:TGadget=CreatePanel(0,0,ClientWidth(tabTabber0),ClientHeight(tabTabber0),tabTabber0,0)
		SetGadgetLayout TabPanel0,1,1,1,1
			Global trvTreeView0:TGadget=CreateTreeView(2,14,234,593,TabPanel0)
			SetGadgetLayout trvTreeView0,1,0,1,0
		Global TabPanel1:TGadget=CreatePanel(0,0,ClientWidth(tabTabber0),ClientHeight(tabTabber0),tabTabber0,0)
		HideGadget TabPanel1
		SetGadgetLayout TabPanel1,1,1,1,1
		HideGadget TabPanel1
			Global trvTreeView1:TGadget=CreateTreeView(3,12,234,595,TabPanel1)
			SetGadgetLayout trvTreeView1,1,0,1,0
		Global TabPanel2:TGadget=CreatePanel(0,0,ClientWidth(tabTabber0),ClientHeight(tabTabber0),tabTabber0,0)
		HideGadget TabPanel2
		SetGadgetLayout TabPanel2,1,1,1,1
		HideGadget TabPanel2
		
		Global sidebutt:TGadget=CreateButton("Side Window",124,83,96,16,TabPanel2,3)
		SetGadgetLayout sidebutt,1,0,1,0
				
		Global cobComboBox1:TGadget=CreateComboBox(127,12,96,20,TabPanel2)
			AddGadgetItem cobComboBox1,"1"
			AddGadgetItem cobComboBox1,"2"
			AddGadgetItem cobComboBox1,"3"
			AddGadgetItem cobComboBox1,"4"
			AddGadgetItem cobComboBox1,"5"
			AddGadgetItem cobComboBox1,"6"
			SelectGadgetItem cobComboBox1,0
			SetGadgetLayout cobComboBox1,1,0,1,0
		Global windowselected:TGadget=CreateComboBox(127,47,96,20,TabPanel2)
			AddGadgetItem windowselected,"1"
			AddGadgetItem windowselected,"2"
			AddGadgetItem windowselected,"3"
			AddGadgetItem windowselected,"4"
			AddGadgetItem windowselected,"5"
			AddGadgetItem windowselected,"6"
			SelectGadgetItem windowselected,0
			SetGadgetLayout windowselected,1,0,1,0
		Global SetWindowTex:TGadget=CreateButton("Set Texture",12,50,103,15,TabPanel2,0)
			SetGadgetLayout SetWindowTex,1,0,1,0
		Global SetWindowCnt:TGadget=CreateButton("Set Window Amount",14,15,99,16,TabPanel2,0)
			SetGadgetLayout SetWindowCnt,1,0,1,0
		Global TabPanel3:TGadget=CreatePanel(0,0,ClientWidth(tabTabber0),ClientHeight(tabTabber0),tabTabber0,0)
		HideGadget TabPanel3
		SetGadgetLayout TabPanel3,1,1,1,1
		HideGadget TabPanel3
			Global trvTreeView4:TGadget=CreateTreeView(3,10,235,593,TabPanel3)
			SetGadgetLayout trvTreeView4,1,0,1,0
	Global cavCanvas0:TGadget=CreateCanvas(259,32,701,637,Window)
	SetGadgetLayout cavCanvas0,1,2,1,2
	Global exportmeshbutt:TGadget=CreateButton("Export Building",836,3,77,24,Window,1)
	SetGadgetLayout exportmeshbutt,1,0,1,0

	
IcoStrip = LoadIconStrip("media/icostrip.png")	

SetGadgetIconStrip trvTreeView0,IcoStrip 
SetGadgetIconStrip trvTreeView1,IcoStrip 	
'SetGadgetIconStrip trvTreeView2,IcoStrip 	
SetGadgetIconStrip trvTreeView4,IcoStrip 
	
	
	
Global hwnd=QueryGadget(cavCanvas0, QUERY_HWND)




	
	'If Not dir RuntimeError "failed to read current directory"
Global t:String
Global ext:String
Global basedir:Tgadget 
Global roofdir:Tgadget
Global texturedir[1000]
Global texcount
Global count
Global filenames:String


Global Model_List:TList = CreateList()
Global Roof_List:TList = CreateList()
Global Texture_List:TList = CreateList()
Global Texturebase_list:TList = CreateList()
Global Textureroof_list:TList = CreateList()
Global Texturebeam_list:TList = CreateList()
Global Texturewindow_list:TList = CreateList()
Global Door_list:TList = CreateList()
Global DoorPoint 
Global Surf_list:TList = CreateList()


'-------------------------------------------GET BASE FILES------------------------------------------------------
enumFiles(Model_List,"Meshes/Base/")	
enumFiles(Roof_List,"Meshes/Roofs/")
enumFiles(Door_List,"Meshes/Doors/")
enumFiles(Texture_list,"Textures/")







basedir=AddTreeViewNode("bases",trvTreeView0,1)

For t$=EachIn Model_List

	

	't$ = NextFile(dir)
	If t="" Exit
	If t = "." Or t = ".." Continue
	
		ext = ExtractExt(t$)
		
			Select ext
				Case "b3d"
					AddTreeViewNode StripAll(t),basedir,2
			
			End Select
Next		



'----------------------------------------------------------------------------------------------------------------

'---------------------------------------------GET ROOF FILES-----------------------------------------------------

roofdir=AddTreeViewNode("Roofs",trvTreeView1,1)	
	For r$=EachIn roof_List
	

	't$ = NextFile(dir)
	If r="" Exit
	If r = "." Or r = ".." Continue
	
		ext = ExtractExt(r$)
		
			Select ext
				Case "b3d"
					AddTreeViewNode StripAll(r),roofdir,2

				
			End Select
	Next		
'------------------------------------------------------------------------------------------------------------------



'---------------------------------------------GET Door FILES-----------------------------------------------------

Doordir=AddTreeViewNode("Doors",trvTreeView4,1)	
	For r$=EachIn Door_List
	

	't$ = NextFile(dir)
	If r="" Exit
	If r = "." Or r = ".." Continue
	
		ext = ExtractExt(r$)
		
			Select ext
				Case "b3d"
					AddTreeViewNode StripAll(r),Doordir,2
				
			End Select
	Next		




' ---------------------------------------
' Initialisation graphics
' ---------------------------------------



	TGlobal.width = Window.clientwidth()
	TGlobal.height = Window.clientheight()


	TGlobal.depth=32
	TGlobal.mode=0
	TGlobal.rate=60

	SetGraphics CanvasGraphics(cavCanvas0)

	TTexture.TextureFilter("",9)
		
	glewInit() ' required for glActiveTexture

	glEnable(GL_LIGHTING)
	glLightModeli(GL_LIGHT_MODEL_COLOR_CONTROL,GL_SEPARATE_SPECULAR_COLOR)
	glLightModeli(GL_LIGHT_MODEL_LOCAL_VIEWER,GL_TRUE)
		
	glEnable(GL_NORMALIZE)
	
	glEnable(GL_SCISSOR_TEST)

	glEnable(GL_DEPTH_TEST)
	glClearDepth(1.0)						
	glDepthFunc(GL_LEQUAL)
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST)

	glAlphaFunc(GL_GEQUAL,0.5)
			
	glEnableClientState(GL_VERTEX_ARRAY)
	glEnableClientState(GL_COLOR_ARRAY)
	glEnableClientState(GL_NORMAL_ARRAY)

	glLineWidth(4)
	
' ---------------------------------------
' Change to "media" folder
' ---------------------------------------
ChangeDir("media")


' ---------------------------------------
' Define ambiant color
' ---------------------------------------

AmbientLight(128 , 128 , 128)


' ---------------------------------------
'   Skybox
' ---------------------------------------

'Global mesh_skybox = MakeSkyBox("media/sky")
'ScaleEntity mesh_skybox , .5 , .5 , .5


' ---------------------------------------
' Camera
' ---------------------------------------
camera= CreateCamera()
TranslateEntity(camera, 0,3,-10)
CameraClsColor(camera, 0, 0, 0)

CameraClsColor camera,0,0,128
CameraRange camera,1,4000


'-mainloop--------------------------------------------------------------
CreateTimer( 60 )

While True

		Mouse.Update()
		
 WaitEvent()

Select EventID()

		Case EVENT_GADGETACTION						' interacted with gadget
			DoGadgetAction(EventSource())
			
		Case EVENT_KEYDOWN
					Select EventData()
					
								Case KEY_UP
									UpKey = True
									
								Case KEY_DOWN
									DownKey = True	
											
								Case KEY_LEFT
									LeftKey = True
									
								Case KEY_RIGHT
									RightKey = True

								Case KEY_W
									KeyW = True
								Case KEY_A
									KeyA = True
									
								Case KEY_S
									KeyS = True
									
								Case KEY_D
									KeyD = True 
									
								Case Key_Space
									Spacekey = True
									

					End Select

	Case EVENT_KEYUP
						Select EventData()
						
							Case KEY_UP
								UpKey = False
								
							Case KEY_DOWN
								DownKey = False	
										
							Case KEY_LEFT
								LeftKey = False
								
							Case KEY_RIGHT
								RightKey = False	
								
							Case KEY_W
									KeyW = False
									
							Case KEY_A
									KeyA = False
									
							Case KEY_S
							
									KeyS = False
							Case KEY_D
									KeyD = False 
									
							Case Key_Space
								spacekey = False	
								
									
						EndSelect
			
					
			Case EVENT_MOUSEDOWN
						Select EventData()
							Case 1
							
								MouseRight = True
							Case 2
							
							
							
						End Select		


									
		    Case EVENT_MOUSEMOVE
	             MouseXEndPosi = EventX()
	             MouseYEndPosi = EventY()
								
									
											
			Case EVENT_MOUSEUP
						Select EventData()
						Case MouseRight 
								MouseRight  = False
					    End Select
					
					  	 	flagXDown=0

			Case EVENT_WINDOWCLOSE
					
					   'ClearGraphics()
                       End

			Case EVENT_WINDOWSIZE

				    TGlobal.width = Window.clientwidth()
					TGlobal.height = Window.clientheight()
				


 
			
			 Case EVENT_TIMERTICK
                       
					'RedrawGadget cavCanvas0
					'RenderWorld()
 							cx#= 0
							cz#= 0
							

							If UpKey  Then cz#=cz#+5.0
							If LeftKey Then cx#=cx#-5.0
							If RightKey Then cx#=cx#+5.0
							If DownKey Then cz# = cz# - 5.0
							
							
							'If KeyW Then KeyZ = KeyZ + .1'pitch#=pitch#-1.0
							'If KeyA Then KeyZ = KeyZ - .1'yaw# = yaw# + 1.0
							''If KeyS Then KeyX = KeyX + .1'pitch# = pitch# + 1.0
							If KeyD Then KeyX = KeyX - .1'yaw# = yaw# - 1.0
							
							Rem
							If MouseRight Then
															
									If terrain Then
							
										res = DM_PickTerrain(camera,MouseXEndPosi,MouseYEndPosi)	
													
									EndIf

      								If res = True
        								'Sphere:CEntity = DM_CopyEntity(sphere, COPY_REF)
										'DebugLog DM_pickedX()
										'DebugLog DM_pickedY()
										'DebugLog DM_pickedZ()
        								DM_PositionEntity(Sphere, DM_PickedX() , DM_PickedY()+1, DM_PickedZ())
      								EndIf
      						EndIf	
							
							End Rem
								
							If spacekey Then
							
								RotateEntity camera,Mouse.y,Mouse.x,0
								
							EndIf
				
								MoveEntity(camera,cx# * 0.5 , cy# * 0.5 , cz# * 0.5)

			
						
							RedrawGadget cavCanvas0
					
          
			 Case EVENT_GADGETPAINT
				
	
					' SetGraphics CanvasGraphics(cavCanvas0)
					


						'UpdateWorld()
						RenderWorld()
						renders=renders+1
	
						If MilliSecs()-old_ms>=1000
							old_ms=MilliSecs()
							fps=renders
							renders=0
						EndIf
						


			Flip
			
  				'DrawText(10, 15, "FPS : " + fps)  										
			                      
        EndSelect
Wend
		
		

'-gadget actions--------------------------------------------------------

Function DoGadgetAction( gadget:Object )
	Select gadget
	
	
		Case SetWindowCnt	
			WindowCnt$ = GadgetItemText(cobComboBox1,SelectedGadgetItem(cobComboBox1))
			ChangeDir("../Meshes/")
				For i = 1 To WindowCnt.ToInt()
					WindowsCnt[i] = LoadMesh("Windows/Window1.b3d")
					PositionEntity WindowsCnt[i],WindowsX[i],WindowsY[i],WindowsZ[i]
				Next
			

			
		Case SetWindowTex
				WindowCnt$ = GadgetItemText(windowselected,SelectedGadgetItem(windowselected))
				texfile$ = RequestFile("Pick Base Texture","bmp,png,jpg")
				WindowsTex[WindowCnt.ToInt()] = LoadTexture(texfile$)
				TextureLoadedMesh(WindowsCnt[WindowCnt.ToInt()],WindowsTex[WindowCnt.ToInt()])

		'WindowsTex[6],WindowsCnt[6],WindowsX[6],WindowsY[6],WindowsZ[6]
	
	
	
		Case tabTabber0	' user changed tabber item
			item = SelectedGadgetItem( tabTabber0 )
			Select item
				Case 0
					ShowGadget TabPanel0
					HideGadget TabPanel1
					HideGadget TabPanel2
					HideGadget TabPanel3
				Case 1
					HideGadget TabPanel0
					ShowGadget TabPanel1
					HideGadget TabPanel2
					HideGadget TabPanel3
				Case 2
					HideGadget TabPanel0
					HideGadget TabPanel1
					ShowGadget TabPanel2
					HideGadget TabPanel3
				Case 3
					HideGadget TabPanel0
					HideGadget TabPanel1
					HideGadget TabPanel2
					ShowGadget TabPanel3
					
					
					
			End Select

		Case trvTreeView0	' user selected treeview node
			Local BaseNode:TGadget = SelectedTreeViewNode(trvTreeView0)
			Local Bs:String=String(BaseNode.name)
			ChangeDir("../Meshes/")
				BaseModel = LoadAnimMesh("Baseext/"+Bs+".b3d")
				BaseModelBase = LoadMesh("Base/"+Bs+".b3d")
				EntityFX(BaseModelBase,1)
				Roofpoint = FindChild(BaseModel,"roof")
				DoorPoint = FindChild(BaseModel,"door")
				'Roof Coords
				RoofX = EntityX(Roofpoint)
				RoofY = EntityY(Roofpoint)
				RoofZ = EntityZ(Roofpoint)
				'Door Coords
				DoorX = EntityX(DoorPoint)
				DoorY = EntityY(DoorPoint)
				DoorZ = EntityZ(DoorPoint)

				For wincount = 1 To 6
					
						WinChild[wincount]= FindChild(BaseModel,"window"+wincount)
					If WinChild[wincount]Then	
						WindowsX[wincount] = EntityX(WinChild[wincount])
						WindowsY[wincount] = EntityY(WinChild[wincount])
						WindowsZ[wincount] = EntityZ(WinChild[wincount])
						FreeEntity WinChild[wincount]
					Else
						Exit
					End If	
				Next	
				
				X = 0
				Y = 0
				Z = 0
				ScaleX = 1
				ScaleY = 1
				ScaleZ = 1
     			PositionMesh BaseModel,X,Y,Z
				PositionMesh BaseModelBase ,X,Y,Z
				'ScaleEntity BaseModel,ScaleX,ScaleY,ScaleZ
				'ScaleEntity BaseModelBase,ScaleX,ScaleY,ScaleZ
				FreeEntity Basemodel
				FreeEntity Roofpoint
				FreeEntity DoorPoint 
				FreeEntity WinChild[1]
				ChangeDir("../Textures/")
				texfile$ = RequestFile("Pick Base Texture","bmp,png,jpg",False,CurrentDir()+"Base/")
				BaseTex = LoadTexture(texfile$)
				TextureLoadedMesh(BaseModelBase,Basetex)
				


		Case trvTreeView1	' user selected treeview node
			Local RoofNode:TGadget = SelectedTreeViewNode(trvTreeView1)
			Local rs:String = String(RoofNode.name)
			ChangeDir("../Meshes/")
				RoofModel = LoadMesh("roofs/"+rs+".b3d")
				'EntityParent(RoofModel,Roofpoint,True)
				EntityFX(RoofModel,1)
				X = RoofX
				Y = RoofY
				Z = RoofZ
				ScaleX = 1
				ScaleY = 1
				ScaleZ = 1
				PositionMesh RoofModel,RoofX,RoofY,RoofZ
				'ScaleEntity RoofModel,ScaleX,ScaleY,ScaleZ
				texfile$ = RequestFile("Pick Base Texture","bmp,png,jpg")
				RoofTex = LoadTexture(texfile$)
				TextureLoadedMesh(RoofModel,RoofTex)

		Case trvTreeView4

			Local DoorNode:TGadget = SelectedTreeViewNode(trvTreeView4)
			Local dn:String = String(DoorNode.name)
			ChangeDir("../Meshes/")
				DoorModel = LoadMesh("Doors/"+dn+".b3d")
				'EntityParent(DoorModel,DoorPoint,True)
				X = DoorX 
				Y = DoorY 
				Z = DoorZ
				ScaleX = 1
				ScaleY = 1
				ScaleZ = 1
				PositionEntity DoorModel,DoorX,DoorY,DoorZ
				'ScaleEntity DoorModel,ScaleX,ScaleY,ScaleZ
				texfile$ = RequestFile("Pick Base Texture","bmp,png,jpg")
				DoorTex = LoadTexture(texfile$)
				TextureLoadedMesh(DoorModel,DoorTex )

		Case exportmeshbutt
				'texfile$ = RequestFile("Export Building Mesh","b3d,3ds,x",True)
				
					For wincount = 1 To 6
						AddMesh(BaseModelBase,WindowsCnt[wincount])
						FreeEntity WindowsCnt[wincount-1] 
						FreeEntity BaseModelBase
					Next
					
				AddMesh(WindowsCnt[6],DoorModel)
				AddMesh(RoofModel,DoorModel)
				FreeEntity WindowsCnt[6]
				FreeEntity RoofModel

				'c_surfs = CountSurfaces(mesh)
				
		Case sidebutt
			windowToRotate$ = GadgetItemText(cobComboBox1,SelectedGadgetItem(cobComboBox1))
			TurnEntity WindowsCnt[windowToRotate.ToInt()],0,90,0
			
			
			
			
	End Select
	
End Function

Function enumFilesCallback(callback:Int(path:String),dir:String)
	
	Local folder=ReadDir(dir)
	Local file:String
	
	Repeat
		file=NextFile(folder)
		
		If (file) And (file <> ".") And (file <> "..")
			Local fullPath:String=RealPath(dir+"/"+file)
			
			If FileType(fullPath)=FILETYPE_DIR
				enumFilesCallback(callback,fullPath)
			Else
				callback(fullPath)
			End If
		End If
	Until (file=Null)
	
	CloseDir folder
	
	
End Function

'For each subfolder in the folder <dir> and its subdirectories,
'enumFoldersCallback calls the function <callback>, passing the folder path as an argument.

Function enumFoldersCallback(callback:Int(path:String),dir:String)
	
	Local folder=ReadDir(dir)
	Local file:String
	
	Repeat
		file=NextFile(folder)
		
		If (file) And (file <> ".") And (file <> "..")
			Local fullPath:String=RealPath(dir+"/"+file)
			
			If FileType(fullPath)=FILETYPE_DIR
				callback(fullPath)
				enumFoldersCallback(callback,fullPath)
			End If
		End If
		
	Until (file=Null)
	
	CloseDir folder
		
End Function	

'Adds the full path of all folders found inside the folder <dir> and its subdirectories to a list.
'The <list> parameter is a Linked List which can be created using the CreateList command.
'The list object must be created first, and then passed to this function
 
Function enumFolders(list:TList,dir:String)
	
	Local folder=ReadDir(dir)
	Local file:String

	Repeat
		file=NextFile(folder)
		
		If (file) And (file <> ".") And (file <> "..")
			Local fullPath:String=RealPath(dir+"/"+file)
			
			If FileType(fullPath)=FILETYPE_DIR
				
				list.addLast(fullPath)
				enumFolders(list,fullPath)
				
			End If
		End If
	Until (file=Null)
	
	CloseDir folder
	
	
End Function

'Adds the full path of all files found inside the folder <dir> and its subdirectories to a list.
'The <list> parameter is a Linked List which can be created using the CreateList command.
'The list object must be created first, and then passed to this function

Function enumFiles(list:TList,dir:String)
	
	Local folder=ReadDir(dir)
	Local file:String

	Repeat
		
		file=NextFile(folder)
	
		If (file <> ".") And (file <> "..") And (file)
			Local fullPath:String=RealPath(dir+"/"+file)
		
			If FileType(fullPath)=FILETYPE_DIR
				enumFiles(list,fullPath)
			Else
				list.AddLast(fullPath)
			End If	
		End If
		
	Until (file=Null)
	
	CloseDir folder
	
	
End Function


Function MakeSkyBox( file$ )

	m=CreateMesh()
	'front face
	b=LoadBrush( file$+"_FR.bmp",49 )
	s=CreateSurface( m )
	AddVertex s,-1,+1,-1,0,0'AddVertex s,+1,+1,-1,1,0
	AddVertex s,+1,-1,-1,1,1'AddVertex s,-1,-1,-1,0,1
	AddTriangle s,0,1,2'AddTriangle s,0,2,3
	PaintSurface s,b
	'FreeBrush b
	'Right face
	b=LoadBrush( file$+"_LF.bmp",49 )
	s=CreateSurface( m )
	AddVertex s,+1,+1,-1,0,0'AddVertex s,+1,+1,+1,1,0
	AddVertex s,+1,-1,+1,1,1'AddVertex s,+1,-1,-1,0,1
	AddTriangle s,0,1,2'AddTriangle s,0,2,3
	PaintSurface s,b
	'FreeBrush b
	'back face
	b=LoadBrush( file$+"_BK.bmp",49 )
	s=CreateSurface( m )
	AddVertex s,+1,+1,+1,0,0'AddVertex s,-1,+1,+1,1,0
	AddVertex s,-1,-1,+1,1,1'AddVertex s,+1,-1,+1,0,1
	AddTriangle s,0,1,2'AddTriangle s,0,2,3
	PaintSurface s,b
	'FreeBrush b
	'Left face
	b=LoadBrush( file$+"_RT.bmp",49 )
	s=CreateSurface( m )
	AddVertex s,-1,+1,+1,0,0'AddVertex s,-1,+1,-1,1,0
	AddVertex s,-1,-1,-1,1,1'AddVertex s,-1,-1,+1,0,1
	AddTriangle s,0,1,2'AddTriangle s,0,2,3
	PaintSurface s,b
	'FreeBrush b
	'top face
	b=LoadBrush( file$+"_UP.bmp",49 )
	s=CreateSurface( m )
	AddVertex s,-1,+1,+1,0,1'AddVertex s,+1,+1,+1,0,0
	AddVertex s,+1,+1,-1,1,0'AddVertex s,-1,+1,-1,1,1
	AddTriangle s,0,1,2'AddTriangle s,0,2,3
	PaintSurface s,b
	'FreeBrush b

	ScaleMesh m,1700,1700,1700
	FlipMesh m
	EntityFX m,1
	Return m
	
End Function

Type TMouse
	Field x:Int  = 0
	Field y:Int  = 0
	Field z:Int  = 0
	Field Button:Byte[3] 
	
	Function Init:TMouse()
		Return New TMouse
	End Function
	
	Method Update()
		Local CUR_Event:TEvent = CurrentEvent
		Select Cur_Event.id
			Case EVENT_MOUSEMOVE  
				x = Cur_Event.x
				y = Cur_Event.y   
			Case EVENT_MOUSEWHEEL
				z:+Cur_Event.data
			Case EVENT_MOUSEDOWN
				Button[cur_event.data-1] = True
			Case EVENT_MOUSEUP
				Button[cur_event.data-1] = False
		End Select
	End Method  
	
	Method getX:Int()
		Return x
	End Method  
	
	Method getY:Int()
		Return y
	End Method
	Method getZ:Int()
		Return z
	End Method   
	
	Method GetButton:Byte(_Button:Int=0)
		If _Button >= 0 And _Button <= 2 Then 
			Return Button[_Button]
		EndIf
	End Method
	
End Type

Function TextureLoadedMesh(Mesh:TMesh,Tex:TTexture,Index:Int = 0)
	For Local Surf:TSurface = EachIn Mesh.Surf_list
		BrushTexture(Surf.Brush,Tex,Index)
	Next
End Function

Function invsLoadedMesh(Mesh:TMesh)
	For Local Surf:TSurface = EachIn Mesh.Surf_list
		BrushAlpha(Surf.Brush,0)
	Next
End Function
