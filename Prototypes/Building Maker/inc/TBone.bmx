Type TBone Extends TEntity

	Field n_px#,n_py#,n_pz#,n_sx#,n_sy#,n_sz#,n_rx#,n_ry#,n_rz#,n_qw#,n_qx#,n_qy#,n_qz#
	Field no_verts
	Field verts_id[]
	Field verts_w#[]

	Field keys:TAnimationKeys
	
	Field inv_mat:TMatrix
	
	Field kx#,ky#,kz#,kqw#,kqx#,kqy#,kqz# ' used to store current keyframe in AnimateMesh, for use with transition
	
	Method CopyEntity:TBone(parent_ent:TEntity=Null)
	
		' new bone
		Local bone:TBone=New TBone
		
		' copy contents of child list before adding parent
		For Local ent:TEntity=EachIn child_list
			ent.CopyEntity(bone)
		Next
		
		' add parent, add to list
		bone.AddParent(parent_ent:TEntity)
		bone.EntityListAdd(entity_list)
		
		' update matrix
		If bone.parent<>Null
			bone.mat.Overwrite(bone.parent.mat)
		Else
			bone.mat.LoadIdentity()
		EndIf
		
		' copy entity info
		
		bone.mat.Multiply(mat)

		bone.px#=px#
		bone.py#=py#
		bone.pz#=pz#
		bone.sx#=sx#
		bone.sy#=sy#
		bone.sz#=sz#
		bone.rx#=rx#
		bone.ry#=ry#
		bone.rz#=rz#
		bone.qw#=qw#
		bone.qx#=qx#
		bone.qy#=qy#
		bone.qz#=qz#
		
		bone.name$=name$
		bone.class$=class$
		bone.order=order
		bone.hidden=0'hidden
		
		' copy bone info
		
		bone.n_px#=n_px#
		bone.n_py#=n_py#
		bone.n_pz#=n_pz#
		bone.n_sx#=n_sx#
		bone.n_sy#=n_sy#
		bone.n_sz#=n_sz#
		bone.n_rx#=n_rx#
		bone.n_ry#=n_ry#
		bone.n_rz#=n_rz#
		bone.n_qw#=n_qw#
		bone.n_qx#=n_qx#
		bone.n_qy#=n_qy#
		bone.n_qz#=n_qz#
	
		bone.no_verts=no_verts
	
		bone.verts_id=verts_id[..]
		bone.verts_w#=verts_w#[..]
		
		bone.keys=keys.Copy()
		
		bone.kx#=kx#
		bone.ky#=ky#
		bone.kz#=kz#
		bone.kqw#=kqw#
		bone.kqx#=kqx#
		bone.kqy#=kqy#
		bone.kqz#=kqz#
		
		bone.inv_mat=inv_mat.Copy()

		Return bone
	
	End Method
		
	Method FreeEntity()
	
	End Method
	
	' MiniB3D functions
	
	' Internal - not recommended for general use
		
	Method Update()
	
	End Method

End Type
