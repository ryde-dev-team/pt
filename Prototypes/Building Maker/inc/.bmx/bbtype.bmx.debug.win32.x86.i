import brl.blitz
import brl.appstub
import brl.audio
import brl.bank
import brl.bankstream
import brl.basic
import brl.bmploader
import brl.cocoamaxgui
import brl.d3d7max2d
import brl.directsoundaudio
import brl.eventqueue
import brl.fltkmaxgui
import brl.freeaudioaudio
import brl.freetypefont
import brl.gnet
import brl.jpgloader
import brl.maxgui
import brl.maxutil
import brl.oggloader
import brl.openalaudio
import brl.pngloader
import brl.retro
import brl.tgaloader
import brl.timer
import brl.wavloader
import brl.win32maxgui
import pub.dreamotion3d
import pub.freejoy
import pub.freeprocess
import pub.glew
import pub.macos
TBBType^brl.blitz.Object{
._list:brl.linkedlist.TList&
._link:brl.linkedlist.TLink&
-New%()="_bb_TBBType_New"
-Delete%()="_bb_TBBType_Delete"
-Add%(t:brl.linkedlist.TList)="_bb_TBBType_Add"
-InsertBefore%(t:TBBType)="_bb_TBBType_InsertBefore"
-InsertAfter%(t:TBBType)="_bb_TBBType_InsertAfter"
-Remove%()="_bb_TBBType_Remove"
}="bb_TBBType"
DeleteLast%(t:TBBType)="bb_DeleteLast"
DeleteFirst%(t:TBBType)="bb_DeleteFirst"
DeleteEach%(t:TBBType)="bb_DeleteEach"
ReadString$(in:brl.stream.TStream)="bb_ReadString"
HandleToObject:Object(obj:Object)="bb_HandleToObject"
HandleFromObject%(obj:Object)="bb_HandleFromObject"
