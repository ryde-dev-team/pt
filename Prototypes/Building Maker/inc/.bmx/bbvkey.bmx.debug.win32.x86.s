	format	MS COFF
	extrn	___bb_appstub_appstub
	extrn	___bb_audio_audio
	extrn	___bb_bank_bank
	extrn	___bb_bankstream_bankstream
	extrn	___bb_basic_basic
	extrn	___bb_blitz_blitz
	extrn	___bb_bmploader_bmploader
	extrn	___bb_cocoamaxgui_cocoamaxgui
	extrn	___bb_d3d7max2d_d3d7max2d
	extrn	___bb_directsoundaudio_directsoundaudio
	extrn	___bb_dreamotion3d_dreamotion3d
	extrn	___bb_eventqueue_eventqueue
	extrn	___bb_fltkmaxgui_fltkmaxgui
	extrn	___bb_freeaudioaudio_freeaudioaudio
	extrn	___bb_freejoy_freejoy
	extrn	___bb_freeprocess_freeprocess
	extrn	___bb_freetypefont_freetypefont
	extrn	___bb_glew_glew
	extrn	___bb_gnet_gnet
	extrn	___bb_jpgloader_jpgloader
	extrn	___bb_macos_macos
	extrn	___bb_maxgui_maxgui
	extrn	___bb_maxutil_maxutil
	extrn	___bb_oggloader_oggloader
	extrn	___bb_openalaudio_openalaudio
	extrn	___bb_pngloader_pngloader
	extrn	___bb_retro_retro
	extrn	___bb_tgaloader_tgaloader
	extrn	___bb_timer_timer
	extrn	___bb_wavloader_wavloader
	extrn	___bb_win32maxgui_win32maxgui
	extrn	_bbArrayFromData
	extrn	_bbEmptyArray
	extrn	_bbOnDebugEnterScope
	extrn	_bbOnDebugEnterStm
	extrn	_bbOnDebugLeaveScope
	extrn	_brl_blitz_ArrayBoundsError
	extrn	_brl_polledinput_KeyDown
	extrn	_brl_polledinput_KeyHit
	public	___bb_inc_bbvkey
	public	_bb_ChannelPitch
	public	_bb_FreeBank
	public	_bb_Locate
	public	_bb_LoopSound
	public	_bb_MouseZSpeed
	public	_bb_PlayCDTrack
	public	_bb_SoundVolume
	public	_bb_VKEY
	public	_bb_VKeyDown
	public	_bb_VKeyHit
	section	"code" code
___bb_inc_bbvkey:
	push	ebp
	mov	ebp,esp
	sub	esp,104
	push	ebx
	cmp	dword [_112],0
	je	_113
	mov	eax,0
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_113:
	mov	dword [_112],1
	mov	dword [ebp-4],0
	mov	dword [ebp-8],0
	mov	dword [ebp-12],0
	mov	dword [ebp-16],0
	mov	dword [ebp-20],0
	mov	dword [ebp-24],0
	mov	dword [ebp-28],0
	mov	dword [ebp-32],0
	mov	dword [ebp-36],0
	mov	dword [ebp-40],0
	mov	dword [ebp-44],0
	mov	dword [ebp-48],0
	mov	dword [ebp-52],0
	mov	dword [ebp-56],0
	mov	dword [ebp-60],0
	mov	dword [ebp-64],0
	mov	dword [ebp-68],0
	mov	dword [ebp-72],0
	mov	dword [ebp-76],0
	mov	dword [ebp-80],0
	mov	dword [ebp-84],0
	mov	dword [ebp-88],0
	mov	dword [ebp-92],0
	mov	dword [ebp-96],0
	mov	dword [ebp-100],0
	push	ebp
	push	_83
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	call	___bb_blitz_blitz
	call	___bb_appstub_appstub
	call	___bb_audio_audio
	call	___bb_bank_bank
	call	___bb_bankstream_bankstream
	call	___bb_basic_basic
	call	___bb_bmploader_bmploader
	call	___bb_cocoamaxgui_cocoamaxgui
	call	___bb_d3d7max2d_d3d7max2d
	call	___bb_directsoundaudio_directsoundaudio
	call	___bb_eventqueue_eventqueue
	call	___bb_fltkmaxgui_fltkmaxgui
	call	___bb_freeaudioaudio_freeaudioaudio
	call	___bb_freetypefont_freetypefont
	call	___bb_gnet_gnet
	call	___bb_jpgloader_jpgloader
	call	___bb_maxgui_maxgui
	call	___bb_maxutil_maxutil
	call	___bb_oggloader_oggloader
	call	___bb_openalaudio_openalaudio
	call	___bb_pngloader_pngloader
	call	___bb_retro_retro
	call	___bb_tgaloader_tgaloader
	call	___bb_timer_timer
	call	___bb_wavloader_wavloader
	call	___bb_win32maxgui_win32maxgui
	call	___bb_dreamotion3d_dreamotion3d
	call	___bb_freejoy_freejoy
	call	___bb_freeprocess_freeprocess
	call	___bb_glew_glew
	call	___bb_macos_macos
	push	_51
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [_81]
	and	eax,1
	cmp	eax,0
	jne	_82
	mov	eax,dword [ebp-4]
	mov	dword [_53+224],eax
	mov	eax,dword [ebp-8]
	mov	dword [_53+232],eax
	mov	eax,dword [ebp-12]
	mov	dword [_53+276],eax
	mov	eax,dword [ebp-16]
	mov	dword [_53+280],eax
	mov	eax,dword [ebp-20]
	mov	dword [_53+336],eax
	mov	eax,dword [ebp-24]
	mov	dword [_53+576],eax
	mov	eax,dword [ebp-28]
	mov	dword [_53+612],eax
	mov	eax,dword [ebp-32]
	mov	dword [_53+640],eax
	mov	eax,dword [ebp-36]
	mov	dword [_53+648],eax
	mov	eax,dword [ebp-40]
	mov	dword [_53+656],eax
	mov	eax,dword [ebp-44]
	mov	dword [_53+696],eax
	mov	eax,dword [ebp-48]
	mov	dword [_53+704],eax
	mov	eax,dword [ebp-52]
	mov	dword [_53+712],eax
	mov	eax,dword [ebp-56]
	mov	dword [_53+716],eax
	mov	eax,dword [ebp-60]
	mov	dword [_53+788],eax
	mov	eax,dword [ebp-64]
	mov	dword [_53+876],eax
	mov	eax,dword [ebp-68]
	mov	dword [_53+880],eax
	mov	eax,dword [ebp-72]
	mov	dword [_53+916],eax
	mov	eax,dword [ebp-76]
	mov	dword [_53+920],eax
	mov	eax,dword [ebp-80]
	mov	dword [_53+924],eax
	mov	eax,dword [ebp-84]
	mov	dword [_53+928],eax
	mov	eax,dword [ebp-88]
	mov	dword [_53+932],eax
	mov	eax,dword [ebp-92]
	mov	dword [_53+936],eax
	mov	eax,dword [ebp-96]
	mov	dword [_53+944],eax
	mov	eax,dword [ebp-100]
	mov	dword [_53+948],eax
	push	_53
	push	238
	push	_79
	call	_bbArrayFromData
	add	esp,12
	mov	dword [ebp-104],eax
	mov	eax,dword [ebp-104]
	inc	dword [eax+4]
	mov	eax,dword [ebp-104]
	mov	dword [_bb_VKEY],eax
	or	dword [_81],1
_82:
	mov	ebx,0
_19:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_bb_VKeyDown:
	push	ebp
	mov	ebp,esp
	sub	esp,4
	push	ebx
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	push	ebp
	push	_117
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_114
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	mov	eax,dword [_bb_VKEY]
	cmp	ebx,dword [eax+20]
	jb	_116
	call	_brl_blitz_ArrayBoundsError
_116:
	mov	eax,dword [_bb_VKEY]
	push	dword [eax+ebx*4+24]
	call	_brl_polledinput_KeyDown
	add	esp,4
	mov	ebx,eax
_22:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_bb_VKeyHit:
	push	ebp
	mov	ebp,esp
	sub	esp,4
	push	ebx
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	push	ebp
	push	_123
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_120
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	mov	eax,dword [_bb_VKEY]
	cmp	ebx,dword [eax+20]
	jb	_122
	call	_brl_blitz_ArrayBoundsError
_122:
	mov	eax,dword [_bb_VKEY]
	push	dword [eax+ebx*4+24]
	call	_brl_polledinput_KeyHit
	add	esp,4
	mov	ebx,eax
_25:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_bb_Locate:
	push	ebp
	mov	ebp,esp
	sub	esp,8
	push	ebx
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	mov	eax,dword [ebp+12]
	mov	dword [ebp-8],eax
	push	ebp
	push	_126
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_125
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,0
_29:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_bb_MouseZSpeed:
	push	ebp
	mov	ebp,esp
	push	ebx
	push	ebp
	push	_131
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_130
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,0
_31:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_bb_FreeBank:
	push	ebp
	mov	ebp,esp
	sub	esp,4
	push	ebx
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	push	ebp
	push	_134
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_133
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,0
_34:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_bb_LoopSound:
	push	ebp
	mov	ebp,esp
	sub	esp,4
	push	ebx
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	push	ebp
	push	_138
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_137
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,0
_37:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_bb_ChannelPitch:
	push	ebp
	mov	ebp,esp
	sub	esp,8
	push	ebx
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	mov	eax,dword [ebp+12]
	mov	dword [ebp-8],eax
	push	ebp
	push	_142
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_141
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,0
_41:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_bb_PlayCDTrack:
	push	ebp
	mov	ebp,esp
	sub	esp,8
	push	ebx
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	mov	eax,dword [ebp+12]
	mov	dword [ebp-8],eax
	push	ebp
	push	_147
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_146
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,0
_45:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_bb_SoundVolume:
	push	ebp
	mov	ebp,esp
	sub	esp,8
	push	ebx
	mov	eax,dword [ebp+8]
	mov	dword [ebp-8],eax
	fld	dword [ebp+12]
	fstp	dword [ebp-4]
	push	ebp
	push	_152
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_151
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,0
_49:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
	section	"data" data writeable align 8
	align	4
_112:
	dd	0
_84:
	db	"bbvkey",0
_85:
	db	"KEY_ALT",0
_79:
	db	"i",0
_86:
	db	"KEY_CAPSLOCK",0
_87:
	db	"KEY_NUMLOCK",0
_88:
	db	"KEY_SCROLL",0
_89:
	db	"KEY_NUMSLASH",0
_90:
	db	"KEY_MEDIA_PREV_TRACK",0
_91:
	db	"KEY_MEDIA_NEXT_TRACK",0
_92:
	db	"KEY_VOLUME_MUTE",0
_93:
	db	"KEY_MEDIA_PLAY_PAUSE",0
_94:
	db	"KEY_MEDIA_STOP",0
_95:
	db	"KEY_VOLUME_DOWN",0
_96:
	db	"KEY_VOLUME_UP",0
_97:
	db	"KEY_BROWSER_HOME",0
_98:
	db	"KEY_DECIMAL",0
_99:
	db	"KEY_PAUSE",0
_100:
	db	"KEY_LWIN",0
_101:
	db	"KEY_RWIN",0
_102:
	db	"KEY_BROWSER_SEARCH",0
_103:
	db	"KEY_BROWSER_FAVORITES",0
_104:
	db	"KEY_BROWSER_REFRESH",0
_105:
	db	"KEY_BROWSER_STOP",0
_106:
	db	"KEY_BROWSER_FORWARD",0
_107:
	db	"KEY_BROWSER_BACK",0
_108:
	db	"KEY_LAUNCH_MAIL",0
_109:
	db	"KEY_LAUNCH_MEDIA_SELECT",0
_110:
	db	"VKEY",0
_111:
	db	"[]i",0
	align	4
_bb_VKEY:
	dd	_bbEmptyArray
	align	4
_83:
	dd	1
	dd	_84
	dd	2
	dd	_85
	dd	_79
	dd	-4
	dd	2
	dd	_86
	dd	_79
	dd	-8
	dd	2
	dd	_87
	dd	_79
	dd	-12
	dd	2
	dd	_88
	dd	_79
	dd	-16
	dd	2
	dd	_89
	dd	_79
	dd	-20
	dd	2
	dd	_90
	dd	_79
	dd	-24
	dd	2
	dd	_91
	dd	_79
	dd	-28
	dd	2
	dd	_92
	dd	_79
	dd	-32
	dd	2
	dd	_93
	dd	_79
	dd	-36
	dd	2
	dd	_94
	dd	_79
	dd	-40
	dd	2
	dd	_95
	dd	_79
	dd	-44
	dd	2
	dd	_96
	dd	_79
	dd	-48
	dd	2
	dd	_97
	dd	_79
	dd	-52
	dd	2
	dd	_98
	dd	_79
	dd	-56
	dd	2
	dd	_99
	dd	_79
	dd	-60
	dd	2
	dd	_100
	dd	_79
	dd	-64
	dd	2
	dd	_101
	dd	_79
	dd	-68
	dd	2
	dd	_102
	dd	_79
	dd	-72
	dd	2
	dd	_103
	dd	_79
	dd	-76
	dd	2
	dd	_104
	dd	_79
	dd	-80
	dd	2
	dd	_105
	dd	_79
	dd	-84
	dd	2
	dd	_106
	dd	_79
	dd	-88
	dd	2
	dd	_107
	dd	_79
	dd	-92
	dd	2
	dd	_108
	dd	_79
	dd	-96
	dd	2
	dd	_109
	dd	_79
	dd	-100
	dd	4
	dd	_110
	dd	_111
	dd	_bb_VKEY
	dd	0
_52:
	db	"C:/Documents and Settings/ckob/Desktop/coding/BuildingMaker/inc/bbvkey.bmx",0
	align	4
_51:
	dd	_52
	dd	3
	dd	1
	align	4
_81:
	dd	0
	align	4
_53:
	dd	0
	dd	27
	dd	49
	dd	50
	dd	51
	dd	52
	dd	53
	dd	54
	dd	55
	dd	56
	dd	57
	dd	48
	dd	189
	dd	187
	dd	8
	dd	9
	dd	81
	dd	87
	dd	69
	dd	82
	dd	84
	dd	89
	dd	85
	dd	73
	dd	79
	dd	80
	dd	219
	dd	221
	dd	13
	dd	162
	dd	65
	dd	83
	dd	68
	dd	70
	dd	71
	dd	72
	dd	74
	dd	75
	dd	76
	dd	186
	dd	222
	dd	192
	dd	160
	dd	226
	dd	90
	dd	88
	dd	67
	dd	86
	dd	66
	dd	78
	dd	77
	dd	188
	dd	190
	dd	191
	dd	161
	dd	106
	dd	0
	dd	32
	dd	0
	dd	112
	dd	113
	dd	114
	dd	115
	dd	116
	dd	117
	dd	118
	dd	119
	dd	120
	dd	121
	dd	0
	dd	0
	dd	103
	dd	104
	dd	105
	dd	109
	dd	100
	dd	101
	dd	102
	dd	107
	dd	97
	dd	98
	dd	99
	dd	96
	dd	110
	dd	0
	dd	122
	dd	123
	dd	0
	dd	0
	dd	0
	dd	0
	dd	0
	dd	0
	dd	0
	dd	0
	dd	0
	dd	0
	dd	0
	dd	0
	dd	0
	dd	0
	dd	0
	dd	0
	dd	0
	dd	0
	dd	0
	dd	0
	dd	0
	dd	0
	dd	0
	dd	0
	dd	0
	dd	0
	dd	0
	dd	0
	dd	0
	dd	0
	dd	0
	dd	0
	dd	0
	dd	0
	dd	0
	dd	0
	dd	0
	dd	0
	dd	0
	dd	0
	dd	0
	dd	0
	dd	0
	dd	0
	dd	0
	dd	0
	dd	0
	dd	0
	dd	0
	dd	0
	dd	0
	dd	0
	dd	0
	dd	0
	dd	187
	dd	0
	dd	0
	dd	0
	dd	0
	dd	0
	dd	0
	dd	0
	dd	0
	dd	0
	dd	0
	dd	0
	dd	0
	dd	0
	dd	0
	dd	13
	dd	163
	dd	0
	dd	0
	dd	0
	dd	0
	dd	0
	dd	0
	dd	0
	dd	0
	dd	0
	dd	0
	dd	0
	dd	0
	dd	0
	dd	0
	dd	0
	dd	0
	dd	0
	dd	0
	dd	0
	dd	0
	dd	0
	dd	0
	dd	0
	dd	111
	dd	0
	dd	44
	dd	0
	dd	0
	dd	0
	dd	0
	dd	0
	dd	0
	dd	0
	dd	0
	dd	0
	dd	0
	dd	0
	dd	0
	dd	0
	dd	0
	dd	0
	dd	36
	dd	38
	dd	33
	dd	0
	dd	37
	dd	0
	dd	39
	dd	0
	dd	35
	dd	40
	dd	34
	dd	45
	dd	46
	dd	0
	dd	0
	dd	0
	dd	0
	dd	0
	dd	0
	dd	0
	dd	0
	dd	0
	dd	0
	dd	0
	dd	0
	dd	0
	dd	0
	dd	0
	dd	0
	dd	0
	dd	0
	dd	0
	dd	0
	dd	0
	dd	0
	dd	0
	dd	0
	dd	0
	dd	0
_118:
	db	"VKeyDown",0
_119:
	db	"key",0
	align	4
_117:
	dd	1
	dd	_118
	dd	2
	dd	_119
	dd	_79
	dd	-4
	dd	0
	align	4
_114:
	dd	_52
	dd	26
	dd	24
_124:
	db	"VKeyHit",0
	align	4
_123:
	dd	1
	dd	_124
	dd	2
	dd	_119
	dd	_79
	dd	-4
	dd	0
	align	4
_120:
	dd	_52
	dd	27
	dd	23
_127:
	db	"Locate",0
_128:
	db	"x",0
_129:
	db	"y",0
	align	4
_126:
	dd	1
	dd	_127
	dd	2
	dd	_128
	dd	_79
	dd	-4
	dd	2
	dd	_129
	dd	_79
	dd	-8
	dd	0
	align	4
_125:
	dd	_52
	dd	31
	dd	24
_132:
	db	"MouseZSpeed",0
	align	4
_131:
	dd	1
	dd	_132
	dd	0
	align	4
_130:
	dd	_52
	dd	32
	dd	24
_135:
	db	"FreeBank",0
_136:
	db	"bank",0
	align	4
_134:
	dd	1
	dd	_135
	dd	2
	dd	_136
	dd	_79
	dd	-4
	dd	0
	align	4
_133:
	dd	_52
	dd	33
	dd	25
_139:
	db	"LoopSound",0
_140:
	db	"sound",0
	align	4
_138:
	dd	1
	dd	_139
	dd	2
	dd	_140
	dd	_79
	dd	-4
	dd	0
	align	4
_137:
	dd	_52
	dd	34
	dd	27
_143:
	db	"ChannelPitch",0
_144:
	db	"channel",0
_145:
	db	"hz",0
	align	4
_142:
	dd	1
	dd	_143
	dd	2
	dd	_144
	dd	_79
	dd	-4
	dd	2
	dd	_145
	dd	_79
	dd	-8
	dd	0
	align	4
_141:
	dd	_52
	dd	35
	dd	35
_148:
	db	"PlayCDTrack",0
_149:
	db	"track",0
_150:
	db	"mode",0
	align	4
_147:
	dd	1
	dd	_148
	dd	2
	dd	_149
	dd	_79
	dd	-4
	dd	2
	dd	_150
	dd	_79
	dd	-8
	dd	0
	align	4
_146:
	dd	_52
	dd	36
	dd	38
_153:
	db	"SoundVolume",0
_154:
	db	"volume",0
_155:
	db	"f",0
	align	4
_152:
	dd	1
	dd	_153
	dd	2
	dd	_140
	dd	_79
	dd	-8
	dd	2
	dd	_154
	dd	_155
	dd	-4
	dd	0
	align	4
_151:
	dd	_52
	dd	37
	dd	39
