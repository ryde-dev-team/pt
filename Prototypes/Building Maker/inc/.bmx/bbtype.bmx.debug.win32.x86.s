	format	MS COFF
	extrn	___bb_appstub_appstub
	extrn	___bb_audio_audio
	extrn	___bb_bank_bank
	extrn	___bb_bankstream_bankstream
	extrn	___bb_basic_basic
	extrn	___bb_blitz_blitz
	extrn	___bb_bmploader_bmploader
	extrn	___bb_cocoamaxgui_cocoamaxgui
	extrn	___bb_d3d7max2d_d3d7max2d
	extrn	___bb_directsoundaudio_directsoundaudio
	extrn	___bb_dreamotion3d_dreamotion3d
	extrn	___bb_eventqueue_eventqueue
	extrn	___bb_fltkmaxgui_fltkmaxgui
	extrn	___bb_freeaudioaudio_freeaudioaudio
	extrn	___bb_freejoy_freejoy
	extrn	___bb_freeprocess_freeprocess
	extrn	___bb_freetypefont_freetypefont
	extrn	___bb_glew_glew
	extrn	___bb_gnet_gnet
	extrn	___bb_jpgloader_jpgloader
	extrn	___bb_macos_macos
	extrn	___bb_maxgui_maxgui
	extrn	___bb_maxutil_maxutil
	extrn	___bb_oggloader_oggloader
	extrn	___bb_openalaudio_openalaudio
	extrn	___bb_pngloader_pngloader
	extrn	___bb_retro_retro
	extrn	___bb_tgaloader_tgaloader
	extrn	___bb_timer_timer
	extrn	___bb_wavloader_wavloader
	extrn	___bb_win32maxgui_win32maxgui
	extrn	_bbEmptyString
	extrn	_bbGCFree
	extrn	_bbHandleFromObject
	extrn	_bbNullObject
	extrn	_bbObjectClass
	extrn	_bbObjectCompare
	extrn	_bbObjectCtor
	extrn	_bbObjectDowncast
	extrn	_bbObjectDtor
	extrn	_bbObjectFree
	extrn	_bbObjectReserved
	extrn	_bbObjectSendMessage
	extrn	_bbObjectToString
	extrn	_bbOnDebugEnterScope
	extrn	_bbOnDebugEnterStm
	extrn	_bbOnDebugLeaveScope
	extrn	_brl_blitz_NullObjectError
	extrn	_brl_stream_ReadInt
	extrn	_brl_stream_ReadString
	public	___bb_inc_bbtype
	public	__bb_TBBType_Add
	public	__bb_TBBType_Delete
	public	__bb_TBBType_InsertAfter
	public	__bb_TBBType_InsertBefore
	public	__bb_TBBType_New
	public	__bb_TBBType_Remove
	public	_bb_DeleteEach
	public	_bb_DeleteFirst
	public	_bb_DeleteLast
	public	_bb_HandleFromObject
	public	_bb_HandleToObject
	public	_bb_ReadString
	public	_bb_TBBType
	section	"code" code
___bb_inc_bbtype:
	push	ebp
	mov	ebp,esp
	push	ebx
	cmp	dword [_68],0
	je	_69
	mov	eax,0
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_69:
	mov	dword [_68],1
	push	ebp
	push	_66
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	call	___bb_blitz_blitz
	call	___bb_appstub_appstub
	call	___bb_audio_audio
	call	___bb_bank_bank
	call	___bb_bankstream_bankstream
	call	___bb_basic_basic
	call	___bb_bmploader_bmploader
	call	___bb_cocoamaxgui_cocoamaxgui
	call	___bb_d3d7max2d_d3d7max2d
	call	___bb_directsoundaudio_directsoundaudio
	call	___bb_eventqueue_eventqueue
	call	___bb_fltkmaxgui_fltkmaxgui
	call	___bb_freeaudioaudio_freeaudioaudio
	call	___bb_freetypefont_freetypefont
	call	___bb_gnet_gnet
	call	___bb_jpgloader_jpgloader
	call	___bb_maxgui_maxgui
	call	___bb_maxutil_maxutil
	call	___bb_oggloader_oggloader
	call	___bb_openalaudio_openalaudio
	call	___bb_pngloader_pngloader
	call	___bb_retro_retro
	call	___bb_tgaloader_tgaloader
	call	___bb_timer_timer
	call	___bb_wavloader_wavloader
	call	___bb_win32maxgui_win32maxgui
	call	___bb_dreamotion3d_dreamotion3d
	call	___bb_freejoy_freejoy
	call	___bb_freeprocess_freeprocess
	call	___bb_glew_glew
	call	___bb_macos_macos
	mov	ebx,0
_25:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_TBBType_New:
	push	ebp
	mov	ebp,esp
	sub	esp,12
	push	ebx
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	push	ebp
	push	_72
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	dword [ebp-4]
	call	_bbObjectCtor
	add	esp,4
	mov	eax,dword [ebp-4]
	mov	dword [eax],_bb_TBBType
	mov	dword [ebp-8],_bbNullObject
	mov	eax,dword [ebp-8]
	inc	dword [eax+4]
	mov	edx,dword [ebp-4]
	mov	eax,dword [ebp-8]
	mov	dword [edx+8],eax
	mov	dword [ebp-12],_bbNullObject
	mov	eax,dword [ebp-12]
	inc	dword [eax+4]
	mov	edx,dword [ebp-4]
	mov	eax,dword [ebp-12]
	mov	dword [edx+12],eax
	mov	ebx,0
_28:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_TBBType_Delete:
	push	ebp
	mov	ebp,esp
	sub	esp,12
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
_31:
	mov	eax,dword [ebp-4]
	mov	eax,dword [eax+12]
	mov	dword [ebp-8],eax
	mov	eax,dword [ebp-8]
	dec	dword [eax+4]
	jnz	_78
	push	dword [ebp-8]
	call	_bbGCFree
	add	esp,4
_78:
	mov	eax,dword [ebp-4]
	mov	eax,dword [eax+8]
	mov	dword [ebp-12],eax
	mov	eax,dword [ebp-12]
	dec	dword [eax+4]
	jnz	_80
	push	dword [ebp-12]
	call	_bbGCFree
	add	esp,4
_80:
	mov	eax,dword [ebp-4]
	mov	dword [eax],_bbObjectClass
	push	dword [ebp-4]
	call	_bbObjectDtor
	add	esp,4
	mov	eax,0
_76:
	mov	esp,ebp
	pop	ebp
	ret
__bb_TBBType_Add:
	push	ebp
	mov	ebp,esp
	sub	esp,56
	push	ebx
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	mov	eax,dword [ebp+12]
	mov	dword [ebp-8],eax
	push	ebp
	push	_102
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_81
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [ebp-4]
	mov	dword [ebp-12],eax
	cmp	dword [ebp-12],_bbNullObject
	jne	_84
	call	_brl_blitz_NullObjectError
_84:
	mov	eax,dword [ebp-12]
	mov	dword [ebp-16],eax
	mov	eax,dword [ebp-8]
	mov	dword [ebp-20],eax
	mov	eax,dword [ebp-20]
	inc	dword [eax+4]
	mov	eax,dword [ebp-20]
	mov	dword [ebp-24],eax
	mov	eax,dword [ebp-16]
	mov	eax,dword [eax+8]
	mov	dword [ebp-28],eax
	mov	eax,dword [ebp-28]
	dec	dword [eax+4]
	jnz	_89
	push	dword [ebp-28]
	call	_bbGCFree
	add	esp,4
_89:
	mov	edx,dword [ebp-16]
	mov	eax,dword [ebp-24]
	mov	dword [edx+8],eax
	push	_90
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [ebp-4]
	mov	dword [ebp-32],eax
	cmp	dword [ebp-32],_bbNullObject
	jne	_92
	call	_brl_blitz_NullObjectError
_92:
	mov	eax,dword [ebp-32]
	mov	dword [ebp-36],eax
	mov	eax,dword [ebp-4]
	mov	dword [ebp-40],eax
	cmp	dword [ebp-40],_bbNullObject
	jne	_95
	call	_brl_blitz_NullObjectError
_95:
	mov	eax,dword [ebp-40]
	mov	eax,dword [eax+8]
	mov	dword [ebp-44],eax
	cmp	dword [ebp-44],_bbNullObject
	jne	_97
	call	_brl_blitz_NullObjectError
_97:
	push	dword [ebp-4]
	push	dword [ebp-44]
	mov	eax,dword [ebp-44]
	mov	eax,dword [eax]
	call	dword [eax+68]
	add	esp,8
	mov	dword [ebp-48],eax
	mov	eax,dword [ebp-48]
	inc	dword [eax+4]
	mov	eax,dword [ebp-48]
	mov	dword [ebp-52],eax
	mov	eax,dword [ebp-36]
	mov	eax,dword [eax+12]
	mov	dword [ebp-56],eax
	mov	eax,dword [ebp-56]
	dec	dword [eax+4]
	jnz	_101
	push	dword [ebp-56]
	call	_bbGCFree
	add	esp,4
_101:
	mov	edx,dword [ebp-36]
	mov	eax,dword [ebp-52]
	mov	dword [edx+12],eax
	mov	ebx,0
_35:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_TBBType_InsertBefore:
	push	ebp
	mov	ebp,esp
	sub	esp,48
	push	ebx
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	mov	eax,dword [ebp+12]
	mov	dword [ebp-8],eax
	push	ebp
	push	_124
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_105
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [ebp-4]
	mov	dword [ebp-12],eax
	cmp	dword [ebp-12],_bbNullObject
	jne	_107
	call	_brl_blitz_NullObjectError
_107:
	mov	eax,dword [ebp-12]
	mov	eax,dword [eax+12]
	mov	dword [ebp-16],eax
	cmp	dword [ebp-16],_bbNullObject
	jne	_109
	call	_brl_blitz_NullObjectError
_109:
	push	dword [ebp-16]
	mov	eax,dword [ebp-16]
	mov	eax,dword [eax]
	call	dword [eax+60]
	add	esp,4
	push	_110
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [ebp-4]
	mov	dword [ebp-20],eax
	cmp	dword [ebp-20],_bbNullObject
	jne	_112
	call	_brl_blitz_NullObjectError
_112:
	mov	eax,dword [ebp-20]
	mov	dword [ebp-24],eax
	mov	eax,dword [ebp-4]
	mov	dword [ebp-28],eax
	cmp	dword [ebp-28],_bbNullObject
	jne	_115
	call	_brl_blitz_NullObjectError
_115:
	mov	eax,dword [ebp-28]
	mov	eax,dword [eax+8]
	mov	dword [ebp-32],eax
	cmp	dword [ebp-32],_bbNullObject
	jne	_117
	call	_brl_blitz_NullObjectError
_117:
	mov	eax,dword [ebp-8]
	mov	dword [ebp-36],eax
	cmp	dword [ebp-36],_bbNullObject
	jne	_119
	call	_brl_blitz_NullObjectError
_119:
	mov	eax,dword [ebp-36]
	push	dword [eax+12]
	push	dword [ebp-4]
	push	dword [ebp-32]
	mov	eax,dword [ebp-32]
	mov	eax,dword [eax]
	call	dword [eax+96]
	add	esp,12
	mov	dword [ebp-40],eax
	mov	eax,dword [ebp-40]
	inc	dword [eax+4]
	mov	eax,dword [ebp-40]
	mov	dword [ebp-44],eax
	mov	eax,dword [ebp-24]
	mov	eax,dword [eax+12]
	mov	dword [ebp-48],eax
	mov	eax,dword [ebp-48]
	dec	dword [eax+4]
	jnz	_123
	push	dword [ebp-48]
	call	_bbGCFree
	add	esp,4
_123:
	mov	edx,dword [ebp-24]
	mov	eax,dword [ebp-44]
	mov	dword [edx+12],eax
	mov	ebx,0
_39:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_TBBType_InsertAfter:
	push	ebp
	mov	ebp,esp
	sub	esp,48
	push	ebx
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	mov	eax,dword [ebp+12]
	mov	dword [ebp-8],eax
	push	ebp
	push	_145
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_126
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [ebp-4]
	mov	dword [ebp-12],eax
	cmp	dword [ebp-12],_bbNullObject
	jne	_128
	call	_brl_blitz_NullObjectError
_128:
	mov	eax,dword [ebp-12]
	mov	eax,dword [eax+12]
	mov	dword [ebp-16],eax
	cmp	dword [ebp-16],_bbNullObject
	jne	_130
	call	_brl_blitz_NullObjectError
_130:
	push	dword [ebp-16]
	mov	eax,dword [ebp-16]
	mov	eax,dword [eax]
	call	dword [eax+60]
	add	esp,4
	push	_131
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [ebp-4]
	mov	dword [ebp-20],eax
	cmp	dword [ebp-20],_bbNullObject
	jne	_133
	call	_brl_blitz_NullObjectError
_133:
	mov	eax,dword [ebp-20]
	mov	dword [ebp-24],eax
	mov	eax,dword [ebp-4]
	mov	dword [ebp-28],eax
	cmp	dword [ebp-28],_bbNullObject
	jne	_136
	call	_brl_blitz_NullObjectError
_136:
	mov	eax,dword [ebp-28]
	mov	eax,dword [eax+8]
	mov	dword [ebp-32],eax
	cmp	dword [ebp-32],_bbNullObject
	jne	_138
	call	_brl_blitz_NullObjectError
_138:
	mov	eax,dword [ebp-8]
	mov	dword [ebp-36],eax
	cmp	dword [ebp-36],_bbNullObject
	jne	_140
	call	_brl_blitz_NullObjectError
_140:
	mov	eax,dword [ebp-36]
	push	dword [eax+12]
	push	dword [ebp-4]
	push	dword [ebp-32]
	mov	eax,dword [ebp-32]
	mov	eax,dword [eax]
	call	dword [eax+100]
	add	esp,12
	mov	dword [ebp-40],eax
	mov	eax,dword [ebp-40]
	inc	dword [eax+4]
	mov	eax,dword [ebp-40]
	mov	dword [ebp-44],eax
	mov	eax,dword [ebp-24]
	mov	eax,dword [eax+12]
	mov	dword [ebp-48],eax
	mov	eax,dword [ebp-48]
	dec	dword [eax+4]
	jnz	_144
	push	dword [ebp-48]
	call	_bbGCFree
	add	esp,4
_144:
	mov	edx,dword [ebp-24]
	mov	eax,dword [ebp-44]
	mov	dword [edx+12],eax
	mov	ebx,0
_43:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_TBBType_Remove:
	push	ebp
	mov	ebp,esp
	sub	esp,12
	push	ebx
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	push	ebp
	push	_152
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_147
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [ebp-4]
	mov	dword [ebp-8],eax
	cmp	dword [ebp-8],_bbNullObject
	jne	_149
	call	_brl_blitz_NullObjectError
_149:
	mov	eax,dword [ebp-8]
	mov	eax,dword [eax+8]
	mov	dword [ebp-12],eax
	cmp	dword [ebp-12],_bbNullObject
	jne	_151
	call	_brl_blitz_NullObjectError
_151:
	push	dword [ebp-4]
	push	dword [ebp-12]
	mov	eax,dword [ebp-12]
	mov	eax,dword [eax]
	call	dword [eax+116]
	add	esp,8
	mov	ebx,0
_46:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_bb_DeleteLast:
	push	ebp
	mov	ebp,esp
	sub	esp,16
	push	ebx
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	push	ebp
	push	_163
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_154
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	cmp	dword [ebp-4],_bbNullObject
	je	_155
	push	_156
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [ebp-4]
	mov	dword [ebp-8],eax
	cmp	dword [ebp-8],_bbNullObject
	jne	_158
	call	_brl_blitz_NullObjectError
_158:
	mov	eax,dword [ebp-8]
	mov	eax,dword [eax+8]
	mov	dword [ebp-12],eax
	cmp	dword [ebp-12],_bbNullObject
	jne	_160
	call	_brl_blitz_NullObjectError
_160:
	push	_bb_TBBType
	push	dword [ebp-12]
	mov	eax,dword [ebp-12]
	mov	eax,dword [eax]
	call	dword [eax+76]
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	mov	dword [ebp-16],eax
	cmp	dword [ebp-16],_bbNullObject
	jne	_162
	call	_brl_blitz_NullObjectError
_162:
	push	dword [ebp-16]
	mov	eax,dword [ebp-16]
	mov	eax,dword [eax]
	call	dword [eax+60]
	add	esp,4
_155:
	mov	ebx,0
_49:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_bb_DeleteFirst:
	push	ebp
	mov	ebp,esp
	sub	esp,16
	push	ebx
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	push	ebp
	push	_174
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_165
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	cmp	dword [ebp-4],_bbNullObject
	je	_166
	push	_167
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [ebp-4]
	mov	dword [ebp-8],eax
	cmp	dword [ebp-8],_bbNullObject
	jne	_169
	call	_brl_blitz_NullObjectError
_169:
	mov	eax,dword [ebp-8]
	mov	eax,dword [eax+8]
	mov	dword [ebp-12],eax
	cmp	dword [ebp-12],_bbNullObject
	jne	_171
	call	_brl_blitz_NullObjectError
_171:
	push	_bb_TBBType
	push	dword [ebp-12]
	mov	eax,dword [ebp-12]
	mov	eax,dword [eax]
	call	dword [eax+72]
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	mov	dword [ebp-16],eax
	cmp	dword [ebp-16],_bbNullObject
	jne	_173
	call	_brl_blitz_NullObjectError
_173:
	push	dword [ebp-16]
	mov	eax,dword [ebp-16]
	mov	eax,dword [eax]
	call	dword [eax+60]
	add	esp,4
_166:
	mov	ebx,0
_52:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_bb_DeleteEach:
	push	ebp
	mov	ebp,esp
	sub	esp,12
	push	ebx
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	push	ebp
	push	_183
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_176
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	cmp	dword [ebp-4],_bbNullObject
	je	_177
	push	_178
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [ebp-4]
	mov	dword [ebp-8],eax
	cmp	dword [ebp-8],_bbNullObject
	jne	_180
	call	_brl_blitz_NullObjectError
_180:
	mov	eax,dword [ebp-8]
	mov	eax,dword [eax+8]
	mov	dword [ebp-12],eax
	cmp	dword [ebp-12],_bbNullObject
	jne	_182
	call	_brl_blitz_NullObjectError
_182:
	push	dword [ebp-12]
	mov	eax,dword [ebp-12]
	mov	eax,dword [eax]
	call	dword [eax+52]
	add	esp,4
_177:
	mov	ebx,0
_55:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_bb_ReadString:
	push	ebp
	mov	ebp,esp
	sub	esp,12
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	mov	dword [ebp-8],0
	push	ebp
	push	_193
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_185
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-8],0
	push	_187
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-4]
	call	_brl_stream_ReadInt
	add	esp,4
	mov	dword [ebp-8],eax
	push	_188
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [ebp-8]
	cmp	eax,0
	setg	al
	movzx	eax,al
	cmp	eax,0
	je	_189
	mov	eax,dword [ebp-8]
	cmp	eax,1048576
	setl	al
	movzx	eax,al
_189:
	cmp	eax,0
	je	_191
	push	_192
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-8]
	push	dword [ebp-4]
	call	_brl_stream_ReadString
	add	esp,8
	mov	dword [ebp-12],eax
	jmp	_58
_191:
	mov	dword [ebp-12],_bbEmptyString
_58:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,dword [ebp-12]
	mov	esp,ebp
	pop	ebp
	ret
_bb_HandleToObject:
	push	ebp
	mov	ebp,esp
	sub	esp,8
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	push	ebp
	push	_200
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_199
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [ebp-4]
	mov	dword [ebp-8],eax
_61:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,dword [ebp-8]
	mov	esp,ebp
	pop	ebp
	ret
_bb_HandleFromObject:
	push	ebp
	mov	ebp,esp
	sub	esp,8
	push	ebx
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	mov	dword [ebp-8],0
	push	ebp
	push	_207
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_204
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-4]
	call	_bb_HandleToObject
	add	esp,4
	push	eax
	call	_bbHandleFromObject
	add	esp,4
	mov	dword [ebp-8],eax
	push	_206
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-8]
_64:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
	section	"data" data writeable align 8
	align	4
_68:
	dd	0
_67:
	db	"bbtype",0
	align	4
_66:
	dd	1
	dd	_67
	dd	0
_73:
	db	"New",0
_74:
	db	"Self",0
_75:
	db	":TBBType",0
	align	4
_72:
	dd	1
	dd	_73
	dd	2
	dd	_74
	dd	_75
	dd	-4
	dd	0
_20:
	db	"TBBType",0
_21:
	db	"_list",0
_22:
	db	":brl.linkedlist.TList",0
_23:
	db	"_link",0
_24:
	db	":brl.linkedlist.TLink",0
	align	4
_19:
	dd	2
	dd	_20
	dd	3
	dd	_21
	dd	_22
	dd	8
	dd	3
	dd	_23
	dd	_24
	dd	12
	dd	0
	align	4
_bb_TBBType:
	dd	_bbObjectClass
	dd	_bbObjectFree
	dd	_19
	dd	16
	dd	__bb_TBBType_New
	dd	__bb_TBBType_Delete
	dd	_bbObjectToString
	dd	_bbObjectCompare
	dd	_bbObjectSendMessage
	dd	_bbObjectReserved
	dd	_bbObjectReserved
	dd	_bbObjectReserved
	dd	__bb_TBBType_Add
	dd	__bb_TBBType_InsertBefore
	dd	__bb_TBBType_InsertAfter
	dd	__bb_TBBType_Remove
_103:
	db	"Add",0
_104:
	db	"t",0
	align	4
_102:
	dd	1
	dd	_103
	dd	2
	dd	_74
	dd	_75
	dd	-4
	dd	2
	dd	_104
	dd	_22
	dd	-8
	dd	0
_82:
	db	"C:/Documents and Settings/ckob/Desktop/coding/BuildingMaker/inc/bbtype.bmx",0
	align	4
_81:
	dd	_82
	dd	9
	dd	3
	align	4
_90:
	dd	_82
	dd	10
	dd	3
_125:
	db	"InsertBefore",0
	align	4
_124:
	dd	1
	dd	_125
	dd	2
	dd	_74
	dd	_75
	dd	-4
	dd	2
	dd	_104
	dd	_75
	dd	-8
	dd	0
	align	4
_105:
	dd	_82
	dd	14
	dd	3
	align	4
_110:
	dd	_82
	dd	15
	dd	3
_146:
	db	"InsertAfter",0
	align	4
_145:
	dd	1
	dd	_146
	dd	2
	dd	_74
	dd	_75
	dd	-4
	dd	2
	dd	_104
	dd	_75
	dd	-8
	dd	0
	align	4
_126:
	dd	_82
	dd	19
	dd	3
	align	4
_131:
	dd	_82
	dd	20
	dd	3
_153:
	db	"Remove",0
	align	4
_152:
	dd	1
	dd	_153
	dd	2
	dd	_74
	dd	_75
	dd	-4
	dd	0
	align	4
_147:
	dd	_82
	dd	24
	dd	3
_164:
	db	"DeleteLast",0
	align	4
_163:
	dd	1
	dd	_164
	dd	2
	dd	_104
	dd	_75
	dd	-4
	dd	0
	align	4
_154:
	dd	_82
	dd	30
	dd	2
	align	4
_156:
	dd	_82
	dd	30
	dd	7
_175:
	db	"DeleteFirst",0
	align	4
_174:
	dd	1
	dd	_175
	dd	2
	dd	_104
	dd	_75
	dd	-4
	dd	0
	align	4
_165:
	dd	_82
	dd	34
	dd	2
	align	4
_167:
	dd	_82
	dd	34
	dd	7
_184:
	db	"DeleteEach",0
	align	4
_183:
	dd	1
	dd	_184
	dd	2
	dd	_104
	dd	_75
	dd	-4
	dd	0
	align	4
_176:
	dd	_82
	dd	38
	dd	2
	align	4
_178:
	dd	_82
	dd	38
	dd	7
_194:
	db	"ReadString",0
_195:
	db	"in",0
_196:
	db	":brl.stream.TStream",0
_197:
	db	"length",0
_198:
	db	"i",0
	align	4
_193:
	dd	1
	dd	_194
	dd	2
	dd	_195
	dd	_196
	dd	-4
	dd	2
	dd	_197
	dd	_198
	dd	-8
	dd	0
	align	4
_185:
	dd	_82
	dd	42
	dd	2
	align	4
_187:
	dd	_82
	dd	43
	dd	2
	align	4
_188:
	dd	_82
	dd	44
	dd	2
	align	4
_192:
	dd	_82
	dd	44
	dd	35
_201:
	db	"HandleToObject",0
_202:
	db	"obj",0
_203:
	db	":Object",0
	align	4
_200:
	dd	1
	dd	_201
	dd	2
	dd	_202
	dd	_203
	dd	-4
	dd	0
	align	4
_199:
	dd	_82
	dd	48
	dd	2
_208:
	db	"HandleFromObject",0
_209:
	db	"h",0
	align	4
_207:
	dd	1
	dd	_208
	dd	2
	dd	_202
	dd	_203
	dd	-4
	dd	2
	dd	_209
	dd	_198
	dd	-8
	dd	0
	align	4
_204:
	dd	_82
	dd	52
	dd	2
	align	4
_206:
	dd	_82
	dd	53
	dd	2
