	format	MS COFF
	extrn	___bb_appstub_appstub
	extrn	___bb_audio_audio
	extrn	___bb_bank_bank
	extrn	___bb_bankstream_bankstream
	extrn	___bb_basic_basic
	extrn	___bb_blitz_blitz
	extrn	___bb_bmploader_bmploader
	extrn	___bb_cocoamaxgui_cocoamaxgui
	extrn	___bb_d3d7max2d_d3d7max2d
	extrn	___bb_directsoundaudio_directsoundaudio
	extrn	___bb_eventqueue_eventqueue
	extrn	___bb_fltkmaxgui_fltkmaxgui
	extrn	___bb_freeaudioaudio_freeaudioaudio
	extrn	___bb_freejoy_freejoy
	extrn	___bb_freeprocess_freeprocess
	extrn	___bb_freetypefont_freetypefont
	extrn	___bb_glew_glew
	extrn	___bb_gnet_gnet
	extrn	___bb_jpgloader_jpgloader
	extrn	___bb_macos_macos
	extrn	___bb_maxgui_maxgui
	extrn	___bb_maxutil_maxutil
	extrn	___bb_oggloader_oggloader
	extrn	___bb_openalaudio_openalaudio
	extrn	___bb_pngloader_pngloader
	extrn	___bb_reflection_reflection
	extrn	___bb_retro_retro
	extrn	___bb_tgaloader_tgaloader
	extrn	___bb_timer_timer
	extrn	___bb_wavloader_wavloader
	extrn	___bb_win32maxgui_win32maxgui
	extrn	_bbEmptyString
	extrn	_bbGCFree
	extrn	_bbHandleFromObject
	extrn	_bbNullObject
	extrn	_bbObjectClass
	extrn	_bbObjectCompare
	extrn	_bbObjectCtor
	extrn	_bbObjectDowncast
	extrn	_bbObjectDtor
	extrn	_bbObjectFree
	extrn	_bbObjectRegisterType
	extrn	_bbObjectReserved
	extrn	_bbObjectSendMessage
	extrn	_bbObjectToString
	extrn	_brl_stream_ReadInt
	extrn	_brl_stream_ReadString
	public	___bb_inc_bbtype
	public	__bb_TBBType_Add
	public	__bb_TBBType_Delete
	public	__bb_TBBType_InsertAfter
	public	__bb_TBBType_InsertBefore
	public	__bb_TBBType_New
	public	__bb_TBBType_Remove
	public	_bb_DeleteEach
	public	_bb_DeleteFirst
	public	_bb_DeleteLast
	public	_bb_HandleFromObject
	public	_bb_HandleToObject
	public	_bb_ReadString
	public	_bb_TBBType
	section	"code" code
___bb_inc_bbtype:
	push	ebp
	mov	ebp,esp
	cmp	dword [_74],0
	je	_75
	mov	eax,0
	mov	esp,ebp
	pop	ebp
	ret
_75:
	mov	dword [_74],1
	call	___bb_blitz_blitz
	call	___bb_appstub_appstub
	call	___bb_audio_audio
	call	___bb_bank_bank
	call	___bb_bankstream_bankstream
	call	___bb_basic_basic
	call	___bb_bmploader_bmploader
	call	___bb_cocoamaxgui_cocoamaxgui
	call	___bb_d3d7max2d_d3d7max2d
	call	___bb_directsoundaudio_directsoundaudio
	call	___bb_eventqueue_eventqueue
	call	___bb_fltkmaxgui_fltkmaxgui
	call	___bb_freeaudioaudio_freeaudioaudio
	call	___bb_freetypefont_freetypefont
	call	___bb_gnet_gnet
	call	___bb_jpgloader_jpgloader
	call	___bb_maxgui_maxgui
	call	___bb_maxutil_maxutil
	call	___bb_oggloader_oggloader
	call	___bb_openalaudio_openalaudio
	call	___bb_pngloader_pngloader
	call	___bb_reflection_reflection
	call	___bb_retro_retro
	call	___bb_tgaloader_tgaloader
	call	___bb_timer_timer
	call	___bb_wavloader_wavloader
	call	___bb_win32maxgui_win32maxgui
	call	___bb_freejoy_freejoy
	call	___bb_freeprocess_freeprocess
	call	___bb_glew_glew
	call	___bb_macos_macos
	push	_bb_TBBType
	call	_bbObjectRegisterType
	add	esp,4
	mov	eax,0
_33:
	mov	esp,ebp
	pop	ebp
	ret
__bb_TBBType_New:
	push	ebp
	mov	ebp,esp
	push	ebx
	mov	ebx,dword [ebp+8]
	push	ebx
	call	_bbObjectCtor
	add	esp,4
	mov	dword [ebx],_bb_TBBType
	mov	eax,_bbNullObject
	inc	dword [eax+4]
	mov	dword [ebx+8],eax
	mov	eax,_bbNullObject
	inc	dword [eax+4]
	mov	dword [ebx+12],eax
	mov	eax,0
_36:
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_TBBType_Delete:
	push	ebp
	mov	ebp,esp
	push	ebx
	mov	ebx,dword [ebp+8]
_39:
	mov	eax,dword [ebx+12]
	dec	dword [eax+4]
	jnz	_80
	push	eax
	call	_bbGCFree
	add	esp,4
_80:
	mov	eax,dword [ebx+8]
	dec	dword [eax+4]
	jnz	_82
	push	eax
	call	_bbGCFree
	add	esp,4
_82:
	mov	dword [ebx],_bbObjectClass
	push	ebx
	call	_bbObjectDtor
	add	esp,4
	mov	eax,0
_78:
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_TBBType_Add:
	push	ebp
	mov	ebp,esp
	push	ebx
	push	esi
	mov	esi,dword [ebp+8]
	mov	ebx,dword [ebp+12]
	inc	dword [ebx+4]
	mov	eax,dword [esi+8]
	dec	dword [eax+4]
	jnz	_86
	push	eax
	call	_bbGCFree
	add	esp,4
_86:
	mov	dword [esi+8],ebx
	mov	eax,dword [esi+8]
	push	esi
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+68]
	add	esp,8
	inc	dword [eax+4]
	mov	ebx,eax
	mov	eax,dword [esi+12]
	dec	dword [eax+4]
	jnz	_91
	push	eax
	call	_bbGCFree
	add	esp,4
_91:
	mov	dword [esi+12],ebx
	mov	eax,0
_43:
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_TBBType_InsertBefore:
	push	ebp
	mov	ebp,esp
	push	ebx
	push	esi
	mov	ebx,dword [ebp+8]
	mov	esi,dword [ebp+12]
	mov	eax,dword [ebx+12]
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+60]
	add	esp,4
	mov	eax,dword [ebx+8]
	push	dword [esi+12]
	push	ebx
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+96]
	add	esp,12
	inc	dword [eax+4]
	mov	esi,eax
	mov	eax,dword [ebx+12]
	dec	dword [eax+4]
	jnz	_97
	push	eax
	call	_bbGCFree
	add	esp,4
_97:
	mov	dword [ebx+12],esi
	mov	eax,0
_47:
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_TBBType_InsertAfter:
	push	ebp
	mov	ebp,esp
	push	ebx
	push	esi
	mov	ebx,dword [ebp+8]
	mov	esi,dword [ebp+12]
	mov	eax,dword [ebx+12]
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+60]
	add	esp,4
	mov	eax,dword [ebx+8]
	push	dword [esi+12]
	push	ebx
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+100]
	add	esp,12
	inc	dword [eax+4]
	mov	esi,eax
	mov	eax,dword [ebx+12]
	dec	dword [eax+4]
	jnz	_103
	push	eax
	call	_bbGCFree
	add	esp,4
_103:
	mov	dword [ebx+12],esi
	mov	eax,0
_51:
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_TBBType_Remove:
	push	ebp
	mov	ebp,esp
	mov	edx,dword [ebp+8]
	mov	eax,dword [edx+8]
	push	edx
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+116]
	add	esp,8
	mov	eax,0
_54:
	mov	esp,ebp
	pop	ebp
	ret
_bb_DeleteLast:
	push	ebp
	mov	ebp,esp
	mov	eax,dword [ebp+8]
	cmp	eax,_bbNullObject
	je	_105
	mov	eax,dword [eax+8]
	push	_bb_TBBType
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+76]
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+60]
	add	esp,4
_105:
	mov	eax,0
_57:
	mov	esp,ebp
	pop	ebp
	ret
_bb_DeleteFirst:
	push	ebp
	mov	ebp,esp
	mov	eax,dword [ebp+8]
	cmp	eax,_bbNullObject
	je	_108
	mov	eax,dword [eax+8]
	push	_bb_TBBType
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+72]
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+60]
	add	esp,4
_108:
	mov	eax,0
_60:
	mov	esp,ebp
	pop	ebp
	ret
_bb_DeleteEach:
	push	ebp
	mov	ebp,esp
	mov	eax,dword [ebp+8]
	cmp	eax,_bbNullObject
	je	_111
	mov	eax,dword [eax+8]
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+52]
	add	esp,4
_111:
	mov	eax,0
_63:
	mov	esp,ebp
	pop	ebp
	ret
_bb_ReadString:
	push	ebp
	mov	ebp,esp
	push	ebx
	mov	ebx,dword [ebp+8]
	push	ebx
	call	_brl_stream_ReadInt
	add	esp,4
	mov	edx,eax
	cmp	edx,0
	setg	al
	movzx	eax,al
	cmp	eax,0
	je	_114
	cmp	edx,1048576
	setl	al
	movzx	eax,al
_114:
	cmp	eax,0
	je	_116
	push	edx
	push	ebx
	call	_brl_stream_ReadString
	add	esp,8
	jmp	_66
_116:
	mov	eax,_bbEmptyString
_66:
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_bb_HandleToObject:
	push	ebp
	mov	ebp,esp
	mov	eax,dword [ebp+8]
_69:
	mov	esp,ebp
	pop	ebp
	ret
_bb_HandleFromObject:
	push	ebp
	mov	ebp,esp
	mov	eax,dword [ebp+8]
	push	eax
	call	_bb_HandleToObject
	add	esp,4
	push	eax
	call	_bbHandleFromObject
	add	esp,4
_72:
	mov	esp,ebp
	pop	ebp
	ret
	section	"data" data writeable align 8
	align	4
_74:
	dd	0
_19:
	db	"TBBType",0
_20:
	db	"_list",0
_21:
	db	":brl.linkedlist.TList",0
_22:
	db	"_link",0
_23:
	db	":brl.linkedlist.TLink",0
_24:
	db	"New",0
_25:
	db	"()i",0
_26:
	db	"Delete",0
_27:
	db	"Add",0
_28:
	db	"(:brl.linkedlist.TList)i",0
_29:
	db	"InsertBefore",0
_30:
	db	"(:TBBType)i",0
_31:
	db	"InsertAfter",0
_32:
	db	"Remove",0
	align	4
_18:
	dd	2
	dd	_19
	dd	3
	dd	_20
	dd	_21
	dd	8
	dd	3
	dd	_22
	dd	_23
	dd	12
	dd	6
	dd	_24
	dd	_25
	dd	16
	dd	6
	dd	_26
	dd	_25
	dd	20
	dd	6
	dd	_27
	dd	_28
	dd	48
	dd	6
	dd	_29
	dd	_30
	dd	52
	dd	6
	dd	_31
	dd	_30
	dd	56
	dd	6
	dd	_32
	dd	_25
	dd	60
	dd	0
	align	4
_bb_TBBType:
	dd	_bbObjectClass
	dd	_bbObjectFree
	dd	_18
	dd	16
	dd	__bb_TBBType_New
	dd	__bb_TBBType_Delete
	dd	_bbObjectToString
	dd	_bbObjectCompare
	dd	_bbObjectSendMessage
	dd	_bbObjectReserved
	dd	_bbObjectReserved
	dd	_bbObjectReserved
	dd	__bb_TBBType_Add
	dd	__bb_TBBType_InsertBefore
	dd	__bb_TBBType_InsertAfter
	dd	__bb_TBBType_Remove
