	format	MS COFF
	extrn	___bb_blitz_blitz
	extrn	___bb_inc_bbtype
	extrn	___bb_inc_bbvkey
	extrn	___bb_minib3d_minib3d
	extrn	__sidesign_minib3d_TGlobal_depth
	extrn	__sidesign_minib3d_TGlobal_height
	extrn	__sidesign_minib3d_TGlobal_mode
	extrn	__sidesign_minib3d_TGlobal_rate
	extrn	__sidesign_minib3d_TGlobal_width
	extrn	_bbArrayNew1D
	extrn	_bbEmptyArray
	extrn	_bbEmptyString
	extrn	_bbEnd
	extrn	_bbFloatToInt
	extrn	_bbGCFree
	extrn	_bbHandleFromObject
	extrn	_bbHandleToObject
	extrn	_bbMilliSecs
	extrn	_bbNullObject
	extrn	_bbObjectClass
	extrn	_bbObjectCompare
	extrn	_bbObjectCtor
	extrn	_bbObjectDowncast
	extrn	_bbObjectDtor
	extrn	_bbObjectFree
	extrn	_bbObjectNew
	extrn	_bbObjectRegisterType
	extrn	_bbObjectReserved
	extrn	_bbObjectSendMessage
	extrn	_bbObjectToString
	extrn	_bbStringAsc
	extrn	_bbStringClass
	extrn	_bbStringCompare
	extrn	_bbStringConcat
	extrn	_bbStringFromChar
	extrn	_bbStringFromInt
	extrn	_bbStringToInt
	extrn	_brl_eventqueue_CurrentEvent
	extrn	_brl_eventqueue_EventData
	extrn	_brl_eventqueue_EventID
	extrn	_brl_eventqueue_EventSource
	extrn	_brl_eventqueue_EventX
	extrn	_brl_eventqueue_EventY
	extrn	_brl_eventqueue_WaitEvent
	extrn	_brl_filesystem_ChangeDir
	extrn	_brl_filesystem_CloseDir
	extrn	_brl_filesystem_CurrentDir
	extrn	_brl_filesystem_ExtractExt
	extrn	_brl_filesystem_FileType
	extrn	_brl_filesystem_NextFile
	extrn	_brl_filesystem_ReadDir
	extrn	_brl_filesystem_RealPath
	extrn	_brl_filesystem_StripAll
	extrn	_brl_glgraphics_GLGraphicsDriver
	extrn	_brl_graphics_Flip
	extrn	_brl_graphics_SetGraphics
	extrn	_brl_graphics_SetGraphicsDriver
	extrn	_brl_linkedlist_CreateList
	extrn	_brl_maxgui_AddGadgetItem
	extrn	_brl_maxgui_AddTreeViewNode
	extrn	_brl_maxgui_CanvasGraphics
	extrn	_brl_maxgui_ClientHeight
	extrn	_brl_maxgui_ClientWidth
	extrn	_brl_maxgui_CreateButton
	extrn	_brl_maxgui_CreateCanvas
	extrn	_brl_maxgui_CreateComboBox
	extrn	_brl_maxgui_CreatePanel
	extrn	_brl_maxgui_CreateTabber
	extrn	_brl_maxgui_CreateTreeView
	extrn	_brl_maxgui_CreateWindow
	extrn	_brl_maxgui_GadgetItemText
	extrn	_brl_maxgui_HideGadget
	extrn	_brl_maxgui_LoadIconStrip
	extrn	_brl_maxgui_QueryGadget
	extrn	_brl_maxgui_RedrawGadget
	extrn	_brl_maxgui_SelectGadgetItem
	extrn	_brl_maxgui_SelectedGadgetItem
	extrn	_brl_maxgui_SelectedTreeViewNode
	extrn	_brl_maxgui_SetGadgetIconStrip
	extrn	_brl_maxgui_SetGadgetLayout
	extrn	_brl_maxgui_ShowGadget
	extrn	_brl_maxgui_TGadget
	extrn	_brl_maxgui_TIconStrip
	extrn	_brl_retro_Mid
	extrn	_brl_stream_ReadByte
	extrn	_brl_stream_ReadFloat
	extrn	_brl_stream_ReadInt
	extrn	_brl_stream_SeekStream
	extrn	_brl_stream_StreamPos
	extrn	_brl_stream_TStream
	extrn	_brl_stream_WriteByte
	extrn	_brl_stream_WriteFloat
	extrn	_brl_stream_WriteInt
	extrn	_brl_system_RequestFile
	extrn	_brl_timer_CreateTimer
	extrn	_glAlphaFunc@8
	extrn	_glClearDepth@8
	extrn	_glDepthFunc@4
	extrn	_glEnable@4
	extrn	_glEnableClientState@4
	extrn	_glHint@8
	extrn	_glLightModeli@8
	extrn	_glLineWidth@4
	extrn	_glewInit
	extrn	_sidesign_minib3d_AddMesh
	extrn	_sidesign_minib3d_AddTriangle
	extrn	_sidesign_minib3d_AddVertex
	extrn	_sidesign_minib3d_AmbientLight
	extrn	_sidesign_minib3d_BrushAlpha
	extrn	_sidesign_minib3d_BrushTexture
	extrn	_sidesign_minib3d_CameraClsColor
	extrn	_sidesign_minib3d_CameraRange
	extrn	_sidesign_minib3d_CreateCamera
	extrn	_sidesign_minib3d_CreateMesh
	extrn	_sidesign_minib3d_CreateSurface
	extrn	_sidesign_minib3d_EntityFX
	extrn	_sidesign_minib3d_EntityX
	extrn	_sidesign_minib3d_EntityY
	extrn	_sidesign_minib3d_EntityZ
	extrn	_sidesign_minib3d_FindChild
	extrn	_sidesign_minib3d_FlipMesh
	extrn	_sidesign_minib3d_FreeEntity
	extrn	_sidesign_minib3d_LoadAnimMesh
	extrn	_sidesign_minib3d_LoadBrush
	extrn	_sidesign_minib3d_LoadMesh
	extrn	_sidesign_minib3d_LoadTexture
	extrn	_sidesign_minib3d_MoveEntity
	extrn	_sidesign_minib3d_PaintSurface
	extrn	_sidesign_minib3d_PositionEntity
	extrn	_sidesign_minib3d_PositionMesh
	extrn	_sidesign_minib3d_RenderWorld
	extrn	_sidesign_minib3d_RotateEntity
	extrn	_sidesign_minib3d_ScaleMesh
	extrn	_sidesign_minib3d_TBrush
	extrn	_sidesign_minib3d_TEntity
	extrn	_sidesign_minib3d_TMesh
	extrn	_sidesign_minib3d_TSurface
	extrn	_sidesign_minib3d_TTexture
	extrn	_sidesign_minib3d_TranslateEntity
	extrn	_sidesign_minib3d_TurnEntity
	public	__bb_TMouse_Delete
	public	__bb_TMouse_GetButton
	public	__bb_TMouse_Init
	public	__bb_TMouse_New
	public	__bb_TMouse_Update
	public	__bb_TMouse_getX
	public	__bb_TMouse_getY
	public	__bb_TMouse_getZ
	public	__bb_main
	public	_bb_BaseMesh
	public	_bb_BaseModel
	public	_bb_BaseModelBase
	public	_bb_DoGadgetAction
	public	_bb_DoorModel
	public	_bb_DoorPoint
	public	_bb_DoorX
	public	_bb_DoorY
	public	_bb_DoorZ
	public	_bb_Door_list
	public	_bb_KeyX
	public	_bb_KeyY
	public	_bb_KeyZ
	public	_bb_MakeSkyBox
	public	_bb_Model_List
	public	_bb_Mouse
	public	_bb_MouseXEndPosi
	public	_bb_MouseYEndPosi
	public	_bb_RoofModel
	public	_bb_RoofPoint
	public	_bb_RoofX
	public	_bb_RoofY
	public	_bb_RoofZ
	public	_bb_Roof_List
	public	_bb_SetWindowCnt
	public	_bb_SetWindowTex
	public	_bb_Surf_list
	public	_bb_TMouse
	public	_bb_TabPanel0
	public	_bb_TabPanel1
	public	_bb_TabPanel2
	public	_bb_TabPanel3
	public	_bb_Terrain
	public	_bb_TextureLoadedMesh
	public	_bb_Texture_List
	public	_bb_Texturebase_list
	public	_bb_Texturebeam_list
	public	_bb_Textureroof_list
	public	_bb_Texturewindow_list
	public	_bb_WinChild
	public	_bb_Window
	public	_bb_WindowCnt
	public	_bb_WindowsCnt
	public	_bb_WindowsTex
	public	_bb_WindowsX
	public	_bb_WindowsY
	public	_bb_WindowsZ
	public	_bb_add
	public	_bb_b3dBeginChunk
	public	_bb_b3dChunkSize
	public	_bb_b3dEndChunk
	public	_bb_b3dExitChunk
	public	_bb_b3dReadByte
	public	_bb_b3dReadChunk
	public	_bb_b3dReadFloat
	public	_bb_b3dReadInt
	public	_bb_b3dReadString
	public	_bb_b3dSetFile
	public	_bb_b3dWriteByte
	public	_bb_b3dWriteFloat
	public	_bb_b3dWriteInt
	public	_bb_b3dWriteString
	public	_bb_b3d_file
	public	_bb_b3d_stack
	public	_bb_b3d_tos
	public	_bb_basedir
	public	_bb_c_surfs
	public	_bb_camera
	public	_bb_cavCanvas0
	public	_bb_cobComboBox1
	public	_bb_count
	public	_bb_enumFiles
	public	_bb_enumFilesCallback
	public	_bb_enumFolders
	public	_bb_enumFoldersCallback
	public	_bb_exportmeshbutt
	public	_bb_ext
	public	_bb_filenames
	public	_bb_hwnd
	public	_bb_invsLoadedMesh
	public	_bb_mesh
	public	_bb_nodeselected
	public	_bb_res
	public	_bb_roofdir
	public	_bb_sidebutt
	public	_bb_skydome
	public	_bb_t
	public	_bb_tabTabber0
	public	_bb_texcount
	public	_bb_texture
	public	_bb_texturedir
	public	_bb_trvTreeView0
	public	_bb_trvTreeView1
	public	_bb_trvTreeView4
	public	_bb_windowselected
	section	"code" code
__bb_main:
	push	ebp
	mov	ebp,esp
	sub	esp,64
	push	ebx
	push	esi
	push	edi
	cmp	dword [_441],0
	je	_442
	mov	eax,0
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_442:
	mov	dword [_441],1
	fld	dword [_584]
	fstp	dword [ebp-24]
	mov	dword [ebp-56],0
	mov	dword [ebp-60],0
	call	___bb_blitz_blitz
	call	___bb_inc_bbtype
	call	___bb_inc_bbvkey
	call	___bb_minib3d_minib3d
	push	_bb_TMouse
	call	_bbObjectRegisterType
	add	esp,4
	mov	eax,dword [_224]
	and	eax,1
	cmp	eax,0
	jne	_225
	push	101
	push	_222
	call	_bbArrayNew1D
	add	esp,8
	inc	dword [eax+4]
	mov	dword [_bb_b3d_stack],eax
	or	dword [_224],1
_225:
	push	10
	call	_brl_glgraphics_GLGraphicsDriver
	push	eax
	call	_brl_graphics_SetGraphicsDriver
	add	esp,8
	mov	eax,dword [_224]
	and	eax,2
	cmp	eax,0
	jne	_227
	call	dword [_bb_TMouse+48]
	inc	dword [eax+4]
	mov	dword [_bb_Mouse],eax
	or	dword [_224],2
_227:
	mov	eax,dword [_224]
	and	eax,4
	cmp	eax,0
	jne	_230
	push	50
	push	_228
	call	_bbArrayNew1D
	add	esp,8
	inc	dword [eax+4]
	mov	dword [_bb_add],eax
	or	dword [_224],4
_230:
	mov	dword [ebp-44],0
	mov	dword [ebp-40],0
	mov	dword [ebp-36],0
	mov	dword [ebp-32],0
	mov	dword [ebp-28],0
	mov	dword [ebp-48],0
	mov	dword [ebp-52],0
	mov	eax,dword [_224]
	and	eax,8
	cmp	eax,0
	jne	_243
	push	10
	push	_241
	call	_bbArrayNew1D
	add	esp,8
	inc	dword [eax+4]
	mov	dword [_bb_WindowsTex],eax
	or	dword [_224],8
_243:
	mov	eax,dword [_224]
	and	eax,16
	cmp	eax,0
	jne	_246
	push	10
	push	_244
	call	_bbArrayNew1D
	add	esp,8
	inc	dword [eax+4]
	mov	dword [_bb_WindowsCnt],eax
	or	dword [_224],16
_246:
	mov	eax,dword [_224]
	and	eax,32
	cmp	eax,0
	jne	_249
	push	10
	push	_247
	call	_bbArrayNew1D
	add	esp,8
	inc	dword [eax+4]
	mov	dword [_bb_WindowsX],eax
	or	dword [_224],32
_249:
	mov	eax,dword [_224]
	and	eax,64
	cmp	eax,0
	jne	_252
	push	10
	push	_250
	call	_bbArrayNew1D
	add	esp,8
	inc	dword [eax+4]
	mov	dword [_bb_WindowsY],eax
	or	dword [_224],64
_252:
	mov	eax,dword [_224]
	and	eax,128
	cmp	eax,0
	jne	_255
	push	10
	push	_253
	call	_bbArrayNew1D
	add	esp,8
	inc	dword [eax+4]
	mov	dword [_bb_WindowsZ],eax
	or	dword [_224],128
_255:
	mov	eax,dword [_224]
	and	eax,256
	cmp	eax,0
	jne	_258
	push	10
	push	_256
	call	_bbArrayNew1D
	add	esp,8
	inc	dword [eax+4]
	mov	dword [_bb_WinChild],eax
	or	dword [_224],256
_258:
	mov	eax,dword [_224]
	and	eax,512
	cmp	eax,0
	jne	_260
	push	3
	push	_brl_maxgui_TGadget
	push	0
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	push	704
	push	978
	push	0
	push	0
	push	_30
	call	_brl_maxgui_CreateWindow
	add	esp,28
	inc	dword [eax+4]
	mov	dword [_bb_Window],eax
	or	dword [_224],512
_260:
	mov	eax,dword [_224]
	and	eax,1024
	cmp	eax,0
	jne	_262
	push	0
	push	dword [_bb_Window]
	push	658
	push	245
	push	12
	push	10
	call	_brl_maxgui_CreateTabber
	add	esp,24
	inc	dword [eax+4]
	mov	dword [_bb_tabTabber0],eax
	or	dword [_224],1024
_262:
	push	_bbNullObject
	push	_1
	push	-1
	push	1
	push	_31
	push	dword [_bb_tabTabber0]
	call	_brl_maxgui_AddGadgetItem
	add	esp,24
	push	_bbNullObject
	push	_1
	push	-1
	push	0
	push	_32
	push	dword [_bb_tabTabber0]
	call	_brl_maxgui_AddGadgetItem
	add	esp,24
	push	_bbNullObject
	push	_1
	push	-1
	push	0
	push	_33
	push	dword [_bb_tabTabber0]
	call	_brl_maxgui_AddGadgetItem
	add	esp,24
	push	_bbNullObject
	push	_1
	push	-1
	push	0
	push	_34
	push	dword [_bb_tabTabber0]
	call	_brl_maxgui_AddGadgetItem
	add	esp,24
	push	2
	push	1
	push	2
	push	1
	push	dword [_bb_tabTabber0]
	call	_brl_maxgui_SetGadgetLayout
	add	esp,20
	mov	eax,dword [_224]
	and	eax,2048
	cmp	eax,0
	jne	_264
	push	_1
	push	0
	push	dword [_bb_tabTabber0]
	push	dword [_bb_tabTabber0]
	call	_brl_maxgui_ClientHeight
	add	esp,4
	push	eax
	push	dword [_bb_tabTabber0]
	call	_brl_maxgui_ClientWidth
	add	esp,4
	push	eax
	push	0
	push	0
	call	_brl_maxgui_CreatePanel
	add	esp,28
	inc	dword [eax+4]
	mov	dword [_bb_TabPanel0],eax
	or	dword [_224],2048
_264:
	push	1
	push	1
	push	1
	push	1
	push	dword [_bb_TabPanel0]
	call	_brl_maxgui_SetGadgetLayout
	add	esp,20
	mov	eax,dword [_224]
	and	eax,4096
	cmp	eax,0
	jne	_266
	push	0
	push	dword [_bb_TabPanel0]
	push	593
	push	234
	push	14
	push	2
	call	_brl_maxgui_CreateTreeView
	add	esp,24
	inc	dword [eax+4]
	mov	dword [_bb_trvTreeView0],eax
	or	dword [_224],4096
_266:
	push	0
	push	1
	push	0
	push	1
	push	dword [_bb_trvTreeView0]
	call	_brl_maxgui_SetGadgetLayout
	add	esp,20
	mov	eax,dword [_224]
	and	eax,8192
	cmp	eax,0
	jne	_268
	push	_1
	push	0
	push	dword [_bb_tabTabber0]
	push	dword [_bb_tabTabber0]
	call	_brl_maxgui_ClientHeight
	add	esp,4
	push	eax
	push	dword [_bb_tabTabber0]
	call	_brl_maxgui_ClientWidth
	add	esp,4
	push	eax
	push	0
	push	0
	call	_brl_maxgui_CreatePanel
	add	esp,28
	inc	dword [eax+4]
	mov	dword [_bb_TabPanel1],eax
	or	dword [_224],8192
_268:
	push	dword [_bb_TabPanel1]
	call	_brl_maxgui_HideGadget
	add	esp,4
	push	1
	push	1
	push	1
	push	1
	push	dword [_bb_TabPanel1]
	call	_brl_maxgui_SetGadgetLayout
	add	esp,20
	push	dword [_bb_TabPanel1]
	call	_brl_maxgui_HideGadget
	add	esp,4
	mov	eax,dword [_224]
	and	eax,16384
	cmp	eax,0
	jne	_270
	push	0
	push	dword [_bb_TabPanel1]
	push	595
	push	234
	push	12
	push	3
	call	_brl_maxgui_CreateTreeView
	add	esp,24
	inc	dword [eax+4]
	mov	dword [_bb_trvTreeView1],eax
	or	dword [_224],16384
_270:
	push	0
	push	1
	push	0
	push	1
	push	dword [_bb_trvTreeView1]
	call	_brl_maxgui_SetGadgetLayout
	add	esp,20
	mov	eax,dword [_224]
	and	eax,32768
	cmp	eax,0
	jne	_272
	push	_1
	push	0
	push	dword [_bb_tabTabber0]
	push	dword [_bb_tabTabber0]
	call	_brl_maxgui_ClientHeight
	add	esp,4
	push	eax
	push	dword [_bb_tabTabber0]
	call	_brl_maxgui_ClientWidth
	add	esp,4
	push	eax
	push	0
	push	0
	call	_brl_maxgui_CreatePanel
	add	esp,28
	inc	dword [eax+4]
	mov	dword [_bb_TabPanel2],eax
	or	dword [_224],32768
_272:
	push	dword [_bb_TabPanel2]
	call	_brl_maxgui_HideGadget
	add	esp,4
	push	1
	push	1
	push	1
	push	1
	push	dword [_bb_TabPanel2]
	call	_brl_maxgui_SetGadgetLayout
	add	esp,20
	push	dword [_bb_TabPanel2]
	call	_brl_maxgui_HideGadget
	add	esp,4
	mov	eax,dword [_224]
	and	eax,65536
	cmp	eax,0
	jne	_274
	push	3
	push	dword [_bb_TabPanel2]
	push	16
	push	96
	push	83
	push	124
	push	_35
	call	_brl_maxgui_CreateButton
	add	esp,28
	inc	dword [eax+4]
	mov	dword [_bb_sidebutt],eax
	or	dword [_224],65536
_274:
	push	0
	push	1
	push	0
	push	1
	push	dword [_bb_sidebutt]
	call	_brl_maxgui_SetGadgetLayout
	add	esp,20
	mov	eax,dword [_224]
	and	eax,131072
	cmp	eax,0
	jne	_276
	push	0
	push	dword [_bb_TabPanel2]
	push	20
	push	96
	push	12
	push	127
	call	_brl_maxgui_CreateComboBox
	add	esp,24
	inc	dword [eax+4]
	mov	dword [_bb_cobComboBox1],eax
	or	dword [_224],131072
_276:
	push	_bbNullObject
	push	_1
	push	-1
	push	0
	push	_36
	push	dword [_bb_cobComboBox1]
	call	_brl_maxgui_AddGadgetItem
	add	esp,24
	push	_bbNullObject
	push	_1
	push	-1
	push	0
	push	_37
	push	dword [_bb_cobComboBox1]
	call	_brl_maxgui_AddGadgetItem
	add	esp,24
	push	_bbNullObject
	push	_1
	push	-1
	push	0
	push	_38
	push	dword [_bb_cobComboBox1]
	call	_brl_maxgui_AddGadgetItem
	add	esp,24
	push	_bbNullObject
	push	_1
	push	-1
	push	0
	push	_39
	push	dword [_bb_cobComboBox1]
	call	_brl_maxgui_AddGadgetItem
	add	esp,24
	push	_bbNullObject
	push	_1
	push	-1
	push	0
	push	_40
	push	dword [_bb_cobComboBox1]
	call	_brl_maxgui_AddGadgetItem
	add	esp,24
	push	_bbNullObject
	push	_1
	push	-1
	push	0
	push	_41
	push	dword [_bb_cobComboBox1]
	call	_brl_maxgui_AddGadgetItem
	add	esp,24
	push	0
	push	dword [_bb_cobComboBox1]
	call	_brl_maxgui_SelectGadgetItem
	add	esp,8
	push	0
	push	1
	push	0
	push	1
	push	dword [_bb_cobComboBox1]
	call	_brl_maxgui_SetGadgetLayout
	add	esp,20
	mov	eax,dword [_224]
	and	eax,262144
	cmp	eax,0
	jne	_278
	push	0
	push	dword [_bb_TabPanel2]
	push	20
	push	96
	push	47
	push	127
	call	_brl_maxgui_CreateComboBox
	add	esp,24
	inc	dword [eax+4]
	mov	dword [_bb_windowselected],eax
	or	dword [_224],262144
_278:
	push	_bbNullObject
	push	_1
	push	-1
	push	0
	push	_36
	push	dword [_bb_windowselected]
	call	_brl_maxgui_AddGadgetItem
	add	esp,24
	push	_bbNullObject
	push	_1
	push	-1
	push	0
	push	_37
	push	dword [_bb_windowselected]
	call	_brl_maxgui_AddGadgetItem
	add	esp,24
	push	_bbNullObject
	push	_1
	push	-1
	push	0
	push	_38
	push	dword [_bb_windowselected]
	call	_brl_maxgui_AddGadgetItem
	add	esp,24
	push	_bbNullObject
	push	_1
	push	-1
	push	0
	push	_39
	push	dword [_bb_windowselected]
	call	_brl_maxgui_AddGadgetItem
	add	esp,24
	push	_bbNullObject
	push	_1
	push	-1
	push	0
	push	_40
	push	dword [_bb_windowselected]
	call	_brl_maxgui_AddGadgetItem
	add	esp,24
	push	_bbNullObject
	push	_1
	push	-1
	push	0
	push	_41
	push	dword [_bb_windowselected]
	call	_brl_maxgui_AddGadgetItem
	add	esp,24
	push	0
	push	dword [_bb_windowselected]
	call	_brl_maxgui_SelectGadgetItem
	add	esp,8
	push	0
	push	1
	push	0
	push	1
	push	dword [_bb_windowselected]
	call	_brl_maxgui_SetGadgetLayout
	add	esp,20
	mov	eax,dword [_224]
	and	eax,524288
	cmp	eax,0
	jne	_280
	push	0
	push	dword [_bb_TabPanel2]
	push	15
	push	103
	push	50
	push	12
	push	_42
	call	_brl_maxgui_CreateButton
	add	esp,28
	inc	dword [eax+4]
	mov	dword [_bb_SetWindowTex],eax
	or	dword [_224],524288
_280:
	push	0
	push	1
	push	0
	push	1
	push	dword [_bb_SetWindowTex]
	call	_brl_maxgui_SetGadgetLayout
	add	esp,20
	mov	eax,dword [_224]
	and	eax,1048576
	cmp	eax,0
	jne	_282
	push	0
	push	dword [_bb_TabPanel2]
	push	16
	push	99
	push	15
	push	14
	push	_43
	call	_brl_maxgui_CreateButton
	add	esp,28
	inc	dword [eax+4]
	mov	dword [_bb_SetWindowCnt],eax
	or	dword [_224],1048576
_282:
	push	0
	push	1
	push	0
	push	1
	push	dword [_bb_SetWindowCnt]
	call	_brl_maxgui_SetGadgetLayout
	add	esp,20
	mov	eax,dword [_224]
	and	eax,2097152
	cmp	eax,0
	jne	_284
	push	_1
	push	0
	push	dword [_bb_tabTabber0]
	push	dword [_bb_tabTabber0]
	call	_brl_maxgui_ClientHeight
	add	esp,4
	push	eax
	push	dword [_bb_tabTabber0]
	call	_brl_maxgui_ClientWidth
	add	esp,4
	push	eax
	push	0
	push	0
	call	_brl_maxgui_CreatePanel
	add	esp,28
	inc	dword [eax+4]
	mov	dword [_bb_TabPanel3],eax
	or	dword [_224],2097152
_284:
	push	dword [_bb_TabPanel3]
	call	_brl_maxgui_HideGadget
	add	esp,4
	push	1
	push	1
	push	1
	push	1
	push	dword [_bb_TabPanel3]
	call	_brl_maxgui_SetGadgetLayout
	add	esp,20
	push	dword [_bb_TabPanel3]
	call	_brl_maxgui_HideGadget
	add	esp,4
	mov	eax,dword [_224]
	and	eax,4194304
	cmp	eax,0
	jne	_286
	push	0
	push	dword [_bb_TabPanel3]
	push	593
	push	235
	push	10
	push	3
	call	_brl_maxgui_CreateTreeView
	add	esp,24
	inc	dword [eax+4]
	mov	dword [_bb_trvTreeView4],eax
	or	dword [_224],4194304
_286:
	push	0
	push	1
	push	0
	push	1
	push	dword [_bb_trvTreeView4]
	call	_brl_maxgui_SetGadgetLayout
	add	esp,20
	mov	eax,dword [_224]
	and	eax,8388608
	cmp	eax,0
	jne	_288
	push	0
	push	dword [_bb_Window]
	push	637
	push	701
	push	32
	push	259
	call	_brl_maxgui_CreateCanvas
	add	esp,24
	inc	dword [eax+4]
	mov	dword [_bb_cavCanvas0],eax
	or	dword [_224],8388608
_288:
	push	2
	push	1
	push	2
	push	1
	push	dword [_bb_cavCanvas0]
	call	_brl_maxgui_SetGadgetLayout
	add	esp,20
	mov	eax,dword [_224]
	and	eax,16777216
	cmp	eax,0
	jne	_290
	push	1
	push	dword [_bb_Window]
	push	24
	push	77
	push	3
	push	836
	push	_44
	call	_brl_maxgui_CreateButton
	add	esp,28
	inc	dword [eax+4]
	mov	dword [_bb_exportmeshbutt],eax
	or	dword [_224],16777216
_290:
	push	0
	push	1
	push	0
	push	1
	push	dword [_bb_exportmeshbutt]
	call	_brl_maxgui_SetGadgetLayout
	add	esp,20
	push	_45
	call	_brl_maxgui_LoadIconStrip
	add	esp,4
	push	eax
	call	_bbHandleFromObject
	add	esp,4
	mov	ebx,eax
	push	_brl_maxgui_TIconStrip
	push	ebx
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	push	dword [_bb_trvTreeView0]
	call	_brl_maxgui_SetGadgetIconStrip
	add	esp,8
	push	_brl_maxgui_TIconStrip
	push	ebx
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	push	dword [_bb_trvTreeView1]
	call	_brl_maxgui_SetGadgetIconStrip
	add	esp,8
	push	_brl_maxgui_TIconStrip
	push	ebx
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	push	dword [_bb_trvTreeView4]
	call	_brl_maxgui_SetGadgetIconStrip
	add	esp,8
	mov	eax,dword [_224]
	and	eax,33554432
	cmp	eax,0
	jne	_292
	push	1
	push	dword [_bb_cavCanvas0]
	call	_brl_maxgui_QueryGadget
	add	esp,8
	mov	dword [_bb_hwnd],eax
	or	dword [_224],33554432
_292:
	mov	eax,dword [_224]
	and	eax,67108864
	cmp	eax,0
	jne	_295
	push	1000
	push	_293
	call	_bbArrayNew1D
	add	esp,8
	inc	dword [eax+4]
	mov	dword [_bb_texturedir],eax
	or	dword [_224],67108864
_295:
	mov	eax,dword [_224]
	and	eax,134217728
	cmp	eax,0
	jne	_297
	call	_brl_linkedlist_CreateList
	inc	dword [eax+4]
	mov	dword [_bb_Model_List],eax
	or	dword [_224],134217728
_297:
	mov	eax,dword [_224]
	and	eax,268435456
	cmp	eax,0
	jne	_299
	call	_brl_linkedlist_CreateList
	inc	dword [eax+4]
	mov	dword [_bb_Roof_List],eax
	or	dword [_224],268435456
_299:
	mov	eax,dword [_224]
	and	eax,536870912
	cmp	eax,0
	jne	_301
	call	_brl_linkedlist_CreateList
	inc	dword [eax+4]
	mov	dword [_bb_Texture_List],eax
	or	dword [_224],536870912
_301:
	mov	eax,dword [_224]
	and	eax,1073741824
	cmp	eax,0
	jne	_303
	call	_brl_linkedlist_CreateList
	inc	dword [eax+4]
	mov	dword [_bb_Texturebase_list],eax
	or	dword [_224],1073741824
_303:
	mov	eax,dword [_224]
	and	eax,-2147483648
	cmp	eax,0
	jne	_305
	call	_brl_linkedlist_CreateList
	inc	dword [eax+4]
	mov	dword [_bb_Textureroof_list],eax
	or	dword [_224],-2147483648
_305:
	mov	eax,dword [_307]
	and	eax,1
	cmp	eax,0
	jne	_308
	call	_brl_linkedlist_CreateList
	inc	dword [eax+4]
	mov	dword [_bb_Texturebeam_list],eax
	or	dword [_307],1
_308:
	mov	eax,dword [_307]
	and	eax,2
	cmp	eax,0
	jne	_310
	call	_brl_linkedlist_CreateList
	inc	dword [eax+4]
	mov	dword [_bb_Texturewindow_list],eax
	or	dword [_307],2
_310:
	mov	eax,dword [_307]
	and	eax,4
	cmp	eax,0
	jne	_312
	call	_brl_linkedlist_CreateList
	inc	dword [eax+4]
	mov	dword [_bb_Door_list],eax
	or	dword [_307],4
_312:
	mov	eax,dword [_307]
	and	eax,8
	cmp	eax,0
	jne	_314
	call	_brl_linkedlist_CreateList
	inc	dword [eax+4]
	mov	dword [_bb_Surf_list],eax
	or	dword [_307],8
_314:
	push	_46
	push	dword [_bb_Model_List]
	call	_bb_enumFiles
	add	esp,8
	push	_47
	push	dword [_bb_Roof_List]
	call	_bb_enumFiles
	add	esp,8
	push	_48
	push	dword [_bb_Door_list]
	call	_bb_enumFiles
	add	esp,8
	push	_49
	push	dword [_bb_Texture_List]
	call	_bb_enumFiles
	add	esp,8
	push	1
	push	dword [_bb_trvTreeView0]
	push	_50
	call	_brl_maxgui_AddTreeViewNode
	add	esp,12
	mov	ebx,eax
	inc	dword [ebx+4]
	mov	eax,dword [_bb_basedir]
	dec	dword [eax+4]
	jnz	_318
	push	eax
	call	_bbGCFree
	add	esp,4
_318:
	mov	dword [_bb_basedir],ebx
	mov	ebx,dword [_bb_Model_List]
	mov	eax,ebx
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+140]
	add	esp,4
	mov	edi,eax
	jmp	_51
_53:
	mov	eax,edi
	push	_bbStringClass
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+52]
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	inc	dword [eax+4]
	mov	esi,eax
	mov	eax,dword [_bb_t]
	dec	dword [eax+4]
	jnz	_327
	push	eax
	call	_bbGCFree
	add	esp,4
_327:
	mov	dword [_bb_t],esi
	cmp	dword [_bb_t],_bbNullObject
	je	_51
	push	_1
	push	dword [_bb_t]
	call	_bbStringCompare
	add	esp,8
	cmp	eax,0
	jne	_328
	jmp	_52
_328:
	push	_54
	push	dword [_bb_t]
	call	_bbStringCompare
	add	esp,8
	cmp	eax,0
	sete	al
	movzx	eax,al
	cmp	eax,0
	jne	_329
	push	_55
	push	dword [_bb_t]
	call	_bbStringCompare
	add	esp,8
	cmp	eax,0
	sete	al
	movzx	eax,al
_329:
	cmp	eax,0
	je	_331
	jmp	_51
_331:
	push	dword [_bb_t]
	call	_brl_filesystem_ExtractExt
	add	esp,4
	inc	dword [eax+4]
	mov	esi,eax
	mov	eax,dword [_bb_ext]
	dec	dword [eax+4]
	jnz	_335
	push	eax
	call	_bbGCFree
	add	esp,4
_335:
	mov	dword [_bb_ext],esi
	mov	eax,dword [_bb_ext]
	push	_56
	push	eax
	call	_bbStringCompare
	add	esp,8
	cmp	eax,0
	je	_338
	jmp	_337
_338:
	push	2
	push	dword [_bb_basedir]
	push	dword [_bb_t]
	call	_brl_filesystem_StripAll
	add	esp,4
	push	eax
	call	_brl_maxgui_AddTreeViewNode
	add	esp,12
_337:
_51:
	mov	eax,edi
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+48]
	add	esp,4
	cmp	eax,0
	jne	_53
_52:
	push	1
	push	dword [_bb_trvTreeView1]
	push	_32
	call	_brl_maxgui_AddTreeViewNode
	add	esp,12
	mov	ebx,eax
	inc	dword [ebx+4]
	mov	eax,dword [_bb_roofdir]
	dec	dword [eax+4]
	jnz	_342
	push	eax
	call	_bbGCFree
	add	esp,4
_342:
	mov	dword [_bb_roofdir],ebx
	mov	edi,dword [_bb_Roof_List]
	mov	eax,edi
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+140]
	add	esp,4
	mov	dword [ebp-12],eax
	jmp	_57
_59:
	mov	eax,dword [ebp-12]
	push	_bbStringClass
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+52]
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	mov	esi,eax
	cmp	esi,_bbNullObject
	je	_57
	push	_1
	push	esi
	call	_bbStringCompare
	add	esp,8
	cmp	eax,0
	jne	_349
	jmp	_58
_349:
	push	_54
	push	esi
	call	_bbStringCompare
	add	esp,8
	cmp	eax,0
	sete	al
	movzx	eax,al
	cmp	eax,0
	jne	_350
	push	_55
	push	esi
	call	_bbStringCompare
	add	esp,8
	cmp	eax,0
	sete	al
	movzx	eax,al
_350:
	cmp	eax,0
	je	_352
	jmp	_57
_352:
	push	esi
	call	_brl_filesystem_ExtractExt
	add	esp,4
	inc	dword [eax+4]
	mov	ebx,eax
	mov	eax,dword [_bb_ext]
	dec	dword [eax+4]
	jnz	_356
	push	eax
	call	_bbGCFree
	add	esp,4
_356:
	mov	dword [_bb_ext],ebx
	mov	eax,dword [_bb_ext]
	push	_56
	push	eax
	call	_bbStringCompare
	add	esp,8
	cmp	eax,0
	je	_359
	jmp	_358
_359:
	push	2
	push	dword [_bb_roofdir]
	push	esi
	call	_brl_filesystem_StripAll
	add	esp,4
	push	eax
	call	_brl_maxgui_AddTreeViewNode
	add	esp,12
_358:
_57:
	mov	eax,dword [ebp-12]
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+48]
	add	esp,4
	cmp	eax,0
	jne	_59
_58:
	push	1
	push	dword [_bb_trvTreeView4]
	push	_34
	call	_brl_maxgui_AddTreeViewNode
	add	esp,12
	push	eax
	call	_bbHandleFromObject
	add	esp,4
	mov	dword [ebp-20],eax
	mov	edi,dword [_bb_Door_list]
	mov	eax,edi
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+140]
	add	esp,4
	mov	dword [ebp-16],eax
	jmp	_60
_62:
	mov	eax,dword [ebp-16]
	push	_bbStringClass
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+52]
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	mov	esi,eax
	cmp	esi,_bbNullObject
	je	_60
	push	_1
	push	esi
	call	_bbStringCompare
	add	esp,8
	cmp	eax,0
	jne	_366
	jmp	_61
_366:
	push	_54
	push	esi
	call	_bbStringCompare
	add	esp,8
	cmp	eax,0
	sete	al
	movzx	eax,al
	cmp	eax,0
	jne	_367
	push	_55
	push	esi
	call	_bbStringCompare
	add	esp,8
	cmp	eax,0
	sete	al
	movzx	eax,al
_367:
	cmp	eax,0
	je	_369
	jmp	_60
_369:
	push	esi
	call	_brl_filesystem_ExtractExt
	add	esp,4
	inc	dword [eax+4]
	mov	ebx,eax
	mov	eax,dword [_bb_ext]
	dec	dword [eax+4]
	jnz	_373
	push	eax
	call	_bbGCFree
	add	esp,4
_373:
	mov	dword [_bb_ext],ebx
	mov	eax,dword [_bb_ext]
	push	_56
	push	eax
	call	_bbStringCompare
	add	esp,8
	cmp	eax,0
	je	_376
	jmp	_375
_376:
	push	2
	push	_brl_maxgui_TGadget
	push	dword [ebp-20]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	push	esi
	call	_brl_filesystem_StripAll
	add	esp,4
	push	eax
	call	_brl_maxgui_AddTreeViewNode
	add	esp,12
_375:
_60:
	mov	eax,dword [ebp-16]
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+48]
	add	esp,4
	cmp	eax,0
	jne	_62
_61:
	mov	eax,dword [_bb_Window]
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+188]
	add	esp,4
	mov	dword [__sidesign_minib3d_TGlobal_width],eax
	mov	eax,dword [_bb_Window]
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+192]
	add	esp,4
	mov	dword [__sidesign_minib3d_TGlobal_height],eax
	mov	dword [__sidesign_minib3d_TGlobal_depth],32
	mov	dword [__sidesign_minib3d_TGlobal_mode],0
	mov	dword [__sidesign_minib3d_TGlobal_rate],60
	push	dword [_bb_cavCanvas0]
	call	_brl_maxgui_CanvasGraphics
	add	esp,4
	push	eax
	call	_brl_graphics_SetGraphics
	add	esp,4
	push	9
	push	_1
	call	dword [_sidesign_minib3d_TTexture+112]
	add	esp,8
	call	_glewInit
	push	2896
	call	_glEnable@4
	push	33274
	push	33272
	call	_glLightModeli@8
	push	1
	push	2897
	call	_glLightModeli@8
	push	2977
	call	_glEnable@4
	push	3089
	call	_glEnable@4
	push	2929
	call	_glEnable@4
	fld1
	sub	esp,8
	fstp	qword [esp]
	call	_glClearDepth@8
	push	515
	call	_glDepthFunc@4
	push	4354
	push	3152
	call	_glHint@8
	push	1056964608
	push	518
	call	_glAlphaFunc@8
	push	32884
	call	_glEnableClientState@4
	push	32886
	call	_glEnableClientState@4
	push	32885
	call	_glEnableClientState@4
	push	1082130432
	call	_glLineWidth@4
	push	_63
	call	_brl_filesystem_ChangeDir
	add	esp,4
	push	1124073472
	push	1124073472
	push	1124073472
	call	_sidesign_minib3d_AmbientLight
	add	esp,12
	push	_bbNullObject
	call	_sidesign_minib3d_CreateCamera
	add	esp,4
	mov	ebx,eax
	inc	dword [ebx+4]
	mov	eax,dword [_bb_camera]
	dec	dword [eax+4]
	jnz	_382
	push	eax
	call	_bbGCFree
	add	esp,4
_382:
	mov	dword [_bb_camera],ebx
	push	0
	push	-1054867456
	push	1077936128
	push	0
	push	dword [_bb_camera]
	call	_sidesign_minib3d_TranslateEntity
	add	esp,20
	push	0
	push	0
	push	0
	push	dword [_bb_camera]
	call	_sidesign_minib3d_CameraClsColor
	add	esp,16
	push	1124073472
	push	0
	push	0
	push	dword [_bb_camera]
	call	_sidesign_minib3d_CameraClsColor
	add	esp,16
	push	1165623296
	push	1065353216
	push	dword [_bb_camera]
	call	_sidesign_minib3d_CameraRange
	add	esp,12
	push	_bbNullObject
	push	1114636288
	call	_brl_timer_CreateTimer
	add	esp,8
	jmp	_64
_66:
	mov	eax,dword [_bb_Mouse]
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+52]
	add	esp,4
	call	_brl_eventqueue_WaitEvent
	call	_brl_eventqueue_EventID
	cmp	eax,8193
	je	_386
	cmp	eax,513
	je	_387
	cmp	eax,514
	je	_388
	cmp	eax,1025
	je	_389
	cmp	eax,1027
	je	_390
	cmp	eax,1026
	je	_391
	cmp	eax,16387
	je	_392
	cmp	eax,16386
	je	_393
	cmp	eax,2049
	je	_394
	cmp	eax,8194
	je	_395
	jmp	_385
_386:
	call	_brl_eventqueue_EventSource
	push	eax
	call	_bb_DoGadgetAction
	add	esp,4
	jmp	_385
_387:
	call	_brl_eventqueue_EventData
	cmp	eax,38
	je	_398
	cmp	eax,40
	je	_399
	cmp	eax,37
	je	_400
	cmp	eax,39
	je	_401
	cmp	eax,87
	je	_402
	cmp	eax,65
	je	_403
	cmp	eax,83
	je	_404
	cmp	eax,68
	je	_405
	cmp	eax,32
	je	_406
	jmp	_397
_398:
	mov	dword [ebp-44],1
	jmp	_397
_399:
	mov	dword [ebp-40],1
	jmp	_397
_400:
	mov	dword [ebp-36],1
	jmp	_397
_401:
	mov	dword [ebp-32],1
	jmp	_397
_402:
	jmp	_397
_403:
	jmp	_397
_404:
	jmp	_397
_405:
	mov	dword [ebp-52],1
	jmp	_397
_406:
	mov	dword [ebp-48],1
_397:
	jmp	_385
_388:
	call	_brl_eventqueue_EventData
	cmp	eax,38
	je	_409
	cmp	eax,40
	je	_410
	cmp	eax,37
	je	_411
	cmp	eax,39
	je	_412
	cmp	eax,87
	je	_413
	cmp	eax,65
	je	_414
	cmp	eax,83
	je	_415
	cmp	eax,68
	je	_416
	cmp	eax,32
	je	_417
	jmp	_408
_409:
	mov	dword [ebp-44],0
	jmp	_408
_410:
	mov	dword [ebp-40],0
	jmp	_408
_411:
	mov	dword [ebp-36],0
	jmp	_408
_412:
	mov	dword [ebp-32],0
	jmp	_408
_413:
	jmp	_408
_414:
	jmp	_408
_415:
	jmp	_408
_416:
	mov	dword [ebp-52],0
	jmp	_408
_417:
	mov	dword [ebp-48],0
_408:
	jmp	_385
_389:
	call	_brl_eventqueue_EventData
	cmp	eax,1
	je	_420
	cmp	eax,2
	je	_421
	jmp	_419
_420:
	mov	dword [ebp-28],1
	jmp	_419
_421:
_419:
	jmp	_385
_390:
	call	_brl_eventqueue_EventX
	mov	dword [ebp+-64],eax
	fild	dword [ebp+-64]
	fstp	dword [_bb_MouseXEndPosi]
	call	_brl_eventqueue_EventY
	mov	dword [ebp+-64],eax
	fild	dword [ebp+-64]
	fstp	dword [_bb_MouseYEndPosi]
	jmp	_385
_391:
	call	_brl_eventqueue_EventData
	cmp	eax,dword [ebp-28]
	je	_424
	jmp	_423
_424:
	mov	dword [ebp-28],0
_423:
	jmp	_385
_392:
	call	_bbEnd
	jmp	_385
_393:
	mov	eax,dword [_bb_Window]
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+188]
	add	esp,4
	mov	dword [__sidesign_minib3d_TGlobal_width],eax
	mov	eax,dword [_bb_Window]
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+192]
	add	esp,4
	mov	dword [__sidesign_minib3d_TGlobal_height],eax
	jmp	_385
_394:
	fld	dword [_585]
	fstp	dword [ebp-8]
	fld	dword [_586]
	fstp	dword [ebp-4]
	cmp	dword [ebp-44],0
	je	_430
	fld	dword [ebp-4]
	fadd	dword [_587]
	fstp	dword [ebp-4]
_430:
	cmp	dword [ebp-36],0
	je	_431
	fld	dword [ebp-8]
	fsub	dword [_588]
	fstp	dword [ebp-8]
_431:
	cmp	dword [ebp-32],0
	je	_432
	fld	dword [ebp-8]
	fadd	dword [_589]
	fstp	dword [ebp-8]
_432:
	cmp	dword [ebp-40],0
	je	_433
	fld	dword [ebp-4]
	fsub	dword [_590]
	fstp	dword [ebp-4]
_433:
	cmp	dword [ebp-52],0
	je	_434
	fld	dword [_bb_KeyX]
	fsub	dword [_591]
	fstp	dword [_bb_KeyX]
_434:
	cmp	dword [ebp-48],0
	je	_435
	push	0
	push	0
	mov	eax,dword [_bb_Mouse]
	mov	eax,dword [eax+8]
	mov	dword [ebp+-64],eax
	fild	dword [ebp+-64]
	sub	esp,4
	fstp	dword [esp]
	mov	eax,dword [_bb_Mouse]
	mov	eax,dword [eax+12]
	mov	dword [ebp+-64],eax
	fild	dword [ebp+-64]
	sub	esp,4
	fstp	dword [esp]
	push	dword [_bb_camera]
	call	_sidesign_minib3d_RotateEntity
	add	esp,20
_435:
	fld	dword [ebp-4]
	fmul	dword [_592]
	sub	esp,4
	fstp	dword [esp]
	fld	dword [ebp-24]
	fmul	dword [_593]
	sub	esp,4
	fstp	dword [esp]
	fld	dword [ebp-8]
	fmul	dword [_594]
	sub	esp,4
	fstp	dword [esp]
	push	dword [_bb_camera]
	call	_sidesign_minib3d_MoveEntity
	add	esp,16
	push	dword [_bb_cavCanvas0]
	call	_brl_maxgui_RedrawGadget
	add	esp,4
	jmp	_385
_395:
	call	_sidesign_minib3d_RenderWorld
	add	dword [ebp-56],1
	call	_bbMilliSecs
	sub	eax,dword [ebp-60]
	cmp	eax,1000
	jl	_439
	call	_bbMilliSecs
	mov	dword [ebp-60],eax
	mov	dword [ebp-56],0
_439:
	push	-1
	call	_brl_graphics_Flip
	add	esp,4
_385:
_64:
	mov	eax,1
	cmp	eax,0
	jne	_66
_65:
	mov	eax,0
_132:
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_bb_b3dSetFile:
	push	ebp
	mov	ebp,esp
	mov	eax,dword [ebp+8]
	mov	dword [_bb_b3d_tos],0
	mov	dword [_bb_b3d_file],eax
	mov	eax,0
_135:
	mov	esp,ebp
	pop	ebp
	ret
_bb_b3dReadByte:
	push	ebp
	mov	ebp,esp
	push	_brl_stream_TStream
	push	dword [_bb_b3d_file]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_brl_stream_ReadByte
	add	esp,4
_137:
	mov	esp,ebp
	pop	ebp
	ret
_bb_b3dReadInt:
	push	ebp
	mov	ebp,esp
	push	_brl_stream_TStream
	push	dword [_bb_b3d_file]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_brl_stream_ReadInt
	add	esp,4
_139:
	mov	esp,ebp
	pop	ebp
	ret
_bb_b3dReadFloat:
	push	ebp
	mov	ebp,esp
	push	_brl_stream_TStream
	push	dword [_bb_b3d_file]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_brl_stream_ReadFloat
	add	esp,4
_141:
	mov	esp,ebp
	pop	ebp
	ret
_bb_b3dReadString:
	push	ebp
	mov	ebp,esp
	push	ebx
_20:
_18:
	call	_bb_b3dReadByte
	cmp	eax,0
	jne	_444
	mov	eax,dword [_bb_t]
	jmp	_143
_444:
	push	eax
	call	_bbStringFromChar
	add	esp,4
	push	eax
	push	dword [_bb_t]
	call	_bbStringConcat
	add	esp,8
	inc	dword [eax+4]
	mov	ebx,eax
	mov	eax,dword [_bb_t]
	dec	dword [eax+4]
	jnz	_448
	push	eax
	call	_bbGCFree
	add	esp,4
_448:
	mov	dword [_bb_t],ebx
	jmp	_20
_143:
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_bb_b3dReadChunk:
	push	ebp
	mov	ebp,esp
	sub	esp,4
	push	ebx
	push	esi
	push	edi
	mov	esi,_bbEmptyString
	mov	ebx,1
	jmp	_450
_23:
	call	_bb_b3dReadByte
	push	eax
	call	_bbStringFromChar
	add	esp,4
	push	eax
	push	esi
	call	_bbStringConcat
	add	esp,8
	mov	esi,eax
_21:
	add	ebx,1
_450:
	cmp	ebx,4
	jle	_23
_22:
	push	_brl_stream_TStream
	push	dword [_bb_b3d_file]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_brl_stream_ReadInt
	add	esp,4
	mov	dword [ebp-4],eax
	add	dword [_bb_b3d_tos],1
	mov	ebx,dword [_bb_b3d_stack]
	mov	edi,dword [_bb_b3d_tos]
	push	_brl_stream_TStream
	push	dword [_bb_b3d_file]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_brl_stream_StreamPos
	add	esp,4
	add	eax,dword [ebp-4]
	mov	dword [ebx+edi*4+24],eax
	mov	eax,esi
_145:
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_bb_b3dExitChunk:
	push	ebp
	mov	ebp,esp
	mov	edx,dword [_bb_b3d_stack]
	mov	eax,dword [_bb_b3d_tos]
	push	dword [edx+eax*4+24]
	push	_brl_stream_TStream
	push	dword [_bb_b3d_file]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_brl_stream_SeekStream
	add	esp,8
	sub	dword [_bb_b3d_tos],1
	mov	eax,0
_147:
	mov	esp,ebp
	pop	ebp
	ret
_bb_b3dChunkSize:
	push	ebp
	mov	ebp,esp
	push	ebx
	mov	edx,dword [_bb_b3d_stack]
	mov	eax,dword [_bb_b3d_tos]
	mov	ebx,dword [edx+eax*4+24]
	push	_brl_stream_TStream
	push	dword [_bb_b3d_file]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_brl_stream_StreamPos
	add	esp,4
	sub	ebx,eax
	mov	eax,ebx
_149:
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_bb_b3dWriteByte:
	push	ebp
	mov	ebp,esp
	mov	eax,dword [ebp+8]
	push	eax
	push	_brl_stream_TStream
	push	dword [_bb_b3d_file]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_brl_stream_WriteByte
	add	esp,8
	mov	eax,0
_152:
	mov	esp,ebp
	pop	ebp
	ret
_bb_b3dWriteInt:
	push	ebp
	mov	ebp,esp
	mov	eax,dword [ebp+8]
	push	eax
	push	_brl_stream_TStream
	push	dword [_bb_b3d_file]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_brl_stream_WriteInt
	add	esp,8
	mov	eax,0
_155:
	mov	esp,ebp
	pop	ebp
	ret
_bb_b3dWriteFloat:
	push	ebp
	mov	ebp,esp
	fld	dword [ebp+8]
	sub	esp,4
	fstp	dword [esp]
	push	_brl_stream_TStream
	push	dword [_bb_b3d_file]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_brl_stream_WriteFloat
	add	esp,8
	mov	eax,0
_158:
	mov	esp,ebp
	pop	ebp
	ret
_bb_b3dWriteString:
	push	ebp
	mov	ebp,esp
	sub	esp,4
	push	ebx
	push	esi
	push	edi
	mov	edi,dword [ebp+8]
	mov	esi,1
	mov	eax,dword [edi+8]
	mov	dword [ebp-4],eax
	jmp	_454
_26:
	push	1
	push	esi
	push	edi
	call	_brl_retro_Mid
	add	esp,12
	push	eax
	call	_bbStringAsc
	add	esp,4
	mov	ebx,eax
	push	ebx
	call	_bb_b3dWriteByte
	add	esp,4
	cmp	ebx,0
	jne	_457
	mov	eax,0
	jmp	_161
_457:
_24:
	add	esi,1
_454:
	cmp	esi,dword [ebp-4]
	jle	_26
_25:
	push	0
	call	_bb_b3dWriteByte
	add	esp,4
	mov	eax,0
_161:
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_bb_b3dBeginChunk:
	push	ebp
	mov	ebp,esp
	push	ebx
	push	esi
	mov	esi,dword [ebp+8]
	add	dword [_bb_b3d_tos],1
	mov	ebx,1
	jmp	_459
_29:
	push	1
	push	ebx
	push	esi
	call	_brl_retro_Mid
	add	esp,12
	push	eax
	call	_bbStringAsc
	add	esp,4
	push	eax
	call	_bb_b3dWriteByte
	add	esp,4
_27:
	add	ebx,1
_459:
	cmp	ebx,4
	jle	_29
_28:
	push	0
	call	_bb_b3dWriteInt
	add	esp,4
	mov	esi,dword [_bb_b3d_stack]
	mov	ebx,dword [_bb_b3d_tos]
	push	_brl_stream_TStream
	push	dword [_bb_b3d_file]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_brl_stream_StreamPos
	add	esp,4
	mov	dword [esi+ebx*4+24],eax
	mov	eax,0
_164:
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_bb_b3dEndChunk:
	push	ebp
	mov	ebp,esp
	push	ebx
	push	_brl_stream_TStream
	push	dword [_bb_b3d_file]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_brl_stream_StreamPos
	add	esp,4
	mov	ebx,eax
	mov	edx,dword [_bb_b3d_stack]
	mov	eax,dword [_bb_b3d_tos]
	mov	eax,dword [edx+eax*4+24]
	sub	eax,4
	push	eax
	push	_brl_stream_TStream
	push	dword [_bb_b3d_file]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_brl_stream_SeekStream
	add	esp,8
	mov	ecx,ebx
	mov	edx,dword [_bb_b3d_stack]
	mov	eax,dword [_bb_b3d_tos]
	sub	ecx,dword [edx+eax*4+24]
	push	ecx
	call	_bb_b3dWriteInt
	add	esp,4
	push	ebx
	push	_brl_stream_TStream
	push	dword [_bb_b3d_file]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_brl_stream_SeekStream
	add	esp,8
	sub	dword [_bb_b3d_tos],1
	mov	eax,0
_166:
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_bb_DoGadgetAction:
	push	ebp
	mov	ebp,esp
	sub	esp,4
	push	ebx
	push	esi
	push	edi
	mov	eax,dword [ebp+8]
	cmp	eax,dword [_bb_SetWindowCnt]
	je	_463
	cmp	eax,dword [_bb_SetWindowTex]
	je	_464
	cmp	eax,dword [_bb_tabTabber0]
	je	_465
	cmp	eax,dword [_bb_trvTreeView0]
	je	_466
	cmp	eax,dword [_bb_trvTreeView1]
	je	_467
	cmp	eax,dword [_bb_trvTreeView4]
	je	_468
	cmp	eax,dword [_bb_exportmeshbutt]
	je	_469
	cmp	eax,dword [_bb_sidebutt]
	je	_470
	jmp	_462
_463:
	push	dword [_bb_cobComboBox1]
	call	_brl_maxgui_SelectedGadgetItem
	add	esp,4
	push	eax
	push	dword [_bb_cobComboBox1]
	call	_brl_maxgui_GadgetItemText
	add	esp,8
	mov	ebx,eax
	inc	dword [ebx+4]
	mov	eax,dword [_bb_WindowCnt]
	dec	dword [eax+4]
	jnz	_474
	push	eax
	call	_bbGCFree
	add	esp,4
_474:
	mov	dword [_bb_WindowCnt],ebx
	push	_67
	call	_brl_filesystem_ChangeDir
	add	esp,4
	mov	esi,1
	push	dword [_bb_WindowCnt]
	call	_bbStringToInt
	add	esp,4
	mov	edi,eax
	jmp	_476
_70:
	mov	ebx,dword [_bb_WindowsCnt]
	push	_bbNullObject
	push	_71
	call	_sidesign_minib3d_LoadMesh
	add	esp,8
	push	eax
	call	_bbHandleFromObject
	add	esp,4
	mov	dword [ebx+esi*4+24],eax
	push	0
	mov	eax,dword [_bb_WindowsZ]
	mov	eax,dword [eax+esi*4+24]
	mov	dword [ebp+-4],eax
	fild	dword [ebp+-4]
	sub	esp,4
	fstp	dword [esp]
	mov	eax,dword [_bb_WindowsY]
	mov	eax,dword [eax+esi*4+24]
	mov	dword [ebp+-4],eax
	fild	dword [ebp+-4]
	sub	esp,4
	fstp	dword [esp]
	mov	eax,dword [_bb_WindowsX]
	mov	eax,dword [eax+esi*4+24]
	mov	dword [ebp+-4],eax
	fild	dword [ebp+-4]
	sub	esp,4
	fstp	dword [esp]
	push	_sidesign_minib3d_TEntity
	mov	eax,dword [_bb_WindowsCnt]
	push	dword [eax+esi*4+24]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_sidesign_minib3d_PositionEntity
	add	esp,20
_68:
	add	esi,1
_476:
	cmp	esi,edi
	jle	_70
_69:
	jmp	_462
_464:
	push	dword [_bb_windowselected]
	call	_brl_maxgui_SelectedGadgetItem
	add	esp,4
	push	eax
	push	dword [_bb_windowselected]
	call	_brl_maxgui_GadgetItemText
	add	esp,8
	mov	ebx,eax
	inc	dword [ebx+4]
	mov	eax,dword [_bb_WindowCnt]
	dec	dword [eax+4]
	jnz	_481
	push	eax
	call	_bbGCFree
	add	esp,4
_481:
	mov	dword [_bb_WindowCnt],ebx
	push	_1
	push	0
	push	_73
	push	_72
	call	_brl_system_RequestFile
	add	esp,16
	mov	ebx,eax
	mov	esi,dword [_bb_WindowsTex]
	push	dword [_bb_WindowCnt]
	call	_bbStringToInt
	add	esp,4
	shl	eax,2
	add	esi,eax
	push	1
	push	ebx
	call	_sidesign_minib3d_LoadTexture
	add	esp,8
	push	eax
	call	_bbHandleFromObject
	add	esp,4
	mov	dword [esi+24],eax
	push	0
	push	_sidesign_minib3d_TTexture
	mov	ebx,dword [_bb_WindowsTex]
	push	dword [_bb_WindowCnt]
	call	_bbStringToInt
	add	esp,4
	push	dword [ebx+eax*4+24]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	push	_sidesign_minib3d_TMesh
	mov	ebx,dword [_bb_WindowsCnt]
	push	dword [_bb_WindowCnt]
	call	_bbStringToInt
	add	esp,4
	push	dword [ebx+eax*4+24]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_bb_TextureLoadedMesh
	add	esp,12
	jmp	_462
_465:
	push	dword [_bb_tabTabber0]
	call	_brl_maxgui_SelectedGadgetItem
	add	esp,4
	cmp	eax,0
	je	_487
	cmp	eax,1
	je	_488
	cmp	eax,2
	je	_489
	cmp	eax,3
	je	_490
	jmp	_486
_487:
	push	dword [_bb_TabPanel0]
	call	_brl_maxgui_ShowGadget
	add	esp,4
	push	dword [_bb_TabPanel1]
	call	_brl_maxgui_HideGadget
	add	esp,4
	push	dword [_bb_TabPanel2]
	call	_brl_maxgui_HideGadget
	add	esp,4
	push	dword [_bb_TabPanel3]
	call	_brl_maxgui_HideGadget
	add	esp,4
	jmp	_486
_488:
	push	dword [_bb_TabPanel0]
	call	_brl_maxgui_HideGadget
	add	esp,4
	push	dword [_bb_TabPanel1]
	call	_brl_maxgui_ShowGadget
	add	esp,4
	push	dword [_bb_TabPanel2]
	call	_brl_maxgui_HideGadget
	add	esp,4
	push	dword [_bb_TabPanel3]
	call	_brl_maxgui_HideGadget
	add	esp,4
	jmp	_486
_489:
	push	dword [_bb_TabPanel0]
	call	_brl_maxgui_HideGadget
	add	esp,4
	push	dword [_bb_TabPanel1]
	call	_brl_maxgui_HideGadget
	add	esp,4
	push	dword [_bb_TabPanel2]
	call	_brl_maxgui_ShowGadget
	add	esp,4
	push	dword [_bb_TabPanel3]
	call	_brl_maxgui_HideGadget
	add	esp,4
	jmp	_486
_490:
	push	dword [_bb_TabPanel0]
	call	_brl_maxgui_HideGadget
	add	esp,4
	push	dword [_bb_TabPanel1]
	call	_brl_maxgui_HideGadget
	add	esp,4
	push	dword [_bb_TabPanel2]
	call	_brl_maxgui_HideGadget
	add	esp,4
	push	dword [_bb_TabPanel3]
	call	_brl_maxgui_ShowGadget
	add	esp,4
_486:
	jmp	_462
_466:
	push	dword [_bb_trvTreeView0]
	call	_brl_maxgui_SelectedTreeViewNode
	add	esp,4
	mov	ebx,dword [eax+36]
	push	_67
	call	_brl_filesystem_ChangeDir
	add	esp,4
	push	_bbNullObject
	push	_75
	push	ebx
	push	_74
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_sidesign_minib3d_LoadAnimMesh
	add	esp,8
	push	eax
	call	_bbHandleFromObject
	add	esp,4
	mov	dword [_bb_BaseModel],eax
	push	_bbNullObject
	push	_75
	push	ebx
	push	_76
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_sidesign_minib3d_LoadMesh
	add	esp,8
	push	eax
	call	_bbHandleFromObject
	add	esp,4
	mov	dword [_bb_BaseModelBase],eax
	push	1
	push	_sidesign_minib3d_TEntity
	push	dword [_bb_BaseModelBase]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_sidesign_minib3d_EntityFX
	add	esp,8
	push	_77
	push	_sidesign_minib3d_TEntity
	push	dword [_bb_BaseModel]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_sidesign_minib3d_FindChild
	add	esp,8
	push	eax
	call	_bbHandleFromObject
	add	esp,4
	mov	dword [_bb_RoofPoint],eax
	push	_78
	push	_sidesign_minib3d_TEntity
	push	dword [_bb_BaseModel]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_sidesign_minib3d_FindChild
	add	esp,8
	push	eax
	call	_bbHandleFromObject
	add	esp,4
	mov	dword [_bb_DoorPoint],eax
	push	0
	push	_sidesign_minib3d_TEntity
	push	dword [_bb_RoofPoint]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_sidesign_minib3d_EntityX
	add	esp,8
	fstp	dword [_bb_RoofX]
	push	0
	push	_sidesign_minib3d_TEntity
	push	dword [_bb_RoofPoint]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_sidesign_minib3d_EntityY
	add	esp,8
	fstp	dword [_bb_RoofY]
	push	0
	push	_sidesign_minib3d_TEntity
	push	dword [_bb_RoofPoint]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_sidesign_minib3d_EntityZ
	add	esp,8
	fstp	dword [_bb_RoofZ]
	push	0
	push	_sidesign_minib3d_TEntity
	push	dword [_bb_DoorPoint]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_sidesign_minib3d_EntityX
	add	esp,8
	fstp	dword [_bb_DoorX]
	push	0
	push	_sidesign_minib3d_TEntity
	push	dword [_bb_DoorPoint]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_sidesign_minib3d_EntityY
	add	esp,8
	fstp	dword [_bb_DoorY]
	push	0
	push	_sidesign_minib3d_TEntity
	push	dword [_bb_DoorPoint]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_sidesign_minib3d_EntityZ
	add	esp,8
	fstp	dword [_bb_DoorZ]
	mov	esi,1
	jmp	_494
_81:
	mov	ebx,dword [_bb_WinChild]
	push	esi
	call	_bbStringFromInt
	add	esp,4
	push	eax
	push	_82
	call	_bbStringConcat
	add	esp,8
	push	eax
	push	_sidesign_minib3d_TEntity
	push	dword [_bb_BaseModel]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_sidesign_minib3d_FindChild
	add	esp,8
	push	eax
	call	_bbHandleFromObject
	add	esp,4
	mov	dword [ebx+esi*4+24],eax
	mov	eax,dword [_bb_WinChild]
	cmp	dword [eax+esi*4+24],0
	je	_495
	mov	ebx,dword [_bb_WindowsX]
	push	0
	push	_sidesign_minib3d_TEntity
	mov	eax,dword [_bb_WinChild]
	push	dword [eax+esi*4+24]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_sidesign_minib3d_EntityX
	add	esp,8
	sub	esp,8
	fstp	qword [esp]
	call	_bbFloatToInt
	add	esp,8
	mov	dword [ebx+esi*4+24],eax
	mov	ebx,dword [_bb_WindowsY]
	push	0
	push	_sidesign_minib3d_TEntity
	mov	eax,dword [_bb_WinChild]
	push	dword [eax+esi*4+24]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_sidesign_minib3d_EntityY
	add	esp,8
	sub	esp,8
	fstp	qword [esp]
	call	_bbFloatToInt
	add	esp,8
	mov	dword [ebx+esi*4+24],eax
	mov	ebx,dword [_bb_WindowsZ]
	push	0
	push	_sidesign_minib3d_TEntity
	mov	eax,dword [_bb_WinChild]
	push	dword [eax+esi*4+24]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_sidesign_minib3d_EntityZ
	add	esp,8
	sub	esp,8
	fstp	qword [esp]
	call	_bbFloatToInt
	add	esp,8
	mov	dword [ebx+esi*4+24],eax
	push	_sidesign_minib3d_TEntity
	mov	eax,dword [_bb_WinChild]
	push	dword [eax+esi*4+24]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_sidesign_minib3d_FreeEntity
	add	esp,4
	jmp	_496
_495:
	jmp	_80
_496:
_79:
	add	esi,1
_494:
	cmp	esi,6
	jle	_81
_80:
	mov	edi,0
	mov	esi,0
	mov	ebx,0
	mov	dword [ebp+-4],ebx
	fild	dword [ebp+-4]
	sub	esp,4
	fstp	dword [esp]
	mov	dword [ebp+-4],esi
	fild	dword [ebp+-4]
	sub	esp,4
	fstp	dword [esp]
	mov	dword [ebp+-4],edi
	fild	dword [ebp+-4]
	sub	esp,4
	fstp	dword [esp]
	push	_sidesign_minib3d_TMesh
	push	dword [_bb_BaseModel]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_sidesign_minib3d_PositionMesh
	add	esp,16
	mov	dword [ebp+-4],ebx
	fild	dword [ebp+-4]
	sub	esp,4
	fstp	dword [esp]
	mov	dword [ebp+-4],esi
	fild	dword [ebp+-4]
	sub	esp,4
	fstp	dword [esp]
	mov	dword [ebp+-4],edi
	fild	dword [ebp+-4]
	sub	esp,4
	fstp	dword [esp]
	push	_sidesign_minib3d_TMesh
	push	dword [_bb_BaseModelBase]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_sidesign_minib3d_PositionMesh
	add	esp,16
	push	_sidesign_minib3d_TEntity
	push	dword [_bb_BaseModel]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_sidesign_minib3d_FreeEntity
	add	esp,4
	push	_sidesign_minib3d_TEntity
	push	dword [_bb_RoofPoint]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_sidesign_minib3d_FreeEntity
	add	esp,4
	push	_sidesign_minib3d_TEntity
	push	dword [_bb_DoorPoint]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_sidesign_minib3d_FreeEntity
	add	esp,4
	push	_sidesign_minib3d_TEntity
	mov	eax,dword [_bb_WinChild]
	push	dword [eax+4+24]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_sidesign_minib3d_FreeEntity
	add	esp,4
	push	_83
	call	_brl_filesystem_ChangeDir
	add	esp,4
	push	_76
	call	_brl_filesystem_CurrentDir
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	push	0
	push	_73
	push	_72
	call	_brl_system_RequestFile
	add	esp,16
	mov	ebx,eax
	push	1
	push	ebx
	call	_sidesign_minib3d_LoadTexture
	add	esp,8
	push	eax
	call	_bbHandleFromObject
	add	esp,4
	push	0
	push	_sidesign_minib3d_TTexture
	push	eax
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	push	_sidesign_minib3d_TMesh
	push	dword [_bb_BaseModelBase]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_bb_TextureLoadedMesh
	add	esp,12
	jmp	_462
_467:
	push	dword [_bb_trvTreeView1]
	call	_brl_maxgui_SelectedTreeViewNode
	add	esp,4
	mov	ebx,dword [eax+36]
	push	_67
	call	_brl_filesystem_ChangeDir
	add	esp,4
	push	_bbNullObject
	push	_75
	push	ebx
	push	_84
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_sidesign_minib3d_LoadMesh
	add	esp,8
	push	eax
	call	_bbHandleFromObject
	add	esp,4
	mov	dword [_bb_RoofModel],eax
	push	1
	push	_sidesign_minib3d_TEntity
	push	dword [_bb_RoofModel]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_sidesign_minib3d_EntityFX
	add	esp,8
	fld	dword [_bb_RoofX]
	sub	esp,8
	fstp	qword [esp]
	call	_bbFloatToInt
	add	esp,8
	fld	dword [_bb_RoofY]
	sub	esp,8
	fstp	qword [esp]
	call	_bbFloatToInt
	add	esp,8
	fld	dword [_bb_RoofZ]
	sub	esp,8
	fstp	qword [esp]
	call	_bbFloatToInt
	add	esp,8
	push	dword [_bb_RoofZ]
	push	dword [_bb_RoofY]
	push	dword [_bb_RoofX]
	push	_sidesign_minib3d_TMesh
	push	dword [_bb_RoofModel]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_sidesign_minib3d_PositionMesh
	add	esp,16
	push	_1
	push	0
	push	_73
	push	_72
	call	_brl_system_RequestFile
	add	esp,16
	mov	ebx,eax
	push	1
	push	ebx
	call	_sidesign_minib3d_LoadTexture
	add	esp,8
	push	eax
	call	_bbHandleFromObject
	add	esp,4
	push	0
	push	_sidesign_minib3d_TTexture
	push	eax
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	push	_sidesign_minib3d_TMesh
	push	dword [_bb_RoofModel]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_bb_TextureLoadedMesh
	add	esp,12
	jmp	_462
_468:
	push	dword [_bb_trvTreeView4]
	call	_brl_maxgui_SelectedTreeViewNode
	add	esp,4
	mov	ebx,dword [eax+36]
	push	_67
	call	_brl_filesystem_ChangeDir
	add	esp,4
	push	_bbNullObject
	push	_75
	push	ebx
	push	_85
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_sidesign_minib3d_LoadMesh
	add	esp,8
	push	eax
	call	_bbHandleFromObject
	add	esp,4
	mov	dword [_bb_DoorModel],eax
	fld	dword [_bb_DoorX]
	sub	esp,8
	fstp	qword [esp]
	call	_bbFloatToInt
	add	esp,8
	fld	dword [_bb_DoorY]
	sub	esp,8
	fstp	qword [esp]
	call	_bbFloatToInt
	add	esp,8
	fld	dword [_bb_DoorZ]
	sub	esp,8
	fstp	qword [esp]
	call	_bbFloatToInt
	add	esp,8
	push	0
	push	dword [_bb_DoorZ]
	push	dword [_bb_DoorY]
	push	dword [_bb_DoorX]
	push	_sidesign_minib3d_TEntity
	push	dword [_bb_DoorModel]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_sidesign_minib3d_PositionEntity
	add	esp,20
	push	_1
	push	0
	push	_73
	push	_72
	call	_brl_system_RequestFile
	add	esp,16
	mov	ebx,eax
	push	1
	push	ebx
	call	_sidesign_minib3d_LoadTexture
	add	esp,8
	push	eax
	call	_bbHandleFromObject
	add	esp,4
	push	0
	push	_sidesign_minib3d_TTexture
	push	eax
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	push	_sidesign_minib3d_TMesh
	push	dword [_bb_DoorModel]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_bb_TextureLoadedMesh
	add	esp,12
	jmp	_462
_469:
	mov	esi,1
	jmp	_510
_88:
	push	_sidesign_minib3d_TMesh
	mov	eax,dword [_bb_WindowsCnt]
	push	dword [eax+esi*4+24]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	push	_sidesign_minib3d_TMesh
	push	dword [_bb_BaseModelBase]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_sidesign_minib3d_AddMesh
	add	esp,8
	push	_sidesign_minib3d_TEntity
	mov	edx,dword [_bb_WindowsCnt]
	mov	eax,esi
	sub	eax,1
	push	dword [edx+eax*4+24]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_sidesign_minib3d_FreeEntity
	add	esp,4
	push	_sidesign_minib3d_TEntity
	push	dword [_bb_BaseModelBase]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_sidesign_minib3d_FreeEntity
	add	esp,4
_86:
	add	esi,1
_510:
	cmp	esi,6
	jle	_88
_87:
	push	_sidesign_minib3d_TMesh
	push	dword [_bb_DoorModel]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	push	_sidesign_minib3d_TMesh
	mov	eax,dword [_bb_WindowsCnt]
	push	dword [eax+24+24]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_sidesign_minib3d_AddMesh
	add	esp,8
	push	_sidesign_minib3d_TMesh
	push	dword [_bb_DoorModel]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	push	_sidesign_minib3d_TMesh
	push	dword [_bb_RoofModel]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_sidesign_minib3d_AddMesh
	add	esp,8
	push	_sidesign_minib3d_TEntity
	mov	eax,dword [_bb_WindowsCnt]
	push	dword [eax+24+24]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_sidesign_minib3d_FreeEntity
	add	esp,4
	push	_sidesign_minib3d_TEntity
	push	dword [_bb_RoofModel]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_sidesign_minib3d_FreeEntity
	add	esp,4
	jmp	_462
_470:
	push	dword [_bb_cobComboBox1]
	call	_brl_maxgui_SelectedGadgetItem
	add	esp,4
	push	eax
	push	dword [_bb_cobComboBox1]
	call	_brl_maxgui_GadgetItemText
	add	esp,8
	push	0
	push	0
	push	1119092736
	push	0
	push	_sidesign_minib3d_TEntity
	mov	ebx,dword [_bb_WindowsCnt]
	push	eax
	call	_bbStringToInt
	add	esp,4
	push	dword [ebx+eax*4+24]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_sidesign_minib3d_TurnEntity
	add	esp,20
_462:
	mov	eax,0
_169:
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_bb_enumFilesCallback:
	push	ebp
	mov	ebp,esp
	sub	esp,4
	push	ebx
	push	esi
	push	edi
	mov	edi,dword [ebp+8]
	push	dword [ebp+12]
	call	_brl_filesystem_ReadDir
	add	esp,4
	mov	dword [ebp-4],eax
_91:
	push	dword [ebp-4]
	call	_brl_filesystem_NextFile
	add	esp,4
	mov	esi,eax
	mov	eax,dword [esi+8]
	cmp	eax,0
	je	_514
	push	_54
	push	esi
	call	_bbStringCompare
	add	esp,8
	cmp	eax,0
	setne	al
	movzx	eax,al
_514:
	cmp	eax,0
	je	_516
	push	_55
	push	esi
	call	_bbStringCompare
	add	esp,8
	cmp	eax,0
	setne	al
	movzx	eax,al
_516:
	cmp	eax,0
	je	_518
	push	esi
	push	_92
	push	dword [ebp+12]
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_brl_filesystem_RealPath
	add	esp,4
	mov	ebx,eax
	push	ebx
	call	_brl_filesystem_FileType
	add	esp,4
	cmp	eax,2
	jne	_520
	push	ebx
	push	edi
	call	_bb_enumFilesCallback
	add	esp,8
	jmp	_521
_520:
	push	ebx
	call	edi
	add	esp,4
_521:
_518:
_89:
	push	_bbEmptyString
	push	esi
	call	_bbStringCompare
	add	esp,8
	cmp	eax,0
	jne	_91
_90:
	push	dword [ebp-4]
	call	_brl_filesystem_CloseDir
	add	esp,4
	mov	eax,0
_173:
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_bb_enumFoldersCallback:
	push	ebp
	mov	ebp,esp
	sub	esp,4
	push	ebx
	push	esi
	push	edi
	mov	edi,dword [ebp+8]
	push	dword [ebp+12]
	call	_brl_filesystem_ReadDir
	add	esp,4
	mov	dword [ebp-4],eax
_95:
	push	dword [ebp-4]
	call	_brl_filesystem_NextFile
	add	esp,4
	mov	esi,eax
	mov	eax,dword [esi+8]
	cmp	eax,0
	je	_524
	push	_54
	push	esi
	call	_bbStringCompare
	add	esp,8
	cmp	eax,0
	setne	al
	movzx	eax,al
_524:
	cmp	eax,0
	je	_526
	push	_55
	push	esi
	call	_bbStringCompare
	add	esp,8
	cmp	eax,0
	setne	al
	movzx	eax,al
_526:
	cmp	eax,0
	je	_528
	push	esi
	push	_92
	push	dword [ebp+12]
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_brl_filesystem_RealPath
	add	esp,4
	mov	ebx,eax
	push	ebx
	call	_brl_filesystem_FileType
	add	esp,4
	cmp	eax,2
	jne	_530
	push	ebx
	call	edi
	add	esp,4
	push	ebx
	push	edi
	call	_bb_enumFoldersCallback
	add	esp,8
_530:
_528:
_93:
	push	_bbEmptyString
	push	esi
	call	_bbStringCompare
	add	esp,8
	cmp	eax,0
	jne	_95
_94:
	push	dword [ebp-4]
	call	_brl_filesystem_CloseDir
	add	esp,4
	mov	eax,0
_177:
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_bb_enumFolders:
	push	ebp
	mov	ebp,esp
	sub	esp,4
	push	ebx
	push	esi
	push	edi
	mov	edi,dword [ebp+8]
	push	dword [ebp+12]
	call	_brl_filesystem_ReadDir
	add	esp,4
	mov	dword [ebp-4],eax
_98:
	push	dword [ebp-4]
	call	_brl_filesystem_NextFile
	add	esp,4
	mov	esi,eax
	mov	eax,dword [esi+8]
	cmp	eax,0
	je	_533
	push	_54
	push	esi
	call	_bbStringCompare
	add	esp,8
	cmp	eax,0
	setne	al
	movzx	eax,al
_533:
	cmp	eax,0
	je	_535
	push	_55
	push	esi
	call	_bbStringCompare
	add	esp,8
	cmp	eax,0
	setne	al
	movzx	eax,al
_535:
	cmp	eax,0
	je	_537
	push	esi
	push	_92
	push	dword [ebp+12]
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_brl_filesystem_RealPath
	add	esp,4
	mov	ebx,eax
	push	ebx
	call	_brl_filesystem_FileType
	add	esp,4
	cmp	eax,2
	jne	_539
	mov	eax,edi
	push	ebx
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+68]
	add	esp,8
	push	ebx
	push	edi
	call	_bb_enumFolders
	add	esp,8
_539:
_537:
_96:
	push	_bbEmptyString
	push	esi
	call	_bbStringCompare
	add	esp,8
	cmp	eax,0
	jne	_98
_97:
	push	dword [ebp-4]
	call	_brl_filesystem_CloseDir
	add	esp,4
	mov	eax,0
_181:
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_bb_enumFiles:
	push	ebp
	mov	ebp,esp
	sub	esp,4
	push	ebx
	push	esi
	push	edi
	mov	edi,dword [ebp+8]
	push	dword [ebp+12]
	call	_brl_filesystem_ReadDir
	add	esp,4
	mov	dword [ebp-4],eax
_101:
	push	dword [ebp-4]
	call	_brl_filesystem_NextFile
	add	esp,4
	mov	esi,eax
	push	_54
	push	esi
	call	_bbStringCompare
	add	esp,8
	cmp	eax,0
	setne	al
	movzx	eax,al
	cmp	eax,0
	je	_543
	push	_55
	push	esi
	call	_bbStringCompare
	add	esp,8
	cmp	eax,0
	setne	al
	movzx	eax,al
_543:
	cmp	eax,0
	je	_545
	mov	eax,dword [esi+8]
_545:
	cmp	eax,0
	je	_547
	push	esi
	push	_92
	push	dword [ebp+12]
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_brl_filesystem_RealPath
	add	esp,4
	mov	ebx,eax
	push	ebx
	call	_brl_filesystem_FileType
	add	esp,4
	cmp	eax,2
	jne	_549
	push	ebx
	push	edi
	call	_bb_enumFiles
	add	esp,8
	jmp	_550
_549:
	mov	eax,edi
	push	ebx
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+68]
	add	esp,8
_550:
_547:
_99:
	push	_bbEmptyString
	push	esi
	call	_bbStringCompare
	add	esp,8
	cmp	eax,0
	jne	_101
_100:
	push	dword [ebp-4]
	call	_brl_filesystem_CloseDir
	add	esp,4
	mov	eax,0
_185:
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_bb_MakeSkyBox:
	push	ebp
	mov	ebp,esp
	push	ebx
	push	esi
	push	edi
	push	_bbNullObject
	call	_sidesign_minib3d_CreateMesh
	add	esp,4
	push	eax
	call	_bbHandleFromObject
	add	esp,4
	mov	edi,eax
	push	1065353216
	push	1065353216
	push	49
	push	_102
	push	dword [ebp+8]
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_sidesign_minib3d_LoadBrush
	add	esp,16
	push	eax
	call	_bbHandleFromObject
	add	esp,4
	mov	esi,eax
	push	_bbNullObject
	push	_sidesign_minib3d_TMesh
	push	edi
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_sidesign_minib3d_CreateSurface
	add	esp,8
	push	eax
	call	_bbHandleFromObject
	add	esp,4
	mov	ebx,eax
	push	0
	push	0
	push	0
	push	-1082130432
	push	1065353216
	push	-1082130432
	push	_sidesign_minib3d_TSurface
	push	ebx
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_sidesign_minib3d_AddVertex
	add	esp,28
	push	0
	push	1065353216
	push	1065353216
	push	-1082130432
	push	-1082130432
	push	1065353216
	push	_sidesign_minib3d_TSurface
	push	ebx
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_sidesign_minib3d_AddVertex
	add	esp,28
	push	2
	push	1
	push	0
	push	_sidesign_minib3d_TSurface
	push	ebx
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_sidesign_minib3d_AddTriangle
	add	esp,16
	push	_sidesign_minib3d_TBrush
	push	esi
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	push	_sidesign_minib3d_TSurface
	push	ebx
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_sidesign_minib3d_PaintSurface
	add	esp,8
	push	1065353216
	push	1065353216
	push	49
	push	_103
	push	dword [ebp+8]
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_sidesign_minib3d_LoadBrush
	add	esp,16
	push	eax
	call	_bbHandleFromObject
	add	esp,4
	mov	esi,eax
	push	_bbNullObject
	push	_sidesign_minib3d_TMesh
	push	edi
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_sidesign_minib3d_CreateSurface
	add	esp,8
	push	eax
	call	_bbHandleFromObject
	add	esp,4
	mov	ebx,eax
	push	0
	push	0
	push	0
	push	-1082130432
	push	1065353216
	push	1065353216
	push	_sidesign_minib3d_TSurface
	push	ebx
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_sidesign_minib3d_AddVertex
	add	esp,28
	push	0
	push	1065353216
	push	1065353216
	push	1065353216
	push	-1082130432
	push	1065353216
	push	_sidesign_minib3d_TSurface
	push	ebx
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_sidesign_minib3d_AddVertex
	add	esp,28
	push	2
	push	1
	push	0
	push	_sidesign_minib3d_TSurface
	push	ebx
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_sidesign_minib3d_AddTriangle
	add	esp,16
	push	_sidesign_minib3d_TBrush
	push	esi
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	push	_sidesign_minib3d_TSurface
	push	ebx
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_sidesign_minib3d_PaintSurface
	add	esp,8
	push	1065353216
	push	1065353216
	push	49
	push	_104
	push	dword [ebp+8]
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_sidesign_minib3d_LoadBrush
	add	esp,16
	push	eax
	call	_bbHandleFromObject
	add	esp,4
	mov	esi,eax
	push	_bbNullObject
	push	_sidesign_minib3d_TMesh
	push	edi
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_sidesign_minib3d_CreateSurface
	add	esp,8
	push	eax
	call	_bbHandleFromObject
	add	esp,4
	mov	ebx,eax
	push	0
	push	0
	push	0
	push	1065353216
	push	1065353216
	push	1065353216
	push	_sidesign_minib3d_TSurface
	push	ebx
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_sidesign_minib3d_AddVertex
	add	esp,28
	push	0
	push	1065353216
	push	1065353216
	push	1065353216
	push	-1082130432
	push	-1082130432
	push	_sidesign_minib3d_TSurface
	push	ebx
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_sidesign_minib3d_AddVertex
	add	esp,28
	push	2
	push	1
	push	0
	push	_sidesign_minib3d_TSurface
	push	ebx
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_sidesign_minib3d_AddTriangle
	add	esp,16
	push	_sidesign_minib3d_TBrush
	push	esi
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	push	_sidesign_minib3d_TSurface
	push	ebx
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_sidesign_minib3d_PaintSurface
	add	esp,8
	push	1065353216
	push	1065353216
	push	49
	push	_105
	push	dword [ebp+8]
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_sidesign_minib3d_LoadBrush
	add	esp,16
	push	eax
	call	_bbHandleFromObject
	add	esp,4
	mov	esi,eax
	push	_bbNullObject
	push	_sidesign_minib3d_TMesh
	push	edi
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_sidesign_minib3d_CreateSurface
	add	esp,8
	push	eax
	call	_bbHandleFromObject
	add	esp,4
	mov	ebx,eax
	push	0
	push	0
	push	0
	push	1065353216
	push	1065353216
	push	-1082130432
	push	_sidesign_minib3d_TSurface
	push	ebx
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_sidesign_minib3d_AddVertex
	add	esp,28
	push	0
	push	1065353216
	push	1065353216
	push	-1082130432
	push	-1082130432
	push	-1082130432
	push	_sidesign_minib3d_TSurface
	push	ebx
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_sidesign_minib3d_AddVertex
	add	esp,28
	push	2
	push	1
	push	0
	push	_sidesign_minib3d_TSurface
	push	ebx
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_sidesign_minib3d_AddTriangle
	add	esp,16
	push	_sidesign_minib3d_TBrush
	push	esi
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	push	_sidesign_minib3d_TSurface
	push	ebx
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_sidesign_minib3d_PaintSurface
	add	esp,8
	push	1065353216
	push	1065353216
	push	49
	push	_106
	push	dword [ebp+8]
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_sidesign_minib3d_LoadBrush
	add	esp,16
	push	eax
	call	_bbHandleFromObject
	add	esp,4
	mov	esi,eax
	push	_bbNullObject
	push	_sidesign_minib3d_TMesh
	push	edi
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_sidesign_minib3d_CreateSurface
	add	esp,8
	push	eax
	call	_bbHandleFromObject
	add	esp,4
	mov	ebx,eax
	push	0
	push	1065353216
	push	0
	push	1065353216
	push	1065353216
	push	-1082130432
	push	_sidesign_minib3d_TSurface
	push	ebx
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_sidesign_minib3d_AddVertex
	add	esp,28
	push	0
	push	0
	push	1065353216
	push	-1082130432
	push	1065353216
	push	1065353216
	push	_sidesign_minib3d_TSurface
	push	ebx
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_sidesign_minib3d_AddVertex
	add	esp,28
	push	2
	push	1
	push	0
	push	_sidesign_minib3d_TSurface
	push	ebx
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_sidesign_minib3d_AddTriangle
	add	esp,16
	push	_sidesign_minib3d_TBrush
	push	esi
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	push	_sidesign_minib3d_TSurface
	push	ebx
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_sidesign_minib3d_PaintSurface
	add	esp,8
	push	1154777088
	push	1154777088
	push	1154777088
	push	_sidesign_minib3d_TMesh
	push	edi
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_sidesign_minib3d_ScaleMesh
	add	esp,16
	push	_sidesign_minib3d_TMesh
	push	edi
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_sidesign_minib3d_FlipMesh
	add	esp,4
	push	1
	push	_sidesign_minib3d_TEntity
	push	edi
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_sidesign_minib3d_EntityFX
	add	esp,8
	mov	eax,edi
_188:
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_TMouse_New:
	push	ebp
	mov	ebp,esp
	push	ebx
	mov	ebx,dword [ebp+8]
	push	ebx
	call	_bbObjectCtor
	add	esp,4
	mov	dword [ebx],_bb_TMouse
	mov	dword [ebx+8],0
	mov	dword [ebx+12],0
	mov	dword [ebx+16],0
	push	3
	push	_555
	call	_bbArrayNew1D
	add	esp,8
	inc	dword [eax+4]
	mov	dword [ebx+20],eax
	mov	eax,0
_191:
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_TMouse_Delete:
	push	ebp
	mov	ebp,esp
	push	ebx
	mov	ebx,dword [ebp+8]
_194:
	mov	eax,dword [ebx+20]
	dec	dword [eax+4]
	jnz	_559
	push	eax
	call	_bbGCFree
	add	esp,4
_559:
	mov	dword [ebx],_bbObjectClass
	push	ebx
	call	_bbObjectDtor
	add	esp,4
	mov	eax,0
_557:
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_TMouse_Init:
	push	ebp
	mov	ebp,esp
	push	_bb_TMouse
	call	_bbObjectNew
	add	esp,4
_196:
	mov	esp,ebp
	pop	ebp
	ret
__bb_TMouse_Update:
	push	ebp
	mov	ebp,esp
	mov	edx,dword [ebp+8]
	mov	eax,dword [_brl_eventqueue_CurrentEvent]
	mov	ecx,dword [eax+8]
	cmp	ecx,1027
	je	_563
	cmp	ecx,1028
	je	_564
	cmp	ecx,1025
	je	_565
	cmp	ecx,1026
	je	_566
	jmp	_562
_563:
	mov	ecx,dword [eax+24]
	mov	dword [edx+8],ecx
	mov	eax,dword [eax+28]
	mov	dword [edx+12],eax
	jmp	_562
_564:
	mov	eax,dword [eax+16]
	add	dword [edx+16],eax
	jmp	_562
_565:
	mov	edx,dword [edx+20]
	mov	eax,dword [eax+16]
	sub	eax,1
	mov	byte [edx+eax+24],1
	jmp	_562
_566:
	mov	edx,dword [edx+20]
	mov	eax,dword [eax+16]
	sub	eax,1
	mov	byte [edx+eax+24],0
_562:
	mov	eax,0
_199:
	mov	esp,ebp
	pop	ebp
	ret
__bb_TMouse_getX:
	push	ebp
	mov	ebp,esp
	mov	eax,dword [ebp+8]
	mov	eax,dword [eax+8]
_202:
	mov	esp,ebp
	pop	ebp
	ret
__bb_TMouse_getY:
	push	ebp
	mov	ebp,esp
	mov	eax,dword [ebp+8]
	mov	eax,dword [eax+12]
_205:
	mov	esp,ebp
	pop	ebp
	ret
__bb_TMouse_getZ:
	push	ebp
	mov	ebp,esp
	mov	eax,dword [ebp+8]
	mov	eax,dword [eax+16]
_208:
	mov	esp,ebp
	pop	ebp
	ret
__bb_TMouse_GetButton:
	push	ebp
	mov	ebp,esp
	sub	esp,4
	mov	ecx,dword [ebp+8]
	mov	edx,dword [ebp+12]
	cmp	edx,0
	setge	al
	movzx	eax,al
	cmp	eax,0
	je	_567
	cmp	edx,2
	setle	al
	movzx	eax,al
_567:
	cmp	eax,0
	je	_569
	mov	eax,dword [ecx+20]
	movzx	eax,byte [eax+edx+24]
	mov	eax,eax
	mov	byte [ebp-4],al
	jmp	_212
_569:
	mov	byte [ebp-4],0
_212:
	movzx	eax,byte [ebp-4]
	mov	esp,ebp
	pop	ebp
	ret
_bb_TextureLoadedMesh:
	push	ebp
	mov	ebp,esp
	push	ebx
	push	esi
	push	edi
	mov	eax,dword [ebp+8]
	mov	edi,dword [ebp+16]
	mov	esi,dword [eax+264]
	mov	eax,esi
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+140]
	add	esp,4
	mov	ebx,eax
	jmp	_107
_109:
	mov	eax,ebx
	push	_sidesign_minib3d_TSurface
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+52]
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	cmp	eax,_bbNullObject
	je	_107
	push	0
	push	edi
	push	dword [ebp+12]
	push	dword [eax+72]
	call	_sidesign_minib3d_BrushTexture
	add	esp,16
_107:
	mov	eax,ebx
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+48]
	add	esp,4
	cmp	eax,0
	jne	_109
_108:
	mov	eax,0
_217:
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_bb_invsLoadedMesh:
	push	ebp
	mov	ebp,esp
	push	ebx
	push	esi
	mov	eax,dword [ebp+8]
	mov	esi,dword [eax+264]
	push	esi
	mov	eax,dword [esi]
	call	dword [eax+140]
	add	esp,4
	mov	ebx,eax
	jmp	_110
_112:
	push	_sidesign_minib3d_TSurface
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+52]
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	cmp	eax,_bbNullObject
	je	_110
	push	0
	push	dword [eax+72]
	call	_sidesign_minib3d_BrushAlpha
	add	esp,8
_110:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+48]
	add	esp,4
	cmp	eax,0
	jne	_112
_111:
	mov	eax,0
_220:
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
	section	"data" data writeable align 8
	align	4
_441:
	dd	0
	align	4
_584:
	dd	0x0
_114:
	db	"TMouse",0
_115:
	db	"x",0
_116:
	db	"i",0
_117:
	db	"y",0
_118:
	db	"z",0
_119:
	db	"Button",0
_120:
	db	"[]b",0
_121:
	db	"New",0
_122:
	db	"()i",0
_123:
	db	"Delete",0
_124:
	db	"Init",0
_125:
	db	"():TMouse",0
_126:
	db	"Update",0
_127:
	db	"getX",0
_128:
	db	"getY",0
_129:
	db	"getZ",0
_130:
	db	"GetButton",0
_131:
	db	"(i)b",0
	align	4
_113:
	dd	2
	dd	_114
	dd	3
	dd	_115
	dd	_116
	dd	8
	dd	3
	dd	_117
	dd	_116
	dd	12
	dd	3
	dd	_118
	dd	_116
	dd	16
	dd	3
	dd	_119
	dd	_120
	dd	20
	dd	6
	dd	_121
	dd	_122
	dd	16
	dd	6
	dd	_123
	dd	_122
	dd	20
	dd	7
	dd	_124
	dd	_125
	dd	48
	dd	6
	dd	_126
	dd	_122
	dd	52
	dd	6
	dd	_127
	dd	_122
	dd	56
	dd	6
	dd	_128
	dd	_122
	dd	60
	dd	6
	dd	_129
	dd	_122
	dd	64
	dd	6
	dd	_130
	dd	_131
	dd	68
	dd	0
	align	4
_bb_TMouse:
	dd	_bbObjectClass
	dd	_bbObjectFree
	dd	_113
	dd	24
	dd	__bb_TMouse_New
	dd	__bb_TMouse_Delete
	dd	_bbObjectToString
	dd	_bbObjectCompare
	dd	_bbObjectSendMessage
	dd	_bbObjectReserved
	dd	_bbObjectReserved
	dd	_bbObjectReserved
	dd	__bb_TMouse_Init
	dd	__bb_TMouse_Update
	dd	__bb_TMouse_getX
	dd	__bb_TMouse_getY
	dd	__bb_TMouse_getZ
	dd	__bb_TMouse_GetButton
	align	4
_224:
	dd	0
_222:
	db	"i",0
	align	4
_bb_b3d_stack:
	dd	_bbEmptyArray
	align	4
_bb_b3d_file:
	dd	0
	align	4
_bb_b3d_tos:
	dd	0
	align	4
_bb_Mouse:
	dd	_bbNullObject
	align	4
_bb_BaseModel:
	dd	0
	align	4
_bb_RoofModel:
	dd	0
	align	4
_bb_DoorModel:
	dd	0
	align	4
_bb_BaseModelBase:
	dd	0
_228:
	db	"i",0
	align	4
_bb_add:
	dd	_bbEmptyArray
	align	4
_bb_camera:
	dd	_bbNullObject
	align	4
_bb_skydome:
	dd	0
	align	4
_bb_mesh:
	dd	0
	align	4
_bb_texture:
	dd	0
_bb_res:
	db	0
	align	4
_bb_Terrain:
	dd	0
	align	4
_bb_RoofX:
	dd	0x0
	align	4
_bb_RoofY:
	dd	0x0
	align	4
_bb_RoofZ:
	dd	0x0
	align	4
_bb_DoorX:
	dd	0x0
	align	4
_bb_DoorY:
	dd	0x0
	align	4
_bb_DoorZ:
	dd	0x0
_241:
	db	"i",0
	align	4
_bb_WindowsTex:
	dd	_bbEmptyArray
_244:
	db	"i",0
	align	4
_bb_WindowsCnt:
	dd	_bbEmptyArray
_247:
	db	"i",0
	align	4
_bb_WindowsX:
	dd	_bbEmptyArray
_250:
	db	"i",0
	align	4
_bb_WindowsY:
	dd	_bbEmptyArray
_253:
	db	"i",0
	align	4
_bb_WindowsZ:
	dd	_bbEmptyArray
_256:
	db	"i",0
	align	4
_bb_WinChild:
	dd	_bbEmptyArray
	align	4
_bb_c_surfs:
	dd	0
	align	4
_bb_WindowCnt:
	dd	_bbEmptyString
	align	4
_bb_BaseMesh:
	dd	0
	align	4
_bb_KeyX:
	dd	0x0
	align	4
_bb_KeyY:
	dd	0x0
	align	4
_bb_KeyZ:
	dd	0x0
	align	4
_bb_nodeselected:
	dd	0
	align	4
_bb_RoofPoint:
	dd	0
	align	4
_bb_MouseXEndPosi:
	dd	0x0
	align	4
_bb_MouseYEndPosi:
	dd	0x0
	align	4
_30:
	dd	_bbStringClass
	dd	2147483647
	dd	13
	dw	66,117,105,108,100,105,110,103,77,97,107,101,114
	align	4
_bb_Window:
	dd	_bbNullObject
	align	4
_bb_tabTabber0:
	dd	_bbNullObject
	align	4
_1:
	dd	_bbStringClass
	dd	2147483647
	dd	0
	align	4
_31:
	dd	_bbStringClass
	dd	2147483647
	dd	4
	dw	66,97,115,101
	align	4
_32:
	dd	_bbStringClass
	dd	2147483647
	dd	5
	dw	82,111,111,102,115
	align	4
_33:
	dd	_bbStringClass
	dd	2147483647
	dd	7
	dw	87,105,110,100,111,119,115
	align	4
_34:
	dd	_bbStringClass
	dd	2147483647
	dd	5
	dw	68,111,111,114,115
	align	4
_bb_TabPanel0:
	dd	_bbNullObject
	align	4
_bb_trvTreeView0:
	dd	_bbNullObject
	align	4
_bb_TabPanel1:
	dd	_bbNullObject
	align	4
_bb_trvTreeView1:
	dd	_bbNullObject
	align	4
_bb_TabPanel2:
	dd	_bbNullObject
	align	4
_35:
	dd	_bbStringClass
	dd	2147483647
	dd	11
	dw	83,105,100,101,32,87,105,110,100,111,119
	align	4
_bb_sidebutt:
	dd	_bbNullObject
	align	4
_bb_cobComboBox1:
	dd	_bbNullObject
	align	4
_36:
	dd	_bbStringClass
	dd	2147483647
	dd	1
	dw	49
	align	4
_37:
	dd	_bbStringClass
	dd	2147483647
	dd	1
	dw	50
	align	4
_38:
	dd	_bbStringClass
	dd	2147483647
	dd	1
	dw	51
	align	4
_39:
	dd	_bbStringClass
	dd	2147483647
	dd	1
	dw	52
	align	4
_40:
	dd	_bbStringClass
	dd	2147483647
	dd	1
	dw	53
	align	4
_41:
	dd	_bbStringClass
	dd	2147483647
	dd	1
	dw	54
	align	4
_bb_windowselected:
	dd	_bbNullObject
	align	4
_42:
	dd	_bbStringClass
	dd	2147483647
	dd	11
	dw	83,101,116,32,84,101,120,116,117,114,101
	align	4
_bb_SetWindowTex:
	dd	_bbNullObject
	align	4
_43:
	dd	_bbStringClass
	dd	2147483647
	dd	17
	dw	83,101,116,32,87,105,110,100,111,119,32,65,109,111,117,110
	dw	116
	align	4
_bb_SetWindowCnt:
	dd	_bbNullObject
	align	4
_bb_TabPanel3:
	dd	_bbNullObject
	align	4
_bb_trvTreeView4:
	dd	_bbNullObject
	align	4
_bb_cavCanvas0:
	dd	_bbNullObject
	align	4
_44:
	dd	_bbStringClass
	dd	2147483647
	dd	15
	dw	69,120,112,111,114,116,32,66,117,105,108,100,105,110,103
	align	4
_bb_exportmeshbutt:
	dd	_bbNullObject
	align	4
_45:
	dd	_bbStringClass
	dd	2147483647
	dd	18
	dw	109,101,100,105,97,47,105,99,111,115,116,114,105,112,46,112
	dw	110,103
	align	4
_bb_hwnd:
	dd	0
	align	4
_bb_t:
	dd	_bbEmptyString
	align	4
_bb_ext:
	dd	_bbEmptyString
	align	4
_bb_basedir:
	dd	_bbNullObject
	align	4
_bb_roofdir:
	dd	_bbNullObject
_293:
	db	"i",0
	align	4
_bb_texturedir:
	dd	_bbEmptyArray
	align	4
_bb_texcount:
	dd	0
	align	4
_bb_count:
	dd	0
	align	4
_bb_filenames:
	dd	_bbEmptyString
	align	4
_bb_Model_List:
	dd	_bbNullObject
	align	4
_bb_Roof_List:
	dd	_bbNullObject
	align	4
_bb_Texture_List:
	dd	_bbNullObject
	align	4
_bb_Texturebase_list:
	dd	_bbNullObject
	align	4
_bb_Textureroof_list:
	dd	_bbNullObject
	align	4
_307:
	dd	0
	align	4
_bb_Texturebeam_list:
	dd	_bbNullObject
	align	4
_bb_Texturewindow_list:
	dd	_bbNullObject
	align	4
_bb_Door_list:
	dd	_bbNullObject
	align	4
_bb_DoorPoint:
	dd	0
	align	4
_bb_Surf_list:
	dd	_bbNullObject
	align	4
_46:
	dd	_bbStringClass
	dd	2147483647
	dd	12
	dw	77,101,115,104,101,115,47,66,97,115,101,47
	align	4
_47:
	dd	_bbStringClass
	dd	2147483647
	dd	13
	dw	77,101,115,104,101,115,47,82,111,111,102,115,47
	align	4
_48:
	dd	_bbStringClass
	dd	2147483647
	dd	13
	dw	77,101,115,104,101,115,47,68,111,111,114,115,47
	align	4
_49:
	dd	_bbStringClass
	dd	2147483647
	dd	9
	dw	84,101,120,116,117,114,101,115,47
	align	4
_50:
	dd	_bbStringClass
	dd	2147483647
	dd	5
	dw	98,97,115,101,115
	align	4
_54:
	dd	_bbStringClass
	dd	2147483647
	dd	1
	dw	46
	align	4
_55:
	dd	_bbStringClass
	dd	2147483647
	dd	2
	dw	46,46
	align	4
_56:
	dd	_bbStringClass
	dd	2147483647
	dd	3
	dw	98,51,100
	align	4
_63:
	dd	_bbStringClass
	dd	2147483647
	dd	5
	dw	109,101,100,105,97
	align	4
_585:
	dd	0x0
	align	4
_586:
	dd	0x0
	align	4
_587:
	dd	0x40a00000
	align	4
_588:
	dd	0x40a00000
	align	4
_589:
	dd	0x40a00000
	align	4
_590:
	dd	0x40a00000
	align	4
_591:
	dd	0x3dcccccd
	align	4
_592:
	dd	0x3f000000
	align	4
_593:
	dd	0x3f000000
	align	4
_594:
	dd	0x3f000000
	align	4
_67:
	dd	_bbStringClass
	dd	2147483647
	dd	10
	dw	46,46,47,77,101,115,104,101,115,47
	align	4
_71:
	dd	_bbStringClass
	dd	2147483647
	dd	19
	dw	87,105,110,100,111,119,115,47,87,105,110,100,111,119,49,46
	dw	98,51,100
	align	4
_73:
	dd	_bbStringClass
	dd	2147483647
	dd	11
	dw	98,109,112,44,112,110,103,44,106,112,103
	align	4
_72:
	dd	_bbStringClass
	dd	2147483647
	dd	17
	dw	80,105,99,107,32,66,97,115,101,32,84,101,120,116,117,114
	dw	101
	align	4
_75:
	dd	_bbStringClass
	dd	2147483647
	dd	4
	dw	46,98,51,100
	align	4
_74:
	dd	_bbStringClass
	dd	2147483647
	dd	8
	dw	66,97,115,101,101,120,116,47
	align	4
_76:
	dd	_bbStringClass
	dd	2147483647
	dd	5
	dw	66,97,115,101,47
	align	4
_77:
	dd	_bbStringClass
	dd	2147483647
	dd	4
	dw	114,111,111,102
	align	4
_78:
	dd	_bbStringClass
	dd	2147483647
	dd	4
	dw	100,111,111,114
	align	4
_82:
	dd	_bbStringClass
	dd	2147483647
	dd	6
	dw	119,105,110,100,111,119
	align	4
_83:
	dd	_bbStringClass
	dd	2147483647
	dd	12
	dw	46,46,47,84,101,120,116,117,114,101,115,47
	align	4
_84:
	dd	_bbStringClass
	dd	2147483647
	dd	6
	dw	114,111,111,102,115,47
	align	4
_85:
	dd	_bbStringClass
	dd	2147483647
	dd	6
	dw	68,111,111,114,115,47
	align	4
_92:
	dd	_bbStringClass
	dd	2147483647
	dd	1
	dw	47
	align	4
_102:
	dd	_bbStringClass
	dd	2147483647
	dd	7
	dw	95,70,82,46,98,109,112
	align	4
_103:
	dd	_bbStringClass
	dd	2147483647
	dd	7
	dw	95,76,70,46,98,109,112
	align	4
_104:
	dd	_bbStringClass
	dd	2147483647
	dd	7
	dw	95,66,75,46,98,109,112
	align	4
_105:
	dd	_bbStringClass
	dd	2147483647
	dd	7
	dw	95,82,84,46,98,109,112
	align	4
_106:
	dd	_bbStringClass
	dd	2147483647
	dd	7
	dw	95,85,80,46,98,109,112
_555:
	db	"b",0
