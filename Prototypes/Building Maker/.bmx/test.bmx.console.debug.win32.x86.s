	format	MS COFF
	extrn	___bb_appstub_appstub
	extrn	___bb_audio_audio
	extrn	___bb_blitz_blitz
	extrn	___bb_cocoamaxgui_cocoamaxgui
	extrn	___bb_d3d7max2d_d3d7max2d
	extrn	___bb_directsoundaudio_directsoundaudio
	extrn	___bb_eventqueue_eventqueue
	extrn	___bb_fltkmaxgui_fltkmaxgui
	extrn	___bb_freeaudioaudio_freeaudioaudio
	extrn	___bb_freejoy_freejoy
	extrn	___bb_freeprocess_freeprocess
	extrn	___bb_freetypefont_freetypefont
	extrn	___bb_gnet_gnet
	extrn	___bb_macos_macos
	extrn	___bb_maxgui_maxgui
	extrn	___bb_maxutil_maxutil
	extrn	___bb_minib3d_minib3d
	extrn	___bb_oggloader_oggloader
	extrn	___bb_openalaudio_openalaudio
	extrn	___bb_reflection_reflection
	extrn	___bb_tgaloader_tgaloader
	extrn	___bb_timer_timer
	extrn	___bb_wavloader_wavloader
	extrn	___bb_win32maxgui_win32maxgui
	extrn	_bbAppTitle
	extrn	_bbEnd
	extrn	_bbGCFree
	extrn	_bbHandleFromObject
	extrn	_bbHandleToObject
	extrn	_bbNullObject
	extrn	_bbObjectDowncast
	extrn	_bbOnDebugEnterScope
	extrn	_bbOnDebugEnterStm
	extrn	_bbOnDebugLeaveScope
	extrn	_bbStringClass
	extrn	_brl_glmax2d_GLMax2DDriver
	extrn	_brl_graphics_Flip
	extrn	_brl_graphics_SetGraphicsDriver
	extrn	_brl_max2d_Cls
	extrn	_brl_polledinput_KeyHit
	extrn	_sidesign_minib3d_CameraClsColor
	extrn	_sidesign_minib3d_CreateCamera
	extrn	_sidesign_minib3d_CreateLight
	extrn	_sidesign_minib3d_EntityAlpha
	extrn	_sidesign_minib3d_EntityOrder
	extrn	_sidesign_minib3d_EntityParent
	extrn	_sidesign_minib3d_Graphics3D
	extrn	_sidesign_minib3d_LoadMesh
	extrn	_sidesign_minib3d_LoadSprite
	extrn	_sidesign_minib3d_PositionEntity
	extrn	_sidesign_minib3d_RenderWorld
	extrn	_sidesign_minib3d_ScaleSprite
	extrn	_sidesign_minib3d_SpriteViewMode
	extrn	_sidesign_minib3d_TCamera
	extrn	_sidesign_minib3d_TEntity
	extrn	_sidesign_minib3d_TSprite
	extrn	_sidesign_minib3d_TranslateEntity
	extrn	_sidesign_minib3d_UpdateWorld
	public	__bb_main
	section	"code" code
__bb_main:
	push	ebp
	mov	ebp,esp
	sub	esp,16
	push	ebx
	cmp	dword [_64],0
	je	_65
	mov	eax,0
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_65:
	mov	dword [_64],1
	mov	dword [ebp-4],0
	mov	dword [ebp-8],0
	mov	dword [ebp-12],0
	mov	dword [ebp-16],0
	push	ebp
	push	_57
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	call	___bb_blitz_blitz
	call	___bb_minib3d_minib3d
	call	___bb_appstub_appstub
	call	___bb_audio_audio
	call	___bb_cocoamaxgui_cocoamaxgui
	call	___bb_d3d7max2d_d3d7max2d
	call	___bb_directsoundaudio_directsoundaudio
	call	___bb_eventqueue_eventqueue
	call	___bb_fltkmaxgui_fltkmaxgui
	call	___bb_freeaudioaudio_freeaudioaudio
	call	___bb_freetypefont_freetypefont
	call	___bb_gnet_gnet
	call	___bb_maxgui_maxgui
	call	___bb_maxutil_maxutil
	call	___bb_oggloader_oggloader
	call	___bb_openalaudio_openalaudio
	call	___bb_reflection_reflection
	call	___bb_tgaloader_tgaloader
	call	___bb_timer_timer
	call	___bb_wavloader_wavloader
	call	___bb_win32maxgui_win32maxgui
	call	___bb_freejoy_freejoy
	call	___bb_freeprocess_freeprocess
	call	___bb_macos_macos
	push	_26
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,_18
	inc	dword [ebx+4]
	mov	eax,dword [_bbAppTitle]
	dec	dword [eax+4]
	jnz	_31
	push	eax
	call	_bbGCFree
	add	esp,4
_31:
	mov	dword [_bbAppTitle],ebx
	push	_32
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	10
	call	_brl_glmax2d_GLMax2DDriver
	push	eax
	call	_brl_graphics_SetGraphicsDriver
	add	esp,8
	push	_33
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	60
	push	0
	push	2
	push	600
	push	800
	call	_sidesign_minib3d_Graphics3D
	add	esp,20
	push	_34
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_bbNullObject
	call	_sidesign_minib3d_CreateCamera
	add	esp,4
	push	eax
	call	_bbHandleFromObject
	add	esp,4
	mov	dword [ebp-4],eax
	push	_36
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	0
	push	-1041235968
	push	1092616192
	push	0
	push	_sidesign_minib3d_TEntity
	push	dword [ebp-4]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_sidesign_minib3d_TranslateEntity
	add	esp,20
	push	_37
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	0
	push	0
	push	0
	push	_sidesign_minib3d_TCamera
	push	dword [ebp-4]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_sidesign_minib3d_CameraClsColor
	add	esp,16
	push	_38
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	1132396544
	push	1132396544
	push	0
	push	_sidesign_minib3d_TCamera
	push	dword [ebp-4]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_sidesign_minib3d_CameraClsColor
	add	esp,16
	push	_39
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_bbNullObject
	push	1
	call	_sidesign_minib3d_CreateLight
	add	esp,8
	push	eax
	call	_bbHandleFromObject
	add	esp,4
	mov	dword [ebp-8],eax
	push	_41
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_bbNullObject
	push	_19
	call	_sidesign_minib3d_LoadMesh
	add	esp,8
	push	eax
	call	_bbHandleFromObject
	add	esp,4
	mov	dword [ebp-12],eax
	push	_43
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	0
	push	1112014848
	push	0
	push	0
	push	_sidesign_minib3d_TEntity
	push	dword [ebp-12]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_sidesign_minib3d_PositionEntity
	add	esp,20
	push	_44
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_bbNullObject
	push	1
	push	_20
	call	_sidesign_minib3d_LoadSprite
	add	esp,12
	push	eax
	call	_bbHandleFromObject
	add	esp,4
	mov	dword [ebp-16],eax
	push	_46
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	4
	push	_sidesign_minib3d_TSprite
	push	dword [ebp-16]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_sidesign_minib3d_SpriteViewMode
	add	esp,8
	push	_47
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	1086324736
	push	1086324736
	push	_sidesign_minib3d_TSprite
	push	dword [ebp-16]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_sidesign_minib3d_ScaleSprite
	add	esp,12
	push	_48
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	-1
	push	_sidesign_minib3d_TEntity
	push	dword [ebp-16]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_sidesign_minib3d_EntityOrder
	add	esp,8
	push	_49
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	1065353216
	push	_sidesign_minib3d_TEntity
	push	dword [ebp-16]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_sidesign_minib3d_EntityAlpha
	add	esp,8
	push	_50
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	0
	push	_sidesign_minib3d_TEntity
	push	dword [ebp-12]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	push	_sidesign_minib3d_TEntity
	push	dword [ebp-16]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_sidesign_minib3d_EntityParent
	add	esp,12
	push	_51
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	jmp	_21
_23:
	push	_52
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	call	_brl_max2d_Cls
	push	_53
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	call	_sidesign_minib3d_RenderWorld
	push	_54
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	1065353216
	call	_sidesign_minib3d_UpdateWorld
	add	esp,4
	push	_55
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	-1
	call	_brl_graphics_Flip
	add	esp,4
_21:
	push	27
	call	_brl_polledinput_KeyHit
	add	esp,4
	cmp	eax,0
	je	_23
_22:
	push	_56
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	call	_bbEnd
	mov	ebx,0
_24:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
	section	"data" data writeable align 8
	align	4
_64:
	dd	0
_58:
	db	"test",0
_59:
	db	"camera",0
_60:
	db	"i",0
_61:
	db	"light",0
_62:
	db	"terrain",0
_63:
	db	"sprite",0
	align	4
_57:
	dd	1
	dd	_58
	dd	2
	dd	_59
	dd	_60
	dd	-4
	dd	2
	dd	_61
	dd	_60
	dd	-8
	dd	2
	dd	_62
	dd	_60
	dd	-12
	dd	2
	dd	_63
	dd	_60
	dd	-16
	dd	0
_27:
	db	"C:/Documents and Settings/ckob/Desktop/coding/BuildingMaker/test.bmx",0
	align	4
_26:
	dd	_27
	dd	2
	dd	1
	align	4
_18:
	dd	_bbStringClass
	dd	2147483647
	dd	43
	dw	84,104,101,32,65,114,99,104,105,116,101,99,116,32,86,105
	dw	101,119,101,114,32,45,32,80,114,101,115,115,32,69,115,99
	dw	97,112,101,32,116,111,32,69,120,105,116
	align	4
_32:
	dd	_27
	dd	3
	dd	1
	align	4
_33:
	dd	_27
	dd	5
	dd	1
	align	4
_34:
	dd	_27
	dd	8
	dd	1
	align	4
_36:
	dd	_27
	dd	9
	dd	1
	align	4
_37:
	dd	_27
	dd	10
	dd	1
	align	4
_38:
	dd	_27
	dd	12
	dd	1
	align	4
_39:
	dd	_27
	dd	14
	dd	1
	align	4
_41:
	dd	_27
	dd	16
	dd	1
	align	4
_19:
	dd	_bbStringClass
	dd	2147483647
	dd	21
	dw	77,101,115,104,101,115,47,68,111,111,114,115,47,68,111,111
	dw	114,46,98,51,100
	align	4
_43:
	dd	_27
	dd	17
	dd	1
	align	4
_44:
	dd	_27
	dd	19
	dd	1
	align	4
_20:
	dd	_bbStringClass
	dd	2147483647
	dd	11
	dw	109,101,100,105,97,47,49,46,80,78,71
	align	4
_46:
	dd	_27
	dd	20
	dd	1
	align	4
_47:
	dd	_27
	dd	21
	dd	1
	align	4
_48:
	dd	_27
	dd	22
	dd	1
	align	4
_49:
	dd	_27
	dd	23
	dd	1
	align	4
_50:
	dd	_27
	dd	24
	dd	1
	align	4
_51:
	dd	_27
	dd	31
	dd	1
	align	4
_52:
	dd	_27
	dd	32
	dd	1
	align	4
_53:
	dd	_27
	dd	35
	dd	1
	align	4
_54:
	dd	_27
	dd	36
	dd	1
	align	4
_55:
	dd	_27
	dd	41
	dd	1
	align	4
_56:
	dd	_27
	dd	43
	dd	1
