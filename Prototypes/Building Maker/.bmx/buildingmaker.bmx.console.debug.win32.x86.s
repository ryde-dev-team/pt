	format	MS COFF
	extrn	___bb_blitz_blitz
	extrn	___bb_inc_bbtype
	extrn	___bb_inc_bbvkey
	extrn	___bb_minib3d_minib3d
	extrn	__sidesign_minib3d_TGlobal_depth
	extrn	__sidesign_minib3d_TGlobal_height
	extrn	__sidesign_minib3d_TGlobal_mode
	extrn	__sidesign_minib3d_TGlobal_rate
	extrn	__sidesign_minib3d_TGlobal_width
	extrn	_bbArrayNew1D
	extrn	_bbEmptyArray
	extrn	_bbEmptyString
	extrn	_bbEnd
	extrn	_bbFloatToInt
	extrn	_bbGCFree
	extrn	_bbHandleFromObject
	extrn	_bbHandleToObject
	extrn	_bbMilliSecs
	extrn	_bbNullObject
	extrn	_bbObjectClass
	extrn	_bbObjectCompare
	extrn	_bbObjectCtor
	extrn	_bbObjectDowncast
	extrn	_bbObjectDtor
	extrn	_bbObjectFree
	extrn	_bbObjectNew
	extrn	_bbObjectReserved
	extrn	_bbObjectSendMessage
	extrn	_bbObjectToString
	extrn	_bbOnDebugEnterScope
	extrn	_bbOnDebugEnterStm
	extrn	_bbOnDebugLeaveScope
	extrn	_bbStringAsc
	extrn	_bbStringClass
	extrn	_bbStringCompare
	extrn	_bbStringConcat
	extrn	_bbStringFromChar
	extrn	_bbStringFromInt
	extrn	_bbStringToInt
	extrn	_bb_VKEY
	extrn	_brl_blitz_ArrayBoundsError
	extrn	_brl_blitz_NullObjectError
	extrn	_brl_eventqueue_CurrentEvent
	extrn	_brl_eventqueue_EventData
	extrn	_brl_eventqueue_EventID
	extrn	_brl_eventqueue_EventSource
	extrn	_brl_eventqueue_EventX
	extrn	_brl_eventqueue_EventY
	extrn	_brl_eventqueue_WaitEvent
	extrn	_brl_filesystem_ChangeDir
	extrn	_brl_filesystem_CloseDir
	extrn	_brl_filesystem_CurrentDir
	extrn	_brl_filesystem_ExtractExt
	extrn	_brl_filesystem_FileType
	extrn	_brl_filesystem_NextFile
	extrn	_brl_filesystem_ReadDir
	extrn	_brl_filesystem_RealPath
	extrn	_brl_filesystem_StripAll
	extrn	_brl_glgraphics_GLGraphicsDriver
	extrn	_brl_graphics_Flip
	extrn	_brl_graphics_SetGraphics
	extrn	_brl_graphics_SetGraphicsDriver
	extrn	_brl_linkedlist_CreateList
	extrn	_brl_maxgui_AddGadgetItem
	extrn	_brl_maxgui_AddTreeViewNode
	extrn	_brl_maxgui_CanvasGraphics
	extrn	_brl_maxgui_ClientHeight
	extrn	_brl_maxgui_ClientWidth
	extrn	_brl_maxgui_CreateButton
	extrn	_brl_maxgui_CreateCanvas
	extrn	_brl_maxgui_CreateComboBox
	extrn	_brl_maxgui_CreatePanel
	extrn	_brl_maxgui_CreateTabber
	extrn	_brl_maxgui_CreateTreeView
	extrn	_brl_maxgui_CreateWindow
	extrn	_brl_maxgui_GadgetItemText
	extrn	_brl_maxgui_HideGadget
	extrn	_brl_maxgui_LoadIconStrip
	extrn	_brl_maxgui_QueryGadget
	extrn	_brl_maxgui_RedrawGadget
	extrn	_brl_maxgui_SelectGadgetItem
	extrn	_brl_maxgui_SelectedGadgetItem
	extrn	_brl_maxgui_SelectedTreeViewNode
	extrn	_brl_maxgui_SetGadgetIconStrip
	extrn	_brl_maxgui_SetGadgetLayout
	extrn	_brl_maxgui_ShowGadget
	extrn	_brl_maxgui_TGadget
	extrn	_brl_maxgui_TIconStrip
	extrn	_brl_retro_Mid
	extrn	_brl_stream_ReadByte
	extrn	_brl_stream_ReadFloat
	extrn	_brl_stream_ReadInt
	extrn	_brl_stream_SeekStream
	extrn	_brl_stream_StreamPos
	extrn	_brl_stream_TStream
	extrn	_brl_stream_WriteByte
	extrn	_brl_stream_WriteFloat
	extrn	_brl_stream_WriteInt
	extrn	_brl_system_RequestFile
	extrn	_brl_timer_CreateTimer
	extrn	_glAlphaFunc@8
	extrn	_glClearDepth@8
	extrn	_glDepthFunc@4
	extrn	_glEnable@4
	extrn	_glEnableClientState@4
	extrn	_glHint@8
	extrn	_glLightModeli@8
	extrn	_glLineWidth@4
	extrn	_glewInit
	extrn	_sidesign_minib3d_AddMesh
	extrn	_sidesign_minib3d_AddTriangle
	extrn	_sidesign_minib3d_AddVertex
	extrn	_sidesign_minib3d_AmbientLight
	extrn	_sidesign_minib3d_BrushAlpha
	extrn	_sidesign_minib3d_BrushTexture
	extrn	_sidesign_minib3d_CameraClsColor
	extrn	_sidesign_minib3d_CameraRange
	extrn	_sidesign_minib3d_CreateCamera
	extrn	_sidesign_minib3d_CreateMesh
	extrn	_sidesign_minib3d_CreateSurface
	extrn	_sidesign_minib3d_EntityFX
	extrn	_sidesign_minib3d_EntityX
	extrn	_sidesign_minib3d_EntityY
	extrn	_sidesign_minib3d_EntityZ
	extrn	_sidesign_minib3d_FindChild
	extrn	_sidesign_minib3d_FlipMesh
	extrn	_sidesign_minib3d_FreeEntity
	extrn	_sidesign_minib3d_LoadAnimMesh
	extrn	_sidesign_minib3d_LoadBrush
	extrn	_sidesign_minib3d_LoadMesh
	extrn	_sidesign_minib3d_LoadTexture
	extrn	_sidesign_minib3d_MoveEntity
	extrn	_sidesign_minib3d_PaintSurface
	extrn	_sidesign_minib3d_PositionEntity
	extrn	_sidesign_minib3d_PositionMesh
	extrn	_sidesign_minib3d_RenderWorld
	extrn	_sidesign_minib3d_RotateEntity
	extrn	_sidesign_minib3d_ScaleMesh
	extrn	_sidesign_minib3d_TBrush
	extrn	_sidesign_minib3d_TEntity
	extrn	_sidesign_minib3d_TMesh
	extrn	_sidesign_minib3d_TSurface
	extrn	_sidesign_minib3d_TTexture
	extrn	_sidesign_minib3d_TranslateEntity
	extrn	_sidesign_minib3d_TurnEntity
	public	__bb_TMouse_Delete
	public	__bb_TMouse_GetButton
	public	__bb_TMouse_Init
	public	__bb_TMouse_New
	public	__bb_TMouse_Update
	public	__bb_TMouse_getX
	public	__bb_TMouse_getY
	public	__bb_TMouse_getZ
	public	__bb_main
	public	_bb_BaseMesh
	public	_bb_BaseModel
	public	_bb_BaseModelBase
	public	_bb_DoGadgetAction
	public	_bb_DoorModel
	public	_bb_DoorPoint
	public	_bb_DoorX
	public	_bb_DoorY
	public	_bb_DoorZ
	public	_bb_Door_list
	public	_bb_KeyX
	public	_bb_KeyY
	public	_bb_KeyZ
	public	_bb_MakeSkyBox
	public	_bb_Model_List
	public	_bb_Mouse
	public	_bb_MouseXEndPosi
	public	_bb_MouseYEndPosi
	public	_bb_RoofModel
	public	_bb_RoofPoint
	public	_bb_RoofX
	public	_bb_RoofY
	public	_bb_RoofZ
	public	_bb_Roof_List
	public	_bb_SetWindowCnt
	public	_bb_SetWindowTex
	public	_bb_Surf_list
	public	_bb_TMouse
	public	_bb_TabPanel0
	public	_bb_TabPanel1
	public	_bb_TabPanel2
	public	_bb_TabPanel3
	public	_bb_Terrain
	public	_bb_TextureLoadedMesh
	public	_bb_Texture_List
	public	_bb_Texturebase_list
	public	_bb_Texturebeam_list
	public	_bb_Textureroof_list
	public	_bb_Texturewindow_list
	public	_bb_WinChild
	public	_bb_Window
	public	_bb_WindowCnt
	public	_bb_WindowsCnt
	public	_bb_WindowsTex
	public	_bb_WindowsX
	public	_bb_WindowsY
	public	_bb_WindowsZ
	public	_bb_add
	public	_bb_b3dBeginChunk
	public	_bb_b3dChunkSize
	public	_bb_b3dEndChunk
	public	_bb_b3dExitChunk
	public	_bb_b3dReadByte
	public	_bb_b3dReadChunk
	public	_bb_b3dReadFloat
	public	_bb_b3dReadInt
	public	_bb_b3dReadString
	public	_bb_b3dSetFile
	public	_bb_b3dWriteByte
	public	_bb_b3dWriteFloat
	public	_bb_b3dWriteInt
	public	_bb_b3dWriteString
	public	_bb_b3d_file
	public	_bb_b3d_stack
	public	_bb_b3d_tos
	public	_bb_basedir
	public	_bb_c_surfs
	public	_bb_camera
	public	_bb_cavCanvas0
	public	_bb_cobComboBox1
	public	_bb_count
	public	_bb_enumFiles
	public	_bb_enumFilesCallback
	public	_bb_enumFolders
	public	_bb_enumFoldersCallback
	public	_bb_exportmeshbutt
	public	_bb_ext
	public	_bb_filenames
	public	_bb_hwnd
	public	_bb_invsLoadedMesh
	public	_bb_mesh
	public	_bb_nodeselected
	public	_bb_res
	public	_bb_roofdir
	public	_bb_sidebutt
	public	_bb_skydome
	public	_bb_t
	public	_bb_tabTabber0
	public	_bb_texcount
	public	_bb_texture
	public	_bb_texturedir
	public	_bb_trvTreeView0
	public	_bb_trvTreeView1
	public	_bb_trvTreeView4
	public	_bb_windowselected
	section	"code" code
__bb_main:
	push	ebp
	mov	ebp,esp
	sub	esp,408
	push	ebx
	cmp	dword [_790],0
	je	_791
	mov	eax,0
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_791:
	mov	dword [_790],1
	mov	dword [ebp-20],0
	mov	dword [ebp-24],0
	mov	dword [ebp-28],0
	mov	dword [ebp-32],0
	mov	dword [ebp-36],0
	mov	dword [ebp-40],0
	mov	dword [ebp-44],0
	mov	dword [ebp-48],0
	mov	dword [ebp-52],0
	mov	dword [ebp-56],0
	mov	dword [ebp-60],0
	mov	dword [ebp-4],_bbEmptyString
	mov	dword [ebp-64],0
	mov	dword [ebp-68],0
	fldz
	fstp	dword [ebp-8]
	fldz
	fstp	dword [ebp-12]
	fldz
	fstp	dword [ebp-16]
	mov	dword [ebp-72],0
	mov	dword [ebp-76],0
	mov	dword [ebp-80],0
	push	ebp
	push	_686
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	call	___bb_blitz_blitz
	call	___bb_inc_bbtype
	call	___bb_inc_bbvkey
	call	___bb_minib3d_minib3d
	push	_212
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [_216]
	and	eax,1
	cmp	eax,0
	jne	_217
	push	101
	push	_214
	call	_bbArrayNew1D
	add	esp,8
	mov	dword [ebp-84],eax
	mov	eax,dword [ebp-84]
	inc	dword [eax+4]
	mov	eax,dword [ebp-84]
	mov	dword [_bb_b3d_stack],eax
	or	dword [_216],1
_217:
	push	_218
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_219
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	10
	call	_brl_glgraphics_GLGraphicsDriver
	push	eax
	call	_brl_graphics_SetGraphicsDriver
	add	esp,8
	push	_221
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [_216]
	and	eax,2
	cmp	eax,0
	jne	_223
	call	dword [_bb_TMouse+48]
	mov	dword [ebp-88],eax
	mov	eax,dword [ebp-88]
	inc	dword [eax+4]
	mov	eax,dword [ebp-88]
	mov	dword [_bb_Mouse],eax
	or	dword [_216],2
_223:
	push	_224
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_225
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_226
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_227
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_228
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [_216]
	and	eax,4
	cmp	eax,0
	jne	_231
	push	50
	push	_229
	call	_bbArrayNew1D
	add	esp,8
	mov	dword [ebp-92],eax
	mov	eax,dword [ebp-92]
	inc	dword [eax+4]
	mov	eax,dword [ebp-92]
	mov	dword [_bb_add],eax
	or	dword [_216],4
_231:
	push	_232
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-20],0
	push	_234
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-24],0
	push	_236
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-28],0
	push	_238
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-32],0
	push	_240
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-36],0
	push	_242
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-40],0
	push	_244
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-44],0
	push	_246
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-48],0
	push	_248
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-52],0
	push	_250
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-56],0
	push	_252
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_253
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_254
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_255
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_256
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_257
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_258
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_259
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_260
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [_216]
	and	eax,8
	cmp	eax,0
	jne	_263
	push	10
	push	_261
	call	_bbArrayNew1D
	add	esp,8
	mov	dword [ebp-96],eax
	mov	eax,dword [ebp-96]
	inc	dword [eax+4]
	mov	eax,dword [ebp-96]
	mov	dword [_bb_WindowsTex],eax
	or	dword [_216],8
_263:
	mov	eax,dword [_216]
	and	eax,16
	cmp	eax,0
	jne	_266
	push	10
	push	_264
	call	_bbArrayNew1D
	add	esp,8
	mov	dword [ebp-100],eax
	mov	eax,dword [ebp-100]
	inc	dword [eax+4]
	mov	eax,dword [ebp-100]
	mov	dword [_bb_WindowsCnt],eax
	or	dword [_216],16
_266:
	mov	eax,dword [_216]
	and	eax,32
	cmp	eax,0
	jne	_269
	push	10
	push	_267
	call	_bbArrayNew1D
	add	esp,8
	mov	dword [ebp-104],eax
	mov	eax,dword [ebp-104]
	inc	dword [eax+4]
	mov	eax,dword [ebp-104]
	mov	dword [_bb_WindowsX],eax
	or	dword [_216],32
_269:
	mov	eax,dword [_216]
	and	eax,64
	cmp	eax,0
	jne	_272
	push	10
	push	_270
	call	_bbArrayNew1D
	add	esp,8
	mov	dword [ebp-108],eax
	mov	eax,dword [ebp-108]
	inc	dword [eax+4]
	mov	eax,dword [ebp-108]
	mov	dword [_bb_WindowsY],eax
	or	dword [_216],64
_272:
	mov	eax,dword [_216]
	and	eax,128
	cmp	eax,0
	jne	_275
	push	10
	push	_273
	call	_bbArrayNew1D
	add	esp,8
	mov	dword [ebp-112],eax
	mov	eax,dword [ebp-112]
	inc	dword [eax+4]
	mov	eax,dword [ebp-112]
	mov	dword [_bb_WindowsZ],eax
	or	dword [_216],128
_275:
	mov	eax,dword [_216]
	and	eax,256
	cmp	eax,0
	jne	_278
	push	10
	push	_276
	call	_bbArrayNew1D
	add	esp,8
	mov	dword [ebp-116],eax
	mov	eax,dword [ebp-116]
	inc	dword [eax+4]
	mov	eax,dword [ebp-116]
	mov	dword [_bb_WinChild],eax
	or	dword [_216],256
_278:
	push	_279
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_280
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_281
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_282
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_283
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_284
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_285
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_286
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_287
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_288
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_289
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [_216]
	and	eax,512
	cmp	eax,0
	jne	_291
	push	3
	push	_brl_maxgui_TGadget
	push	0
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	push	704
	push	978
	push	0
	push	0
	push	_31
	call	_brl_maxgui_CreateWindow
	add	esp,28
	mov	dword [ebp-120],eax
	mov	eax,dword [ebp-120]
	inc	dword [eax+4]
	mov	eax,dword [ebp-120]
	mov	dword [_bb_Window],eax
	or	dword [_216],512
_291:
	push	_292
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [_216]
	and	eax,1024
	cmp	eax,0
	jne	_294
	push	0
	push	dword [_bb_Window]
	push	658
	push	245
	push	12
	push	10
	call	_brl_maxgui_CreateTabber
	add	esp,24
	mov	dword [ebp-124],eax
	mov	eax,dword [ebp-124]
	inc	dword [eax+4]
	mov	eax,dword [ebp-124]
	mov	dword [_bb_tabTabber0],eax
	or	dword [_216],1024
_294:
	push	_295
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_bbNullObject
	push	_1
	push	-1
	push	1
	push	_32
	push	dword [_bb_tabTabber0]
	call	_brl_maxgui_AddGadgetItem
	add	esp,24
	push	_296
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_bbNullObject
	push	_1
	push	-1
	push	0
	push	_33
	push	dword [_bb_tabTabber0]
	call	_brl_maxgui_AddGadgetItem
	add	esp,24
	push	_297
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_bbNullObject
	push	_1
	push	-1
	push	0
	push	_34
	push	dword [_bb_tabTabber0]
	call	_brl_maxgui_AddGadgetItem
	add	esp,24
	push	_298
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_bbNullObject
	push	_1
	push	-1
	push	0
	push	_35
	push	dword [_bb_tabTabber0]
	call	_brl_maxgui_AddGadgetItem
	add	esp,24
	push	_299
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	2
	push	1
	push	2
	push	1
	push	dword [_bb_tabTabber0]
	call	_brl_maxgui_SetGadgetLayout
	add	esp,20
	push	_300
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [_216]
	and	eax,2048
	cmp	eax,0
	jne	_302
	push	_1
	push	0
	push	dword [_bb_tabTabber0]
	push	dword [_bb_tabTabber0]
	call	_brl_maxgui_ClientHeight
	add	esp,4
	push	eax
	push	dword [_bb_tabTabber0]
	call	_brl_maxgui_ClientWidth
	add	esp,4
	push	eax
	push	0
	push	0
	call	_brl_maxgui_CreatePanel
	add	esp,28
	mov	dword [ebp-128],eax
	mov	eax,dword [ebp-128]
	inc	dword [eax+4]
	mov	eax,dword [ebp-128]
	mov	dword [_bb_TabPanel0],eax
	or	dword [_216],2048
_302:
	push	_303
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	1
	push	1
	push	1
	push	1
	push	dword [_bb_TabPanel0]
	call	_brl_maxgui_SetGadgetLayout
	add	esp,20
	push	_304
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [_216]
	and	eax,4096
	cmp	eax,0
	jne	_306
	push	0
	push	dword [_bb_TabPanel0]
	push	593
	push	234
	push	14
	push	2
	call	_brl_maxgui_CreateTreeView
	add	esp,24
	mov	dword [ebp-132],eax
	mov	eax,dword [ebp-132]
	inc	dword [eax+4]
	mov	eax,dword [ebp-132]
	mov	dword [_bb_trvTreeView0],eax
	or	dword [_216],4096
_306:
	push	_307
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	0
	push	1
	push	0
	push	1
	push	dword [_bb_trvTreeView0]
	call	_brl_maxgui_SetGadgetLayout
	add	esp,20
	push	_308
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [_216]
	and	eax,8192
	cmp	eax,0
	jne	_310
	push	_1
	push	0
	push	dword [_bb_tabTabber0]
	push	dword [_bb_tabTabber0]
	call	_brl_maxgui_ClientHeight
	add	esp,4
	push	eax
	push	dword [_bb_tabTabber0]
	call	_brl_maxgui_ClientWidth
	add	esp,4
	push	eax
	push	0
	push	0
	call	_brl_maxgui_CreatePanel
	add	esp,28
	mov	dword [ebp-136],eax
	mov	eax,dword [ebp-136]
	inc	dword [eax+4]
	mov	eax,dword [ebp-136]
	mov	dword [_bb_TabPanel1],eax
	or	dword [_216],8192
_310:
	push	_311
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [_bb_TabPanel1]
	call	_brl_maxgui_HideGadget
	add	esp,4
	push	_312
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	1
	push	1
	push	1
	push	1
	push	dword [_bb_TabPanel1]
	call	_brl_maxgui_SetGadgetLayout
	add	esp,20
	push	_313
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [_bb_TabPanel1]
	call	_brl_maxgui_HideGadget
	add	esp,4
	push	_314
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [_216]
	and	eax,16384
	cmp	eax,0
	jne	_316
	push	0
	push	dword [_bb_TabPanel1]
	push	595
	push	234
	push	12
	push	3
	call	_brl_maxgui_CreateTreeView
	add	esp,24
	mov	dword [ebp-140],eax
	mov	eax,dword [ebp-140]
	inc	dword [eax+4]
	mov	eax,dword [ebp-140]
	mov	dword [_bb_trvTreeView1],eax
	or	dword [_216],16384
_316:
	push	_317
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	0
	push	1
	push	0
	push	1
	push	dword [_bb_trvTreeView1]
	call	_brl_maxgui_SetGadgetLayout
	add	esp,20
	push	_318
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [_216]
	and	eax,32768
	cmp	eax,0
	jne	_320
	push	_1
	push	0
	push	dword [_bb_tabTabber0]
	push	dword [_bb_tabTabber0]
	call	_brl_maxgui_ClientHeight
	add	esp,4
	push	eax
	push	dword [_bb_tabTabber0]
	call	_brl_maxgui_ClientWidth
	add	esp,4
	push	eax
	push	0
	push	0
	call	_brl_maxgui_CreatePanel
	add	esp,28
	mov	dword [ebp-144],eax
	mov	eax,dword [ebp-144]
	inc	dword [eax+4]
	mov	eax,dword [ebp-144]
	mov	dword [_bb_TabPanel2],eax
	or	dword [_216],32768
_320:
	push	_321
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [_bb_TabPanel2]
	call	_brl_maxgui_HideGadget
	add	esp,4
	push	_322
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	1
	push	1
	push	1
	push	1
	push	dword [_bb_TabPanel2]
	call	_brl_maxgui_SetGadgetLayout
	add	esp,20
	push	_323
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [_bb_TabPanel2]
	call	_brl_maxgui_HideGadget
	add	esp,4
	push	_324
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [_216]
	and	eax,65536
	cmp	eax,0
	jne	_326
	push	3
	push	dword [_bb_TabPanel2]
	push	16
	push	96
	push	83
	push	124
	push	_36
	call	_brl_maxgui_CreateButton
	add	esp,28
	mov	dword [ebp-148],eax
	mov	eax,dword [ebp-148]
	inc	dword [eax+4]
	mov	eax,dword [ebp-148]
	mov	dword [_bb_sidebutt],eax
	or	dword [_216],65536
_326:
	push	_327
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	0
	push	1
	push	0
	push	1
	push	dword [_bb_sidebutt]
	call	_brl_maxgui_SetGadgetLayout
	add	esp,20
	push	_328
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [_216]
	and	eax,131072
	cmp	eax,0
	jne	_330
	push	0
	push	dword [_bb_TabPanel2]
	push	20
	push	96
	push	12
	push	127
	call	_brl_maxgui_CreateComboBox
	add	esp,24
	mov	dword [ebp-152],eax
	mov	eax,dword [ebp-152]
	inc	dword [eax+4]
	mov	eax,dword [ebp-152]
	mov	dword [_bb_cobComboBox1],eax
	or	dword [_216],131072
_330:
	push	_331
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_bbNullObject
	push	_1
	push	-1
	push	0
	push	_37
	push	dword [_bb_cobComboBox1]
	call	_brl_maxgui_AddGadgetItem
	add	esp,24
	push	_332
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_bbNullObject
	push	_1
	push	-1
	push	0
	push	_38
	push	dword [_bb_cobComboBox1]
	call	_brl_maxgui_AddGadgetItem
	add	esp,24
	push	_333
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_bbNullObject
	push	_1
	push	-1
	push	0
	push	_39
	push	dword [_bb_cobComboBox1]
	call	_brl_maxgui_AddGadgetItem
	add	esp,24
	push	_334
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_bbNullObject
	push	_1
	push	-1
	push	0
	push	_40
	push	dword [_bb_cobComboBox1]
	call	_brl_maxgui_AddGadgetItem
	add	esp,24
	push	_335
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_bbNullObject
	push	_1
	push	-1
	push	0
	push	_41
	push	dword [_bb_cobComboBox1]
	call	_brl_maxgui_AddGadgetItem
	add	esp,24
	push	_336
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_bbNullObject
	push	_1
	push	-1
	push	0
	push	_42
	push	dword [_bb_cobComboBox1]
	call	_brl_maxgui_AddGadgetItem
	add	esp,24
	push	_337
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	0
	push	dword [_bb_cobComboBox1]
	call	_brl_maxgui_SelectGadgetItem
	add	esp,8
	push	_338
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	0
	push	1
	push	0
	push	1
	push	dword [_bb_cobComboBox1]
	call	_brl_maxgui_SetGadgetLayout
	add	esp,20
	push	_339
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [_216]
	and	eax,262144
	cmp	eax,0
	jne	_341
	push	0
	push	dword [_bb_TabPanel2]
	push	20
	push	96
	push	47
	push	127
	call	_brl_maxgui_CreateComboBox
	add	esp,24
	mov	dword [ebp-156],eax
	mov	eax,dword [ebp-156]
	inc	dword [eax+4]
	mov	eax,dword [ebp-156]
	mov	dword [_bb_windowselected],eax
	or	dword [_216],262144
_341:
	push	_342
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_bbNullObject
	push	_1
	push	-1
	push	0
	push	_37
	push	dword [_bb_windowselected]
	call	_brl_maxgui_AddGadgetItem
	add	esp,24
	push	_343
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_bbNullObject
	push	_1
	push	-1
	push	0
	push	_38
	push	dword [_bb_windowselected]
	call	_brl_maxgui_AddGadgetItem
	add	esp,24
	push	_344
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_bbNullObject
	push	_1
	push	-1
	push	0
	push	_39
	push	dword [_bb_windowselected]
	call	_brl_maxgui_AddGadgetItem
	add	esp,24
	push	_345
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_bbNullObject
	push	_1
	push	-1
	push	0
	push	_40
	push	dword [_bb_windowselected]
	call	_brl_maxgui_AddGadgetItem
	add	esp,24
	push	_346
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_bbNullObject
	push	_1
	push	-1
	push	0
	push	_41
	push	dword [_bb_windowselected]
	call	_brl_maxgui_AddGadgetItem
	add	esp,24
	push	_347
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_bbNullObject
	push	_1
	push	-1
	push	0
	push	_42
	push	dword [_bb_windowselected]
	call	_brl_maxgui_AddGadgetItem
	add	esp,24
	push	_348
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	0
	push	dword [_bb_windowselected]
	call	_brl_maxgui_SelectGadgetItem
	add	esp,8
	push	_349
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	0
	push	1
	push	0
	push	1
	push	dword [_bb_windowselected]
	call	_brl_maxgui_SetGadgetLayout
	add	esp,20
	push	_350
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [_216]
	and	eax,524288
	cmp	eax,0
	jne	_352
	push	0
	push	dword [_bb_TabPanel2]
	push	15
	push	103
	push	50
	push	12
	push	_43
	call	_brl_maxgui_CreateButton
	add	esp,28
	mov	dword [ebp-160],eax
	mov	eax,dword [ebp-160]
	inc	dword [eax+4]
	mov	eax,dword [ebp-160]
	mov	dword [_bb_SetWindowTex],eax
	or	dword [_216],524288
_352:
	push	_353
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	0
	push	1
	push	0
	push	1
	push	dword [_bb_SetWindowTex]
	call	_brl_maxgui_SetGadgetLayout
	add	esp,20
	push	_354
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [_216]
	and	eax,1048576
	cmp	eax,0
	jne	_356
	push	0
	push	dword [_bb_TabPanel2]
	push	16
	push	99
	push	15
	push	14
	push	_44
	call	_brl_maxgui_CreateButton
	add	esp,28
	mov	dword [ebp-164],eax
	mov	eax,dword [ebp-164]
	inc	dword [eax+4]
	mov	eax,dword [ebp-164]
	mov	dword [_bb_SetWindowCnt],eax
	or	dword [_216],1048576
_356:
	push	_357
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	0
	push	1
	push	0
	push	1
	push	dword [_bb_SetWindowCnt]
	call	_brl_maxgui_SetGadgetLayout
	add	esp,20
	push	_358
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [_216]
	and	eax,2097152
	cmp	eax,0
	jne	_360
	push	_1
	push	0
	push	dword [_bb_tabTabber0]
	push	dword [_bb_tabTabber0]
	call	_brl_maxgui_ClientHeight
	add	esp,4
	push	eax
	push	dword [_bb_tabTabber0]
	call	_brl_maxgui_ClientWidth
	add	esp,4
	push	eax
	push	0
	push	0
	call	_brl_maxgui_CreatePanel
	add	esp,28
	mov	dword [ebp-168],eax
	mov	eax,dword [ebp-168]
	inc	dword [eax+4]
	mov	eax,dword [ebp-168]
	mov	dword [_bb_TabPanel3],eax
	or	dword [_216],2097152
_360:
	push	_361
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [_bb_TabPanel3]
	call	_brl_maxgui_HideGadget
	add	esp,4
	push	_362
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	1
	push	1
	push	1
	push	1
	push	dword [_bb_TabPanel3]
	call	_brl_maxgui_SetGadgetLayout
	add	esp,20
	push	_363
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [_bb_TabPanel3]
	call	_brl_maxgui_HideGadget
	add	esp,4
	push	_364
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [_216]
	and	eax,4194304
	cmp	eax,0
	jne	_366
	push	0
	push	dword [_bb_TabPanel3]
	push	593
	push	235
	push	10
	push	3
	call	_brl_maxgui_CreateTreeView
	add	esp,24
	mov	dword [ebp-172],eax
	mov	eax,dword [ebp-172]
	inc	dword [eax+4]
	mov	eax,dword [ebp-172]
	mov	dword [_bb_trvTreeView4],eax
	or	dword [_216],4194304
_366:
	push	_367
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	0
	push	1
	push	0
	push	1
	push	dword [_bb_trvTreeView4]
	call	_brl_maxgui_SetGadgetLayout
	add	esp,20
	push	_368
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [_216]
	and	eax,8388608
	cmp	eax,0
	jne	_370
	push	0
	push	dword [_bb_Window]
	push	637
	push	701
	push	32
	push	259
	call	_brl_maxgui_CreateCanvas
	add	esp,24
	mov	dword [ebp-176],eax
	mov	eax,dword [ebp-176]
	inc	dword [eax+4]
	mov	eax,dword [ebp-176]
	mov	dword [_bb_cavCanvas0],eax
	or	dword [_216],8388608
_370:
	push	_371
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	2
	push	1
	push	2
	push	1
	push	dword [_bb_cavCanvas0]
	call	_brl_maxgui_SetGadgetLayout
	add	esp,20
	push	_372
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [_216]
	and	eax,16777216
	cmp	eax,0
	jne	_374
	push	1
	push	dword [_bb_Window]
	push	24
	push	77
	push	3
	push	836
	push	_45
	call	_brl_maxgui_CreateButton
	add	esp,28
	mov	dword [ebp-180],eax
	mov	eax,dword [ebp-180]
	inc	dword [eax+4]
	mov	eax,dword [ebp-180]
	mov	dword [_bb_exportmeshbutt],eax
	or	dword [_216],16777216
_374:
	push	_375
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	0
	push	1
	push	0
	push	1
	push	dword [_bb_exportmeshbutt]
	call	_brl_maxgui_SetGadgetLayout
	add	esp,20
	push	_376
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_46
	call	_brl_maxgui_LoadIconStrip
	add	esp,4
	push	eax
	call	_bbHandleFromObject
	add	esp,4
	mov	dword [ebp-60],eax
	push	_378
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_brl_maxgui_TIconStrip
	push	dword [ebp-60]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	push	dword [_bb_trvTreeView0]
	call	_brl_maxgui_SetGadgetIconStrip
	add	esp,8
	push	_379
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_brl_maxgui_TIconStrip
	push	dword [ebp-60]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	push	dword [_bb_trvTreeView1]
	call	_brl_maxgui_SetGadgetIconStrip
	add	esp,8
	push	_380
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_brl_maxgui_TIconStrip
	push	dword [ebp-60]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	push	dword [_bb_trvTreeView4]
	call	_brl_maxgui_SetGadgetIconStrip
	add	esp,8
	push	_381
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [_216]
	and	eax,33554432
	cmp	eax,0
	jne	_382
	push	1
	push	dword [_bb_cavCanvas0]
	call	_brl_maxgui_QueryGadget
	add	esp,8
	mov	dword [_bb_hwnd],eax
	or	dword [_216],33554432
_382:
	push	_383
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_384
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_385
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_386
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_387
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [_216]
	and	eax,67108864
	cmp	eax,0
	jne	_390
	push	1000
	push	_388
	call	_bbArrayNew1D
	add	esp,8
	mov	dword [ebp-184],eax
	mov	eax,dword [ebp-184]
	inc	dword [eax+4]
	mov	eax,dword [ebp-184]
	mov	dword [_bb_texturedir],eax
	or	dword [_216],67108864
_390:
	push	_391
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_392
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_393
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_394
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [_216]
	and	eax,134217728
	cmp	eax,0
	jne	_396
	call	_brl_linkedlist_CreateList
	mov	dword [ebp-188],eax
	mov	eax,dword [ebp-188]
	inc	dword [eax+4]
	mov	eax,dword [ebp-188]
	mov	dword [_bb_Model_List],eax
	or	dword [_216],134217728
_396:
	push	_397
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [_216]
	and	eax,268435456
	cmp	eax,0
	jne	_399
	call	_brl_linkedlist_CreateList
	mov	dword [ebp-192],eax
	mov	eax,dword [ebp-192]
	inc	dword [eax+4]
	mov	eax,dword [ebp-192]
	mov	dword [_bb_Roof_List],eax
	or	dword [_216],268435456
_399:
	push	_400
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [_216]
	and	eax,536870912
	cmp	eax,0
	jne	_402
	call	_brl_linkedlist_CreateList
	mov	dword [ebp-196],eax
	mov	eax,dword [ebp-196]
	inc	dword [eax+4]
	mov	eax,dword [ebp-196]
	mov	dword [_bb_Texture_List],eax
	or	dword [_216],536870912
_402:
	push	_403
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [_216]
	and	eax,1073741824
	cmp	eax,0
	jne	_405
	call	_brl_linkedlist_CreateList
	mov	dword [ebp-200],eax
	mov	eax,dword [ebp-200]
	inc	dword [eax+4]
	mov	eax,dword [ebp-200]
	mov	dword [_bb_Texturebase_list],eax
	or	dword [_216],1073741824
_405:
	push	_406
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [_216]
	and	eax,-2147483648
	cmp	eax,0
	jne	_408
	call	_brl_linkedlist_CreateList
	mov	dword [ebp-204],eax
	mov	eax,dword [ebp-204]
	inc	dword [eax+4]
	mov	eax,dword [ebp-204]
	mov	dword [_bb_Textureroof_list],eax
	or	dword [_216],-2147483648
_408:
	push	_409
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [_411]
	and	eax,1
	cmp	eax,0
	jne	_412
	call	_brl_linkedlist_CreateList
	mov	dword [ebp-208],eax
	mov	eax,dword [ebp-208]
	inc	dword [eax+4]
	mov	eax,dword [ebp-208]
	mov	dword [_bb_Texturebeam_list],eax
	or	dword [_411],1
_412:
	push	_413
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [_411]
	and	eax,2
	cmp	eax,0
	jne	_415
	call	_brl_linkedlist_CreateList
	mov	dword [ebp-212],eax
	mov	eax,dword [ebp-212]
	inc	dword [eax+4]
	mov	eax,dword [ebp-212]
	mov	dword [_bb_Texturewindow_list],eax
	or	dword [_411],2
_415:
	push	_416
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [_411]
	and	eax,4
	cmp	eax,0
	jne	_418
	call	_brl_linkedlist_CreateList
	mov	dword [ebp-216],eax
	mov	eax,dword [ebp-216]
	inc	dword [eax+4]
	mov	eax,dword [ebp-216]
	mov	dword [_bb_Door_list],eax
	or	dword [_411],4
_418:
	push	_419
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_420
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [_411]
	and	eax,8
	cmp	eax,0
	jne	_422
	call	_brl_linkedlist_CreateList
	mov	dword [ebp-220],eax
	mov	eax,dword [ebp-220]
	inc	dword [eax+4]
	mov	eax,dword [ebp-220]
	mov	dword [_bb_Surf_list],eax
	or	dword [_411],8
_422:
	push	_423
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_47
	push	dword [_bb_Model_List]
	call	_bb_enumFiles
	add	esp,8
	push	_424
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_48
	push	dword [_bb_Roof_List]
	call	_bb_enumFiles
	add	esp,8
	push	_425
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_49
	push	dword [_bb_Door_list]
	call	_bb_enumFiles
	add	esp,8
	push	_426
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_50
	push	dword [_bb_Texture_List]
	call	_bb_enumFiles
	add	esp,8
	push	_427
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	1
	push	dword [_bb_trvTreeView0]
	push	_51
	call	_brl_maxgui_AddTreeViewNode
	add	esp,12
	mov	dword [ebp-224],eax
	mov	eax,dword [ebp-224]
	inc	dword [eax+4]
	mov	eax,dword [ebp-224]
	mov	dword [ebp-228],eax
	mov	eax,dword [_bb_basedir]
	mov	dword [ebp-232],eax
	mov	eax,dword [ebp-232]
	dec	dword [eax+4]
	jnz	_431
	push	dword [ebp-232]
	call	_bbGCFree
	add	esp,4
_431:
	mov	eax,dword [ebp-228]
	mov	dword [_bb_basedir],eax
	push	_432
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [_bb_Model_List]
	mov	dword [ebp-236],eax
	mov	eax,dword [ebp-236]
	mov	dword [ebp-240],eax
	cmp	dword [ebp-240],_bbNullObject
	jne	_435
	call	_brl_blitz_NullObjectError
_435:
	push	dword [ebp-240]
	mov	eax,dword [ebp-240]
	mov	eax,dword [eax]
	call	dword [eax+140]
	add	esp,4
	mov	dword [ebp-244],eax
	jmp	_52
_54:
	mov	eax,dword [ebp-244]
	mov	dword [ebp-248],eax
	cmp	dword [ebp-248],_bbNullObject
	jne	_440
	call	_brl_blitz_NullObjectError
_440:
	push	_bbStringClass
	push	dword [ebp-248]
	mov	eax,dword [ebp-248]
	mov	eax,dword [eax]
	call	dword [eax+52]
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	mov	dword [ebp-252],eax
	mov	eax,dword [ebp-252]
	inc	dword [eax+4]
	mov	eax,dword [ebp-252]
	mov	dword [ebp-256],eax
	mov	eax,dword [_bb_t]
	mov	dword [ebp-260],eax
	mov	eax,dword [ebp-260]
	dec	dword [eax+4]
	jnz	_444
	push	dword [ebp-260]
	call	_bbGCFree
	add	esp,4
_444:
	mov	eax,dword [ebp-256]
	mov	dword [_bb_t],eax
	cmp	dword [_bb_t],_bbNullObject
	je	_52
	push	_445
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_1
	push	dword [_bb_t]
	call	_bbStringCompare
	add	esp,8
	cmp	eax,0
	jne	_446
	push	_447
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	jmp	_53
_446:
	push	_448
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_55
	push	dword [_bb_t]
	call	_bbStringCompare
	add	esp,8
	cmp	eax,0
	sete	al
	movzx	eax,al
	cmp	eax,0
	jne	_449
	push	_56
	push	dword [_bb_t]
	call	_bbStringCompare
	add	esp,8
	cmp	eax,0
	sete	al
	movzx	eax,al
_449:
	cmp	eax,0
	je	_451
	push	_452
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	jmp	_52
_451:
	push	_453
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [_bb_t]
	call	_brl_filesystem_ExtractExt
	add	esp,4
	mov	dword [ebp-264],eax
	mov	eax,dword [ebp-264]
	inc	dword [eax+4]
	mov	eax,dword [ebp-264]
	mov	dword [ebp-268],eax
	mov	eax,dword [_bb_ext]
	mov	dword [ebp-272],eax
	mov	eax,dword [ebp-272]
	dec	dword [eax+4]
	jnz	_457
	push	dword [ebp-272]
	call	_bbGCFree
	add	esp,4
_457:
	mov	eax,dword [ebp-268]
	mov	dword [_bb_ext],eax
	push	_458
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [_bb_ext]
	mov	dword [ebp-276],eax
	push	_57
	push	dword [ebp-276]
	call	_bbStringCompare
	add	esp,8
	cmp	eax,0
	je	_461
	jmp	_460
_461:
	push	_462
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	2
	push	dword [_bb_basedir]
	push	dword [_bb_t]
	call	_brl_filesystem_StripAll
	add	esp,4
	push	eax
	call	_brl_maxgui_AddTreeViewNode
	add	esp,12
_460:
_52:
	mov	eax,dword [ebp-244]
	mov	dword [ebp-280],eax
	cmp	dword [ebp-280],_bbNullObject
	jne	_438
	call	_brl_blitz_NullObjectError
_438:
	push	dword [ebp-280]
	mov	eax,dword [ebp-280]
	mov	eax,dword [eax]
	call	dword [eax+48]
	add	esp,4
	cmp	eax,0
	jne	_54
_53:
	push	_463
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	1
	push	dword [_bb_trvTreeView1]
	push	_33
	call	_brl_maxgui_AddTreeViewNode
	add	esp,12
	mov	dword [ebp-284],eax
	mov	eax,dword [ebp-284]
	inc	dword [eax+4]
	mov	eax,dword [ebp-284]
	mov	dword [ebp-288],eax
	mov	eax,dword [_bb_roofdir]
	mov	dword [ebp-292],eax
	mov	eax,dword [ebp-292]
	dec	dword [eax+4]
	jnz	_467
	push	dword [ebp-292]
	call	_bbGCFree
	add	esp,4
_467:
	mov	eax,dword [ebp-288]
	mov	dword [_bb_roofdir],eax
	push	_468
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [_bb_Roof_List]
	mov	dword [ebp-296],eax
	mov	eax,dword [ebp-296]
	mov	dword [ebp-300],eax
	cmp	dword [ebp-300],_bbNullObject
	jne	_472
	call	_brl_blitz_NullObjectError
_472:
	push	dword [ebp-300]
	mov	eax,dword [ebp-300]
	mov	eax,dword [eax]
	call	dword [eax+140]
	add	esp,4
	mov	dword [ebp-304],eax
	jmp	_58
_60:
	mov	eax,dword [ebp-304]
	mov	dword [ebp-308],eax
	cmp	dword [ebp-308],_bbNullObject
	jne	_477
	call	_brl_blitz_NullObjectError
_477:
	push	_bbStringClass
	push	dword [ebp-308]
	mov	eax,dword [ebp-308]
	mov	eax,dword [eax]
	call	dword [eax+52]
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	mov	dword [ebp-4],eax
	cmp	dword [ebp-4],_bbNullObject
	je	_58
	push	_478
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_1
	push	dword [ebp-4]
	call	_bbStringCompare
	add	esp,8
	cmp	eax,0
	jne	_479
	push	_480
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	jmp	_59
_479:
	push	_481
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_55
	push	dword [ebp-4]
	call	_bbStringCompare
	add	esp,8
	cmp	eax,0
	sete	al
	movzx	eax,al
	cmp	eax,0
	jne	_482
	push	_56
	push	dword [ebp-4]
	call	_bbStringCompare
	add	esp,8
	cmp	eax,0
	sete	al
	movzx	eax,al
_482:
	cmp	eax,0
	je	_484
	push	_485
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	jmp	_58
_484:
	push	_486
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-4]
	call	_brl_filesystem_ExtractExt
	add	esp,4
	mov	dword [ebp-312],eax
	mov	eax,dword [ebp-312]
	inc	dword [eax+4]
	mov	eax,dword [ebp-312]
	mov	dword [ebp-316],eax
	mov	eax,dword [_bb_ext]
	mov	dword [ebp-320],eax
	mov	eax,dword [ebp-320]
	dec	dword [eax+4]
	jnz	_490
	push	dword [ebp-320]
	call	_bbGCFree
	add	esp,4
_490:
	mov	eax,dword [ebp-316]
	mov	dword [_bb_ext],eax
	push	_491
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [_bb_ext]
	mov	dword [ebp-324],eax
	push	_57
	push	dword [ebp-324]
	call	_bbStringCompare
	add	esp,8
	cmp	eax,0
	je	_494
	jmp	_493
_494:
	push	_495
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	2
	push	dword [_bb_roofdir]
	push	dword [ebp-4]
	call	_brl_filesystem_StripAll
	add	esp,4
	push	eax
	call	_brl_maxgui_AddTreeViewNode
	add	esp,12
_493:
_58:
	mov	eax,dword [ebp-304]
	mov	dword [ebp-328],eax
	cmp	dword [ebp-328],_bbNullObject
	jne	_475
	call	_brl_blitz_NullObjectError
_475:
	push	dword [ebp-328]
	mov	eax,dword [ebp-328]
	mov	eax,dword [eax]
	call	dword [eax+48]
	add	esp,4
	cmp	eax,0
	jne	_60
_59:
	push	_496
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	1
	push	dword [_bb_trvTreeView4]
	push	_35
	call	_brl_maxgui_AddTreeViewNode
	add	esp,12
	push	eax
	call	_bbHandleFromObject
	add	esp,4
	mov	dword [ebp-64],eax
	push	_498
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [_bb_Door_list]
	mov	dword [ebp-332],eax
	mov	eax,dword [ebp-332]
	mov	dword [ebp-336],eax
	cmp	dword [ebp-336],_bbNullObject
	jne	_501
	call	_brl_blitz_NullObjectError
_501:
	push	dword [ebp-336]
	mov	eax,dword [ebp-336]
	mov	eax,dword [eax]
	call	dword [eax+140]
	add	esp,4
	mov	dword [ebp-340],eax
	jmp	_61
_63:
	mov	eax,dword [ebp-340]
	mov	dword [ebp-344],eax
	cmp	dword [ebp-344],_bbNullObject
	jne	_506
	call	_brl_blitz_NullObjectError
_506:
	push	_bbStringClass
	push	dword [ebp-344]
	mov	eax,dword [ebp-344]
	mov	eax,dword [eax]
	call	dword [eax+52]
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	mov	dword [ebp-4],eax
	cmp	dword [ebp-4],_bbNullObject
	je	_61
	push	_507
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_1
	push	dword [ebp-4]
	call	_bbStringCompare
	add	esp,8
	cmp	eax,0
	jne	_508
	push	_509
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	jmp	_62
_508:
	push	_510
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_55
	push	dword [ebp-4]
	call	_bbStringCompare
	add	esp,8
	cmp	eax,0
	sete	al
	movzx	eax,al
	cmp	eax,0
	jne	_511
	push	_56
	push	dword [ebp-4]
	call	_bbStringCompare
	add	esp,8
	cmp	eax,0
	sete	al
	movzx	eax,al
_511:
	cmp	eax,0
	je	_513
	push	_514
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	jmp	_61
_513:
	push	_515
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-4]
	call	_brl_filesystem_ExtractExt
	add	esp,4
	mov	dword [ebp-348],eax
	mov	eax,dword [ebp-348]
	inc	dword [eax+4]
	mov	eax,dword [ebp-348]
	mov	dword [ebp-352],eax
	mov	eax,dword [_bb_ext]
	mov	dword [ebp-356],eax
	mov	eax,dword [ebp-356]
	dec	dword [eax+4]
	jnz	_519
	push	dword [ebp-356]
	call	_bbGCFree
	add	esp,4
_519:
	mov	eax,dword [ebp-352]
	mov	dword [_bb_ext],eax
	push	_520
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [_bb_ext]
	mov	dword [ebp-360],eax
	push	_57
	push	dword [ebp-360]
	call	_bbStringCompare
	add	esp,8
	cmp	eax,0
	je	_523
	jmp	_522
_523:
	push	_524
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	2
	push	_brl_maxgui_TGadget
	push	dword [ebp-64]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	push	dword [ebp-4]
	call	_brl_filesystem_StripAll
	add	esp,4
	push	eax
	call	_brl_maxgui_AddTreeViewNode
	add	esp,12
_522:
_61:
	mov	eax,dword [ebp-340]
	mov	dword [ebp-364],eax
	cmp	dword [ebp-364],_bbNullObject
	jne	_504
	call	_brl_blitz_NullObjectError
_504:
	push	dword [ebp-364]
	mov	eax,dword [ebp-364]
	mov	eax,dword [eax]
	call	dword [eax+48]
	add	esp,4
	cmp	eax,0
	jne	_63
_62:
	push	_525
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [_bb_Window]
	mov	dword [ebp-368],eax
	cmp	dword [ebp-368],_bbNullObject
	jne	_527
	call	_brl_blitz_NullObjectError
_527:
	push	dword [ebp-368]
	mov	eax,dword [ebp-368]
	mov	eax,dword [eax]
	call	dword [eax+188]
	add	esp,4
	mov	dword [__sidesign_minib3d_TGlobal_width],eax
	push	_528
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [_bb_Window]
	mov	dword [ebp-372],eax
	cmp	dword [ebp-372],_bbNullObject
	jne	_530
	call	_brl_blitz_NullObjectError
_530:
	push	dword [ebp-372]
	mov	eax,dword [ebp-372]
	mov	eax,dword [eax]
	call	dword [eax+192]
	add	esp,4
	mov	dword [__sidesign_minib3d_TGlobal_height],eax
	push	_531
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [__sidesign_minib3d_TGlobal_depth],32
	push	_532
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [__sidesign_minib3d_TGlobal_mode],0
	push	_533
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [__sidesign_minib3d_TGlobal_rate],60
	push	_534
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [_bb_cavCanvas0]
	call	_brl_maxgui_CanvasGraphics
	add	esp,4
	push	eax
	call	_brl_graphics_SetGraphics
	add	esp,4
	push	_535
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	9
	push	_1
	call	dword [_sidesign_minib3d_TTexture+112]
	add	esp,8
	push	_536
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	call	_glewInit
	push	_537
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	2896
	call	_glEnable@4
	push	_538
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	33274
	push	33272
	call	_glLightModeli@8
	push	_539
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	1
	push	2897
	call	_glLightModeli@8
	push	_540
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	2977
	call	_glEnable@4
	push	_541
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	3089
	call	_glEnable@4
	push	_542
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	2929
	call	_glEnable@4
	push	_543
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	fld1
	sub	esp,8
	fstp	qword [esp]
	call	_glClearDepth@8
	push	_544
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	515
	call	_glDepthFunc@4
	push	_545
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	4354
	push	3152
	call	_glHint@8
	push	_546
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	1056964608
	push	518
	call	_glAlphaFunc@8
	push	_547
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	32884
	call	_glEnableClientState@4
	push	_548
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	32886
	call	_glEnableClientState@4
	push	_549
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	32885
	call	_glEnableClientState@4
	push	_550
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	1082130432
	call	_glLineWidth@4
	push	_551
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_64
	call	_brl_filesystem_ChangeDir
	add	esp,4
	push	_552
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	1124073472
	push	1124073472
	push	1124073472
	call	_sidesign_minib3d_AmbientLight
	add	esp,12
	push	_553
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_bbNullObject
	call	_sidesign_minib3d_CreateCamera
	add	esp,4
	mov	dword [ebp-376],eax
	mov	eax,dword [ebp-376]
	inc	dword [eax+4]
	mov	eax,dword [ebp-376]
	mov	dword [ebp-380],eax
	mov	eax,dword [_bb_camera]
	mov	dword [ebp-384],eax
	mov	eax,dword [ebp-384]
	dec	dword [eax+4]
	jnz	_557
	push	dword [ebp-384]
	call	_bbGCFree
	add	esp,4
_557:
	mov	eax,dword [ebp-380]
	mov	dword [_bb_camera],eax
	push	_558
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	0
	push	-1054867456
	push	1077936128
	push	0
	push	dword [_bb_camera]
	call	_sidesign_minib3d_TranslateEntity
	add	esp,20
	push	_559
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	0
	push	0
	push	0
	push	dword [_bb_camera]
	call	_sidesign_minib3d_CameraClsColor
	add	esp,16
	push	_560
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	1124073472
	push	0
	push	0
	push	dword [_bb_camera]
	call	_sidesign_minib3d_CameraClsColor
	add	esp,16
	push	_561
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	1165623296
	push	1065353216
	push	dword [_bb_camera]
	call	_sidesign_minib3d_CameraRange
	add	esp,12
	push	_562
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_bbNullObject
	push	1114636288
	call	_brl_timer_CreateTimer
	add	esp,8
	push	_563
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	jmp	_65
_67:
	push	_564
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [_bb_Mouse]
	mov	dword [ebp-388],eax
	cmp	dword [ebp-388],_bbNullObject
	jne	_566
	call	_brl_blitz_NullObjectError
_566:
	push	dword [ebp-388]
	mov	eax,dword [ebp-388]
	mov	eax,dword [eax]
	call	dword [eax+52]
	add	esp,4
	push	_567
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	call	_brl_eventqueue_WaitEvent
	push	_568
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	call	_brl_eventqueue_EventID
	cmp	eax,8193
	je	_571
	cmp	eax,513
	je	_572
	cmp	eax,514
	je	_573
	cmp	eax,1025
	je	_574
	cmp	eax,1027
	je	_575
	cmp	eax,1026
	je	_576
	cmp	eax,16387
	je	_577
	cmp	eax,16386
	je	_578
	cmp	eax,2049
	je	_579
	cmp	eax,8194
	je	_580
	jmp	_570
_571:
	push	_581
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	call	_brl_eventqueue_EventSource
	push	eax
	call	_bb_DoGadgetAction
	add	esp,4
	jmp	_570
_572:
	push	_582
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	call	_brl_eventqueue_EventData
	cmp	eax,38
	je	_585
	cmp	eax,40
	je	_586
	cmp	eax,37
	je	_587
	cmp	eax,39
	je	_588
	cmp	eax,87
	je	_589
	cmp	eax,65
	je	_590
	cmp	eax,83
	je	_591
	cmp	eax,68
	je	_592
	cmp	eax,32
	je	_593
	jmp	_584
_585:
	push	_594
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-20],1
	jmp	_584
_586:
	push	_595
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-24],1
	jmp	_584
_587:
	push	_596
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-28],1
	jmp	_584
_588:
	push	_597
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-32],1
	jmp	_584
_589:
	push	_598
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-44],1
	jmp	_584
_590:
	push	_599
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-48],1
	jmp	_584
_591:
	push	_600
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-52],1
	jmp	_584
_592:
	push	_601
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-56],1
	jmp	_584
_593:
	push	_602
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-40],1
_584:
	jmp	_570
_573:
	push	_603
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	call	_brl_eventqueue_EventData
	cmp	eax,38
	je	_606
	cmp	eax,40
	je	_607
	cmp	eax,37
	je	_608
	cmp	eax,39
	je	_609
	cmp	eax,87
	je	_610
	cmp	eax,65
	je	_611
	cmp	eax,83
	je	_612
	cmp	eax,68
	je	_613
	cmp	eax,32
	je	_614
	jmp	_605
_606:
	push	_615
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-20],0
	jmp	_605
_607:
	push	_616
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-24],0
	jmp	_605
_608:
	push	_617
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-28],0
	jmp	_605
_609:
	push	_618
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-32],0
	jmp	_605
_610:
	push	_619
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-44],0
	jmp	_605
_611:
	push	_620
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-48],0
	jmp	_605
_612:
	push	_621
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-52],0
	jmp	_605
_613:
	push	_622
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-56],0
	jmp	_605
_614:
	push	_623
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-40],0
_605:
	jmp	_570
_574:
	push	_624
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	call	_brl_eventqueue_EventData
	cmp	eax,1
	je	_627
	cmp	eax,2
	je	_628
	jmp	_626
_627:
	push	_629
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-36],1
	jmp	_626
_628:
_626:
	jmp	_570
_575:
	push	_630
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	call	_brl_eventqueue_EventX
	mov	dword [ebp+-408],eax
	fild	dword [ebp+-408]
	fstp	dword [_bb_MouseXEndPosi]
	push	_631
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	call	_brl_eventqueue_EventY
	mov	dword [ebp+-408],eax
	fild	dword [ebp+-408]
	fstp	dword [_bb_MouseYEndPosi]
	jmp	_570
_576:
	push	_632
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	call	_brl_eventqueue_EventData
	cmp	eax,dword [ebp-36]
	je	_635
	jmp	_634
_635:
	push	_636
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-36],0
_634:
	push	_637
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-68],0
	jmp	_570
_577:
	push	_639
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	call	_bbEnd
	jmp	_570
_578:
	push	_640
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [_bb_Window]
	mov	dword [ebp-392],eax
	cmp	dword [ebp-392],_bbNullObject
	jne	_642
	call	_brl_blitz_NullObjectError
_642:
	push	dword [ebp-392]
	mov	eax,dword [ebp-392]
	mov	eax,dword [eax]
	call	dword [eax+188]
	add	esp,4
	mov	dword [__sidesign_minib3d_TGlobal_width],eax
	push	_643
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [_bb_Window]
	mov	dword [ebp-396],eax
	cmp	dword [ebp-396],_bbNullObject
	jne	_645
	call	_brl_blitz_NullObjectError
_645:
	push	dword [ebp-396]
	mov	eax,dword [ebp-396]
	mov	eax,dword [eax]
	call	dword [eax+192]
	add	esp,4
	mov	dword [__sidesign_minib3d_TGlobal_height],eax
	jmp	_570
_579:
	push	_646
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	fldz
	fstp	dword [ebp-8]
	push	_648
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	fldz
	fstp	dword [ebp-12]
	push	_650
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	cmp	dword [ebp-20],0
	je	_651
	push	_652
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	fld	dword [ebp-12]
	fadd	dword [_1401]
	fstp	dword [ebp-12]
_651:
	push	_653
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	cmp	dword [ebp-28],0
	je	_654
	push	_655
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	fld	dword [ebp-8]
	fsub	dword [_1402]
	fstp	dword [ebp-8]
_654:
	push	_656
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	cmp	dword [ebp-32],0
	je	_657
	push	_658
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	fld	dword [ebp-8]
	fadd	dword [_1403]
	fstp	dword [ebp-8]
_657:
	push	_659
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	cmp	dword [ebp-24],0
	je	_660
	push	_661
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	fld	dword [ebp-12]
	fsub	dword [_1404]
	fstp	dword [ebp-12]
_660:
	push	_662
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	cmp	dword [ebp-56],0
	je	_663
	push	_664
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	fld	dword [_bb_KeyX]
	fsub	dword [_1405]
	fstp	dword [_bb_KeyX]
_663:
	push	_665
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	cmp	dword [ebp-40],0
	je	_666
	push	_667
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [_bb_Mouse]
	mov	dword [ebp-400],eax
	cmp	dword [ebp-400],_bbNullObject
	jne	_669
	call	_brl_blitz_NullObjectError
_669:
	mov	eax,dword [_bb_Mouse]
	mov	dword [ebp-404],eax
	cmp	dword [ebp-404],_bbNullObject
	jne	_671
	call	_brl_blitz_NullObjectError
_671:
	push	0
	push	0
	mov	eax,dword [ebp-404]
	mov	eax,dword [eax+8]
	mov	dword [ebp+-408],eax
	fild	dword [ebp+-408]
	sub	esp,4
	fstp	dword [esp]
	mov	eax,dword [ebp-400]
	mov	eax,dword [eax+12]
	mov	dword [ebp+-408],eax
	fild	dword [ebp+-408]
	sub	esp,4
	fstp	dword [esp]
	push	dword [_bb_camera]
	call	_sidesign_minib3d_RotateEntity
	add	esp,20
_666:
	push	_672
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	fld	dword [ebp-12]
	fmul	dword [_1406]
	sub	esp,4
	fstp	dword [esp]
	fld	dword [ebp-16]
	fmul	dword [_1407]
	sub	esp,4
	fstp	dword [esp]
	fld	dword [ebp-8]
	fmul	dword [_1408]
	sub	esp,4
	fstp	dword [esp]
	push	dword [_bb_camera]
	call	_sidesign_minib3d_MoveEntity
	add	esp,16
	push	_674
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [_bb_cavCanvas0]
	call	_brl_maxgui_RedrawGadget
	add	esp,4
	jmp	_570
_580:
	push	_675
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	call	_sidesign_minib3d_RenderWorld
	push	_676
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	add	dword [ebp-72],1
	push	_678
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	call	_bbMilliSecs
	sub	eax,dword [ebp-76]
	cmp	eax,1000
	jl	_680
	push	_681
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	call	_bbMilliSecs
	mov	dword [ebp-76],eax
	push	_682
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [ebp-72]
	mov	dword [ebp-80],eax
	push	_684
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-72],0
_680:
	push	_685
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	-1
	call	_brl_graphics_Flip
	add	esp,4
_570:
_65:
	mov	eax,1
	cmp	eax,0
	jne	_67
_66:
	mov	ebx,0
_122:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_bb_b3dSetFile:
	push	ebp
	mov	ebp,esp
	sub	esp,4
	push	ebx
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	push	ebp
	push	_794
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_792
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [_bb_b3d_tos],0
	push	_793
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [ebp-4]
	mov	dword [_bb_b3d_file],eax
	mov	ebx,0
_125:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_bb_b3dReadByte:
	push	ebp
	mov	ebp,esp
	push	ebx
	push	ebp
	push	_798
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_797
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_brl_stream_TStream
	push	dword [_bb_b3d_file]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_brl_stream_ReadByte
	add	esp,4
	mov	ebx,eax
_127:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_bb_b3dReadInt:
	push	ebp
	mov	ebp,esp
	push	ebx
	push	ebp
	push	_801
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_800
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_brl_stream_TStream
	push	dword [_bb_b3d_file]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_brl_stream_ReadInt
	add	esp,4
	mov	ebx,eax
_129:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_bb_b3dReadFloat:
	push	ebp
	mov	ebp,esp
	sub	esp,4
	push	ebp
	push	_804
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_803
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_brl_stream_TStream
	push	dword [_bb_b3d_file]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_brl_stream_ReadFloat
	add	esp,4
	fstp	dword [ebp-4]
_131:
	call	dword [_bbOnDebugLeaveScope]
	fld	dword [ebp-4]
	mov	esp,ebp
	pop	ebp
	ret
_bb_b3dReadString:
	push	ebp
	mov	ebp,esp
	sub	esp,20
	mov	dword [ebp-4],0
	push	ebp
	push	_817
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_806
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
_21:
_19:
	push	_807
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	call	_bb_b3dReadByte
	mov	dword [ebp-4],eax
	push	_809
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	cmp	dword [ebp-4],0
	jne	_810
	push	_811
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [_bb_t]
	mov	dword [ebp-8],eax
	jmp	_133
_810:
	push	_812
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-4]
	call	_bbStringFromChar
	add	esp,4
	push	eax
	push	dword [_bb_t]
	call	_bbStringConcat
	add	esp,8
	mov	dword [ebp-12],eax
	mov	eax,dword [ebp-12]
	inc	dword [eax+4]
	mov	eax,dword [ebp-12]
	mov	dword [ebp-16],eax
	mov	eax,dword [_bb_t]
	mov	dword [ebp-20],eax
	mov	eax,dword [ebp-20]
	dec	dword [eax+4]
	jnz	_816
	push	dword [ebp-20]
	call	_bbGCFree
	add	esp,4
_816:
	mov	eax,dword [ebp-16]
	mov	dword [_bb_t],eax
	jmp	_21
_133:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,dword [ebp-8]
	mov	esp,ebp
	pop	ebp
	ret
_bb_b3dReadChunk:
	push	ebp
	mov	ebp,esp
	sub	esp,20
	push	ebx
	mov	dword [ebp-8],0
	mov	dword [ebp-4],_bbEmptyString
	mov	dword [ebp-12],0
	push	ebp
	push	_833
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_820
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-8],1
	jmp	_822
_24:
	push	_823
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	call	_bb_b3dReadByte
	push	eax
	call	_bbStringFromChar
	add	esp,4
	push	eax
	push	dword [ebp-4]
	call	_bbStringConcat
	add	esp,8
	mov	dword [ebp-4],eax
_22:
	add	dword [ebp-8],1
_822:
	cmp	dword [ebp-8],4
	jle	_24
_23:
	push	_825
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_brl_stream_TStream
	push	dword [_bb_b3d_file]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_brl_stream_ReadInt
	add	esp,4
	mov	dword [ebp-12],eax
	push	_827
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	add	dword [_bb_b3d_tos],1
	push	_828
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [_bb_b3d_tos]
	mov	eax,dword [_bb_b3d_stack]
	cmp	ebx,dword [eax+20]
	jb	_830
	call	_brl_blitz_ArrayBoundsError
_830:
	mov	eax,dword [_bb_b3d_stack]
	shl	ebx,2
	add	eax,ebx
	mov	dword [ebp-16],eax
	mov	ebx,dword [ebp-16]
	push	_brl_stream_TStream
	push	dword [_bb_b3d_file]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_brl_stream_StreamPos
	add	esp,4
	add	eax,dword [ebp-12]
	mov	dword [ebx+24],eax
	push	_832
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [ebp-4]
	mov	dword [ebp-20],eax
_135:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,dword [ebp-20]
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_bb_b3dExitChunk:
	push	ebp
	mov	ebp,esp
	push	ebx
	push	ebp
	push	_842
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_838
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [_bb_b3d_tos]
	mov	eax,dword [_bb_b3d_stack]
	cmp	ebx,dword [eax+20]
	jb	_840
	call	_brl_blitz_ArrayBoundsError
_840:
	mov	eax,dword [_bb_b3d_stack]
	push	dword [eax+ebx*4+24]
	push	_brl_stream_TStream
	push	dword [_bb_b3d_file]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_brl_stream_SeekStream
	add	esp,8
	push	_841
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	sub	dword [_bb_b3d_tos],1
	mov	ebx,0
_137:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_bb_b3dChunkSize:
	push	ebp
	mov	ebp,esp
	push	ebx
	push	ebp
	push	_847
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_844
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [_bb_b3d_tos]
	mov	eax,dword [_bb_b3d_stack]
	cmp	ebx,dword [eax+20]
	jb	_846
	call	_brl_blitz_ArrayBoundsError
_846:
	mov	eax,dword [_bb_b3d_stack]
	mov	ebx,dword [eax+ebx*4+24]
	push	_brl_stream_TStream
	push	dword [_bb_b3d_file]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_brl_stream_StreamPos
	add	esp,4
	sub	ebx,eax
_139:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_bb_b3dWriteByte:
	push	ebp
	mov	ebp,esp
	sub	esp,4
	push	ebx
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	push	ebp
	push	_850
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_849
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-4]
	push	_brl_stream_TStream
	push	dword [_bb_b3d_file]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_brl_stream_WriteByte
	add	esp,8
	mov	ebx,0
_142:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_bb_b3dWriteInt:
	push	ebp
	mov	ebp,esp
	sub	esp,4
	push	ebx
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	push	ebp
	push	_854
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_853
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-4]
	push	_brl_stream_TStream
	push	dword [_bb_b3d_file]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_brl_stream_WriteInt
	add	esp,8
	mov	ebx,0
_145:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_bb_b3dWriteFloat:
	push	ebp
	mov	ebp,esp
	sub	esp,4
	push	ebx
	fld	dword [ebp+8]
	fstp	dword [ebp-4]
	push	ebp
	push	_857
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_856
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-4]
	push	_brl_stream_TStream
	push	dword [_bb_b3d_file]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_brl_stream_WriteFloat
	add	esp,8
	mov	ebx,0
_148:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_bb_b3dWriteString:
	push	ebp
	mov	ebp,esp
	sub	esp,12
	push	ebx
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	mov	dword [ebp-8],0
	mov	dword [ebp-12],0
	push	ebp
	push	_870
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_859
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-8],1
	mov	eax,dword [ebp-4]
	mov	ebx,dword [eax+8]
	jmp	_861
_27:
	push	_863
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	1
	push	dword [ebp-8]
	push	dword [ebp-4]
	call	_brl_retro_Mid
	add	esp,12
	push	eax
	call	_bbStringAsc
	add	esp,4
	mov	dword [ebp-12],eax
	push	_865
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-12]
	call	_bb_b3dWriteByte
	add	esp,4
	push	_866
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	cmp	dword [ebp-12],0
	jne	_867
	push	_868
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,0
	jmp	_151
_867:
_25:
	add	dword [ebp-8],1
_861:
	cmp	dword [ebp-8],ebx
	jle	_27
_26:
	push	_869
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	0
	call	_bb_b3dWriteByte
	add	esp,4
	mov	ebx,0
_151:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_bb_b3dBeginChunk:
	push	ebp
	mov	ebp,esp
	sub	esp,12
	push	ebx
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	mov	dword [ebp-8],0
	push	ebp
	push	_882
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_872
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	add	dword [_bb_b3d_tos],1
	push	_873
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-8],1
	jmp	_875
_30:
	push	_876
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	1
	push	dword [ebp-8]
	push	dword [ebp-4]
	call	_brl_retro_Mid
	add	esp,12
	push	eax
	call	_bbStringAsc
	add	esp,4
	push	eax
	call	_bb_b3dWriteByte
	add	esp,4
_28:
	add	dword [ebp-8],1
_875:
	cmp	dword [ebp-8],4
	jle	_30
_29:
	push	_877
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	0
	call	_bb_b3dWriteInt
	add	esp,4
	push	_878
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [_bb_b3d_tos]
	mov	eax,dword [_bb_b3d_stack]
	cmp	ebx,dword [eax+20]
	jb	_880
	call	_brl_blitz_ArrayBoundsError
_880:
	mov	eax,dword [_bb_b3d_stack]
	shl	ebx,2
	add	eax,ebx
	mov	dword [ebp-12],eax
	mov	ebx,dword [ebp-12]
	push	_brl_stream_TStream
	push	dword [_bb_b3d_file]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_brl_stream_StreamPos
	add	esp,4
	mov	dword [ebx+24],eax
	mov	ebx,0
_154:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_bb_b3dEndChunk:
	push	ebp
	mov	ebp,esp
	sub	esp,4
	push	ebx
	mov	dword [ebp-4],0
	push	ebp
	push	_894
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_884
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_brl_stream_TStream
	push	dword [_bb_b3d_file]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_brl_stream_StreamPos
	add	esp,4
	mov	dword [ebp-4],eax
	push	_886
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [_bb_b3d_tos]
	mov	eax,dword [_bb_b3d_stack]
	cmp	ebx,dword [eax+20]
	jb	_888
	call	_brl_blitz_ArrayBoundsError
_888:
	mov	eax,dword [_bb_b3d_stack]
	mov	eax,dword [eax+ebx*4+24]
	sub	eax,4
	push	eax
	push	_brl_stream_TStream
	push	dword [_bb_b3d_file]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_brl_stream_SeekStream
	add	esp,8
	push	_889
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [_bb_b3d_tos]
	mov	eax,dword [_bb_b3d_stack]
	cmp	ebx,dword [eax+20]
	jb	_891
	call	_brl_blitz_ArrayBoundsError
_891:
	mov	edx,dword [ebp-4]
	mov	eax,dword [_bb_b3d_stack]
	sub	edx,dword [eax+ebx*4+24]
	push	edx
	call	_bb_b3dWriteInt
	add	esp,4
	push	_892
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-4]
	push	_brl_stream_TStream
	push	dword [_bb_b3d_file]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_brl_stream_SeekStream
	add	esp,8
	push	_893
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	sub	dword [_bb_b3d_tos],1
	mov	ebx,0
_156:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_bb_DoGadgetAction:
	push	ebp
	mov	ebp,esp
	sub	esp,160
	push	ebx
	push	esi
	push	edi
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	mov	dword [ebp-40],0
	mov	dword [ebp-8],_bbEmptyString
	mov	dword [ebp-44],0
	mov	dword [ebp-12],_bbNullObject
	mov	dword [ebp-16],_bbEmptyString
	mov	dword [ebp-48],0
	mov	dword [ebp-52],0
	mov	dword [ebp-56],0
	mov	dword [ebp-60],0
	mov	dword [ebp-64],0
	mov	dword [ebp-68],0
	mov	dword [ebp-72],0
	mov	dword [ebp-76],0
	mov	dword [ebp-20],_bbNullObject
	mov	dword [ebp-24],_bbEmptyString
	mov	dword [ebp-80],0
	mov	dword [ebp-28],_bbNullObject
	mov	dword [ebp-32],_bbEmptyString
	mov	dword [ebp-84],0
	mov	dword [ebp-36],_bbEmptyString
	mov	eax,ebp
	push	eax
	push	_1109
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_896
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [ebp-4]
	mov	dword [ebp-88],eax
	mov	eax,dword [_bb_SetWindowCnt]
	cmp	dword [ebp-88],eax
	je	_899
	mov	eax,dword [_bb_SetWindowTex]
	cmp	dword [ebp-88],eax
	je	_900
	mov	eax,dword [_bb_tabTabber0]
	cmp	dword [ebp-88],eax
	je	_901
	mov	eax,dword [_bb_trvTreeView0]
	cmp	dword [ebp-88],eax
	je	_902
	mov	eax,dword [_bb_trvTreeView1]
	cmp	dword [ebp-88],eax
	je	_903
	mov	eax,dword [_bb_trvTreeView4]
	cmp	dword [ebp-88],eax
	je	_904
	mov	eax,dword [_bb_exportmeshbutt]
	cmp	dword [ebp-88],eax
	je	_905
	mov	eax,dword [_bb_sidebutt]
	cmp	dword [ebp-88],eax
	je	_906
	jmp	_898
_899:
	push	_907
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [_bb_cobComboBox1]
	call	_brl_maxgui_SelectedGadgetItem
	add	esp,4
	push	eax
	push	dword [_bb_cobComboBox1]
	call	_brl_maxgui_GadgetItemText
	add	esp,8
	mov	dword [ebp-92],eax
	mov	eax,dword [ebp-92]
	inc	dword [eax+4]
	mov	eax,dword [ebp-92]
	mov	dword [ebp-96],eax
	mov	eax,dword [_bb_WindowCnt]
	mov	dword [ebp-100],eax
	mov	eax,dword [ebp-100]
	dec	dword [eax+4]
	jnz	_911
	push	dword [ebp-100]
	call	_bbGCFree
	add	esp,4
_911:
	mov	eax,dword [ebp-96]
	mov	dword [_bb_WindowCnt],eax
	push	_912
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_68
	call	_brl_filesystem_ChangeDir
	add	esp,4
	push	_913
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-40],1
	push	dword [_bb_WindowCnt]
	call	_bbStringToInt
	add	esp,4
	mov	dword [ebp-156],eax
	jmp	_915
_71:
	push	_917
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-40]
	mov	eax,dword [_bb_WindowsCnt]
	cmp	ebx,dword [eax+20]
	jb	_919
	call	_brl_blitz_ArrayBoundsError
_919:
	mov	eax,dword [_bb_WindowsCnt]
	shl	ebx,2
	add	eax,ebx
	mov	dword [ebp-104],eax
	mov	ebx,dword [ebp-104]
	push	_bbNullObject
	push	_72
	call	_sidesign_minib3d_LoadMesh
	add	esp,8
	push	eax
	call	_bbHandleFromObject
	add	esp,4
	mov	dword [ebx+24],eax
	push	_921
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [ebp-40]
	mov	dword [ebp-152],eax
	mov	eax,dword [_bb_WindowsCnt]
	mov	eax,dword [eax+20]
	cmp	dword [ebp-152],eax
	jb	_923
	call	_brl_blitz_ArrayBoundsError
_923:
	mov	edi,dword [ebp-40]
	mov	eax,dword [_bb_WindowsX]
	cmp	edi,dword [eax+20]
	jb	_925
	call	_brl_blitz_ArrayBoundsError
_925:
	mov	esi,dword [ebp-40]
	mov	eax,dword [_bb_WindowsY]
	cmp	esi,dword [eax+20]
	jb	_927
	call	_brl_blitz_ArrayBoundsError
_927:
	mov	ebx,dword [ebp-40]
	mov	eax,dword [_bb_WindowsZ]
	cmp	ebx,dword [eax+20]
	jb	_929
	call	_brl_blitz_ArrayBoundsError
_929:
	push	0
	mov	eax,dword [_bb_WindowsZ]
	mov	eax,dword [eax+ebx*4+24]
	mov	dword [ebp+-160],eax
	fild	dword [ebp+-160]
	sub	esp,4
	fstp	dword [esp]
	mov	eax,dword [_bb_WindowsY]
	mov	eax,dword [eax+esi*4+24]
	mov	dword [ebp+-160],eax
	fild	dword [ebp+-160]
	sub	esp,4
	fstp	dword [esp]
	mov	eax,dword [_bb_WindowsX]
	mov	eax,dword [eax+edi*4+24]
	mov	dword [ebp+-160],eax
	fild	dword [ebp+-160]
	sub	esp,4
	fstp	dword [esp]
	push	_sidesign_minib3d_TEntity
	mov	edx,dword [_bb_WindowsCnt]
	mov	eax,dword [ebp-152]
	push	dword [edx+eax*4+24]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_sidesign_minib3d_PositionEntity
	add	esp,20
_69:
	add	dword [ebp-40],1
_915:
	mov	eax,dword [ebp-156]
	cmp	dword [ebp-40],eax
	jle	_71
_70:
	jmp	_898
_900:
	push	_930
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [_bb_windowselected]
	call	_brl_maxgui_SelectedGadgetItem
	add	esp,4
	push	eax
	push	dword [_bb_windowselected]
	call	_brl_maxgui_GadgetItemText
	add	esp,8
	mov	dword [ebp-108],eax
	mov	eax,dword [ebp-108]
	inc	dword [eax+4]
	mov	eax,dword [ebp-108]
	mov	dword [ebp-112],eax
	mov	eax,dword [_bb_WindowCnt]
	mov	dword [ebp-116],eax
	mov	eax,dword [ebp-116]
	dec	dword [eax+4]
	jnz	_934
	push	dword [ebp-116]
	call	_bbGCFree
	add	esp,4
_934:
	mov	eax,dword [ebp-112]
	mov	dword [_bb_WindowCnt],eax
	push	_935
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_1
	push	0
	push	_74
	push	_73
	call	_brl_system_RequestFile
	add	esp,16
	mov	dword [ebp-8],eax
	push	_937
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [_bb_WindowCnt]
	call	_bbStringToInt
	add	esp,4
	mov	ebx,eax
	mov	eax,dword [_bb_WindowsTex]
	cmp	ebx,dword [eax+20]
	jb	_939
	call	_brl_blitz_ArrayBoundsError
_939:
	mov	eax,dword [_bb_WindowsTex]
	shl	ebx,2
	add	eax,ebx
	mov	dword [ebp-120],eax
	mov	ebx,dword [ebp-120]
	push	1
	push	dword [ebp-8]
	call	_sidesign_minib3d_LoadTexture
	add	esp,8
	push	eax
	call	_bbHandleFromObject
	add	esp,4
	mov	dword [ebx+24],eax
	push	_941
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [_bb_WindowCnt]
	call	_bbStringToInt
	add	esp,4
	mov	ebx,eax
	mov	eax,dword [_bb_WindowsCnt]
	cmp	ebx,dword [eax+20]
	jb	_943
	call	_brl_blitz_ArrayBoundsError
_943:
	push	dword [_bb_WindowCnt]
	call	_bbStringToInt
	add	esp,4
	mov	esi,eax
	mov	eax,dword [_bb_WindowsTex]
	cmp	esi,dword [eax+20]
	jb	_945
	call	_brl_blitz_ArrayBoundsError
_945:
	push	0
	push	_sidesign_minib3d_TTexture
	mov	eax,dword [_bb_WindowsTex]
	push	dword [eax+esi*4+24]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	push	_sidesign_minib3d_TMesh
	mov	eax,dword [_bb_WindowsCnt]
	push	dword [eax+ebx*4+24]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_bb_TextureLoadedMesh
	add	esp,12
	jmp	_898
_901:
	push	_946
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [_bb_tabTabber0]
	call	_brl_maxgui_SelectedGadgetItem
	add	esp,4
	mov	dword [ebp-44],eax
	push	_948
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [ebp-44]
	cmp	eax,0
	je	_951
	cmp	eax,1
	je	_952
	cmp	eax,2
	je	_953
	cmp	eax,3
	je	_954
	jmp	_950
_951:
	push	_955
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [_bb_TabPanel0]
	call	_brl_maxgui_ShowGadget
	add	esp,4
	push	_956
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [_bb_TabPanel1]
	call	_brl_maxgui_HideGadget
	add	esp,4
	push	_957
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [_bb_TabPanel2]
	call	_brl_maxgui_HideGadget
	add	esp,4
	push	_958
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [_bb_TabPanel3]
	call	_brl_maxgui_HideGadget
	add	esp,4
	jmp	_950
_952:
	push	_959
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [_bb_TabPanel0]
	call	_brl_maxgui_HideGadget
	add	esp,4
	push	_960
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [_bb_TabPanel1]
	call	_brl_maxgui_ShowGadget
	add	esp,4
	push	_961
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [_bb_TabPanel2]
	call	_brl_maxgui_HideGadget
	add	esp,4
	push	_962
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [_bb_TabPanel3]
	call	_brl_maxgui_HideGadget
	add	esp,4
	jmp	_950
_953:
	push	_963
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [_bb_TabPanel0]
	call	_brl_maxgui_HideGadget
	add	esp,4
	push	_964
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [_bb_TabPanel1]
	call	_brl_maxgui_HideGadget
	add	esp,4
	push	_965
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [_bb_TabPanel2]
	call	_brl_maxgui_ShowGadget
	add	esp,4
	push	_966
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [_bb_TabPanel3]
	call	_brl_maxgui_HideGadget
	add	esp,4
	jmp	_950
_954:
	push	_967
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [_bb_TabPanel0]
	call	_brl_maxgui_HideGadget
	add	esp,4
	push	_968
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [_bb_TabPanel1]
	call	_brl_maxgui_HideGadget
	add	esp,4
	push	_969
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [_bb_TabPanel2]
	call	_brl_maxgui_HideGadget
	add	esp,4
	push	_970
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [_bb_TabPanel3]
	call	_brl_maxgui_ShowGadget
	add	esp,4
_950:
	jmp	_898
_902:
	push	_971
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [_bb_trvTreeView0]
	call	_brl_maxgui_SelectedTreeViewNode
	add	esp,4
	mov	dword [ebp-12],eax
	push	_973
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [ebp-12]
	mov	dword [ebp-124],eax
	cmp	dword [ebp-124],_bbNullObject
	jne	_975
	call	_brl_blitz_NullObjectError
_975:
	mov	eax,dword [ebp-124]
	mov	eax,dword [eax+36]
	mov	dword [ebp-16],eax
	push	_977
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_68
	call	_brl_filesystem_ChangeDir
	add	esp,4
	push	_978
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_bbNullObject
	push	_76
	push	dword [ebp-16]
	push	_75
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_sidesign_minib3d_LoadAnimMesh
	add	esp,8
	push	eax
	call	_bbHandleFromObject
	add	esp,4
	mov	dword [_bb_BaseModel],eax
	push	_979
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_bbNullObject
	push	_76
	push	dword [ebp-16]
	push	_77
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_sidesign_minib3d_LoadMesh
	add	esp,8
	push	eax
	call	_bbHandleFromObject
	add	esp,4
	mov	dword [_bb_BaseModelBase],eax
	push	_980
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	1
	push	_sidesign_minib3d_TEntity
	push	dword [_bb_BaseModelBase]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_sidesign_minib3d_EntityFX
	add	esp,8
	push	_981
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_78
	push	_sidesign_minib3d_TEntity
	push	dword [_bb_BaseModel]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_sidesign_minib3d_FindChild
	add	esp,8
	push	eax
	call	_bbHandleFromObject
	add	esp,4
	mov	dword [_bb_RoofPoint],eax
	push	_982
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_79
	push	_sidesign_minib3d_TEntity
	push	dword [_bb_BaseModel]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_sidesign_minib3d_FindChild
	add	esp,8
	push	eax
	call	_bbHandleFromObject
	add	esp,4
	mov	dword [_bb_DoorPoint],eax
	push	_983
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	0
	push	_sidesign_minib3d_TEntity
	push	dword [_bb_RoofPoint]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_sidesign_minib3d_EntityX
	add	esp,8
	fstp	dword [_bb_RoofX]
	push	_984
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	0
	push	_sidesign_minib3d_TEntity
	push	dword [_bb_RoofPoint]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_sidesign_minib3d_EntityY
	add	esp,8
	fstp	dword [_bb_RoofY]
	push	_985
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	0
	push	_sidesign_minib3d_TEntity
	push	dword [_bb_RoofPoint]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_sidesign_minib3d_EntityZ
	add	esp,8
	fstp	dword [_bb_RoofZ]
	push	_986
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	0
	push	_sidesign_minib3d_TEntity
	push	dword [_bb_DoorPoint]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_sidesign_minib3d_EntityX
	add	esp,8
	fstp	dword [_bb_DoorX]
	push	_987
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	0
	push	_sidesign_minib3d_TEntity
	push	dword [_bb_DoorPoint]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_sidesign_minib3d_EntityY
	add	esp,8
	fstp	dword [_bb_DoorY]
	push	_988
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	0
	push	_sidesign_minib3d_TEntity
	push	dword [_bb_DoorPoint]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_sidesign_minib3d_EntityZ
	add	esp,8
	fstp	dword [_bb_DoorZ]
	push	_989
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-48],1
	jmp	_991
_82:
	push	_992
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-48]
	mov	eax,dword [_bb_WinChild]
	cmp	ebx,dword [eax+20]
	jb	_994
	call	_brl_blitz_ArrayBoundsError
_994:
	mov	eax,dword [_bb_WinChild]
	shl	ebx,2
	add	eax,ebx
	mov	dword [ebp-128],eax
	mov	ebx,dword [ebp-128]
	push	dword [ebp-48]
	call	_bbStringFromInt
	add	esp,4
	push	eax
	push	_83
	call	_bbStringConcat
	add	esp,8
	push	eax
	push	_sidesign_minib3d_TEntity
	push	dword [_bb_BaseModel]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_sidesign_minib3d_FindChild
	add	esp,8
	push	eax
	call	_bbHandleFromObject
	add	esp,4
	mov	dword [ebx+24],eax
	push	_996
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-48]
	mov	eax,dword [_bb_WinChild]
	cmp	ebx,dword [eax+20]
	jb	_998
	call	_brl_blitz_ArrayBoundsError
_998:
	mov	eax,dword [_bb_WinChild]
	cmp	dword [eax+ebx*4+24],0
	je	_999
	push	_1000
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-48]
	mov	eax,dword [_bb_WindowsX]
	cmp	ebx,dword [eax+20]
	jb	_1002
	call	_brl_blitz_ArrayBoundsError
_1002:
	mov	eax,dword [_bb_WindowsX]
	shl	ebx,2
	add	eax,ebx
	mov	dword [ebp-132],eax
	mov	esi,dword [ebp-48]
	mov	eax,dword [_bb_WinChild]
	cmp	esi,dword [eax+20]
	jb	_1005
	call	_brl_blitz_ArrayBoundsError
_1005:
	mov	ebx,dword [ebp-132]
	push	0
	push	_sidesign_minib3d_TEntity
	mov	eax,dword [_bb_WinChild]
	push	dword [eax+esi*4+24]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_sidesign_minib3d_EntityX
	add	esp,8
	sub	esp,8
	fstp	qword [esp]
	call	_bbFloatToInt
	add	esp,8
	mov	dword [ebx+24],eax
	push	_1006
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-48]
	mov	eax,dword [_bb_WindowsY]
	cmp	ebx,dword [eax+20]
	jb	_1008
	call	_brl_blitz_ArrayBoundsError
_1008:
	mov	eax,dword [_bb_WindowsY]
	shl	ebx,2
	add	eax,ebx
	mov	dword [ebp-136],eax
	mov	esi,dword [ebp-48]
	mov	eax,dword [_bb_WinChild]
	cmp	esi,dword [eax+20]
	jb	_1011
	call	_brl_blitz_ArrayBoundsError
_1011:
	mov	ebx,dword [ebp-136]
	push	0
	push	_sidesign_minib3d_TEntity
	mov	eax,dword [_bb_WinChild]
	push	dword [eax+esi*4+24]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_sidesign_minib3d_EntityY
	add	esp,8
	sub	esp,8
	fstp	qword [esp]
	call	_bbFloatToInt
	add	esp,8
	mov	dword [ebx+24],eax
	push	_1012
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-48]
	mov	eax,dword [_bb_WindowsZ]
	cmp	ebx,dword [eax+20]
	jb	_1014
	call	_brl_blitz_ArrayBoundsError
_1014:
	mov	eax,dword [_bb_WindowsZ]
	shl	ebx,2
	add	eax,ebx
	mov	dword [ebp-140],eax
	mov	esi,dword [ebp-48]
	mov	eax,dword [_bb_WinChild]
	cmp	esi,dword [eax+20]
	jb	_1017
	call	_brl_blitz_ArrayBoundsError
_1017:
	mov	ebx,dword [ebp-140]
	push	0
	push	_sidesign_minib3d_TEntity
	mov	eax,dword [_bb_WinChild]
	push	dword [eax+esi*4+24]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_sidesign_minib3d_EntityZ
	add	esp,8
	sub	esp,8
	fstp	qword [esp]
	call	_bbFloatToInt
	add	esp,8
	mov	dword [ebx+24],eax
	push	_1018
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-48]
	mov	eax,dword [_bb_WinChild]
	cmp	ebx,dword [eax+20]
	jb	_1020
	call	_brl_blitz_ArrayBoundsError
_1020:
	push	_sidesign_minib3d_TEntity
	mov	eax,dword [_bb_WinChild]
	push	dword [eax+ebx*4+24]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_sidesign_minib3d_FreeEntity
	add	esp,4
	jmp	_1021
_999:
	push	_1022
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	jmp	_81
_1021:
_80:
	add	dword [ebp-48],1
_991:
	cmp	dword [ebp-48],6
	jle	_82
_81:
	push	_1023
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-52],0
	push	_1025
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-56],0
	push	_1027
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-60],0
	push	_1029
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-64],1
	push	_1031
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-68],1
	push	_1033
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-72],1
	push	_1035
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [ebp-60]
	mov	dword [ebp+-160],eax
	fild	dword [ebp+-160]
	sub	esp,4
	fstp	dword [esp]
	mov	eax,dword [ebp-56]
	mov	dword [ebp+-160],eax
	fild	dword [ebp+-160]
	sub	esp,4
	fstp	dword [esp]
	mov	eax,dword [ebp-52]
	mov	dword [ebp+-160],eax
	fild	dword [ebp+-160]
	sub	esp,4
	fstp	dword [esp]
	push	_sidesign_minib3d_TMesh
	push	dword [_bb_BaseModel]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_sidesign_minib3d_PositionMesh
	add	esp,16
	push	_1036
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [ebp-60]
	mov	dword [ebp+-160],eax
	fild	dword [ebp+-160]
	sub	esp,4
	fstp	dword [esp]
	mov	eax,dword [ebp-56]
	mov	dword [ebp+-160],eax
	fild	dword [ebp+-160]
	sub	esp,4
	fstp	dword [esp]
	mov	eax,dword [ebp-52]
	mov	dword [ebp+-160],eax
	fild	dword [ebp+-160]
	sub	esp,4
	fstp	dword [esp]
	push	_sidesign_minib3d_TMesh
	push	dword [_bb_BaseModelBase]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_sidesign_minib3d_PositionMesh
	add	esp,16
	push	_1037
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_sidesign_minib3d_TEntity
	push	dword [_bb_BaseModel]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_sidesign_minib3d_FreeEntity
	add	esp,4
	push	_1038
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_sidesign_minib3d_TEntity
	push	dword [_bb_RoofPoint]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_sidesign_minib3d_FreeEntity
	add	esp,4
	push	_1039
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_sidesign_minib3d_TEntity
	push	dword [_bb_DoorPoint]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_sidesign_minib3d_FreeEntity
	add	esp,4
	push	_1040
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,1
	mov	eax,dword [_bb_WinChild]
	cmp	ebx,dword [eax+20]
	jb	_1042
	call	_brl_blitz_ArrayBoundsError
_1042:
	push	_sidesign_minib3d_TEntity
	mov	eax,dword [_bb_WinChild]
	push	dword [eax+ebx*4+24]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_sidesign_minib3d_FreeEntity
	add	esp,4
	push	_1043
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_84
	call	_brl_filesystem_ChangeDir
	add	esp,4
	push	_1044
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_77
	call	_brl_filesystem_CurrentDir
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	push	0
	push	_74
	push	_73
	call	_brl_system_RequestFile
	add	esp,16
	mov	dword [ebp-8],eax
	push	_1045
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	1
	push	dword [ebp-8]
	call	_sidesign_minib3d_LoadTexture
	add	esp,8
	push	eax
	call	_bbHandleFromObject
	add	esp,4
	mov	dword [ebp-76],eax
	push	_1047
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	0
	push	_sidesign_minib3d_TTexture
	push	dword [ebp-76]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	push	_sidesign_minib3d_TMesh
	push	dword [_bb_BaseModelBase]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_bb_TextureLoadedMesh
	add	esp,12
	jmp	_898
_903:
	push	_1048
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [_bb_trvTreeView1]
	call	_brl_maxgui_SelectedTreeViewNode
	add	esp,4
	mov	dword [ebp-20],eax
	push	_1050
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [ebp-20]
	mov	dword [ebp-144],eax
	cmp	dword [ebp-144],_bbNullObject
	jne	_1052
	call	_brl_blitz_NullObjectError
_1052:
	mov	eax,dword [ebp-144]
	mov	eax,dword [eax+36]
	mov	dword [ebp-24],eax
	push	_1054
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_68
	call	_brl_filesystem_ChangeDir
	add	esp,4
	push	_1055
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_bbNullObject
	push	_76
	push	dword [ebp-24]
	push	_85
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_sidesign_minib3d_LoadMesh
	add	esp,8
	push	eax
	call	_bbHandleFromObject
	add	esp,4
	mov	dword [_bb_RoofModel],eax
	push	_1056
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	1
	push	_sidesign_minib3d_TEntity
	push	dword [_bb_RoofModel]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_sidesign_minib3d_EntityFX
	add	esp,8
	push	_1057
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	fld	dword [_bb_RoofX]
	sub	esp,8
	fstp	qword [esp]
	call	_bbFloatToInt
	add	esp,8
	mov	dword [ebp-52],eax
	push	_1058
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	fld	dword [_bb_RoofY]
	sub	esp,8
	fstp	qword [esp]
	call	_bbFloatToInt
	add	esp,8
	mov	dword [ebp-56],eax
	push	_1059
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	fld	dword [_bb_RoofZ]
	sub	esp,8
	fstp	qword [esp]
	call	_bbFloatToInt
	add	esp,8
	mov	dword [ebp-60],eax
	push	_1060
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-64],1
	push	_1061
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-68],1
	push	_1062
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-72],1
	push	_1063
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [_bb_RoofZ]
	push	dword [_bb_RoofY]
	push	dword [_bb_RoofX]
	push	_sidesign_minib3d_TMesh
	push	dword [_bb_RoofModel]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_sidesign_minib3d_PositionMesh
	add	esp,16
	push	_1064
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_1
	push	0
	push	_74
	push	_73
	call	_brl_system_RequestFile
	add	esp,16
	mov	dword [ebp-8],eax
	push	_1065
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	1
	push	dword [ebp-8]
	call	_sidesign_minib3d_LoadTexture
	add	esp,8
	push	eax
	call	_bbHandleFromObject
	add	esp,4
	mov	dword [ebp-80],eax
	push	_1067
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	0
	push	_sidesign_minib3d_TTexture
	push	dword [ebp-80]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	push	_sidesign_minib3d_TMesh
	push	dword [_bb_RoofModel]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_bb_TextureLoadedMesh
	add	esp,12
	jmp	_898
_904:
	push	_1068
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [_bb_trvTreeView4]
	call	_brl_maxgui_SelectedTreeViewNode
	add	esp,4
	mov	dword [ebp-28],eax
	push	_1070
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [ebp-28]
	mov	dword [ebp-148],eax
	cmp	dword [ebp-148],_bbNullObject
	jne	_1072
	call	_brl_blitz_NullObjectError
_1072:
	mov	eax,dword [ebp-148]
	mov	eax,dword [eax+36]
	mov	dword [ebp-32],eax
	push	_1074
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_68
	call	_brl_filesystem_ChangeDir
	add	esp,4
	push	_1075
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_bbNullObject
	push	_76
	push	dword [ebp-32]
	push	_86
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_sidesign_minib3d_LoadMesh
	add	esp,8
	push	eax
	call	_bbHandleFromObject
	add	esp,4
	mov	dword [_bb_DoorModel],eax
	push	_1076
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	fld	dword [_bb_DoorX]
	sub	esp,8
	fstp	qword [esp]
	call	_bbFloatToInt
	add	esp,8
	mov	dword [ebp-52],eax
	push	_1077
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	fld	dword [_bb_DoorY]
	sub	esp,8
	fstp	qword [esp]
	call	_bbFloatToInt
	add	esp,8
	mov	dword [ebp-56],eax
	push	_1078
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	fld	dword [_bb_DoorZ]
	sub	esp,8
	fstp	qword [esp]
	call	_bbFloatToInt
	add	esp,8
	mov	dword [ebp-60],eax
	push	_1079
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-64],1
	push	_1080
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-68],1
	push	_1081
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-72],1
	push	_1082
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	0
	push	dword [_bb_DoorZ]
	push	dword [_bb_DoorY]
	push	dword [_bb_DoorX]
	push	_sidesign_minib3d_TEntity
	push	dword [_bb_DoorModel]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_sidesign_minib3d_PositionEntity
	add	esp,20
	push	_1083
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_1
	push	0
	push	_74
	push	_73
	call	_brl_system_RequestFile
	add	esp,16
	mov	dword [ebp-8],eax
	push	_1084
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	1
	push	dword [ebp-8]
	call	_sidesign_minib3d_LoadTexture
	add	esp,8
	push	eax
	call	_bbHandleFromObject
	add	esp,4
	mov	dword [ebp-84],eax
	push	_1086
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	0
	push	_sidesign_minib3d_TTexture
	push	dword [ebp-84]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	push	_sidesign_minib3d_TMesh
	push	dword [_bb_DoorModel]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_bb_TextureLoadedMesh
	add	esp,12
	jmp	_898
_905:
	push	_1087
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-48],1
	jmp	_1088
_89:
	push	_1089
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-48]
	mov	eax,dword [_bb_WindowsCnt]
	cmp	ebx,dword [eax+20]
	jb	_1091
	call	_brl_blitz_ArrayBoundsError
_1091:
	push	_sidesign_minib3d_TMesh
	mov	eax,dword [_bb_WindowsCnt]
	push	dword [eax+ebx*4+24]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	push	_sidesign_minib3d_TMesh
	push	dword [_bb_BaseModelBase]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_sidesign_minib3d_AddMesh
	add	esp,8
	push	_1092
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-48]
	sub	ebx,1
	mov	eax,dword [_bb_WindowsCnt]
	cmp	ebx,dword [eax+20]
	jb	_1094
	call	_brl_blitz_ArrayBoundsError
_1094:
	push	_sidesign_minib3d_TEntity
	mov	eax,dword [_bb_WindowsCnt]
	push	dword [eax+ebx*4+24]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_sidesign_minib3d_FreeEntity
	add	esp,4
	push	_1095
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_sidesign_minib3d_TEntity
	push	dword [_bb_BaseModelBase]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_sidesign_minib3d_FreeEntity
	add	esp,4
_87:
	add	dword [ebp-48],1
_1088:
	cmp	dword [ebp-48],6
	jle	_89
_88:
	push	_1096
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,6
	mov	eax,dword [_bb_WindowsCnt]
	cmp	ebx,dword [eax+20]
	jb	_1098
	call	_brl_blitz_ArrayBoundsError
_1098:
	push	_sidesign_minib3d_TMesh
	push	dword [_bb_DoorModel]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	push	_sidesign_minib3d_TMesh
	mov	eax,dword [_bb_WindowsCnt]
	push	dword [eax+ebx*4+24]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_sidesign_minib3d_AddMesh
	add	esp,8
	push	_1099
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_sidesign_minib3d_TMesh
	push	dword [_bb_DoorModel]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	push	_sidesign_minib3d_TMesh
	push	dword [_bb_RoofModel]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_sidesign_minib3d_AddMesh
	add	esp,8
	push	_1100
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,6
	mov	eax,dword [_bb_WindowsCnt]
	cmp	ebx,dword [eax+20]
	jb	_1102
	call	_brl_blitz_ArrayBoundsError
_1102:
	push	_sidesign_minib3d_TEntity
	mov	eax,dword [_bb_WindowsCnt]
	push	dword [eax+ebx*4+24]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_sidesign_minib3d_FreeEntity
	add	esp,4
	push	_1103
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_sidesign_minib3d_TEntity
	push	dword [_bb_RoofModel]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_sidesign_minib3d_FreeEntity
	add	esp,4
	jmp	_898
_906:
	push	_1104
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [_bb_cobComboBox1]
	call	_brl_maxgui_SelectedGadgetItem
	add	esp,4
	push	eax
	push	dword [_bb_cobComboBox1]
	call	_brl_maxgui_GadgetItemText
	add	esp,8
	mov	dword [ebp-36],eax
	push	_1106
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-36]
	call	_bbStringToInt
	add	esp,4
	mov	ebx,eax
	mov	eax,dword [_bb_WindowsCnt]
	cmp	ebx,dword [eax+20]
	jb	_1108
	call	_brl_blitz_ArrayBoundsError
_1108:
	push	0
	push	0
	push	1119092736
	push	0
	push	_sidesign_minib3d_TEntity
	mov	eax,dword [_bb_WindowsCnt]
	push	dword [eax+ebx*4+24]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_sidesign_minib3d_TurnEntity
	add	esp,20
_898:
	mov	ebx,0
_159:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_bb_enumFilesCallback:
	push	ebp
	mov	ebp,esp
	sub	esp,20
	push	ebx
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	mov	eax,dword [ebp+12]
	mov	dword [ebp-8],eax
	mov	dword [ebp-20],0
	mov	dword [ebp-12],_bbEmptyString
	mov	dword [ebp-16],_bbEmptyString
	push	ebp
	push	_1152
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1132
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-8]
	call	_brl_filesystem_ReadDir
	add	esp,4
	mov	dword [ebp-20],eax
	push	_1134
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-12],_bbEmptyString
	push	_1136
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
_92:
	push	_1137
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-20]
	call	_brl_filesystem_NextFile
	add	esp,4
	mov	dword [ebp-12],eax
	push	_1138
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [ebp-12]
	mov	eax,dword [eax+8]
	cmp	eax,0
	je	_1139
	push	_55
	push	dword [ebp-12]
	call	_bbStringCompare
	add	esp,8
	cmp	eax,0
	setne	al
	movzx	eax,al
_1139:
	cmp	eax,0
	je	_1141
	push	_56
	push	dword [ebp-12]
	call	_bbStringCompare
	add	esp,8
	cmp	eax,0
	setne	al
	movzx	eax,al
_1141:
	cmp	eax,0
	je	_1143
	push	_1144
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-12]
	push	_93
	push	dword [ebp-8]
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_brl_filesystem_RealPath
	add	esp,4
	mov	dword [ebp-16],eax
	push	_1146
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-16]
	call	_brl_filesystem_FileType
	add	esp,4
	cmp	eax,2
	jne	_1147
	push	_1148
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-16]
	push	dword [ebp-4]
	call	_bb_enumFilesCallback
	add	esp,8
	jmp	_1149
_1147:
	push	_1150
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-16]
	call	dword [ebp-4]
	add	esp,4
_1149:
_1143:
_90:
	push	_bbEmptyString
	push	dword [ebp-12]
	call	_bbStringCompare
	add	esp,8
	cmp	eax,0
	jne	_92
_91:
	push	_1151
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-20]
	call	_brl_filesystem_CloseDir
	add	esp,4
	mov	ebx,0
_163:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_bb_enumFoldersCallback:
	push	ebp
	mov	ebp,esp
	sub	esp,20
	push	ebx
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	mov	eax,dword [ebp+12]
	mov	dword [ebp-8],eax
	mov	dword [ebp-20],0
	mov	dword [ebp-12],_bbEmptyString
	mov	dword [ebp-16],_bbEmptyString
	push	ebp
	push	_1178
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1159
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-8]
	call	_brl_filesystem_ReadDir
	add	esp,4
	mov	dword [ebp-20],eax
	push	_1161
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-12],_bbEmptyString
	push	_1163
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
_96:
	push	_1164
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-20]
	call	_brl_filesystem_NextFile
	add	esp,4
	mov	dword [ebp-12],eax
	push	_1165
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [ebp-12]
	mov	eax,dword [eax+8]
	cmp	eax,0
	je	_1166
	push	_55
	push	dword [ebp-12]
	call	_bbStringCompare
	add	esp,8
	cmp	eax,0
	setne	al
	movzx	eax,al
_1166:
	cmp	eax,0
	je	_1168
	push	_56
	push	dword [ebp-12]
	call	_bbStringCompare
	add	esp,8
	cmp	eax,0
	setne	al
	movzx	eax,al
_1168:
	cmp	eax,0
	je	_1170
	push	_1171
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-12]
	push	_93
	push	dword [ebp-8]
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_brl_filesystem_RealPath
	add	esp,4
	mov	dword [ebp-16],eax
	push	_1173
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-16]
	call	_brl_filesystem_FileType
	add	esp,4
	cmp	eax,2
	jne	_1174
	push	_1175
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-16]
	call	dword [ebp-4]
	add	esp,4
	push	_1176
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-16]
	push	dword [ebp-4]
	call	_bb_enumFoldersCallback
	add	esp,8
_1174:
_1170:
_94:
	push	_bbEmptyString
	push	dword [ebp-12]
	call	_bbStringCompare
	add	esp,8
	cmp	eax,0
	jne	_96
_95:
	push	_1177
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-20]
	call	_brl_filesystem_CloseDir
	add	esp,4
	mov	ebx,0
_167:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_bb_enumFolders:
	push	ebp
	mov	ebp,esp
	sub	esp,24
	push	ebx
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	mov	eax,dword [ebp+12]
	mov	dword [ebp-8],eax
	mov	dword [ebp-20],0
	mov	dword [ebp-12],_bbEmptyString
	mov	dword [ebp-16],_bbEmptyString
	push	ebp
	push	_1201
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1180
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-8]
	call	_brl_filesystem_ReadDir
	add	esp,4
	mov	dword [ebp-20],eax
	push	_1182
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-12],_bbEmptyString
	push	_1184
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
_99:
	push	_1185
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-20]
	call	_brl_filesystem_NextFile
	add	esp,4
	mov	dword [ebp-12],eax
	push	_1186
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [ebp-12]
	mov	eax,dword [eax+8]
	cmp	eax,0
	je	_1187
	push	_55
	push	dword [ebp-12]
	call	_bbStringCompare
	add	esp,8
	cmp	eax,0
	setne	al
	movzx	eax,al
_1187:
	cmp	eax,0
	je	_1189
	push	_56
	push	dword [ebp-12]
	call	_bbStringCompare
	add	esp,8
	cmp	eax,0
	setne	al
	movzx	eax,al
_1189:
	cmp	eax,0
	je	_1191
	push	_1192
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-12]
	push	_93
	push	dword [ebp-8]
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_brl_filesystem_RealPath
	add	esp,4
	mov	dword [ebp-16],eax
	push	_1194
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-16]
	call	_brl_filesystem_FileType
	add	esp,4
	cmp	eax,2
	jne	_1195
	push	_1196
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [ebp-4]
	mov	dword [ebp-24],eax
	cmp	dword [ebp-24],_bbNullObject
	jne	_1198
	call	_brl_blitz_NullObjectError
_1198:
	push	dword [ebp-16]
	push	dword [ebp-24]
	mov	eax,dword [ebp-24]
	mov	eax,dword [eax]
	call	dword [eax+68]
	add	esp,8
	push	_1199
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-16]
	push	dword [ebp-4]
	call	_bb_enumFolders
	add	esp,8
_1195:
_1191:
_97:
	push	_bbEmptyString
	push	dword [ebp-12]
	call	_bbStringCompare
	add	esp,8
	cmp	eax,0
	jne	_99
_98:
	push	_1200
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-20]
	call	_brl_filesystem_CloseDir
	add	esp,4
	mov	ebx,0
_171:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_bb_enumFiles:
	push	ebp
	mov	ebp,esp
	sub	esp,24
	push	ebx
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	mov	eax,dword [ebp+12]
	mov	dword [ebp-8],eax
	mov	dword [ebp-20],0
	mov	dword [ebp-12],_bbEmptyString
	mov	dword [ebp-16],_bbEmptyString
	push	ebp
	push	_1226
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1204
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-8]
	call	_brl_filesystem_ReadDir
	add	esp,4
	mov	dword [ebp-20],eax
	push	_1206
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-12],_bbEmptyString
	push	_1208
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
_102:
	push	_1209
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-20]
	call	_brl_filesystem_NextFile
	add	esp,4
	mov	dword [ebp-12],eax
	push	_1210
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_55
	push	dword [ebp-12]
	call	_bbStringCompare
	add	esp,8
	cmp	eax,0
	setne	al
	movzx	eax,al
	cmp	eax,0
	je	_1211
	push	_56
	push	dword [ebp-12]
	call	_bbStringCompare
	add	esp,8
	cmp	eax,0
	setne	al
	movzx	eax,al
_1211:
	cmp	eax,0
	je	_1213
	mov	eax,dword [ebp-12]
	mov	eax,dword [eax+8]
_1213:
	cmp	eax,0
	je	_1215
	push	_1216
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-12]
	push	_93
	push	dword [ebp-8]
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_brl_filesystem_RealPath
	add	esp,4
	mov	dword [ebp-16],eax
	push	_1218
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-16]
	call	_brl_filesystem_FileType
	add	esp,4
	cmp	eax,2
	jne	_1219
	push	_1220
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-16]
	push	dword [ebp-4]
	call	_bb_enumFiles
	add	esp,8
	jmp	_1221
_1219:
	push	_1222
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [ebp-4]
	mov	dword [ebp-24],eax
	cmp	dword [ebp-24],_bbNullObject
	jne	_1224
	call	_brl_blitz_NullObjectError
_1224:
	push	dword [ebp-16]
	push	dword [ebp-24]
	mov	eax,dword [ebp-24]
	mov	eax,dword [eax]
	call	dword [eax+68]
	add	esp,8
_1221:
_1215:
_100:
	push	_bbEmptyString
	push	dword [ebp-12]
	call	_bbStringCompare
	add	esp,8
	cmp	eax,0
	jne	_102
_101:
	push	_1225
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-20]
	call	_brl_filesystem_CloseDir
	add	esp,4
	mov	ebx,0
_175:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_bb_MakeSkyBox:
	push	ebp
	mov	ebp,esp
	sub	esp,16
	push	ebx
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	mov	dword [ebp-8],0
	mov	dword [ebp-12],0
	mov	dword [ebp-16],0
	push	ebp
	push	_1266
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1228
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_bbNullObject
	call	_sidesign_minib3d_CreateMesh
	add	esp,4
	push	eax
	call	_bbHandleFromObject
	add	esp,4
	mov	dword [ebp-8],eax
	push	_1230
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	1065353216
	push	1065353216
	push	49
	push	_103
	push	dword [ebp-4]
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_sidesign_minib3d_LoadBrush
	add	esp,16
	push	eax
	call	_bbHandleFromObject
	add	esp,4
	mov	dword [ebp-12],eax
	push	_1232
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_bbNullObject
	push	_sidesign_minib3d_TMesh
	push	dword [ebp-8]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_sidesign_minib3d_CreateSurface
	add	esp,8
	push	eax
	call	_bbHandleFromObject
	add	esp,4
	mov	dword [ebp-16],eax
	push	_1234
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	0
	push	0
	push	0
	push	-1082130432
	push	1065353216
	push	-1082130432
	push	_sidesign_minib3d_TSurface
	push	dword [ebp-16]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_sidesign_minib3d_AddVertex
	add	esp,28
	push	_1235
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	0
	push	1065353216
	push	1065353216
	push	-1082130432
	push	-1082130432
	push	1065353216
	push	_sidesign_minib3d_TSurface
	push	dword [ebp-16]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_sidesign_minib3d_AddVertex
	add	esp,28
	push	_1236
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	2
	push	1
	push	0
	push	_sidesign_minib3d_TSurface
	push	dword [ebp-16]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_sidesign_minib3d_AddTriangle
	add	esp,16
	push	_1237
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_sidesign_minib3d_TBrush
	push	dword [ebp-12]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	push	_sidesign_minib3d_TSurface
	push	dword [ebp-16]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_sidesign_minib3d_PaintSurface
	add	esp,8
	push	_1238
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	1065353216
	push	1065353216
	push	49
	push	_104
	push	dword [ebp-4]
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_sidesign_minib3d_LoadBrush
	add	esp,16
	push	eax
	call	_bbHandleFromObject
	add	esp,4
	mov	dword [ebp-12],eax
	push	_1239
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_bbNullObject
	push	_sidesign_minib3d_TMesh
	push	dword [ebp-8]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_sidesign_minib3d_CreateSurface
	add	esp,8
	push	eax
	call	_bbHandleFromObject
	add	esp,4
	mov	dword [ebp-16],eax
	push	_1240
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	0
	push	0
	push	0
	push	-1082130432
	push	1065353216
	push	1065353216
	push	_sidesign_minib3d_TSurface
	push	dword [ebp-16]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_sidesign_minib3d_AddVertex
	add	esp,28
	push	_1241
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	0
	push	1065353216
	push	1065353216
	push	1065353216
	push	-1082130432
	push	1065353216
	push	_sidesign_minib3d_TSurface
	push	dword [ebp-16]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_sidesign_minib3d_AddVertex
	add	esp,28
	push	_1242
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	2
	push	1
	push	0
	push	_sidesign_minib3d_TSurface
	push	dword [ebp-16]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_sidesign_minib3d_AddTriangle
	add	esp,16
	push	_1243
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_sidesign_minib3d_TBrush
	push	dword [ebp-12]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	push	_sidesign_minib3d_TSurface
	push	dword [ebp-16]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_sidesign_minib3d_PaintSurface
	add	esp,8
	push	_1244
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	1065353216
	push	1065353216
	push	49
	push	_105
	push	dword [ebp-4]
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_sidesign_minib3d_LoadBrush
	add	esp,16
	push	eax
	call	_bbHandleFromObject
	add	esp,4
	mov	dword [ebp-12],eax
	push	_1245
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_bbNullObject
	push	_sidesign_minib3d_TMesh
	push	dword [ebp-8]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_sidesign_minib3d_CreateSurface
	add	esp,8
	push	eax
	call	_bbHandleFromObject
	add	esp,4
	mov	dword [ebp-16],eax
	push	_1246
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	0
	push	0
	push	0
	push	1065353216
	push	1065353216
	push	1065353216
	push	_sidesign_minib3d_TSurface
	push	dword [ebp-16]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_sidesign_minib3d_AddVertex
	add	esp,28
	push	_1247
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	0
	push	1065353216
	push	1065353216
	push	1065353216
	push	-1082130432
	push	-1082130432
	push	_sidesign_minib3d_TSurface
	push	dword [ebp-16]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_sidesign_minib3d_AddVertex
	add	esp,28
	push	_1248
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	2
	push	1
	push	0
	push	_sidesign_minib3d_TSurface
	push	dword [ebp-16]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_sidesign_minib3d_AddTriangle
	add	esp,16
	push	_1249
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_sidesign_minib3d_TBrush
	push	dword [ebp-12]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	push	_sidesign_minib3d_TSurface
	push	dword [ebp-16]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_sidesign_minib3d_PaintSurface
	add	esp,8
	push	_1250
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	1065353216
	push	1065353216
	push	49
	push	_106
	push	dword [ebp-4]
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_sidesign_minib3d_LoadBrush
	add	esp,16
	push	eax
	call	_bbHandleFromObject
	add	esp,4
	mov	dword [ebp-12],eax
	push	_1251
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_bbNullObject
	push	_sidesign_minib3d_TMesh
	push	dword [ebp-8]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_sidesign_minib3d_CreateSurface
	add	esp,8
	push	eax
	call	_bbHandleFromObject
	add	esp,4
	mov	dword [ebp-16],eax
	push	_1252
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	0
	push	0
	push	0
	push	1065353216
	push	1065353216
	push	-1082130432
	push	_sidesign_minib3d_TSurface
	push	dword [ebp-16]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_sidesign_minib3d_AddVertex
	add	esp,28
	push	_1253
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	0
	push	1065353216
	push	1065353216
	push	-1082130432
	push	-1082130432
	push	-1082130432
	push	_sidesign_minib3d_TSurface
	push	dword [ebp-16]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_sidesign_minib3d_AddVertex
	add	esp,28
	push	_1254
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	2
	push	1
	push	0
	push	_sidesign_minib3d_TSurface
	push	dword [ebp-16]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_sidesign_minib3d_AddTriangle
	add	esp,16
	push	_1255
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_sidesign_minib3d_TBrush
	push	dword [ebp-12]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	push	_sidesign_minib3d_TSurface
	push	dword [ebp-16]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_sidesign_minib3d_PaintSurface
	add	esp,8
	push	_1256
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	1065353216
	push	1065353216
	push	49
	push	_107
	push	dword [ebp-4]
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_sidesign_minib3d_LoadBrush
	add	esp,16
	push	eax
	call	_bbHandleFromObject
	add	esp,4
	mov	dword [ebp-12],eax
	push	_1257
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_bbNullObject
	push	_sidesign_minib3d_TMesh
	push	dword [ebp-8]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_sidesign_minib3d_CreateSurface
	add	esp,8
	push	eax
	call	_bbHandleFromObject
	add	esp,4
	mov	dword [ebp-16],eax
	push	_1258
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	0
	push	1065353216
	push	0
	push	1065353216
	push	1065353216
	push	-1082130432
	push	_sidesign_minib3d_TSurface
	push	dword [ebp-16]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_sidesign_minib3d_AddVertex
	add	esp,28
	push	_1259
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	0
	push	0
	push	1065353216
	push	-1082130432
	push	1065353216
	push	1065353216
	push	_sidesign_minib3d_TSurface
	push	dword [ebp-16]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_sidesign_minib3d_AddVertex
	add	esp,28
	push	_1260
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	2
	push	1
	push	0
	push	_sidesign_minib3d_TSurface
	push	dword [ebp-16]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_sidesign_minib3d_AddTriangle
	add	esp,16
	push	_1261
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_sidesign_minib3d_TBrush
	push	dword [ebp-12]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	push	_sidesign_minib3d_TSurface
	push	dword [ebp-16]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_sidesign_minib3d_PaintSurface
	add	esp,8
	push	_1262
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	1154777088
	push	1154777088
	push	1154777088
	push	_sidesign_minib3d_TMesh
	push	dword [ebp-8]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_sidesign_minib3d_ScaleMesh
	add	esp,16
	push	_1263
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_sidesign_minib3d_TMesh
	push	dword [ebp-8]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_sidesign_minib3d_FlipMesh
	add	esp,4
	push	_1264
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	1
	push	_sidesign_minib3d_TEntity
	push	dword [ebp-8]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_sidesign_minib3d_EntityFX
	add	esp,8
	push	_1265
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-8]
_178:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_TMouse_New:
	push	ebp
	mov	ebp,esp
	sub	esp,8
	push	ebx
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	push	ebp
	push	_1272
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	dword [ebp-4]
	call	_bbObjectCtor
	add	esp,4
	mov	eax,dword [ebp-4]
	mov	dword [eax],_bb_TMouse
	mov	eax,dword [ebp-4]
	mov	dword [eax+8],0
	mov	eax,dword [ebp-4]
	mov	dword [eax+12],0
	mov	eax,dword [ebp-4]
	mov	dword [eax+16],0
	push	3
	push	_1270
	call	_bbArrayNew1D
	add	esp,8
	mov	dword [ebp-8],eax
	mov	eax,dword [ebp-8]
	inc	dword [eax+4]
	mov	edx,dword [ebp-4]
	mov	eax,dword [ebp-8]
	mov	dword [edx+20],eax
	mov	ebx,0
_181:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_TMouse_Delete:
	push	ebp
	mov	ebp,esp
	sub	esp,8
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
_184:
	mov	eax,dword [ebp-4]
	mov	eax,dword [eax+20]
	mov	dword [ebp-8],eax
	mov	eax,dword [ebp-8]
	dec	dword [eax+4]
	jnz	_1277
	push	dword [ebp-8]
	call	_bbGCFree
	add	esp,4
_1277:
	mov	eax,dword [ebp-4]
	mov	dword [eax],_bbObjectClass
	push	dword [ebp-4]
	call	_bbObjectDtor
	add	esp,4
	mov	eax,0
_1275:
	mov	esp,ebp
	pop	ebp
	ret
__bb_TMouse_Init:
	push	ebp
	mov	ebp,esp
	sub	esp,4
	push	ebp
	push	_1279
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1278
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_bb_TMouse
	call	_bbObjectNew
	add	esp,4
	mov	dword [ebp-4],eax
_186:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,dword [ebp-4]
	mov	esp,ebp
	pop	ebp
	ret
__bb_TMouse_Update:
	push	ebp
	mov	ebp,esp
	sub	esp,80
	push	ebx
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	mov	dword [ebp-8],_bbNullObject
	push	ebp
	push	_1328
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1281
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [_brl_eventqueue_CurrentEvent]
	mov	dword [ebp-8],eax
	push	_1283
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [ebp-8]
	mov	dword [ebp-12],eax
	cmp	dword [ebp-12],_bbNullObject
	jne	_1285
	call	_brl_blitz_NullObjectError
_1285:
	mov	eax,dword [ebp-12]
	mov	eax,dword [eax+8]
	cmp	eax,1027
	je	_1288
	cmp	eax,1028
	je	_1289
	cmp	eax,1025
	je	_1290
	cmp	eax,1026
	je	_1291
	jmp	_1287
_1288:
	push	_1292
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [ebp-4]
	mov	dword [ebp-16],eax
	cmp	dword [ebp-16],_bbNullObject
	jne	_1294
	call	_brl_blitz_NullObjectError
_1294:
	mov	eax,dword [ebp-16]
	mov	dword [ebp-20],eax
	mov	eax,dword [ebp-8]
	mov	dword [ebp-24],eax
	cmp	dword [ebp-24],_bbNullObject
	jne	_1297
	call	_brl_blitz_NullObjectError
_1297:
	mov	edx,dword [ebp-20]
	mov	eax,dword [ebp-24]
	mov	eax,dword [eax+24]
	mov	dword [edx+8],eax
	push	_1298
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [ebp-4]
	mov	dword [ebp-28],eax
	cmp	dword [ebp-28],_bbNullObject
	jne	_1300
	call	_brl_blitz_NullObjectError
_1300:
	mov	eax,dword [ebp-28]
	mov	dword [ebp-32],eax
	mov	eax,dword [ebp-8]
	mov	dword [ebp-36],eax
	cmp	dword [ebp-36],_bbNullObject
	jne	_1303
	call	_brl_blitz_NullObjectError
_1303:
	mov	edx,dword [ebp-32]
	mov	eax,dword [ebp-36]
	mov	eax,dword [eax+28]
	mov	dword [edx+12],eax
	jmp	_1287
_1289:
	push	_1304
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [ebp-4]
	mov	dword [ebp-40],eax
	cmp	dword [ebp-40],_bbNullObject
	jne	_1306
	call	_brl_blitz_NullObjectError
_1306:
	mov	eax,dword [ebp-40]
	mov	dword [ebp-44],eax
	mov	eax,dword [ebp-8]
	mov	dword [ebp-48],eax
	cmp	dword [ebp-48],_bbNullObject
	jne	_1309
	call	_brl_blitz_NullObjectError
_1309:
	mov	ecx,dword [ebp-44]
	mov	eax,dword [ebp-44]
	mov	edx,dword [eax+16]
	mov	eax,dword [ebp-48]
	add	edx,dword [eax+16]
	mov	dword [ecx+16],edx
	jmp	_1287
_1290:
	push	_1310
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [ebp-4]
	mov	dword [ebp-52],eax
	cmp	dword [ebp-52],_bbNullObject
	jne	_1312
	call	_brl_blitz_NullObjectError
_1312:
	mov	eax,dword [ebp-52]
	mov	eax,dword [eax+20]
	mov	dword [ebp-56],eax
	mov	eax,dword [ebp-8]
	mov	dword [ebp-60],eax
	cmp	dword [ebp-60],_bbNullObject
	jne	_1315
	call	_brl_blitz_NullObjectError
_1315:
	mov	eax,dword [ebp-60]
	mov	ebx,dword [eax+16]
	sub	ebx,1
	mov	eax,dword [ebp-56]
	cmp	ebx,dword [eax+20]
	jb	_1317
	call	_brl_blitz_ArrayBoundsError
_1317:
	mov	eax,dword [ebp-56]
	add	eax,ebx
	mov	dword [ebp-64],eax
	mov	eax,dword [ebp-64]
	mov	byte [eax+24],1
	jmp	_1287
_1291:
	push	_1319
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [ebp-4]
	mov	dword [ebp-68],eax
	cmp	dword [ebp-68],_bbNullObject
	jne	_1321
	call	_brl_blitz_NullObjectError
_1321:
	mov	eax,dword [ebp-68]
	mov	eax,dword [eax+20]
	mov	dword [ebp-72],eax
	mov	eax,dword [ebp-8]
	mov	dword [ebp-76],eax
	cmp	dword [ebp-76],_bbNullObject
	jne	_1324
	call	_brl_blitz_NullObjectError
_1324:
	mov	eax,dword [ebp-76]
	mov	ebx,dword [eax+16]
	sub	ebx,1
	mov	eax,dword [ebp-72]
	cmp	ebx,dword [eax+20]
	jb	_1326
	call	_brl_blitz_ArrayBoundsError
_1326:
	mov	eax,dword [ebp-72]
	add	eax,ebx
	mov	dword [ebp-80],eax
	mov	eax,dword [ebp-80]
	mov	byte [eax+24],0
_1287:
	mov	ebx,0
_189:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_TMouse_getX:
	push	ebp
	mov	ebp,esp
	sub	esp,8
	push	ebx
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	push	ebp
	push	_1335
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1332
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [ebp-4]
	mov	dword [ebp-8],eax
	cmp	dword [ebp-8],_bbNullObject
	jne	_1334
	call	_brl_blitz_NullObjectError
_1334:
	mov	eax,dword [ebp-8]
	mov	ebx,dword [eax+8]
_192:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_TMouse_getY:
	push	ebp
	mov	ebp,esp
	sub	esp,8
	push	ebx
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	push	ebp
	push	_1340
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1337
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [ebp-4]
	mov	dword [ebp-8],eax
	cmp	dword [ebp-8],_bbNullObject
	jne	_1339
	call	_brl_blitz_NullObjectError
_1339:
	mov	eax,dword [ebp-8]
	mov	ebx,dword [eax+12]
_195:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_TMouse_getZ:
	push	ebp
	mov	ebp,esp
	sub	esp,8
	push	ebx
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	push	ebp
	push	_1345
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1342
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [ebp-4]
	mov	dword [ebp-8],eax
	cmp	dword [ebp-8],_bbNullObject
	jne	_1344
	call	_brl_blitz_NullObjectError
_1344:
	mov	eax,dword [ebp-8]
	mov	ebx,dword [eax+16]
_198:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_TMouse_GetButton:
	push	ebp
	mov	ebp,esp
	sub	esp,20
	push	ebx
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	mov	eax,dword [ebp+12]
	mov	dword [ebp-8],eax
	push	ebp
	push	_1357
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1347
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [ebp-8]
	cmp	eax,0
	setge	al
	movzx	eax,al
	cmp	eax,0
	je	_1348
	mov	eax,dword [ebp-8]
	cmp	eax,2
	setle	al
	movzx	eax,al
_1348:
	cmp	eax,0
	je	_1350
	push	_1351
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [ebp-4]
	mov	dword [ebp-12],eax
	cmp	dword [ebp-12],_bbNullObject
	jne	_1353
	call	_brl_blitz_NullObjectError
_1353:
	mov	eax,dword [ebp-12]
	mov	eax,dword [eax+20]
	mov	dword [ebp-16],eax
	mov	ebx,dword [ebp-8]
	mov	eax,dword [ebp-16]
	cmp	ebx,dword [eax+20]
	jb	_1356
	call	_brl_blitz_ArrayBoundsError
_1356:
	mov	eax,dword [ebp-16]
	movzx	eax,byte [eax+ebx+24]
	mov	eax,eax
	mov	byte [ebp-20],al
	jmp	_202
_1350:
	mov	byte [ebp-20],0
_202:
	call	dword [_bbOnDebugLeaveScope]
	movzx	eax,byte [ebp-20]
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_bb_TextureLoadedMesh:
	push	ebp
	mov	ebp,esp
	sub	esp,44
	push	ebx
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	mov	eax,dword [ebp+12]
	mov	dword [ebp-8],eax
	mov	eax,dword [ebp+16]
	mov	dword [ebp-16],eax
	mov	dword [ebp-12],_bbNullObject
	push	ebp
	push	_1375
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1360
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-12],_bbNullObject
	mov	eax,dword [ebp-4]
	mov	dword [ebp-20],eax
	cmp	dword [ebp-20],_bbNullObject
	jne	_1363
	call	_brl_blitz_NullObjectError
_1363:
	mov	eax,dword [ebp-20]
	mov	eax,dword [eax+272]
	mov	dword [ebp-24],eax
	mov	eax,dword [ebp-24]
	mov	dword [ebp-28],eax
	cmp	dword [ebp-28],_bbNullObject
	jne	_1366
	call	_brl_blitz_NullObjectError
_1366:
	push	dword [ebp-28]
	mov	eax,dword [ebp-28]
	mov	eax,dword [eax]
	call	dword [eax+140]
	add	esp,4
	mov	dword [ebp-32],eax
	jmp	_108
_110:
	mov	eax,dword [ebp-32]
	mov	dword [ebp-36],eax
	cmp	dword [ebp-36],_bbNullObject
	jne	_1371
	call	_brl_blitz_NullObjectError
_1371:
	push	_sidesign_minib3d_TSurface
	push	dword [ebp-36]
	mov	eax,dword [ebp-36]
	mov	eax,dword [eax]
	call	dword [eax+52]
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	mov	dword [ebp-12],eax
	cmp	dword [ebp-12],_bbNullObject
	je	_108
	push	_1372
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [ebp-12]
	mov	dword [ebp-40],eax
	cmp	dword [ebp-40],_bbNullObject
	jne	_1374
	call	_brl_blitz_NullObjectError
_1374:
	push	0
	push	dword [ebp-16]
	push	dword [ebp-8]
	mov	eax,dword [ebp-40]
	push	dword [eax+40]
	call	_sidesign_minib3d_BrushTexture
	add	esp,16
_108:
	mov	eax,dword [ebp-32]
	mov	dword [ebp-44],eax
	cmp	dword [ebp-44],_bbNullObject
	jne	_1369
	call	_brl_blitz_NullObjectError
_1369:
	push	dword [ebp-44]
	mov	eax,dword [ebp-44]
	mov	eax,dword [eax]
	call	dword [eax+48]
	add	esp,4
	cmp	eax,0
	jne	_110
_109:
	mov	ebx,0
_207:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_bb_invsLoadedMesh:
	push	ebp
	mov	ebp,esp
	sub	esp,36
	push	ebx
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	mov	dword [ebp-8],_bbNullObject
	push	ebp
	push	_1399
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1384
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-8],_bbNullObject
	mov	eax,dword [ebp-4]
	mov	dword [ebp-12],eax
	cmp	dword [ebp-12],_bbNullObject
	jne	_1387
	call	_brl_blitz_NullObjectError
_1387:
	mov	eax,dword [ebp-12]
	mov	eax,dword [eax+272]
	mov	dword [ebp-16],eax
	mov	eax,dword [ebp-16]
	mov	dword [ebp-20],eax
	cmp	dword [ebp-20],_bbNullObject
	jne	_1390
	call	_brl_blitz_NullObjectError
_1390:
	push	dword [ebp-20]
	mov	eax,dword [ebp-20]
	mov	eax,dword [eax]
	call	dword [eax+140]
	add	esp,4
	mov	dword [ebp-24],eax
	jmp	_111
_113:
	mov	eax,dword [ebp-24]
	mov	dword [ebp-28],eax
	cmp	dword [ebp-28],_bbNullObject
	jne	_1395
	call	_brl_blitz_NullObjectError
_1395:
	push	_sidesign_minib3d_TSurface
	push	dword [ebp-28]
	mov	eax,dword [ebp-28]
	mov	eax,dword [eax]
	call	dword [eax+52]
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	mov	dword [ebp-8],eax
	cmp	dword [ebp-8],_bbNullObject
	je	_111
	push	_1396
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [ebp-8]
	mov	dword [ebp-32],eax
	cmp	dword [ebp-32],_bbNullObject
	jne	_1398
	call	_brl_blitz_NullObjectError
_1398:
	push	0
	mov	eax,dword [ebp-32]
	push	dword [eax+40]
	call	_sidesign_minib3d_BrushAlpha
	add	esp,8
_111:
	mov	eax,dword [ebp-24]
	mov	dword [ebp-36],eax
	cmp	dword [ebp-36],_bbNullObject
	jne	_1393
	call	_brl_blitz_NullObjectError
_1393:
	push	dword [ebp-36]
	mov	eax,dword [ebp-36]
	mov	eax,dword [eax]
	call	dword [eax+48]
	add	esp,4
	cmp	eax,0
	jne	_113
_112:
	mov	ebx,0
_210:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
	section	"data" data writeable align 8
	align	4
_790:
	dd	0
_687:
	db	"buildingmaker",0
_688:
	db	"VKEY",0
_689:
	db	"[]i",0
_690:
	db	"b3d_stack",0
	align	4
_bb_b3d_stack:
	dd	_bbEmptyArray
_691:
	db	"b3d_file",0
_117:
	db	"i",0
	align	4
_bb_b3d_file:
	dd	0
_692:
	db	"b3d_tos",0
	align	4
_bb_b3d_tos:
	dd	0
_693:
	db	"Mouse",0
_694:
	db	":TMouse",0
	align	4
_bb_Mouse:
	dd	_bbNullObject
_695:
	db	"BaseModel",0
	align	4
_bb_BaseModel:
	dd	0
_696:
	db	"RoofModel",0
	align	4
_bb_RoofModel:
	dd	0
_697:
	db	"DoorModel",0
	align	4
_bb_DoorModel:
	dd	0
_698:
	db	"BaseModelBase",0
	align	4
_bb_BaseModelBase:
	dd	0
_699:
	db	"add",0
	align	4
_bb_add:
	dd	_bbEmptyArray
_700:
	db	"UpKey",0
_701:
	db	"DownKey",0
_702:
	db	"LeftKey",0
_703:
	db	"RightKey",0
_704:
	db	"MouseRight",0
_705:
	db	"SpaceKey",0
_706:
	db	"KeyW",0
_707:
	db	"KeyA",0
_708:
	db	"KeyS",0
_709:
	db	"KeyD",0
_710:
	db	"camera",0
_711:
	db	":sidesign.minib3d.TCamera",0
	align	4
_bb_camera:
	dd	_bbNullObject
_712:
	db	"skydome",0
	align	4
_bb_skydome:
	dd	0
_713:
	db	"mesh",0
	align	4
_bb_mesh:
	dd	0
_714:
	db	"texture",0
	align	4
_bb_texture:
	dd	0
_715:
	db	"res",0
_716:
	db	"b",0
_bb_res:
	db	0
_717:
	db	"Terrain",0
	align	4
_bb_Terrain:
	dd	0
_718:
	db	"RoofX",0
_719:
	db	"f",0
	align	4
_bb_RoofX:
	dd	0x0
_720:
	db	"RoofY",0
	align	4
_bb_RoofY:
	dd	0x0
_721:
	db	"RoofZ",0
	align	4
_bb_RoofZ:
	dd	0x0
_722:
	db	"DoorX",0
	align	4
_bb_DoorX:
	dd	0x0
_723:
	db	"DoorY",0
	align	4
_bb_DoorY:
	dd	0x0
_724:
	db	"DoorZ",0
	align	4
_bb_DoorZ:
	dd	0x0
_725:
	db	"WindowsTex",0
	align	4
_bb_WindowsTex:
	dd	_bbEmptyArray
_726:
	db	"WindowsCnt",0
	align	4
_bb_WindowsCnt:
	dd	_bbEmptyArray
_727:
	db	"WindowsX",0
	align	4
_bb_WindowsX:
	dd	_bbEmptyArray
_728:
	db	"WindowsY",0
	align	4
_bb_WindowsY:
	dd	_bbEmptyArray
_729:
	db	"WindowsZ",0
	align	4
_bb_WindowsZ:
	dd	_bbEmptyArray
_730:
	db	"WinChild",0
	align	4
_bb_WinChild:
	dd	_bbEmptyArray
_731:
	db	"c_surfs",0
	align	4
_bb_c_surfs:
	dd	0
_732:
	db	"WindowCnt",0
_733:
	db	"$",0
	align	4
_bb_WindowCnt:
	dd	_bbEmptyString
_734:
	db	"BaseMesh",0
	align	4
_bb_BaseMesh:
	dd	0
_735:
	db	"KeyX",0
	align	4
_bb_KeyX:
	dd	0x0
_736:
	db	"KeyY",0
	align	4
_bb_KeyY:
	dd	0x0
_737:
	db	"KeyZ",0
	align	4
_bb_KeyZ:
	dd	0x0
_738:
	db	"nodeselected",0
	align	4
_bb_nodeselected:
	dd	0
_739:
	db	"RoofPoint",0
	align	4
_bb_RoofPoint:
	dd	0
_740:
	db	"MouseXEndPosi",0
	align	4
_bb_MouseXEndPosi:
	dd	0x0
_741:
	db	"MouseYEndPosi",0
	align	4
_bb_MouseYEndPosi:
	dd	0x0
_742:
	db	"Window",0
_743:
	db	":brl.maxgui.TGadget",0
	align	4
_bb_Window:
	dd	_bbNullObject
_744:
	db	"tabTabber0",0
	align	4
_bb_tabTabber0:
	dd	_bbNullObject
_745:
	db	"TabPanel0",0
	align	4
_bb_TabPanel0:
	dd	_bbNullObject
_746:
	db	"trvTreeView0",0
	align	4
_bb_trvTreeView0:
	dd	_bbNullObject
_747:
	db	"TabPanel1",0
	align	4
_bb_TabPanel1:
	dd	_bbNullObject
_748:
	db	"trvTreeView1",0
	align	4
_bb_trvTreeView1:
	dd	_bbNullObject
_749:
	db	"TabPanel2",0
	align	4
_bb_TabPanel2:
	dd	_bbNullObject
_750:
	db	"sidebutt",0
	align	4
_bb_sidebutt:
	dd	_bbNullObject
_751:
	db	"cobComboBox1",0
	align	4
_bb_cobComboBox1:
	dd	_bbNullObject
_752:
	db	"windowselected",0
	align	4
_bb_windowselected:
	dd	_bbNullObject
_753:
	db	"SetWindowTex",0
	align	4
_bb_SetWindowTex:
	dd	_bbNullObject
_754:
	db	"SetWindowCnt",0
	align	4
_bb_SetWindowCnt:
	dd	_bbNullObject
_755:
	db	"TabPanel3",0
	align	4
_bb_TabPanel3:
	dd	_bbNullObject
_756:
	db	"trvTreeView4",0
	align	4
_bb_trvTreeView4:
	dd	_bbNullObject
_757:
	db	"cavCanvas0",0
	align	4
_bb_cavCanvas0:
	dd	_bbNullObject
_758:
	db	"exportmeshbutt",0
	align	4
_bb_exportmeshbutt:
	dd	_bbNullObject
_759:
	db	"IcoStrip",0
_760:
	db	"hwnd",0
	align	4
_bb_hwnd:
	dd	0
_761:
	db	"t",0
	align	4
_bb_t:
	dd	_bbEmptyString
_762:
	db	"ext",0
	align	4
_bb_ext:
	dd	_bbEmptyString
_763:
	db	"basedir",0
_764:
	db	":brl.maxgui.Tgadget",0
	align	4
_bb_basedir:
	dd	_bbNullObject
_765:
	db	"roofdir",0
	align	4
_bb_roofdir:
	dd	_bbNullObject
_766:
	db	"texturedir",0
	align	4
_bb_texturedir:
	dd	_bbEmptyArray
_767:
	db	"texcount",0
	align	4
_bb_texcount:
	dd	0
_768:
	db	"count",0
	align	4
_bb_count:
	dd	0
_769:
	db	"filenames",0
	align	4
_bb_filenames:
	dd	_bbEmptyString
_770:
	db	"Model_List",0
_771:
	db	":brl.linkedlist.TList",0
	align	4
_bb_Model_List:
	dd	_bbNullObject
_772:
	db	"Roof_List",0
	align	4
_bb_Roof_List:
	dd	_bbNullObject
_773:
	db	"Texture_List",0
	align	4
_bb_Texture_List:
	dd	_bbNullObject
_774:
	db	"Texturebase_list",0
	align	4
_bb_Texturebase_list:
	dd	_bbNullObject
_775:
	db	"Textureroof_list",0
	align	4
_bb_Textureroof_list:
	dd	_bbNullObject
_776:
	db	"Texturebeam_list",0
	align	4
_bb_Texturebeam_list:
	dd	_bbNullObject
_777:
	db	"Texturewindow_list",0
	align	4
_bb_Texturewindow_list:
	dd	_bbNullObject
_778:
	db	"Door_list",0
	align	4
_bb_Door_list:
	dd	_bbNullObject
_779:
	db	"DoorPoint",0
	align	4
_bb_DoorPoint:
	dd	0
_780:
	db	"Surf_list",0
	align	4
_bb_Surf_list:
	dd	_bbNullObject
_781:
	db	"r",0
_782:
	db	"Doordir",0
_783:
	db	"flagXDown",0
_784:
	db	"cx",0
_785:
	db	"cz",0
_786:
	db	"cy",0
_787:
	db	"renders",0
_788:
	db	"old_ms",0
_789:
	db	"fps",0
	align	4
_686:
	dd	1
	dd	_687
	dd	4
	dd	_688
	dd	_689
	dd	_bb_VKEY
	dd	4
	dd	_690
	dd	_689
	dd	_bb_b3d_stack
	dd	4
	dd	_691
	dd	_117
	dd	_bb_b3d_file
	dd	4
	dd	_692
	dd	_117
	dd	_bb_b3d_tos
	dd	4
	dd	_693
	dd	_694
	dd	_bb_Mouse
	dd	4
	dd	_695
	dd	_117
	dd	_bb_BaseModel
	dd	4
	dd	_696
	dd	_117
	dd	_bb_RoofModel
	dd	4
	dd	_697
	dd	_117
	dd	_bb_DoorModel
	dd	4
	dd	_698
	dd	_117
	dd	_bb_BaseModelBase
	dd	4
	dd	_699
	dd	_689
	dd	_bb_add
	dd	2
	dd	_700
	dd	_117
	dd	-20
	dd	2
	dd	_701
	dd	_117
	dd	-24
	dd	2
	dd	_702
	dd	_117
	dd	-28
	dd	2
	dd	_703
	dd	_117
	dd	-32
	dd	2
	dd	_704
	dd	_117
	dd	-36
	dd	2
	dd	_705
	dd	_117
	dd	-40
	dd	2
	dd	_706
	dd	_117
	dd	-44
	dd	2
	dd	_707
	dd	_117
	dd	-48
	dd	2
	dd	_708
	dd	_117
	dd	-52
	dd	2
	dd	_709
	dd	_117
	dd	-56
	dd	4
	dd	_710
	dd	_711
	dd	_bb_camera
	dd	4
	dd	_712
	dd	_117
	dd	_bb_skydome
	dd	4
	dd	_713
	dd	_117
	dd	_bb_mesh
	dd	4
	dd	_714
	dd	_117
	dd	_bb_texture
	dd	4
	dd	_715
	dd	_716
	dd	_bb_res
	dd	4
	dd	_717
	dd	_117
	dd	_bb_Terrain
	dd	4
	dd	_718
	dd	_719
	dd	_bb_RoofX
	dd	4
	dd	_720
	dd	_719
	dd	_bb_RoofY
	dd	4
	dd	_721
	dd	_719
	dd	_bb_RoofZ
	dd	4
	dd	_722
	dd	_719
	dd	_bb_DoorX
	dd	4
	dd	_723
	dd	_719
	dd	_bb_DoorY
	dd	4
	dd	_724
	dd	_719
	dd	_bb_DoorZ
	dd	4
	dd	_725
	dd	_689
	dd	_bb_WindowsTex
	dd	4
	dd	_726
	dd	_689
	dd	_bb_WindowsCnt
	dd	4
	dd	_727
	dd	_689
	dd	_bb_WindowsX
	dd	4
	dd	_728
	dd	_689
	dd	_bb_WindowsY
	dd	4
	dd	_729
	dd	_689
	dd	_bb_WindowsZ
	dd	4
	dd	_730
	dd	_689
	dd	_bb_WinChild
	dd	4
	dd	_731
	dd	_117
	dd	_bb_c_surfs
	dd	4
	dd	_732
	dd	_733
	dd	_bb_WindowCnt
	dd	4
	dd	_734
	dd	_117
	dd	_bb_BaseMesh
	dd	4
	dd	_735
	dd	_719
	dd	_bb_KeyX
	dd	4
	dd	_736
	dd	_719
	dd	_bb_KeyY
	dd	4
	dd	_737
	dd	_719
	dd	_bb_KeyZ
	dd	4
	dd	_738
	dd	_117
	dd	_bb_nodeselected
	dd	4
	dd	_739
	dd	_117
	dd	_bb_RoofPoint
	dd	4
	dd	_740
	dd	_719
	dd	_bb_MouseXEndPosi
	dd	4
	dd	_741
	dd	_719
	dd	_bb_MouseYEndPosi
	dd	4
	dd	_742
	dd	_743
	dd	_bb_Window
	dd	4
	dd	_744
	dd	_743
	dd	_bb_tabTabber0
	dd	4
	dd	_745
	dd	_743
	dd	_bb_TabPanel0
	dd	4
	dd	_746
	dd	_743
	dd	_bb_trvTreeView0
	dd	4
	dd	_747
	dd	_743
	dd	_bb_TabPanel1
	dd	4
	dd	_748
	dd	_743
	dd	_bb_trvTreeView1
	dd	4
	dd	_749
	dd	_743
	dd	_bb_TabPanel2
	dd	4
	dd	_750
	dd	_743
	dd	_bb_sidebutt
	dd	4
	dd	_751
	dd	_743
	dd	_bb_cobComboBox1
	dd	4
	dd	_752
	dd	_743
	dd	_bb_windowselected
	dd	4
	dd	_753
	dd	_743
	dd	_bb_SetWindowTex
	dd	4
	dd	_754
	dd	_743
	dd	_bb_SetWindowCnt
	dd	4
	dd	_755
	dd	_743
	dd	_bb_TabPanel3
	dd	4
	dd	_756
	dd	_743
	dd	_bb_trvTreeView4
	dd	4
	dd	_757
	dd	_743
	dd	_bb_cavCanvas0
	dd	4
	dd	_758
	dd	_743
	dd	_bb_exportmeshbutt
	dd	2
	dd	_759
	dd	_117
	dd	-60
	dd	4
	dd	_760
	dd	_117
	dd	_bb_hwnd
	dd	4
	dd	_761
	dd	_733
	dd	_bb_t
	dd	4
	dd	_762
	dd	_733
	dd	_bb_ext
	dd	4
	dd	_763
	dd	_764
	dd	_bb_basedir
	dd	4
	dd	_765
	dd	_764
	dd	_bb_roofdir
	dd	4
	dd	_766
	dd	_689
	dd	_bb_texturedir
	dd	4
	dd	_767
	dd	_117
	dd	_bb_texcount
	dd	4
	dd	_768
	dd	_117
	dd	_bb_count
	dd	4
	dd	_769
	dd	_733
	dd	_bb_filenames
	dd	4
	dd	_770
	dd	_771
	dd	_bb_Model_List
	dd	4
	dd	_772
	dd	_771
	dd	_bb_Roof_List
	dd	4
	dd	_773
	dd	_771
	dd	_bb_Texture_List
	dd	4
	dd	_774
	dd	_771
	dd	_bb_Texturebase_list
	dd	4
	dd	_775
	dd	_771
	dd	_bb_Textureroof_list
	dd	4
	dd	_776
	dd	_771
	dd	_bb_Texturebeam_list
	dd	4
	dd	_777
	dd	_771
	dd	_bb_Texturewindow_list
	dd	4
	dd	_778
	dd	_771
	dd	_bb_Door_list
	dd	4
	dd	_779
	dd	_117
	dd	_bb_DoorPoint
	dd	4
	dd	_780
	dd	_771
	dd	_bb_Surf_list
	dd	2
	dd	_781
	dd	_733
	dd	-4
	dd	2
	dd	_782
	dd	_117
	dd	-64
	dd	2
	dd	_783
	dd	_117
	dd	-68
	dd	2
	dd	_784
	dd	_719
	dd	-8
	dd	2
	dd	_785
	dd	_719
	dd	-12
	dd	2
	dd	_786
	dd	_719
	dd	-16
	dd	2
	dd	_787
	dd	_117
	dd	-72
	dd	2
	dd	_788
	dd	_117
	dd	-76
	dd	2
	dd	_789
	dd	_117
	dd	-80
	dd	0
_213:
	db	"C:/Documents and Settings/ckob/Desktop/coding/BuildingMaker/inc/b3dfile.bmx",0
	align	4
_212:
	dd	_213
	dd	8
	dd	1
	align	4
_216:
	dd	0
_214:
	db	"i",0
	align	4
_218:
	dd	_213
	dd	9
	dd	1
_220:
	db	"C:/Documents and Settings/ckob/Desktop/coding/BuildingMaker/buildingmaker.bmx",0
	align	4
_219:
	dd	_220
	dd	11
	dd	1
	align	4
_221:
	dd	_220
	dd	14
	dd	1
_115:
	db	"TMouse",0
_116:
	db	"x",0
_118:
	db	"y",0
_119:
	db	"z",0
_120:
	db	"Button",0
_121:
	db	"[]b",0
	align	4
_114:
	dd	2
	dd	_115
	dd	3
	dd	_116
	dd	_117
	dd	8
	dd	3
	dd	_118
	dd	_117
	dd	12
	dd	3
	dd	_119
	dd	_117
	dd	16
	dd	3
	dd	_120
	dd	_121
	dd	20
	dd	0
	align	4
_bb_TMouse:
	dd	_bbObjectClass
	dd	_bbObjectFree
	dd	_114
	dd	24
	dd	__bb_TMouse_New
	dd	__bb_TMouse_Delete
	dd	_bbObjectToString
	dd	_bbObjectCompare
	dd	_bbObjectSendMessage
	dd	_bbObjectReserved
	dd	_bbObjectReserved
	dd	_bbObjectReserved
	dd	__bb_TMouse_Init
	dd	__bb_TMouse_Update
	dd	__bb_TMouse_getX
	dd	__bb_TMouse_getY
	dd	__bb_TMouse_getZ
	dd	__bb_TMouse_GetButton
	align	4
_224:
	dd	_220
	dd	17
	dd	1
	align	4
_225:
	dd	_220
	dd	18
	dd	1
	align	4
_226:
	dd	_220
	dd	19
	dd	1
	align	4
_227:
	dd	_220
	dd	20
	dd	1
	align	4
_228:
	dd	_220
	dd	24
	dd	1
_229:
	db	"i",0
	align	4
_232:
	dd	_220
	dd	27
	dd	1
	align	4
_234:
	dd	_220
	dd	28
	dd	1
	align	4
_236:
	dd	_220
	dd	29
	dd	1
	align	4
_238:
	dd	_220
	dd	30
	dd	1
	align	4
_240:
	dd	_220
	dd	31
	dd	1
	align	4
_242:
	dd	_220
	dd	32
	dd	1
	align	4
_244:
	dd	_220
	dd	34
	dd	1
	align	4
_246:
	dd	_220
	dd	35
	dd	1
	align	4
_248:
	dd	_220
	dd	36
	dd	1
	align	4
_250:
	dd	_220
	dd	37
	dd	1
	align	4
_252:
	dd	_220
	dd	39
	dd	1
	align	4
_253:
	dd	_220
	dd	40
	dd	1
	align	4
_254:
	dd	_220
	dd	41
	dd	1
	align	4
_255:
	dd	_220
	dd	42
	dd	1
	align	4
_256:
	dd	_220
	dd	43
	dd	1
	align	4
_257:
	dd	_220
	dd	44
	dd	1
	align	4
_258:
	dd	_220
	dd	45
	dd	1
	align	4
_259:
	dd	_220
	dd	46
	dd	1
	align	4
_260:
	dd	_220
	dd	47
	dd	1
_261:
	db	"i",0
_264:
	db	"i",0
_267:
	db	"i",0
_270:
	db	"i",0
_273:
	db	"i",0
_276:
	db	"i",0
	align	4
_279:
	dd	_220
	dd	49
	dd	1
	align	4
_280:
	dd	_220
	dd	50
	dd	1
	align	4
_281:
	dd	_220
	dd	51
	dd	1
	align	4
_282:
	dd	_220
	dd	54
	dd	1
	align	4
_283:
	dd	_220
	dd	55
	dd	1
	align	4
_284:
	dd	_220
	dd	56
	dd	1
	align	4
_285:
	dd	_220
	dd	58
	dd	1
	align	4
_286:
	dd	_220
	dd	59
	dd	1
	align	4
_287:
	dd	_220
	dd	62
	dd	1
	align	4
_288:
	dd	_220
	dd	63
	dd	1
	align	4
_289:
	dd	_220
	dd	65
	dd	1
	align	4
_31:
	dd	_bbStringClass
	dd	2147483647
	dd	13
	dw	66,117,105,108,100,105,110,103,77,97,107,101,114
	align	4
_292:
	dd	_220
	dd	66
	dd	2
	align	4
_295:
	dd	_220
	dd	67
	dd	2
	align	4
_1:
	dd	_bbStringClass
	dd	2147483647
	dd	0
	align	4
_32:
	dd	_bbStringClass
	dd	2147483647
	dd	4
	dw	66,97,115,101
	align	4
_296:
	dd	_220
	dd	68
	dd	2
	align	4
_33:
	dd	_bbStringClass
	dd	2147483647
	dd	5
	dw	82,111,111,102,115
	align	4
_297:
	dd	_220
	dd	69
	dd	2
	align	4
_34:
	dd	_bbStringClass
	dd	2147483647
	dd	7
	dw	87,105,110,100,111,119,115
	align	4
_298:
	dd	_220
	dd	70
	dd	2
	align	4
_35:
	dd	_bbStringClass
	dd	2147483647
	dd	5
	dw	68,111,111,114,115
	align	4
_299:
	dd	_220
	dd	71
	dd	2
	align	4
_300:
	dd	_220
	dd	72
	dd	3
	align	4
_303:
	dd	_220
	dd	73
	dd	3
	align	4
_304:
	dd	_220
	dd	74
	dd	4
	align	4
_307:
	dd	_220
	dd	75
	dd	4
	align	4
_308:
	dd	_220
	dd	76
	dd	3
	align	4
_311:
	dd	_220
	dd	77
	dd	3
	align	4
_312:
	dd	_220
	dd	78
	dd	3
	align	4
_313:
	dd	_220
	dd	79
	dd	3
	align	4
_314:
	dd	_220
	dd	80
	dd	4
	align	4
_317:
	dd	_220
	dd	81
	dd	4
	align	4
_318:
	dd	_220
	dd	82
	dd	3
	align	4
_321:
	dd	_220
	dd	83
	dd	3
	align	4
_322:
	dd	_220
	dd	84
	dd	3
	align	4
_323:
	dd	_220
	dd	85
	dd	3
	align	4
_324:
	dd	_220
	dd	87
	dd	3
	align	4
_36:
	dd	_bbStringClass
	dd	2147483647
	dd	11
	dw	83,105,100,101,32,87,105,110,100,111,119
	align	4
_327:
	dd	_220
	dd	88
	dd	3
	align	4
_328:
	dd	_220
	dd	90
	dd	3
	align	4
_331:
	dd	_220
	dd	91
	dd	4
	align	4
_37:
	dd	_bbStringClass
	dd	2147483647
	dd	1
	dw	49
	align	4
_332:
	dd	_220
	dd	92
	dd	4
	align	4
_38:
	dd	_bbStringClass
	dd	2147483647
	dd	1
	dw	50
	align	4
_333:
	dd	_220
	dd	93
	dd	4
	align	4
_39:
	dd	_bbStringClass
	dd	2147483647
	dd	1
	dw	51
	align	4
_334:
	dd	_220
	dd	94
	dd	4
	align	4
_40:
	dd	_bbStringClass
	dd	2147483647
	dd	1
	dw	52
	align	4
_335:
	dd	_220
	dd	95
	dd	4
	align	4
_41:
	dd	_bbStringClass
	dd	2147483647
	dd	1
	dw	53
	align	4
_336:
	dd	_220
	dd	96
	dd	4
	align	4
_42:
	dd	_bbStringClass
	dd	2147483647
	dd	1
	dw	54
	align	4
_337:
	dd	_220
	dd	97
	dd	4
	align	4
_338:
	dd	_220
	dd	98
	dd	4
	align	4
_339:
	dd	_220
	dd	99
	dd	3
	align	4
_342:
	dd	_220
	dd	100
	dd	4
	align	4
_343:
	dd	_220
	dd	101
	dd	4
	align	4
_344:
	dd	_220
	dd	102
	dd	4
	align	4
_345:
	dd	_220
	dd	103
	dd	4
	align	4
_346:
	dd	_220
	dd	104
	dd	4
	align	4
_347:
	dd	_220
	dd	105
	dd	4
	align	4
_348:
	dd	_220
	dd	106
	dd	4
	align	4
_349:
	dd	_220
	dd	107
	dd	4
	align	4
_350:
	dd	_220
	dd	108
	dd	3
	align	4
_43:
	dd	_bbStringClass
	dd	2147483647
	dd	11
	dw	83,101,116,32,84,101,120,116,117,114,101
	align	4
_353:
	dd	_220
	dd	109
	dd	4
	align	4
_354:
	dd	_220
	dd	110
	dd	3
	align	4
_44:
	dd	_bbStringClass
	dd	2147483647
	dd	17
	dw	83,101,116,32,87,105,110,100,111,119,32,65,109,111,117,110
	dw	116
	align	4
_357:
	dd	_220
	dd	111
	dd	4
	align	4
_358:
	dd	_220
	dd	112
	dd	3
	align	4
_361:
	dd	_220
	dd	113
	dd	3
	align	4
_362:
	dd	_220
	dd	114
	dd	3
	align	4
_363:
	dd	_220
	dd	115
	dd	3
	align	4
_364:
	dd	_220
	dd	116
	dd	4
	align	4
_367:
	dd	_220
	dd	117
	dd	4
	align	4
_368:
	dd	_220
	dd	118
	dd	2
	align	4
_371:
	dd	_220
	dd	119
	dd	2
	align	4
_372:
	dd	_220
	dd	120
	dd	2
	align	4
_45:
	dd	_bbStringClass
	dd	2147483647
	dd	15
	dw	69,120,112,111,114,116,32,66,117,105,108,100,105,110,103
	align	4
_375:
	dd	_220
	dd	121
	dd	2
	align	4
_376:
	dd	_220
	dd	124
	dd	1
	align	4
_46:
	dd	_bbStringClass
	dd	2147483647
	dd	18
	dw	109,101,100,105,97,47,105,99,111,115,116,114,105,112,46,112
	dw	110,103
	align	4
_378:
	dd	_220
	dd	126
	dd	1
	align	4
_379:
	dd	_220
	dd	127
	dd	1
	align	4
_380:
	dd	_220
	dd	129
	dd	1
	align	4
_381:
	dd	_220
	dd	133
	dd	1
	align	4
_383:
	dd	_220
	dd	140
	dd	1
	align	4
_384:
	dd	_220
	dd	141
	dd	1
	align	4
_385:
	dd	_220
	dd	142
	dd	1
	align	4
_386:
	dd	_220
	dd	143
	dd	1
	align	4
_387:
	dd	_220
	dd	144
	dd	1
_388:
	db	"i",0
	align	4
_391:
	dd	_220
	dd	145
	dd	1
	align	4
_392:
	dd	_220
	dd	146
	dd	1
	align	4
_393:
	dd	_220
	dd	147
	dd	1
	align	4
_394:
	dd	_220
	dd	150
	dd	1
	align	4
_397:
	dd	_220
	dd	151
	dd	1
	align	4
_400:
	dd	_220
	dd	152
	dd	1
	align	4
_403:
	dd	_220
	dd	153
	dd	1
	align	4
_406:
	dd	_220
	dd	154
	dd	1
	align	4
_409:
	dd	_220
	dd	155
	dd	1
	align	4
_411:
	dd	0
	align	4
_413:
	dd	_220
	dd	156
	dd	1
	align	4
_416:
	dd	_220
	dd	157
	dd	1
	align	4
_419:
	dd	_220
	dd	158
	dd	1
	align	4
_420:
	dd	_220
	dd	159
	dd	1
	align	4
_423:
	dd	_220
	dd	163
	dd	1
	align	4
_47:
	dd	_bbStringClass
	dd	2147483647
	dd	12
	dw	77,101,115,104,101,115,47,66,97,115,101,47
	align	4
_424:
	dd	_220
	dd	164
	dd	1
	align	4
_48:
	dd	_bbStringClass
	dd	2147483647
	dd	13
	dw	77,101,115,104,101,115,47,82,111,111,102,115,47
	align	4
_425:
	dd	_220
	dd	165
	dd	1
	align	4
_49:
	dd	_bbStringClass
	dd	2147483647
	dd	13
	dw	77,101,115,104,101,115,47,68,111,111,114,115,47
	align	4
_426:
	dd	_220
	dd	166
	dd	1
	align	4
_50:
	dd	_bbStringClass
	dd	2147483647
	dd	9
	dw	84,101,120,116,117,114,101,115,47
	align	4
_427:
	dd	_220
	dd	174
	dd	1
	align	4
_51:
	dd	_bbStringClass
	dd	2147483647
	dd	5
	dw	98,97,115,101,115
	align	4
_432:
	dd	_220
	dd	176
	dd	1
	align	4
_445:
	dd	_220
	dd	181
	dd	2
	align	4
_447:
	dd	_220
	dd	181
	dd	10
	align	4
_448:
	dd	_220
	dd	182
	dd	2
	align	4
_55:
	dd	_bbStringClass
	dd	2147483647
	dd	1
	dw	46
	align	4
_56:
	dd	_bbStringClass
	dd	2147483647
	dd	2
	dw	46,46
	align	4
_452:
	dd	_220
	dd	182
	dd	25
	align	4
_453:
	dd	_220
	dd	184
	dd	3
	align	4
_458:
	dd	_220
	dd	186
	dd	4
	align	4
_57:
	dd	_bbStringClass
	dd	2147483647
	dd	3
	dw	98,51,100
	align	4
_462:
	dd	_220
	dd	188
	dd	6
	align	4
_463:
	dd	_220
	dd	199
	dd	1
	align	4
_468:
	dd	_220
	dd	200
	dd	2
	align	4
_478:
	dd	_220
	dd	204
	dd	2
	align	4
_480:
	dd	_220
	dd	204
	dd	10
	align	4
_481:
	dd	_220
	dd	205
	dd	2
	align	4
_485:
	dd	_220
	dd	205
	dd	25
	align	4
_486:
	dd	_220
	dd	207
	dd	3
	align	4
_491:
	dd	_220
	dd	209
	dd	4
	align	4
_495:
	dd	_220
	dd	211
	dd	6
	align	4
_496:
	dd	_220
	dd	222
	dd	1
	align	4
_498:
	dd	_220
	dd	223
	dd	2
	align	4
_507:
	dd	_220
	dd	227
	dd	2
	align	4
_509:
	dd	_220
	dd	227
	dd	10
	align	4
_510:
	dd	_220
	dd	228
	dd	2
	align	4
_514:
	dd	_220
	dd	228
	dd	25
	align	4
_515:
	dd	_220
	dd	230
	dd	3
	align	4
_520:
	dd	_220
	dd	232
	dd	4
	align	4
_524:
	dd	_220
	dd	234
	dd	6
	align	4
_525:
	dd	_220
	dd	248
	dd	2
	align	4
_528:
	dd	_220
	dd	249
	dd	2
	align	4
_531:
	dd	_220
	dd	252
	dd	2
	align	4
_532:
	dd	_220
	dd	253
	dd	2
	align	4
_533:
	dd	_220
	dd	254
	dd	2
	align	4
_534:
	dd	_220
	dd	256
	dd	2
	align	4
_535:
	dd	_220
	dd	258
	dd	2
	align	4
_536:
	dd	_220
	dd	260
	dd	2
	align	4
_537:
	dd	_220
	dd	262
	dd	2
	align	4
_538:
	dd	_220
	dd	263
	dd	2
	align	4
_539:
	dd	_220
	dd	264
	dd	2
	align	4
_540:
	dd	_220
	dd	266
	dd	2
	align	4
_541:
	dd	_220
	dd	268
	dd	2
	align	4
_542:
	dd	_220
	dd	270
	dd	2
	align	4
_543:
	dd	_220
	dd	271
	dd	2
	align	4
_544:
	dd	_220
	dd	272
	dd	2
	align	4
_545:
	dd	_220
	dd	273
	dd	2
	align	4
_546:
	dd	_220
	dd	275
	dd	2
	align	4
_547:
	dd	_220
	dd	277
	dd	2
	align	4
_548:
	dd	_220
	dd	278
	dd	2
	align	4
_549:
	dd	_220
	dd	279
	dd	2
	align	4
_550:
	dd	_220
	dd	281
	dd	2
	align	4
_551:
	dd	_220
	dd	286
	dd	1
	align	4
_64:
	dd	_bbStringClass
	dd	2147483647
	dd	5
	dw	109,101,100,105,97
	align	4
_552:
	dd	_220
	dd	293
	dd	1
	align	4
_553:
	dd	_220
	dd	307
	dd	1
	align	4
_558:
	dd	_220
	dd	308
	dd	1
	align	4
_559:
	dd	_220
	dd	309
	dd	1
	align	4
_560:
	dd	_220
	dd	311
	dd	1
	align	4
_561:
	dd	_220
	dd	312
	dd	1
	align	4
_562:
	dd	_220
	dd	316
	dd	1
	align	4
_563:
	dd	_220
	dd	318
	dd	1
	align	4
_564:
	dd	_220
	dd	320
	dd	3
	align	4
_567:
	dd	_220
	dd	322
	dd	2
	align	4
_568:
	dd	_220
	dd	324
	dd	1
	align	4
_581:
	dd	_220
	dd	327
	dd	4
	align	4
_582:
	dd	_220
	dd	330
	dd	6
	align	4
_594:
	dd	_220
	dd	333
	dd	10
	align	4
_595:
	dd	_220
	dd	336
	dd	10
	align	4
_596:
	dd	_220
	dd	339
	dd	10
	align	4
_597:
	dd	_220
	dd	342
	dd	10
	align	4
_598:
	dd	_220
	dd	345
	dd	10
	align	4
_599:
	dd	_220
	dd	347
	dd	10
	align	4
_600:
	dd	_220
	dd	350
	dd	10
	align	4
_601:
	dd	_220
	dd	353
	dd	10
	align	4
_602:
	dd	_220
	dd	356
	dd	10
	align	4
_603:
	dd	_220
	dd	362
	dd	7
	align	4
_615:
	dd	_220
	dd	365
	dd	9
	align	4
_616:
	dd	_220
	dd	368
	dd	9
	align	4
_617:
	dd	_220
	dd	371
	dd	9
	align	4
_618:
	dd	_220
	dd	374
	dd	9
	align	4
_619:
	dd	_220
	dd	377
	dd	10
	align	4
_620:
	dd	_220
	dd	380
	dd	10
	align	4
_621:
	dd	_220
	dd	384
	dd	10
	align	4
_622:
	dd	_220
	dd	386
	dd	10
	align	4
_623:
	dd	_220
	dd	389
	dd	9
	align	4
_624:
	dd	_220
	dd	396
	dd	7
	align	4
_629:
	dd	_220
	dd	399
	dd	9
	align	4
_630:
	dd	_220
	dd	409
	dd	15
	align	4
_631:
	dd	_220
	dd	410
	dd	15
	align	4
_632:
	dd	_220
	dd	415
	dd	7
	align	4
_636:
	dd	_220
	dd	417
	dd	9
	align	4
_637:
	dd	_220
	dd	420
	dd	11
	align	4
_639:
	dd	_220
	dd	425
	dd	24
	align	4
_640:
	dd	_220
	dd	429
	dd	9
	align	4
_643:
	dd	_220
	dd	430
	dd	6
	align	4
_646:
	dd	_220
	dd	440
	dd	9
	align	4
_648:
	dd	_220
	dd	441
	dd	8
	align	4
_650:
	dd	_220
	dd	444
	dd	8
	align	4
_652:
	dd	_220
	dd	444
	dd	23
	align	4
_1401:
	dd	0x40a00000
	align	4
_653:
	dd	_220
	dd	445
	dd	8
	align	4
_655:
	dd	_220
	dd	445
	dd	24
	align	4
_1402:
	dd	0x40a00000
	align	4
_656:
	dd	_220
	dd	446
	dd	8
	align	4
_658:
	dd	_220
	dd	446
	dd	25
	align	4
_1403:
	dd	0x40a00000
	align	4
_659:
	dd	_220
	dd	447
	dd	8
	align	4
_661:
	dd	_220
	dd	447
	dd	24
	align	4
_1404:
	dd	0x40a00000
	align	4
_662:
	dd	_220
	dd	453
	dd	8
	align	4
_664:
	dd	_220
	dd	453
	dd	21
	align	4
_1405:
	dd	0x3dcccccd
	align	4
_665:
	dd	_220
	dd	475
	dd	8
	align	4
_667:
	dd	_220
	dd	477
	dd	9
	align	4
_672:
	dd	_220
	dd	481
	dd	9
	align	4
_1406:
	dd	0x3f000000
	align	4
_1407:
	dd	0x3f000000
	align	4
_1408:
	dd	0x3f000000
	align	4
_674:
	dd	_220
	dd	485
	dd	8
	align	4
_675:
	dd	_220
	dd	496
	dd	7
	align	4
_676:
	dd	_220
	dd	497
	dd	7
	align	4
_678:
	dd	_220
	dd	499
	dd	7
	align	4
_681:
	dd	_220
	dd	500
	dd	8
	align	4
_682:
	dd	_220
	dd	501
	dd	8
	align	4
_684:
	dd	_220
	dd	502
	dd	8
	align	4
_685:
	dd	_220
	dd	507
	dd	4
_795:
	db	"b3dSetFile",0
_796:
	db	"file",0
	align	4
_794:
	dd	1
	dd	_795
	dd	2
	dd	_796
	dd	_117
	dd	-4
	dd	0
	align	4
_792:
	dd	_213
	dd	12
	dd	2
	align	4
_793:
	dd	_213
	dd	13
	dd	2
_799:
	db	"b3dReadByte",0
	align	4
_798:
	dd	1
	dd	_799
	dd	0
	align	4
_797:
	dd	_213
	dd	19
	dd	2
_802:
	db	"b3dReadInt",0
	align	4
_801:
	dd	1
	dd	_802
	dd	0
	align	4
_800:
	dd	_213
	dd	23
	dd	2
_805:
	db	"b3dReadFloat",0
	align	4
_804:
	dd	1
	dd	_805
	dd	0
	align	4
_803:
	dd	_213
	dd	27
	dd	2
_818:
	db	"b3dReadString",0
_819:
	db	"ch",0
	align	4
_817:
	dd	1
	dd	_818
	dd	2
	dd	_819
	dd	_117
	dd	-4
	dd	0
	align	4
_806:
	dd	_213
	dd	35
	dd	2
	align	4
_807:
	dd	_213
	dd	32
	dd	3
	align	4
_809:
	dd	_213
	dd	33
	dd	3
	align	4
_811:
	dd	_213
	dd	33
	dd	11
	align	4
_812:
	dd	_213
	dd	34
	dd	3
_834:
	db	"b3dReadChunk",0
_835:
	db	"k",0
_836:
	db	"tag",0
_837:
	db	"sz",0
	align	4
_833:
	dd	1
	dd	_834
	dd	2
	dd	_835
	dd	_117
	dd	-8
	dd	2
	dd	_836
	dd	_733
	dd	-4
	dd	2
	dd	_837
	dd	_117
	dd	-12
	dd	0
	align	4
_820:
	dd	_213
	dd	39
	dd	2
	align	4
_823:
	dd	_213
	dd	40
	dd	3
	align	4
_825:
	dd	_213
	dd	42
	dd	2
	align	4
_827:
	dd	_213
	dd	43
	dd	2
	align	4
_828:
	dd	_213
	dd	44
	dd	2
	align	4
_832:
	dd	_213
	dd	45
	dd	2
_843:
	db	"b3dExitChunk",0
	align	4
_842:
	dd	1
	dd	_843
	dd	0
	align	4
_838:
	dd	_213
	dd	49
	dd	2
	align	4
_841:
	dd	_213
	dd	50
	dd	2
_848:
	db	"b3dChunkSize",0
	align	4
_847:
	dd	1
	dd	_848
	dd	0
	align	4
_844:
	dd	_213
	dd	54
	dd	2
_851:
	db	"b3dWriteByte",0
_852:
	db	"n",0
	align	4
_850:
	dd	1
	dd	_851
	dd	2
	dd	_852
	dd	_117
	dd	-4
	dd	0
	align	4
_849:
	dd	_213
	dd	60
	dd	2
_855:
	db	"b3dWriteInt",0
	align	4
_854:
	dd	1
	dd	_855
	dd	2
	dd	_852
	dd	_117
	dd	-4
	dd	0
	align	4
_853:
	dd	_213
	dd	64
	dd	2
_858:
	db	"b3dWriteFloat",0
	align	4
_857:
	dd	1
	dd	_858
	dd	2
	dd	_852
	dd	_719
	dd	-4
	dd	0
	align	4
_856:
	dd	_213
	dd	68
	dd	2
_871:
	db	"b3dWriteString",0
	align	4
_870:
	dd	1
	dd	_871
	dd	2
	dd	_761
	dd	_733
	dd	-4
	dd	2
	dd	_835
	dd	_117
	dd	-8
	dd	2
	dd	_819
	dd	_117
	dd	-12
	dd	0
	align	4
_859:
	dd	_213
	dd	72
	dd	2
	align	4
_863:
	dd	_213
	dd	73
	dd	3
	align	4
_865:
	dd	_213
	dd	74
	dd	3
	align	4
_866:
	dd	_213
	dd	75
	dd	3
	align	4
_868:
	dd	_213
	dd	75
	dd	11
	align	4
_869:
	dd	_213
	dd	77
	dd	2
_883:
	db	"b3dBeginChunk",0
	align	4
_882:
	dd	1
	dd	_883
	dd	2
	dd	_836
	dd	_733
	dd	-4
	dd	2
	dd	_835
	dd	_117
	dd	-8
	dd	0
	align	4
_872:
	dd	_213
	dd	81
	dd	2
	align	4
_873:
	dd	_213
	dd	82
	dd	2
	align	4
_876:
	dd	_213
	dd	83
	dd	3
	align	4
_877:
	dd	_213
	dd	85
	dd	2
	align	4
_878:
	dd	_213
	dd	86
	dd	2
_895:
	db	"b3dEndChunk",0
	align	4
_894:
	dd	1
	dd	_895
	dd	2
	dd	_852
	dd	_117
	dd	-4
	dd	0
	align	4
_884:
	dd	_213
	dd	90
	dd	2
	align	4
_886:
	dd	_213
	dd	91
	dd	2
	align	4
_889:
	dd	_213
	dd	92
	dd	2
	align	4
_892:
	dd	_213
	dd	93
	dd	2
	align	4
_893:
	dd	_213
	dd	94
	dd	2
_1110:
	db	"DoGadgetAction",0
_1111:
	db	"gadget",0
_1112:
	db	":Object",0
_1113:
	db	"texfile",0
_1114:
	db	"item",0
_1115:
	db	"BaseNode",0
_1116:
	db	"Bs",0
_1117:
	db	"wincount",0
_1118:
	db	"X",0
_1119:
	db	"Y",0
_1120:
	db	"Z",0
_1121:
	db	"ScaleX",0
_1122:
	db	"ScaleY",0
_1123:
	db	"ScaleZ",0
_1124:
	db	"BaseTex",0
_1125:
	db	"RoofNode",0
_1126:
	db	"rs",0
_1127:
	db	"RoofTex",0
_1128:
	db	"DoorNode",0
_1129:
	db	"dn",0
_1130:
	db	"DoorTex",0
_1131:
	db	"windowToRotate",0
	align	4
_1109:
	dd	1
	dd	_1110
	dd	2
	dd	_1111
	dd	_1112
	dd	-4
	dd	2
	dd	_117
	dd	_117
	dd	-40
	dd	2
	dd	_1113
	dd	_733
	dd	-8
	dd	2
	dd	_1114
	dd	_117
	dd	-44
	dd	2
	dd	_1115
	dd	_743
	dd	-12
	dd	2
	dd	_1116
	dd	_733
	dd	-16
	dd	2
	dd	_1117
	dd	_117
	dd	-48
	dd	2
	dd	_1118
	dd	_117
	dd	-52
	dd	2
	dd	_1119
	dd	_117
	dd	-56
	dd	2
	dd	_1120
	dd	_117
	dd	-60
	dd	2
	dd	_1121
	dd	_117
	dd	-64
	dd	2
	dd	_1122
	dd	_117
	dd	-68
	dd	2
	dd	_1123
	dd	_117
	dd	-72
	dd	2
	dd	_1124
	dd	_117
	dd	-76
	dd	2
	dd	_1125
	dd	_743
	dd	-20
	dd	2
	dd	_1126
	dd	_733
	dd	-24
	dd	2
	dd	_1127
	dd	_117
	dd	-80
	dd	2
	dd	_1128
	dd	_743
	dd	-28
	dd	2
	dd	_1129
	dd	_733
	dd	-32
	dd	2
	dd	_1130
	dd	_117
	dd	-84
	dd	2
	dd	_1131
	dd	_733
	dd	-36
	dd	0
	align	4
_896:
	dd	_220
	dd	519
	dd	2
	align	4
_907:
	dd	_220
	dd	523
	dd	4
	align	4
_912:
	dd	_220
	dd	524
	dd	4
	align	4
_68:
	dd	_bbStringClass
	dd	2147483647
	dd	10
	dw	46,46,47,77,101,115,104,101,115,47
	align	4
_913:
	dd	_220
	dd	525
	dd	5
	align	4
_917:
	dd	_220
	dd	526
	dd	6
	align	4
_72:
	dd	_bbStringClass
	dd	2147483647
	dd	19
	dw	87,105,110,100,111,119,115,47,87,105,110,100,111,119,49,46
	dw	98,51,100
	align	4
_921:
	dd	_220
	dd	527
	dd	6
	align	4
_930:
	dd	_220
	dd	533
	dd	5
	align	4
_935:
	dd	_220
	dd	534
	dd	5
	align	4
_74:
	dd	_bbStringClass
	dd	2147483647
	dd	11
	dw	98,109,112,44,112,110,103,44,106,112,103
	align	4
_73:
	dd	_bbStringClass
	dd	2147483647
	dd	17
	dw	80,105,99,107,32,66,97,115,101,32,84,101,120,116,117,114
	dw	101
	align	4
_937:
	dd	_220
	dd	535
	dd	5
	align	4
_941:
	dd	_220
	dd	536
	dd	5
	align	4
_946:
	dd	_220
	dd	543
	dd	4
	align	4
_948:
	dd	_220
	dd	544
	dd	4
	align	4
_955:
	dd	_220
	dd	546
	dd	6
	align	4
_956:
	dd	_220
	dd	547
	dd	6
	align	4
_957:
	dd	_220
	dd	548
	dd	6
	align	4
_958:
	dd	_220
	dd	549
	dd	6
	align	4
_959:
	dd	_220
	dd	551
	dd	6
	align	4
_960:
	dd	_220
	dd	552
	dd	6
	align	4
_961:
	dd	_220
	dd	553
	dd	6
	align	4
_962:
	dd	_220
	dd	554
	dd	6
	align	4
_963:
	dd	_220
	dd	556
	dd	6
	align	4
_964:
	dd	_220
	dd	557
	dd	6
	align	4
_965:
	dd	_220
	dd	558
	dd	6
	align	4
_966:
	dd	_220
	dd	559
	dd	6
	align	4
_967:
	dd	_220
	dd	561
	dd	6
	align	4
_968:
	dd	_220
	dd	562
	dd	6
	align	4
_969:
	dd	_220
	dd	563
	dd	6
	align	4
_970:
	dd	_220
	dd	564
	dd	6
	align	4
_971:
	dd	_220
	dd	571
	dd	4
	align	4
_973:
	dd	_220
	dd	572
	dd	4
	align	4
_977:
	dd	_220
	dd	573
	dd	4
	align	4
_978:
	dd	_220
	dd	574
	dd	5
	align	4
_76:
	dd	_bbStringClass
	dd	2147483647
	dd	4
	dw	46,98,51,100
	align	4
_75:
	dd	_bbStringClass
	dd	2147483647
	dd	8
	dw	66,97,115,101,101,120,116,47
	align	4
_979:
	dd	_220
	dd	575
	dd	5
	align	4
_77:
	dd	_bbStringClass
	dd	2147483647
	dd	5
	dw	66,97,115,101,47
	align	4
_980:
	dd	_220
	dd	576
	dd	5
	align	4
_981:
	dd	_220
	dd	577
	dd	5
	align	4
_78:
	dd	_bbStringClass
	dd	2147483647
	dd	4
	dw	114,111,111,102
	align	4
_982:
	dd	_220
	dd	578
	dd	5
	align	4
_79:
	dd	_bbStringClass
	dd	2147483647
	dd	4
	dw	100,111,111,114
	align	4
_983:
	dd	_220
	dd	580
	dd	5
	align	4
_984:
	dd	_220
	dd	581
	dd	5
	align	4
_985:
	dd	_220
	dd	582
	dd	5
	align	4
_986:
	dd	_220
	dd	584
	dd	5
	align	4
_987:
	dd	_220
	dd	585
	dd	5
	align	4
_988:
	dd	_220
	dd	586
	dd	5
	align	4
_989:
	dd	_220
	dd	588
	dd	5
	align	4
_992:
	dd	_220
	dd	590
	dd	7
	align	4
_83:
	dd	_bbStringClass
	dd	2147483647
	dd	6
	dw	119,105,110,100,111,119
	align	4
_996:
	dd	_220
	dd	591
	dd	6
	align	4
_1000:
	dd	_220
	dd	592
	dd	7
	align	4
_1006:
	dd	_220
	dd	593
	dd	7
	align	4
_1012:
	dd	_220
	dd	594
	dd	7
	align	4
_1018:
	dd	_220
	dd	595
	dd	7
	align	4
_1022:
	dd	_220
	dd	597
	dd	7
	align	4
_1023:
	dd	_220
	dd	601
	dd	5
	align	4
_1025:
	dd	_220
	dd	602
	dd	5
	align	4
_1027:
	dd	_220
	dd	603
	dd	5
	align	4
_1029:
	dd	_220
	dd	604
	dd	5
	align	4
_1031:
	dd	_220
	dd	605
	dd	5
	align	4
_1033:
	dd	_220
	dd	606
	dd	5
	align	4
_1035:
	dd	_220
	dd	607
	dd	9
	align	4
_1036:
	dd	_220
	dd	608
	dd	5
	align	4
_1037:
	dd	_220
	dd	611
	dd	5
	align	4
_1038:
	dd	_220
	dd	612
	dd	5
	align	4
_1039:
	dd	_220
	dd	613
	dd	5
	align	4
_1040:
	dd	_220
	dd	614
	dd	5
	align	4
_1043:
	dd	_220
	dd	615
	dd	5
	align	4
_84:
	dd	_bbStringClass
	dd	2147483647
	dd	12
	dw	46,46,47,84,101,120,116,117,114,101,115,47
	align	4
_1044:
	dd	_220
	dd	616
	dd	5
	align	4
_1045:
	dd	_220
	dd	617
	dd	5
	align	4
_1047:
	dd	_220
	dd	618
	dd	5
	align	4
_1048:
	dd	_220
	dd	623
	dd	4
	align	4
_1050:
	dd	_220
	dd	624
	dd	4
	align	4
_1054:
	dd	_220
	dd	625
	dd	4
	align	4
_1055:
	dd	_220
	dd	626
	dd	5
	align	4
_85:
	dd	_bbStringClass
	dd	2147483647
	dd	6
	dw	114,111,111,102,115,47
	align	4
_1056:
	dd	_220
	dd	628
	dd	5
	align	4
_1057:
	dd	_220
	dd	629
	dd	5
	align	4
_1058:
	dd	_220
	dd	630
	dd	5
	align	4
_1059:
	dd	_220
	dd	631
	dd	5
	align	4
_1060:
	dd	_220
	dd	632
	dd	5
	align	4
_1061:
	dd	_220
	dd	633
	dd	5
	align	4
_1062:
	dd	_220
	dd	634
	dd	5
	align	4
_1063:
	dd	_220
	dd	635
	dd	5
	align	4
_1064:
	dd	_220
	dd	637
	dd	5
	align	4
_1065:
	dd	_220
	dd	638
	dd	5
	align	4
_1067:
	dd	_220
	dd	639
	dd	5
	align	4
_1068:
	dd	_220
	dd	643
	dd	4
	align	4
_1070:
	dd	_220
	dd	644
	dd	4
	align	4
_1074:
	dd	_220
	dd	645
	dd	4
	align	4
_1075:
	dd	_220
	dd	646
	dd	5
	align	4
_86:
	dd	_bbStringClass
	dd	2147483647
	dd	6
	dw	68,111,111,114,115,47
	align	4
_1076:
	dd	_220
	dd	648
	dd	5
	align	4
_1077:
	dd	_220
	dd	649
	dd	5
	align	4
_1078:
	dd	_220
	dd	650
	dd	5
	align	4
_1079:
	dd	_220
	dd	651
	dd	5
	align	4
_1080:
	dd	_220
	dd	652
	dd	5
	align	4
_1081:
	dd	_220
	dd	653
	dd	5
	align	4
_1082:
	dd	_220
	dd	654
	dd	5
	align	4
_1083:
	dd	_220
	dd	656
	dd	5
	align	4
_1084:
	dd	_220
	dd	657
	dd	5
	align	4
_1086:
	dd	_220
	dd	658
	dd	5
	align	4
_1087:
	dd	_220
	dd	663
	dd	6
	align	4
_1089:
	dd	_220
	dd	664
	dd	7
	align	4
_1092:
	dd	_220
	dd	665
	dd	7
	align	4
_1095:
	dd	_220
	dd	666
	dd	7
	align	4
_1096:
	dd	_220
	dd	669
	dd	5
	align	4
_1099:
	dd	_220
	dd	670
	dd	5
	align	4
_1100:
	dd	_220
	dd	671
	dd	5
	align	4
_1103:
	dd	_220
	dd	672
	dd	5
	align	4
_1104:
	dd	_220
	dd	677
	dd	4
	align	4
_1106:
	dd	_220
	dd	678
	dd	4
_1153:
	db	"enumFilesCallback",0
_1154:
	db	"callback",0
_1155:
	db	"($)i",0
_1156:
	db	"dir",0
_1157:
	db	"folder",0
_1158:
	db	"fullPath",0
	align	4
_1152:
	dd	1
	dd	_1153
	dd	2
	dd	_1154
	dd	_1155
	dd	-4
	dd	2
	dd	_1156
	dd	_733
	dd	-8
	dd	2
	dd	_1157
	dd	_117
	dd	-20
	dd	2
	dd	_796
	dd	_733
	dd	-12
	dd	2
	dd	_1158
	dd	_733
	dd	-16
	dd	0
	align	4
_1132:
	dd	_220
	dd	688
	dd	2
	align	4
_1134:
	dd	_220
	dd	689
	dd	2
	align	4
_1136:
	dd	_220
	dd	703
	dd	2
	align	4
_1137:
	dd	_220
	dd	692
	dd	3
	align	4
_1138:
	dd	_220
	dd	694
	dd	3
	align	4
_1144:
	dd	_220
	dd	695
	dd	4
	align	4
_93:
	dd	_bbStringClass
	dd	2147483647
	dd	1
	dw	47
	align	4
_1146:
	dd	_220
	dd	697
	dd	4
	align	4
_1148:
	dd	_220
	dd	698
	dd	5
	align	4
_1150:
	dd	_220
	dd	700
	dd	5
	align	4
_1151:
	dd	_220
	dd	705
	dd	2
_1179:
	db	"enumFoldersCallback",0
	align	4
_1178:
	dd	1
	dd	_1179
	dd	2
	dd	_1154
	dd	_1155
	dd	-4
	dd	2
	dd	_1156
	dd	_733
	dd	-8
	dd	2
	dd	_1157
	dd	_117
	dd	-20
	dd	2
	dd	_796
	dd	_733
	dd	-12
	dd	2
	dd	_1158
	dd	_733
	dd	-16
	dd	0
	align	4
_1159:
	dd	_220
	dd	715
	dd	2
	align	4
_1161:
	dd	_220
	dd	716
	dd	2
	align	4
_1163:
	dd	_220
	dd	730
	dd	2
	align	4
_1164:
	dd	_220
	dd	719
	dd	3
	align	4
_1165:
	dd	_220
	dd	721
	dd	3
	align	4
_1171:
	dd	_220
	dd	722
	dd	4
	align	4
_1173:
	dd	_220
	dd	724
	dd	4
	align	4
_1175:
	dd	_220
	dd	725
	dd	5
	align	4
_1176:
	dd	_220
	dd	726
	dd	5
	align	4
_1177:
	dd	_220
	dd	732
	dd	2
_1202:
	db	"enumFolders",0
_1203:
	db	"list",0
	align	4
_1201:
	dd	1
	dd	_1202
	dd	2
	dd	_1203
	dd	_771
	dd	-4
	dd	2
	dd	_1156
	dd	_733
	dd	-8
	dd	2
	dd	_1157
	dd	_117
	dd	-20
	dd	2
	dd	_796
	dd	_733
	dd	-12
	dd	2
	dd	_1158
	dd	_733
	dd	-16
	dd	0
	align	4
_1180:
	dd	_220
	dd	742
	dd	2
	align	4
_1182:
	dd	_220
	dd	743
	dd	2
	align	4
_1184:
	dd	_220
	dd	758
	dd	2
	align	4
_1185:
	dd	_220
	dd	746
	dd	3
	align	4
_1186:
	dd	_220
	dd	748
	dd	3
	align	4
_1192:
	dd	_220
	dd	749
	dd	4
	align	4
_1194:
	dd	_220
	dd	751
	dd	4
	align	4
_1196:
	dd	_220
	dd	753
	dd	5
	align	4
_1199:
	dd	_220
	dd	754
	dd	5
	align	4
_1200:
	dd	_220
	dd	760
	dd	2
_1227:
	db	"enumFiles",0
	align	4
_1226:
	dd	1
	dd	_1227
	dd	2
	dd	_1203
	dd	_771
	dd	-4
	dd	2
	dd	_1156
	dd	_733
	dd	-8
	dd	2
	dd	_1157
	dd	_117
	dd	-20
	dd	2
	dd	_796
	dd	_733
	dd	-12
	dd	2
	dd	_1158
	dd	_733
	dd	-16
	dd	0
	align	4
_1204:
	dd	_220
	dd	771
	dd	2
	align	4
_1206:
	dd	_220
	dd	772
	dd	2
	align	4
_1208:
	dd	_220
	dd	788
	dd	2
	align	4
_1209:
	dd	_220
	dd	776
	dd	3
	align	4
_1210:
	dd	_220
	dd	778
	dd	3
	align	4
_1216:
	dd	_220
	dd	779
	dd	4
	align	4
_1218:
	dd	_220
	dd	781
	dd	4
	align	4
_1220:
	dd	_220
	dd	782
	dd	5
	align	4
_1222:
	dd	_220
	dd	784
	dd	5
	align	4
_1225:
	dd	_220
	dd	790
	dd	2
_1267:
	db	"MakeSkyBox",0
_1268:
	db	"m",0
_1269:
	db	"s",0
	align	4
_1266:
	dd	1
	dd	_1267
	dd	2
	dd	_796
	dd	_733
	dd	-4
	dd	2
	dd	_1268
	dd	_117
	dd	-8
	dd	2
	dd	_716
	dd	_117
	dd	-12
	dd	2
	dd	_1269
	dd	_117
	dd	-16
	dd	0
	align	4
_1228:
	dd	_220
	dd	798
	dd	2
	align	4
_1230:
	dd	_220
	dd	800
	dd	2
	align	4
_103:
	dd	_bbStringClass
	dd	2147483647
	dd	7
	dw	95,70,82,46,98,109,112
	align	4
_1232:
	dd	_220
	dd	801
	dd	2
	align	4
_1234:
	dd	_220
	dd	802
	dd	2
	align	4
_1235:
	dd	_220
	dd	803
	dd	2
	align	4
_1236:
	dd	_220
	dd	804
	dd	2
	align	4
_1237:
	dd	_220
	dd	805
	dd	2
	align	4
_1238:
	dd	_220
	dd	808
	dd	2
	align	4
_104:
	dd	_bbStringClass
	dd	2147483647
	dd	7
	dw	95,76,70,46,98,109,112
	align	4
_1239:
	dd	_220
	dd	809
	dd	2
	align	4
_1240:
	dd	_220
	dd	810
	dd	2
	align	4
_1241:
	dd	_220
	dd	811
	dd	2
	align	4
_1242:
	dd	_220
	dd	812
	dd	2
	align	4
_1243:
	dd	_220
	dd	813
	dd	2
	align	4
_1244:
	dd	_220
	dd	816
	dd	2
	align	4
_105:
	dd	_bbStringClass
	dd	2147483647
	dd	7
	dw	95,66,75,46,98,109,112
	align	4
_1245:
	dd	_220
	dd	817
	dd	2
	align	4
_1246:
	dd	_220
	dd	818
	dd	2
	align	4
_1247:
	dd	_220
	dd	819
	dd	2
	align	4
_1248:
	dd	_220
	dd	820
	dd	2
	align	4
_1249:
	dd	_220
	dd	821
	dd	2
	align	4
_1250:
	dd	_220
	dd	824
	dd	2
	align	4
_106:
	dd	_bbStringClass
	dd	2147483647
	dd	7
	dw	95,82,84,46,98,109,112
	align	4
_1251:
	dd	_220
	dd	825
	dd	2
	align	4
_1252:
	dd	_220
	dd	826
	dd	2
	align	4
_1253:
	dd	_220
	dd	827
	dd	2
	align	4
_1254:
	dd	_220
	dd	828
	dd	2
	align	4
_1255:
	dd	_220
	dd	829
	dd	2
	align	4
_1256:
	dd	_220
	dd	832
	dd	2
	align	4
_107:
	dd	_bbStringClass
	dd	2147483647
	dd	7
	dw	95,85,80,46,98,109,112
	align	4
_1257:
	dd	_220
	dd	833
	dd	2
	align	4
_1258:
	dd	_220
	dd	834
	dd	2
	align	4
_1259:
	dd	_220
	dd	835
	dd	2
	align	4
_1260:
	dd	_220
	dd	836
	dd	2
	align	4
_1261:
	dd	_220
	dd	837
	dd	2
	align	4
_1262:
	dd	_220
	dd	840
	dd	2
	align	4
_1263:
	dd	_220
	dd	841
	dd	2
	align	4
_1264:
	dd	_220
	dd	842
	dd	2
	align	4
_1265:
	dd	_220
	dd	843
	dd	2
_1273:
	db	"New",0
_1274:
	db	"Self",0
	align	4
_1272:
	dd	1
	dd	_1273
	dd	2
	dd	_1274
	dd	_694
	dd	-4
	dd	0
_1270:
	db	"b",0
_1280:
	db	"Init",0
	align	4
_1279:
	dd	1
	dd	_1280
	dd	0
	align	4
_1278:
	dd	_220
	dd	854
	dd	3
_1329:
	db	"Update",0
_1330:
	db	"CUR_Event",0
_1331:
	db	":brl.event.TEvent",0
	align	4
_1328:
	dd	1
	dd	_1329
	dd	2
	dd	_1274
	dd	_694
	dd	-4
	dd	2
	dd	_1330
	dd	_1331
	dd	-8
	dd	0
	align	4
_1281:
	dd	_220
	dd	858
	dd	3
	align	4
_1283:
	dd	_220
	dd	859
	dd	3
	align	4
_1292:
	dd	_220
	dd	861
	dd	5
	align	4
_1298:
	dd	_220
	dd	862
	dd	5
	align	4
_1304:
	dd	_220
	dd	864
	dd	5
	align	4
_1310:
	dd	_220
	dd	866
	dd	5
	align	4
_1319:
	dd	_220
	dd	868
	dd	5
_1336:
	db	"getX",0
	align	4
_1335:
	dd	1
	dd	_1336
	dd	2
	dd	_1274
	dd	_694
	dd	-4
	dd	0
	align	4
_1332:
	dd	_220
	dd	873
	dd	3
_1341:
	db	"getY",0
	align	4
_1340:
	dd	1
	dd	_1341
	dd	2
	dd	_1274
	dd	_694
	dd	-4
	dd	0
	align	4
_1337:
	dd	_220
	dd	877
	dd	3
_1346:
	db	"getZ",0
	align	4
_1345:
	dd	1
	dd	_1346
	dd	2
	dd	_1274
	dd	_694
	dd	-4
	dd	0
	align	4
_1342:
	dd	_220
	dd	880
	dd	3
_1358:
	db	"GetButton",0
_1359:
	db	"_Button",0
	align	4
_1357:
	dd	1
	dd	_1358
	dd	2
	dd	_1274
	dd	_694
	dd	-4
	dd	2
	dd	_1359
	dd	_117
	dd	-8
	dd	0
	align	4
_1347:
	dd	_220
	dd	884
	dd	3
	align	4
_1351:
	dd	_220
	dd	885
	dd	4
_1376:
	db	"TextureLoadedMesh",0
_1377:
	db	"Mesh",0
_1378:
	db	":sidesign.minib3d.TMesh",0
_1379:
	db	"Tex",0
_1380:
	db	":sidesign.minib3d.TTexture",0
_1381:
	db	"Index",0
_1382:
	db	"Surf",0
_1383:
	db	":sidesign.minib3d.TSurface",0
	align	4
_1375:
	dd	1
	dd	_1376
	dd	2
	dd	_1377
	dd	_1378
	dd	-4
	dd	2
	dd	_1379
	dd	_1380
	dd	-8
	dd	2
	dd	_1381
	dd	_117
	dd	-16
	dd	2
	dd	_1382
	dd	_1383
	dd	-12
	dd	0
	align	4
_1360:
	dd	_220
	dd	892
	dd	2
	align	4
_1372:
	dd	_220
	dd	893
	dd	3
_1400:
	db	"invsLoadedMesh",0
	align	4
_1399:
	dd	1
	dd	_1400
	dd	2
	dd	_1377
	dd	_1378
	dd	-4
	dd	2
	dd	_1382
	dd	_1383
	dd	-8
	dd	0
	align	4
_1384:
	dd	_220
	dd	898
	dd	2
	align	4
_1396:
	dd	_220
	dd	899
	dd	3
