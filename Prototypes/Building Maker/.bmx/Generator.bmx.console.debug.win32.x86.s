	format	MS COFF
	extrn	___bb_appstub_appstub
	extrn	___bb_audio_audio
	extrn	___bb_blitz_blitz
	extrn	___bb_cocoamaxgui_cocoamaxgui
	extrn	___bb_d3d7max2d_d3d7max2d
	extrn	___bb_directsoundaudio_directsoundaudio
	extrn	___bb_dreamotion3d_dreamotion3d
	extrn	___bb_eventqueue_eventqueue
	extrn	___bb_fltkmaxgui_fltkmaxgui
	extrn	___bb_freeaudioaudio_freeaudioaudio
	extrn	___bb_freejoy_freejoy
	extrn	___bb_freeprocess_freeprocess
	extrn	___bb_freetypefont_freetypefont
	extrn	___bb_gnet_gnet
	extrn	___bb_macos_macos
	extrn	___bb_maxgui_maxgui
	extrn	___bb_maxutil_maxutil
	extrn	___bb_minib3d_minib3d
	extrn	___bb_oggloader_oggloader
	extrn	___bb_openalaudio_openalaudio
	extrn	___bb_tgaloader_tgaloader
	extrn	___bb_timer_timer
	extrn	___bb_wavloader_wavloader
	extrn	___bb_win32maxgui_win32maxgui
	extrn	_bbArrayNew1D
	extrn	_bbEmptyArray
	extrn	_bbEmptyString
	extrn	_bbEnd
	extrn	_bbFloatToInt
	extrn	_bbHandleFromObject
	extrn	_bbHandleToObject
	extrn	_bbNullObject
	extrn	_bbObjectDowncast
	extrn	_bbOnDebugEnterScope
	extrn	_bbOnDebugEnterStm
	extrn	_bbOnDebugLeaveScope
	extrn	_bbStringClass
	extrn	_bbStringConcat
	extrn	_bbStringFromInt
	extrn	_brl_blitz_ArrayBoundsError
	extrn	_brl_blitz_DebugLog
	extrn	_brl_stream_CloseStream
	extrn	_brl_stream_WriteLine
	extrn	_brl_stream_WriteStream
	extrn	_brl_system_RequestFile
	extrn	_sidesign_minib3d_EntityX
	extrn	_sidesign_minib3d_EntityY
	extrn	_sidesign_minib3d_EntityZ
	extrn	_sidesign_minib3d_FindChild
	extrn	_sidesign_minib3d_LoadAnimMesh
	extrn	_sidesign_minib3d_TEntity
	public	__bb_main
	public	_bb_WinChild
	public	_bb_WindowsX
	public	_bb_WindowsY
	public	_bb_WindowsZ
	section	"code" code
__bb_main:
	push	ebp
	mov	ebp,esp
	sub	esp,88
	push	ebx
	push	esi
	cmp	dword [_143],0
	je	_144
	mov	eax,0
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_144:
	mov	dword [_143],1
	mov	dword [ebp-4],_bbEmptyString
	mov	dword [ebp-8],_bbNullObject
	mov	dword [ebp-12],_bbEmptyString
	mov	dword [ebp-16],_bbEmptyString
	mov	dword [ebp-20],0
	mov	dword [ebp-24],0
	mov	dword [ebp-28],0
	mov	dword [ebp-32],0
	mov	dword [ebp-36],0
	mov	dword [ebp-40],0
	mov	dword [ebp-44],0
	mov	dword [ebp-48],0
	mov	dword [ebp-52],0
	mov	dword [ebp-56],0
	mov	eax,ebp
	push	eax
	push	_119
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	call	___bb_blitz_blitz
	call	___bb_minib3d_minib3d
	call	___bb_appstub_appstub
	call	___bb_audio_audio
	call	___bb_cocoamaxgui_cocoamaxgui
	call	___bb_d3d7max2d_d3d7max2d
	call	___bb_directsoundaudio_directsoundaudio
	call	___bb_eventqueue_eventqueue
	call	___bb_fltkmaxgui_fltkmaxgui
	call	___bb_freeaudioaudio_freeaudioaudio
	call	___bb_freetypefont_freetypefont
	call	___bb_gnet_gnet
	call	___bb_maxgui_maxgui
	call	___bb_maxutil_maxutil
	call	___bb_oggloader_oggloader
	call	___bb_openalaudio_openalaudio
	call	___bb_tgaloader_tgaloader
	call	___bb_timer_timer
	call	___bb_wavloader_wavloader
	call	___bb_win32maxgui_win32maxgui
	call	___bb_dreamotion3d_dreamotion3d
	call	___bb_freejoy_freejoy
	call	___bb_freeprocess_freeprocess
	call	___bb_macos_macos
	push	_33
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [_37]
	and	eax,1
	cmp	eax,0
	jne	_38
	push	10
	push	_35
	call	_bbArrayNew1D
	add	esp,8
	mov	dword [ebp-60],eax
	mov	eax,dword [ebp-60]
	inc	dword [eax+4]
	mov	eax,dword [ebp-60]
	mov	dword [_bb_WinChild],eax
	or	dword [_37],1
_38:
	mov	eax,dword [_37]
	and	eax,2
	cmp	eax,0
	jne	_41
	push	10
	push	_39
	call	_bbArrayNew1D
	add	esp,8
	mov	dword [ebp-64],eax
	mov	eax,dword [ebp-64]
	inc	dword [eax+4]
	mov	eax,dword [ebp-64]
	mov	dword [_bb_WindowsX],eax
	or	dword [_37],2
_41:
	mov	eax,dword [_37]
	and	eax,4
	cmp	eax,0
	jne	_44
	push	10
	push	_42
	call	_bbArrayNew1D
	add	esp,8
	mov	dword [ebp-68],eax
	mov	eax,dword [ebp-68]
	inc	dword [eax+4]
	mov	eax,dword [ebp-68]
	mov	dword [_bb_WindowsY],eax
	or	dword [_37],4
_44:
	mov	eax,dword [_37]
	and	eax,8
	cmp	eax,0
	jne	_47
	push	10
	push	_45
	call	_bbArrayNew1D
	add	esp,8
	mov	dword [ebp-72],eax
	mov	eax,dword [ebp-72]
	inc	dword [eax+4]
	mov	eax,dword [ebp-72]
	mov	dword [_bb_WindowsZ],eax
	or	dword [_37],8
_47:
	push	_48
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_21
	push	1
	push	_20
	push	_19
	call	_brl_system_RequestFile
	add	esp,16
	mov	dword [ebp-4],eax
	push	_50
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-4]
	call	_brl_stream_WriteStream
	add	esp,4
	mov	dword [ebp-8],eax
	push	_52
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_2
	push	0
	push	_23
	push	_22
	call	_brl_system_RequestFile
	add	esp,16
	mov	dword [ebp-12],eax
	push	_54
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-12]
	push	dword [ebp-8]
	call	_brl_stream_WriteLine
	add	esp,8
	push	_55
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_2
	push	0
	push	_23
	push	_24
	call	_brl_system_RequestFile
	add	esp,16
	mov	dword [ebp-16],eax
	push	_57
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-16]
	push	dword [ebp-8]
	call	_brl_stream_WriteLine
	add	esp,8
	push	_58
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_bbNullObject
	push	dword [ebp-16]
	call	_sidesign_minib3d_LoadAnimMesh
	add	esp,8
	push	eax
	call	_bbHandleFromObject
	add	esp,4
	mov	dword [ebp-20],eax
	push	_60
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_25
	push	_sidesign_minib3d_TEntity
	push	dword [ebp-20]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_sidesign_minib3d_FindChild
	add	esp,8
	push	eax
	call	_bbHandleFromObject
	add	esp,4
	mov	dword [ebp-24],eax
	push	_62
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_26
	push	_sidesign_minib3d_TEntity
	push	dword [ebp-20]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_sidesign_minib3d_FindChild
	add	esp,8
	push	eax
	call	_bbHandleFromObject
	add	esp,4
	mov	dword [ebp-28],eax
	push	_64
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	0
	push	_sidesign_minib3d_TEntity
	push	dword [ebp-24]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_sidesign_minib3d_EntityX
	add	esp,8
	sub	esp,8
	fstp	qword [esp]
	call	_bbFloatToInt
	add	esp,8
	mov	dword [ebp-32],eax
	push	_66
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	0
	push	_sidesign_minib3d_TEntity
	push	dword [ebp-24]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_sidesign_minib3d_EntityY
	add	esp,8
	sub	esp,8
	fstp	qword [esp]
	call	_bbFloatToInt
	add	esp,8
	mov	dword [ebp-36],eax
	push	_68
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	0
	push	_sidesign_minib3d_TEntity
	push	dword [ebp-24]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_sidesign_minib3d_EntityZ
	add	esp,8
	sub	esp,8
	fstp	qword [esp]
	call	_bbFloatToInt
	add	esp,8
	mov	dword [ebp-40],eax
	push	_70
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-32]
	call	_bbStringFromInt
	add	esp,4
	push	eax
	push	dword [ebp-8]
	call	_brl_stream_WriteLine
	add	esp,8
	push	_71
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-36]
	call	_bbStringFromInt
	add	esp,4
	push	eax
	push	dword [ebp-8]
	call	_brl_stream_WriteLine
	add	esp,8
	push	_72
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-40]
	call	_bbStringFromInt
	add	esp,4
	push	eax
	push	dword [ebp-8]
	call	_brl_stream_WriteLine
	add	esp,8
	push	_73
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	0
	push	_sidesign_minib3d_TEntity
	push	dword [ebp-28]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_sidesign_minib3d_EntityX
	add	esp,8
	sub	esp,8
	fstp	qword [esp]
	call	_bbFloatToInt
	add	esp,8
	mov	dword [ebp-44],eax
	push	_75
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	0
	push	_sidesign_minib3d_TEntity
	push	dword [ebp-28]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_sidesign_minib3d_EntityY
	add	esp,8
	sub	esp,8
	fstp	qword [esp]
	call	_bbFloatToInt
	add	esp,8
	mov	dword [ebp-48],eax
	push	_77
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	0
	push	_sidesign_minib3d_TEntity
	push	dword [ebp-28]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_sidesign_minib3d_EntityZ
	add	esp,8
	sub	esp,8
	fstp	qword [esp]
	call	_bbFloatToInt
	add	esp,8
	mov	dword [ebp-52],eax
	push	_79
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-44]
	call	_bbStringFromInt
	add	esp,4
	push	eax
	push	dword [ebp-8]
	call	_brl_stream_WriteLine
	add	esp,8
	push	_80
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-48]
	call	_bbStringFromInt
	add	esp,4
	push	eax
	push	dword [ebp-8]
	call	_brl_stream_WriteLine
	add	esp,8
	push	_81
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-52]
	call	_bbStringFromInt
	add	esp,4
	push	eax
	push	dword [ebp-8]
	call	_brl_stream_WriteLine
	add	esp,8
	push	_82
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-56],1
	jmp	_84
_29:
	push	_85
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-56]
	call	_bbStringFromInt
	add	esp,4
	push	eax
	call	_brl_blitz_DebugLog
	add	esp,4
	push	_86
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-56]
	mov	eax,dword [_bb_WinChild]
	cmp	ebx,dword [eax+20]
	jb	_88
	call	_brl_blitz_ArrayBoundsError
_88:
	mov	eax,dword [_bb_WinChild]
	shl	ebx,2
	add	eax,ebx
	mov	dword [ebp-76],eax
	mov	ebx,dword [ebp-76]
	push	dword [ebp-56]
	call	_bbStringFromInt
	add	esp,4
	push	eax
	push	_30
	call	_bbStringConcat
	add	esp,8
	push	eax
	push	_sidesign_minib3d_TEntity
	push	dword [ebp-20]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_sidesign_minib3d_FindChild
	add	esp,8
	push	eax
	call	_bbHandleFromObject
	add	esp,4
	mov	dword [ebx+24],eax
	push	_90
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-56]
	mov	eax,dword [_bb_WindowsX]
	cmp	ebx,dword [eax+20]
	jb	_92
	call	_brl_blitz_ArrayBoundsError
_92:
	mov	eax,dword [_bb_WindowsX]
	shl	ebx,2
	add	eax,ebx
	mov	dword [ebp-80],eax
	mov	esi,dword [ebp-56]
	mov	eax,dword [_bb_WinChild]
	cmp	esi,dword [eax+20]
	jb	_95
	call	_brl_blitz_ArrayBoundsError
_95:
	mov	ebx,dword [ebp-80]
	push	0
	push	_sidesign_minib3d_TEntity
	mov	eax,dword [_bb_WinChild]
	push	dword [eax+esi*4+24]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_sidesign_minib3d_EntityX
	add	esp,8
	sub	esp,8
	fstp	qword [esp]
	call	_bbFloatToInt
	add	esp,8
	mov	dword [ebx+24],eax
	push	_96
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-56]
	mov	eax,dword [_bb_WindowsY]
	cmp	ebx,dword [eax+20]
	jb	_98
	call	_brl_blitz_ArrayBoundsError
_98:
	mov	eax,dword [_bb_WindowsY]
	shl	ebx,2
	add	eax,ebx
	mov	dword [ebp-84],eax
	mov	esi,dword [ebp-56]
	mov	eax,dword [_bb_WinChild]
	cmp	esi,dword [eax+20]
	jb	_101
	call	_brl_blitz_ArrayBoundsError
_101:
	mov	ebx,dword [ebp-84]
	push	0
	push	_sidesign_minib3d_TEntity
	mov	eax,dword [_bb_WinChild]
	push	dword [eax+esi*4+24]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_sidesign_minib3d_EntityY
	add	esp,8
	sub	esp,8
	fstp	qword [esp]
	call	_bbFloatToInt
	add	esp,8
	mov	dword [ebx+24],eax
	push	_102
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-56]
	mov	eax,dword [_bb_WindowsZ]
	cmp	ebx,dword [eax+20]
	jb	_104
	call	_brl_blitz_ArrayBoundsError
_104:
	mov	eax,dword [_bb_WindowsZ]
	shl	ebx,2
	add	eax,ebx
	mov	dword [ebp-88],eax
	mov	esi,dword [ebp-56]
	mov	eax,dword [_bb_WinChild]
	cmp	esi,dword [eax+20]
	jb	_107
	call	_brl_blitz_ArrayBoundsError
_107:
	mov	ebx,dword [ebp-88]
	push	0
	push	_sidesign_minib3d_TEntity
	mov	eax,dword [_bb_WinChild]
	push	dword [eax+esi*4+24]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_sidesign_minib3d_EntityZ
	add	esp,8
	sub	esp,8
	fstp	qword [esp]
	call	_bbFloatToInt
	add	esp,8
	mov	dword [ebx+24],eax
	push	_108
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-56]
	mov	eax,dword [_bb_WindowsX]
	cmp	ebx,dword [eax+20]
	jb	_110
	call	_brl_blitz_ArrayBoundsError
_110:
	mov	eax,dword [_bb_WindowsX]
	push	dword [eax+ebx*4+24]
	call	_bbStringFromInt
	add	esp,4
	push	eax
	push	dword [ebp-8]
	call	_brl_stream_WriteLine
	add	esp,8
	push	_111
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-56]
	mov	eax,dword [_bb_WindowsY]
	cmp	ebx,dword [eax+20]
	jb	_113
	call	_brl_blitz_ArrayBoundsError
_113:
	mov	eax,dword [_bb_WindowsY]
	push	dword [eax+ebx*4+24]
	call	_bbStringFromInt
	add	esp,4
	push	eax
	push	dword [ebp-8]
	call	_brl_stream_WriteLine
	add	esp,8
	push	_114
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-56]
	mov	eax,dword [_bb_WindowsZ]
	cmp	ebx,dword [eax+20]
	jb	_116
	call	_brl_blitz_ArrayBoundsError
_116:
	mov	eax,dword [_bb_WindowsZ]
	push	dword [eax+ebx*4+24]
	call	_bbStringFromInt
	add	esp,4
	push	eax
	push	dword [ebp-8]
	call	_brl_stream_WriteLine
	add	esp,8
_27:
	add	dword [ebp-56],1
_84:
	cmp	dword [ebp-56],6
	jle	_29
_28:
	push	_117
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-8]
	call	_brl_stream_CloseStream
	add	esp,4
	push	_118
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	call	_bbEnd
	mov	ebx,0
_31:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
	section	"data" data writeable align 8
	align	4
_143:
	dd	0
_120:
	db	"Generator",0
_121:
	db	"WinChild",0
_122:
	db	"[]i",0
	align	4
_bb_WinChild:
	dd	_bbEmptyArray
_123:
	db	"WindowsX",0
	align	4
_bb_WindowsX:
	dd	_bbEmptyArray
_124:
	db	"WindowsY",0
	align	4
_bb_WindowsY:
	dd	_bbEmptyArray
_125:
	db	"WindowsZ",0
	align	4
_bb_WindowsZ:
	dd	_bbEmptyArray
_126:
	db	"SaveFile",0
_127:
	db	"$",0
_128:
	db	"File",0
_129:
	db	":brl.stream.TStream",0
_130:
	db	"BasePrim",0
_131:
	db	"BaseExt",0
_132:
	db	"BaseModel",0
_133:
	db	"i",0
_134:
	db	"Roofpoint",0
_135:
	db	"DoorPoint",0
_136:
	db	"RoofX",0
_137:
	db	"RoofY",0
_138:
	db	"RoofZ",0
_139:
	db	"DoorX",0
_140:
	db	"DoorY",0
_141:
	db	"DoorZ",0
_142:
	db	"wincount",0
	align	4
_119:
	dd	1
	dd	_120
	dd	4
	dd	_121
	dd	_122
	dd	_bb_WinChild
	dd	4
	dd	_123
	dd	_122
	dd	_bb_WindowsX
	dd	4
	dd	_124
	dd	_122
	dd	_bb_WindowsY
	dd	4
	dd	_125
	dd	_122
	dd	_bb_WindowsZ
	dd	2
	dd	_126
	dd	_127
	dd	-4
	dd	2
	dd	_128
	dd	_129
	dd	-8
	dd	2
	dd	_130
	dd	_127
	dd	-12
	dd	2
	dd	_131
	dd	_127
	dd	-16
	dd	2
	dd	_132
	dd	_133
	dd	-20
	dd	2
	dd	_134
	dd	_133
	dd	-24
	dd	2
	dd	_135
	dd	_133
	dd	-28
	dd	2
	dd	_136
	dd	_133
	dd	-32
	dd	2
	dd	_137
	dd	_133
	dd	-36
	dd	2
	dd	_138
	dd	_133
	dd	-40
	dd	2
	dd	_139
	dd	_133
	dd	-44
	dd	2
	dd	_140
	dd	_133
	dd	-48
	dd	2
	dd	_141
	dd	_133
	dd	-52
	dd	2
	dd	_142
	dd	_133
	dd	-56
	dd	0
_34:
	db	"C:/Documents and Settings/ckob/Desktop/coding/BuildingMaker/Generator.bmx",0
	align	4
_33:
	dd	_34
	dd	2
	dd	1
	align	4
_37:
	dd	0
_35:
	db	"i",0
_39:
	db	"i",0
_42:
	db	"i",0
_45:
	db	"i",0
	align	4
_48:
	dd	_34
	dd	4
	dd	1
	align	4
_21:
	dd	_bbStringClass
	dd	2147483647
	dd	11
	dw	109,101,115,104,101,115,47,66,97,115,101
	align	4
_20:
	dd	_bbStringClass
	dd	2147483647
	dd	3
	dw	98,109,102
	align	4
_19:
	dd	_bbStringClass
	dd	2147483647
	dd	7
	dw	83,97,118,101,32,65,115
	align	4
_50:
	dd	_34
	dd	5
	dd	1
	align	4
_52:
	dd	_34
	dd	7
	dd	1
	align	4
_2:
	dd	_bbStringClass
	dd	2147483647
	dd	0
	align	4
_23:
	dd	_bbStringClass
	dd	2147483647
	dd	3
	dw	98,51,100
	align	4
_22:
	dd	_bbStringClass
	dd	2147483647
	dd	19
	dw	80,105,99,107,32,66,97,115,101,32,80,114,105,109,105,116
	dw	105,118,101
	align	4
_54:
	dd	_34
	dd	9
	dd	1
	align	4
_55:
	dd	_34
	dd	11
	dd	1
	align	4
_24:
	dd	_bbStringClass
	dd	2147483647
	dd	18
	dw	80,105,99,107,32,66,97,115,101,32,69,120,116,101,110,100
	dw	101,100
	align	4
_57:
	dd	_34
	dd	12
	dd	1
	align	4
_58:
	dd	_34
	dd	14
	dd	1
	align	4
_60:
	dd	_34
	dd	16
	dd	1
	align	4
_25:
	dd	_bbStringClass
	dd	2147483647
	dd	4
	dw	114,111,111,102
	align	4
_62:
	dd	_34
	dd	17
	dd	1
	align	4
_26:
	dd	_bbStringClass
	dd	2147483647
	dd	4
	dw	100,111,111,114
	align	4
_64:
	dd	_34
	dd	21
	dd	1
	align	4
_66:
	dd	_34
	dd	22
	dd	1
	align	4
_68:
	dd	_34
	dd	23
	dd	1
	align	4
_70:
	dd	_34
	dd	24
	dd	1
	align	4
_71:
	dd	_34
	dd	25
	dd	1
	align	4
_72:
	dd	_34
	dd	26
	dd	1
	align	4
_73:
	dd	_34
	dd	29
	dd	1
	align	4
_75:
	dd	_34
	dd	30
	dd	1
	align	4
_77:
	dd	_34
	dd	31
	dd	1
	align	4
_79:
	dd	_34
	dd	32
	dd	1
	align	4
_80:
	dd	_34
	dd	33
	dd	1
	align	4
_81:
	dd	_34
	dd	34
	dd	1
	align	4
_82:
	dd	_34
	dd	36
	dd	1
	align	4
_85:
	dd	_34
	dd	37
	dd	2
	align	4
_86:
	dd	_34
	dd	38
	dd	3
	align	4
_30:
	dd	_bbStringClass
	dd	2147483647
	dd	6
	dw	119,105,110,100,111,119
	align	4
_90:
	dd	_34
	dd	39
	dd	3
	align	4
_96:
	dd	_34
	dd	40
	dd	3
	align	4
_102:
	dd	_34
	dd	41
	dd	3
	align	4
_108:
	dd	_34
	dd	42
	dd	3
	align	4
_111:
	dd	_34
	dd	43
	dd	3
	align	4
_114:
	dd	_34
	dd	44
	dd	3
	align	4
_117:
	dd	_34
	dd	47
	dd	1
	align	4
_118:
	dd	_34
	dd	48
	dd	1
