	format	MS COFF
	extrn	___bb_appstub_appstub
	extrn	___bb_audio_audio
	extrn	___bb_blitz_blitz
	extrn	___bb_cocoamaxgui_cocoamaxgui
	extrn	___bb_d3d7max2d_d3d7max2d
	extrn	___bb_directsoundaudio_directsoundaudio
	extrn	___bb_dreamotion3d_dreamotion3d
	extrn	___bb_eventqueue_eventqueue
	extrn	___bb_freeaudioaudio_freeaudioaudio
	extrn	___bb_freejoy_freejoy
	extrn	___bb_freeprocess_freeprocess
	extrn	___bb_freetypefont_freetypefont
	extrn	___bb_gnet_gnet
	extrn	___bb_macos_macos
	extrn	___bb_maxgui_maxgui
	extrn	___bb_maxutil_maxutil
	extrn	___bb_minib3d_minib3d
	extrn	___bb_oggloader_oggloader
	extrn	___bb_openalaudio_openalaudio
	extrn	___bb_reflection_reflection
	extrn	___bb_tgaloader_tgaloader
	extrn	___bb_timer_timer
	extrn	___bb_wavloader_wavloader
	extrn	___bb_win32maxgui_win32maxgui
	extrn	_bbAppTitle
	extrn	_bbEnd
	extrn	_bbGCFree
	extrn	_bbHandleFromObject
	extrn	_bbHandleToObject
	extrn	_bbNullObject
	extrn	_bbObjectDowncast
	extrn	_bbOnDebugEnterScope
	extrn	_bbOnDebugEnterStm
	extrn	_bbOnDebugLeaveScope
	extrn	_bbStringClass
	extrn	_brl_glmax2d_GLMax2DDriver
	extrn	_brl_graphics_Flip
	extrn	_brl_graphics_SetGraphicsDriver
	extrn	_brl_max2d_Cls
	extrn	_brl_polledinput_KeyHit
	extrn	_sidesign_minib3d_CameraClsColor
	extrn	_sidesign_minib3d_CreateCamera
	extrn	_sidesign_minib3d_CreateLight
	extrn	_sidesign_minib3d_EntityAlpha
	extrn	_sidesign_minib3d_EntityFX
	extrn	_sidesign_minib3d_EntityX
	extrn	_sidesign_minib3d_EntityY
	extrn	_sidesign_minib3d_EntityZ
	extrn	_sidesign_minib3d_FindChild
	extrn	_sidesign_minib3d_Graphics3D
	extrn	_sidesign_minib3d_LoadAnimMesh
	extrn	_sidesign_minib3d_LoadMesh
	extrn	_sidesign_minib3d_PointEntity
	extrn	_sidesign_minib3d_PositionEntity
	extrn	_sidesign_minib3d_RenderWorld
	extrn	_sidesign_minib3d_ScaleEntity
	extrn	_sidesign_minib3d_TCamera
	extrn	_sidesign_minib3d_TEntity
	extrn	_sidesign_minib3d_TranslateEntity
	extrn	_sidesign_minib3d_UpdateWorld
	public	__bb_main
	section	"code" code
__bb_main:
	push	ebp
	mov	ebp,esp
	sub	esp,20
	push	ebx
	cmp	dword [_67],0
	je	_68
	mov	eax,0
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_68:
	mov	dword [_67],1
	mov	dword [ebp-4],0
	mov	dword [ebp-8],0
	mov	dword [ebp-12],0
	mov	dword [ebp-16],0
	mov	dword [ebp-20],0
	push	ebp
	push	_59
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	call	___bb_blitz_blitz
	call	___bb_minib3d_minib3d
	call	___bb_appstub_appstub
	call	___bb_audio_audio
	call	___bb_cocoamaxgui_cocoamaxgui
	call	___bb_d3d7max2d_d3d7max2d
	call	___bb_directsoundaudio_directsoundaudio
	call	___bb_eventqueue_eventqueue
	call	___bb_freeaudioaudio_freeaudioaudio
	call	___bb_freetypefont_freetypefont
	call	___bb_gnet_gnet
	call	___bb_maxgui_maxgui
	call	___bb_maxutil_maxutil
	call	___bb_oggloader_oggloader
	call	___bb_openalaudio_openalaudio
	call	___bb_reflection_reflection
	call	___bb_tgaloader_tgaloader
	call	___bb_timer_timer
	call	___bb_wavloader_wavloader
	call	___bb_win32maxgui_win32maxgui
	call	___bb_dreamotion3d_dreamotion3d
	call	___bb_freejoy_freejoy
	call	___bb_freeprocess_freeprocess
	call	___bb_macos_macos
	push	_27
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,_18
	inc	dword [ebx+4]
	mov	eax,dword [_bbAppTitle]
	dec	dword [eax+4]
	jnz	_32
	push	eax
	call	_bbGCFree
	add	esp,4
_32:
	mov	dword [_bbAppTitle],ebx
	push	_33
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	10
	call	_brl_glmax2d_GLMax2DDriver
	push	eax
	call	_brl_graphics_SetGraphicsDriver
	add	esp,8
	push	_34
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	60
	push	0
	push	2
	push	600
	push	800
	call	_sidesign_minib3d_Graphics3D
	add	esp,20
	push	_35
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_bbNullObject
	call	_sidesign_minib3d_CreateCamera
	add	esp,4
	push	eax
	call	_bbHandleFromObject
	add	esp,4
	mov	dword [ebp-4],eax
	push	_37
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	0
	push	-1041235968
	push	1092616192
	push	0
	push	_sidesign_minib3d_TEntity
	push	dword [ebp-4]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_sidesign_minib3d_TranslateEntity
	add	esp,20
	push	_38
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	0
	push	0
	push	0
	push	_sidesign_minib3d_TCamera
	push	dword [ebp-4]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_sidesign_minib3d_CameraClsColor
	add	esp,16
	push	_39
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	1132396544
	push	1132396544
	push	0
	push	_sidesign_minib3d_TCamera
	push	dword [ebp-4]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_sidesign_minib3d_CameraClsColor
	add	esp,16
	push	_40
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_bbNullObject
	push	1
	call	_sidesign_minib3d_CreateLight
	add	esp,8
	push	eax
	call	_bbHandleFromObject
	add	esp,4
	mov	dword [ebp-8],eax
	push	_42
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_bbNullObject
	push	_19
	call	_sidesign_minib3d_LoadAnimMesh
	add	esp,8
	push	eax
	call	_bbHandleFromObject
	add	esp,4
	mov	dword [ebp-12],eax
	push	_44
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_20
	push	_sidesign_minib3d_TEntity
	push	dword [ebp-12]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_sidesign_minib3d_FindChild
	add	esp,8
	push	eax
	call	_bbHandleFromObject
	add	esp,4
	mov	dword [ebp-16],eax
	push	_46
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	0
	push	_sidesign_minib3d_TEntity
	push	dword [ebp-16]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_sidesign_minib3d_EntityAlpha
	add	esp,8
	push	_47
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	1
	push	_sidesign_minib3d_TEntity
	push	dword [ebp-12]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_sidesign_minib3d_EntityFX
	add	esp,8
	push	_48
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	0
	push	_sidesign_minib3d_TEntity
	push	dword [ebp-16]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	push	_sidesign_minib3d_TEntity
	push	dword [ebp-4]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_sidesign_minib3d_PointEntity
	add	esp,12
	push	_49
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_bbNullObject
	push	_21
	call	_sidesign_minib3d_LoadMesh
	add	esp,8
	push	eax
	call	_bbHandleFromObject
	add	esp,4
	mov	dword [ebp-20],eax
	push	_51
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	0
	push	1045220557
	push	1045220557
	push	1045220557
	push	_sidesign_minib3d_TEntity
	push	dword [ebp-20]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_sidesign_minib3d_ScaleEntity
	add	esp,20
	push	_52
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	0
	push	0
	push	_sidesign_minib3d_TEntity
	push	dword [ebp-16]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_sidesign_minib3d_EntityZ
	add	esp,8
	sub	esp,4
	fstp	dword [esp]
	push	0
	push	_sidesign_minib3d_TEntity
	push	dword [ebp-16]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_sidesign_minib3d_EntityY
	add	esp,8
	fadd	dword [_69]
	sub	esp,4
	fstp	dword [esp]
	push	0
	push	_sidesign_minib3d_TEntity
	push	dword [ebp-16]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_sidesign_minib3d_EntityX
	add	esp,8
	sub	esp,4
	fstp	dword [esp]
	push	_sidesign_minib3d_TEntity
	push	dword [ebp-20]
	call	_bbHandleToObject
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	push	eax
	call	_sidesign_minib3d_PositionEntity
	add	esp,20
	push	_53
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	jmp	_22
_24:
	push	_54
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	call	_brl_max2d_Cls
	push	_55
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	call	_sidesign_minib3d_RenderWorld
	push	_56
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	1065353216
	call	_sidesign_minib3d_UpdateWorld
	add	esp,4
	push	_57
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	-1
	call	_brl_graphics_Flip
	add	esp,4
_22:
	push	27
	call	_brl_polledinput_KeyHit
	add	esp,4
	cmp	eax,0
	je	_24
_23:
	push	_58
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	call	_bbEnd
	mov	ebx,0
_25:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
	section	"data" data writeable align 8
	align	4
_67:
	dd	0
_60:
	db	"viewer",0
_61:
	db	"camera",0
_62:
	db	"i",0
_63:
	db	"light",0
_64:
	db	"terrain",0
_65:
	db	"center",0
_66:
	db	"temp",0
	align	4
_59:
	dd	1
	dd	_60
	dd	2
	dd	_61
	dd	_62
	dd	-4
	dd	2
	dd	_63
	dd	_62
	dd	-8
	dd	2
	dd	_64
	dd	_62
	dd	-12
	dd	2
	dd	_65
	dd	_62
	dd	-16
	dd	2
	dd	_66
	dd	_62
	dd	-20
	dd	0
_28:
	db	"C:/Documents and Settings/ckob/Desktop/coding/BuildingMaker/viewer.bmx",0
	align	4
_27:
	dd	_28
	dd	2
	dd	1
	align	4
_18:
	dd	_bbStringClass
	dd	2147483647
	dd	43
	dw	84,104,101,32,65,114,99,104,105,116,101,99,116,32,86,105
	dw	101,119,101,114,32,45,32,80,114,101,115,115,32,69,115,99
	dw	97,112,101,32,116,111,32,69,120,105,116
	align	4
_33:
	dd	_28
	dd	3
	dd	1
	align	4
_34:
	dd	_28
	dd	5
	dd	1
	align	4
_35:
	dd	_28
	dd	8
	dd	1
	align	4
_37:
	dd	_28
	dd	9
	dd	1
	align	4
_38:
	dd	_28
	dd	10
	dd	1
	align	4
_39:
	dd	_28
	dd	12
	dd	1
	align	4
_40:
	dd	_28
	dd	14
	dd	1
	align	4
_42:
	dd	_28
	dd	16
	dd	1
	align	4
_19:
	dd	_bbStringClass
	dd	2147483647
	dd	17
	dw	109,101,100,105,97,47,116,101,114,114,97,105,110,46,98,51
	dw	100
	align	4
_44:
	dd	_28
	dd	17
	dd	1
	align	4
_20:
	dd	_bbStringClass
	dd	2147483647
	dd	6
	dw	99,101,110,116,101,114
	align	4
_46:
	dd	_28
	dd	18
	dd	1
	align	4
_47:
	dd	_28
	dd	19
	dd	1
	align	4
_48:
	dd	_28
	dd	20
	dd	1
	align	4
_49:
	dd	_28
	dd	23
	dd	1
	align	4
_21:
	dd	_bbStringClass
	dd	2147483647
	dd	8
	dw	116,101,109,112,46,98,51,100
	align	4
_51:
	dd	_28
	dd	24
	dd	1
	align	4
_52:
	dd	_28
	dd	25
	dd	1
	align	4
_69:
	dd	0x40000000
	align	4
_53:
	dd	_28
	dd	27
	dd	1
	align	4
_54:
	dd	_28
	dd	28
	dd	1
	align	4
_55:
	dd	_28
	dd	31
	dd	1
	align	4
_56:
	dd	_28
	dd	32
	dd	1
	align	4
_57:
	dd	_28
	dd	37
	dd	1
	align	4
_58:
	dd	_28
	dd	39
	dd	1
