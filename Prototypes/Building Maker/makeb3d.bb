;
;simple demo which creates a mesh, writes it to a b3d file,
;then loads it back in and displays it.
;
Include "b3dfile.bb"

Graphics3D 640,480

mesh=CreateSphere( 32 )

WriteBB3D( "temp.b3d",mesh )

FreeEntity mesh

mesh=LoadMesh( "temp.b3d" )

camera=CreateCamera()
PositionEntity camera,0,0,-3

light=CreateLight()
TurnEntity light,45,45,0

While Not KeyHit(1)
	UpdateWorld
	RenderWorld
	Flip
Wend

End

Function WriteBB3D( f_name$,mesh )

	file=WriteFile( f_name$ )

	b3dSetFile( file )
	
	b3dBeginChunk( "BB3D" )
		b3dWriteInt( 1 )	;version
		
		b3dBeginChunk( "BRUS" )
			b3dWriteInt( 0 )					;0 textures per brush
			b3dWriteString( "Shiny Red" )		;name
			b3dWriteFloat( 1 )					;red
			b3dWriteFloat( 0 )					;green
			b3dWriteFloat( 0 )					;blue
			b3dWriteFloat( 1 )					;alpha
			b3dWriteFloat( .75 )				;shininess
			b3dWriteInt( 1 )					;blend
			b3dWriteInt( 0 )					;FX
		b3dEndChunk()	;end of BRUS chunk
		
		b3dBeginChunk( "NODE" )
			b3dWriteString( "entity_name_here!" )
			b3dWriteFloat( 0 )	;x_pos
			b3dWriteFloat( 0 )	;y_pos
			b3dWriteFloat( 0 )	;y_pos
			b3dWriteFloat( 1 )	;x_scale
			b3dWriteFloat( 1 )	;y_scale
			b3dWriteFloat( 1 )	;z_scale
			b3dWriteFloat( 1 )	;rot_w
			b3dWriteFloat( 0 )	;rot_x
			b3dWriteFloat( 0 )	;rot_y
			b3dWriteFloat( 0 )	;rot_z
			WriteMESH( mesh )
		b3dEndChunk()	;end of NODE chunk
		
	b3dEndChunk()	;end of BB3D chunk
	
	CloseFile file
End Function

Function WriteMESH( mesh )

	n_surfs=CountSurfaces( mesh )
	
	b3dBeginChunk( "MESH" )
		b3dWriteInt( -1 )				;no 'entity' brush
		
		b3dBeginChunk( "VRTS" )
			b3dWriteInt( 0 )			;flags - 0=no normal/color
			b3dWriteInt( 0 )			;0 tex_coord sets
			b3dWriteInt( 0 )			;0 coords per set
			
			For k=1 To n_surfs
				surf=GetSurface( mesh,k )
				n_verts=CountVertices( surf )-1
				
				For j=0 To n_verts
					b3dWriteFloat( VertexX( surf,j ) )
					b3dWriteFloat( VertexY( surf,j ) )
					b3dWriteFloat( VertexZ( surf,j ) )
				Next
			Next
		b3dEndChunk()	;end of VRTS chunk
		
		first_vert=0
		For k=1 To n_surfs
			surf=GetSurface( mesh,k )
			n_tris=CountTriangles( surf )-1
			
			b3dBeginChunk( "TRIS" )
				b3dWriteInt( 0 )		;brush for these triangles
				
				For j=0 To n_tris
					b3dWriteInt( first_vert+TriangleVertex( surf,j,0 ) )
					b3dWriteInt( first_vert+TriangleVertex( surf,j,1 ) )
					b3dWriteInt( first_vert+TriangleVertex( surf,j,2 ) )
				Next
				
			b3dEndChunk()	;end of TRIS chunk
			
			first_vert=first_vert+CountVertices( surf )
			
		Next
		
	b3dEndChunk()	;end of MESH chunk
	
End Function