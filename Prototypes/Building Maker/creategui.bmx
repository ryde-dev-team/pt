Global Window:TGadget=CreateWindow("The Architect",0,00,1085,857,0,3)
	Global tabTabber0:TGadget=CreateTabber(4,7,187,497,Window)
	AddGadgetItem tabTabber0,"Base", True
	AddGadgetItem tabTabber0,"Roof", False
	AddGadgetItem tabTabber0,"Window", False
	AddGadgetItem tabTabber0,"Door", False
	SetGadgetLayout tabTabber0,1,2,1,2
		Global TabPanel0:TGadget=CreatePanel(0,0,ClientWidth(tabTabber0),ClientHeight(tabTabber0),tabTabber0,0)
		SetGadgetLayout TabPanel0,1,1,1,1
			Global SimpleBase:TGadget=CreateButton("Simple",2,28,38,32,TabPanel0,1)
			SetGadgetLayout SimpleBase,1,0,1,0
			Global RoundBase:TGadget=CreateButton("Round",43,28,37,32,TabPanel0,1)
			SetGadgetLayout RoundBase,1,0,1,0
			Global LshapeBase:TGadget=CreateButton("L Shape",82,28,45,32,TabPanel0,1)
			SetGadgetLayout LshapeBase,1,0,1,0
			Global horseshoebase:TGadget=CreateButton("U shape",130,28,45,32,TabPanel0,1)
			SetGadgetLayout horseshoebase,1,0,1,0
			'Global secfloorbutt:TGadget=CreateButton("2 Floors",24,85,64,16,TabPanel0,3)
			'SetGadgetLayout secfloorbutt,1,0,1,0
			Global secFloorSimple:TGadget=CreateButton("Simple",2,117,38,32,TabPanel0,1)
			SetGadgetLayout secFloorSimple,1,0,1,0
			Global SecFloorRound:TGadget=CreateButton("Round",43,117,37,32,TabPanel0,1)
			SetGadgetLayout SecFloorRound,1,0,1,0
			Global secfloorL:TGadget=CreateButton("L Shape",82,117,45,32,TabPanel0,1)
			SetGadgetLayout secfloorL,1,0,1,0
			Global secfloorhorse:TGadget=CreateButton("U Shape",130,117,45,32,TabPanel0,1)
			SetGadgetLayout secfloorhorse,1,0,1,0
			Global chbCheckBox0:TGadget=CreateButton("ScaleTop Floor",20,176,91,16,TabPanel0,2)
			SetGadgetLayout chbCheckBox0,1,0,1,0
			Global scalexscroll:TGadget=CreateSlider(21,238,148,16,TabPanel0,1)
			SetGadgetLayout scalexscroll,1,1,0,1
			Global scaleXtxt:TGadget=CreateTextField(54,215,80,20,TabPanel0)
			SetGadgetLayout scaleXtxt,1,0,1,0
			Global lblLabel0=CreateLabel("X:",24,217,17,16,TabPanel0,0)
			SetGadgetLayout lblLabel0,1,0,1,0
			Global scaleyscroll:TGadget=CreateSlider(23,296,148,16,TabPanel0,1)
			SetGadgetLayout scaleyscroll,1,1,0,1
			Global ScaleYtxt:TGadget=CreateTextField(54,271,80,20,TabPanel0)
			SetGadgetLayout ScaleYtxt,1,0,1,0
			Global lblLabel1=CreateLabel("Y:",26,273,17,16,TabPanel0,0)
			SetGadgetLayout lblLabel1,1,0,1,0
			Global lblLabel3=CreateLabel("Z:",27,329,17,16,TabPanel0,0)
			SetGadgetLayout lblLabel3,1,0,1,0
			Global scaleztxt:TGadget=CreateTextField(53,326,80,20,TabPanel0)
			SetGadgetLayout scaleztxt,1,0,1,0
			Global scalezscroll:TGadget=CreateSlider(24,353,148,16,TabPanel0,1)
			SetGadgetLayout scalezscroll,1,1,0,1
			Global setbasetexbutt:TGadget=CreateButton("Set Base Texture",44,386,94,24,TabPanel0,1)
			SetGadgetLayout setbasetexbutt,1,0,1,0
			Global secfloortexbutt:TGadget=CreateButton("2nd Floor Texture",43,415,94,24,TabPanel0,1)
			SetGadgetLayout secfloortexbutt,1,0,1,0
		Global TabPanel1:TGadget=CreatePanel(0,0,ClientWidth(tabTabber0),ClientHeight(tabTabber0),tabTabber0,0)
		HideGadget TabPanel1
		SetGadgetLayout TabPanel1,1,1,1,1
		HideGadget TabPanel1
			Global roofplainbutt:TGadget=CreateButton("Simple",2,27,38,32,TabPanel1,1)
			SetGadgetLayout roofplainbutt,1,0,1,0
			Global roofRoundButt:TGadget=CreateButton("Round",43,27,37,32,TabPanel1,1)
			SetGadgetLayout roofRoundButt,1,0,1,0
			Global roofLbutt:TGadget=CreateButton("L Shape",82,27,45,32,TabPanel1,1)
			SetGadgetLayout roofLbutt,1,0,1,0
			Global roofhorsebutt:TGadget=CreateButton("U Shape",129,26,45,32,TabPanel1,1)
			SetGadgetLayout roofhorsebutt,1,0,1,0
			Global scalexroofslider:TGadget=CreateSlider(34,168,137,16,TabPanel1,1)
			SetGadgetLayout scalexroofslider,1,1,0,1
			Global scaleyroofslider:TGadget=CreateSlider(34,219,137,16,TabPanel1,1)
			SetGadgetLayout scaleyroofslider,1,1,0,1
			Global scalezslider:TGadget=CreateSlider(34,268,137,16,TabPanel1,1)
			SetGadgetLayout scalezslider,1,1,0,1
			Global scalexrooftxt:TGadget=CreateTextField(59,143,80,20,TabPanel1)
			SetGadgetLayout scalexrooftxt,1,0,1,0
			Global scaleyrooftxt:TGadget=CreateTextField(60,194,80,20,TabPanel1)
			SetGadgetLayout scaleyrooftxt,1,0,1,0
			Global scaleroofztxt:TGadget=CreateTextField(60,245,80,20,TabPanel1)
			SetGadgetLayout scaleroofztxt,1,0,1,0
			Global RoofArchButt:TGadget=CreateButton("Arch",2,66,38,32,TabPanel1,1)
			SetGadgetLayout RoofArchButt,1,0,1,0
			Global btnButton22:TGadget=CreateButton("ArchRounded",41,67,68,30,TabPanel1,1)
			SetGadgetLayout btnButton22,1,0,1,0
		Global TabPanel2:TGadget=CreatePanel(0,0,ClientWidth(tabTabber0),ClientHeight(tabTabber0),tabTabber0,0)
		HideGadget TabPanel2
		SetGadgetLayout TabPanel2,1,1,1,1
		HideGadget TabPanel2
			Global cobComboBox0:TGadget=CreateComboBox(38,33,96,20,TabPanel2)
			AddGadgetItem cobComboBox0,"1"
			AddGadgetItem cobComboBox0,"2"
			AddGadgetItem cobComboBox0,"3"
			AddGadgetItem cobComboBox0,"4"
			AddGadgetItem cobComboBox0,"5"
			AddGadgetItem cobComboBox0,"6"
			SelectGadgetItem cobComboBox0,0
			SetGadgetLayout cobComboBox0,1,0,1,0
			Global lblLabel5=CreateLabel("Number of Windows",35,14,96,16,TabPanel2,0)
			SetGadgetLayout lblLabel5,1,0,1,0
			Global cobComboBox1:TGadget=CreateComboBox(37,111,96,20,TabPanel2)
			AddGadgetItem cobComboBox1,"1"
			AddGadgetItem cobComboBox1,"2"
			AddGadgetItem cobComboBox1,"3"
			AddGadgetItem cobComboBox1,"4"
			AddGadgetItem cobComboBox1,"5"
			AddGadgetItem cobComboBox1,"6"
			SelectGadgetItem cobComboBox1,0
			SetGadgetLayout cobComboBox1,1,0,1,0
			Global lblLabel6=CreateLabel("Select Window",46,92,96,16,TabPanel2,0)
			SetGadgetLayout lblLabel6,1,0,1,0
			Global setwintex:TGadget=CreateButton("Set Window Texture",32,147,103,24,TabPanel2,1)
			SetGadgetLayout setwintex,1,0,1,0
		Global TabPanel3:TGadget=CreatePanel(0,0,ClientWidth(tabTabber0),ClientHeight(tabTabber0),tabTabber0,0)
		HideGadget TabPanel3
		SetGadgetLayout TabPanel3,1,1,1,1
		HideGadget TabPanel3
			Global createdoorbutt:TGadget=CreateButton("Create Door",36,16,92,24,TabPanel3,1)
			SetGadgetLayout createdoorbutt,1,0,1,0
			Global doortexbutt:TGadget=CreateButton("Select Door Texture",33,50,100,24,TabPanel3,1)
			SetGadgetLayout doortexbutt,1,0,1,0
	Global cavCanvas0:TGadget=CreateCanvas(197,8,876,816,Window)
	SetGadgetLayout cavCanvas0,1,2,1,2
	Global SaveButt:TGadget=CreateButton("Save",67,533,64,18,Window,1)
	SetGadgetLayout SaveButt,1,0,1,0
	Global ExportB3dbutt:TGadget=CreateButton("Export B3d",68,556,64,18,Window,1)
	SetGadgetLayout ExportB3dbutt,1,0,1,0
	Global Export3dsButt:TGadget=CreateButton("Export 3ds",67,580,64,18,Window,1)
	SetGadgetLayout Export3dsButt,1,0,1,0
	Global ExportXbutt:TGadget=CreateButton("Export X",67,604,64,18,Window,1)
	SetGadgetLayout ExportXbutt,1,0,1,0
