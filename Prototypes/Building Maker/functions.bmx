Type TMouse
	Field x:Int  = 0
	Field y:Int  = 0
	Field z:Int  = 0
	Field Button:Byte[3] 
	
	Function Init:TMouse()
		Return New TMouse
	End Function
	
	Method Update()
		Local CUR_Event:TEvent = CurrentEvent
		Select Cur_Event.id
			Case EVENT_MOUSEMOVE  
				x = Cur_Event.x
				y = Cur_Event.y   
			Case EVENT_MOUSEWHEEL
				cz# = cz# + Cur_Event.data
				DebugLog cz#
			Case EVENT_MOUSEDOWN
				Button[cur_event.data-1] = True
			Case EVENT_MOUSEUP
				Button[cur_event.data-1] = False
		End Select
	End Method  
	
	Method getX:Int()
		Return x
	End Method  
	
	Method getY:Int()
		Return y
	End Method
	Method getZ:Int()
		Return z
	End Method   
	
	Method GetButton:Byte(_Button:Int=0)
		If _Button >= 0 And _Button <= 2 Then 
			Return Button[_Button]
		EndIf
	End Method
	
End Type

Function TextureLoadedMesh(Mesh:TMesh,Tex:TTexture,Index:Int = 0)
	
	For Local Surf:TSurface = EachIn Mesh.Surf_list
		BrushTexture(Surf.Brush,Tex,Index)
	Next
End Function

Function invsLoadedMesh(Mesh:TMesh)
	For Local Surf:TSurface = EachIn Mesh.Surf_list
		BrushAlpha(Surf.Brush,0)
	Next
End Function


Function CreateQuad(P = 0)

	EN = CreateMesh()
	s = CreateSurface(EN)
	v1 = AddVertex(s, 0.0, -1.0, 0.0, 0.0, 1.0)
	v2 = AddVertex(s, 1.0, -1.0, 0.0, 1.0, 1.0)
	v3 = AddVertex(s, 1.0,  0.0, 0.0, 1.0, 0.0)
	v4 = AddVertex(s, 0.0,  0.0, 0.0, 0.0, 0.0)
	AddTriangle s, v3, v2, v1
	AddTriangle s, v4, v3, v1
	EntityParent(EN, P)
	EntityFX(EN, 1 + 8)
	Return EN

End Function



Function Loadbase(bases:Int)
SaveFile_BaseModel = bases
Select bases
	
	Case 0
		
		firstbasefile$ = "Meshes/Base/base1.b3d"
		If BaseModelBase > 0 Then FreeEntity BaseModelBase 
				BaseModel = LoadAnimMesh("Meshes/Baseext/base1.b3d")
				BaseModelBase = LoadMesh("Meshes/Base/base1.b3d")
				EntityFX(BaseModelBase,1)
				Roofpoint = FindChild(BaseModel,"roof")
				DoorPoint = FindChild(BaseModel,"door")
				'Roof Coords
				RoofX = EntityX(Roofpoint)
				RoofY = EntityY(Roofpoint)
				RoofZ = EntityZ(Roofpoint)
				'Door Coords
				DoorX = EntityX(DoorPoint)
				DoorY = EntityY(DoorPoint)
				DoorZ = EntityZ(DoorPoint)

				For wincount = 1 To 6
					
						WinChild[wincount]= FindChild(BaseModel,"window"+wincount)
					If WinChild[wincount]Then	
						WindowsX[wincount] = EntityX(WinChild[wincount])
						WindowsY[wincount] = EntityY(WinChild[wincount])
						WindowsZ[wincount] = EntityZ(WinChild[wincount])
					
						FreeEntity WinChild[wincount]
					Else
						Exit
					End If	
				Next	
				
				X = 0
				Y = 0
				Z = 0
				ScaleX = 1
				ScaleY = 1
				ScaleZ = 1
				SetGadgetText scaleXtxt,ScaleX
				SetGadgetText scaleYtxt,ScaleX
				SetGadgetText scaleZtxt,ScaleX
				
     			PositionMesh BaseModel,X,Y,Z
				PositionMesh BaseModelBase ,X,Y,Z
				'ScaleEntity BaseModel,ScaleX,ScaleY,ScaleZ
				ScaleEntity BaseModelBase,ScaleX,ScaleY,ScaleZ
				FreeEntity Basemodel
				'FreeEntity Roofpoint
				FreeEntity DoorPoint 
				FreeEntity WinChild[1]
				
	Case 1
	firstbasefile$ = "Meshes/Base/baseL.b3d"
		If BaseModelBase > 0 Then FreeEntity BaseModelBase 
				BaseModel = LoadAnimMesh("Meshes/Baseext/baseL.b3d")
				BaseModelBase = LoadMesh("Meshes/Base/baseL.b3d")
				EntityFX(BaseModelBase,1)
				Roofpoint = FindChild(BaseModel,"roof")
				DoorPoint = FindChild(BaseModel,"door")
				'Roof Coords
				RoofX = EntityX(Roofpoint)
				RoofY = EntityY(Roofpoint)
				RoofZ = EntityZ(Roofpoint)
				'Door Coords
				DoorX = EntityX(DoorPoint)
				DoorY = EntityY(DoorPoint)
				DoorZ = EntityZ(DoorPoint)

				For wincount = 1 To 6
					
						WinChild[wincount]= FindChild(BaseModel,"window"+wincount)
					If WinChild[wincount]Then	
						WindowsX[wincount] = EntityX(WinChild[wincount])
						WindowsY[wincount] = EntityY(WinChild[wincount])
						WindowsZ[wincount] = EntityZ(WinChild[wincount])
						FreeEntity WinChild[wincount]
					Else
						Exit
					End If	
				Next	
				
				X = 0
				Y = 0
				Z = 0
				ScaleX = 1
				ScaleY = 1
				ScaleZ = 1
     			PositionMesh BaseModel,X,Y,Z
				PositionMesh BaseModelBase ,X,Y,Z
				'ScaleEntity BaseModel,ScaleX,ScaleY,ScaleZ
				'ScaleEntity BaseModelBase,ScaleX,ScaleY,ScaleZ
				FreeEntity Basemodel
				FreeEntity Roofpoint
				FreeEntity DoorPoint 
				FreeEntity WinChild[1]
			
	Case 2
	firstbasefile$ = "Meshes/Base/baseRound.b3d"
		If BaseModelBase > 0 Then FreeEntity BaseModelBase 
				BaseModel = LoadAnimMesh("Meshes/Baseext/baseRound.b3d")
				BaseModelBase = LoadMesh("Meshes/Base/baseRound.b3d")
				EntityFX(BaseModelBase,1)
				Roofpoint = FindChild(BaseModel,"roof")
				DoorPoint = FindChild(BaseModel,"door")
				'Roof Coords
				RoofX = EntityX(Roofpoint)
				RoofY = EntityY(Roofpoint)
				RoofZ = EntityZ(Roofpoint)
				'Door Coords
				DoorX = EntityX(DoorPoint)
				DoorY = EntityY(DoorPoint)
				DoorZ = EntityZ(DoorPoint)

				For wincount = 1 To 6
					
						WinChild[wincount]= FindChild(BaseModel,"window"+wincount)
					If WinChild[wincount]Then	
						WindowsX[wincount] = EntityX(WinChild[wincount])
						WindowsY[wincount] = EntityY(WinChild[wincount])
						WindowsZ[wincount] = EntityZ(WinChild[wincount])
						FreeEntity WinChild[wincount]
					Else
						Exit
					End If	
				Next	
				
				X = 0
				Y = 0
				Z = 0
				ScaleX = 1
				ScaleY = 1
				ScaleZ = 1
     			PositionMesh BaseModel,X,Y,Z
				PositionMesh BaseModelBase ,X,Y,Z
				'ScaleEntity BaseModel,ScaleX,ScaleY,ScaleZ
				'ScaleEntity BaseModelBase,ScaleX,ScaleY,ScaleZ
				FreeEntity Basemodel
				FreeEntity Roofpoint
				FreeEntity DoorPoint 
				FreeEntity WinChild[1]						
	
Case 3
	firstbasefile$ = "Meshes/Base/baseU.b3d"
	If BaseModelBase > 0 Then FreeEntity BaseModelBase 
				BaseModel = LoadAnimMesh("Meshes/Baseext/baseU.b3d")
				BaseModelBase = LoadMesh("Meshes/Base/baseU.b3d")
				EntityFX(BaseModelBase,1)
				Roofpoint = FindChild(BaseModel,"roof")
				DoorPoint = FindChild(BaseModel,"door")
				'Roof Coords
				RoofX = EntityX(Roofpoint)
				RoofY = EntityY(Roofpoint)
				RoofZ = EntityZ(Roofpoint)
				'Door Coords
				DoorX = EntityX(DoorPoint)
				DoorY = EntityY(DoorPoint)
				DoorZ = EntityZ(DoorPoint)

				For wincount = 1 To 6
					
						WinChild[wincount]= FindChild(BaseModel,"window"+wincount)
					If WinChild[wincount]Then	
						WindowsX[wincount] = EntityX(WinChild[wincount])
						WindowsY[wincount] = EntityY(WinChild[wincount])
						WindowsZ[wincount] = EntityZ(WinChild[wincount])
						FreeEntity WinChild[wincount]
					Else
						Exit
					End If	
				Next	
				
				X = 0
				Y = 0
				Z = 0
				ScaleX = 1
				ScaleY = 1
				ScaleZ = 1
     			PositionMesh BaseModel,X,Y,Z
				PositionMesh BaseModelBase ,X,Y,Z
				'ScaleEntity BaseModel,ScaleX,ScaleY,ScaleZ
				'ScaleEntity BaseModelBase,ScaleX,ScaleY,ScaleZ
				FreeEntity Basemodel
				FreeEntity Roofpoint
				FreeEntity DoorPoint 
				FreeEntity WinChild[1]	

End Select

End Function


Function LoadSecondFloor(floors:Int)
If BaseModelBase > 0 Then
		
		secondlevel = True
		SaveFile_SecBase = floors
		
		AddGadgetItem cobComboBox0,"7"
		AddGadgetItem cobComboBox0,"8"
		AddGadgetItem cobComboBox0,"9"
		AddGadgetItem cobComboBox0,"10"
		AddGadgetItem cobComboBox0,"11"
		AddGadgetItem cobComboBox0,"12"
		
		Select floors
		
			Case 0
				secbasefile$ = "Meshes/Base/base1.b3d"
				If secfloorModelBase > 0 Then FreeEntity secfloorModelBase 
						secfloorModel = LoadAnimMesh("Meshes/Baseext/base1.b3d")
						secfloorModelBase = LoadMesh("Meshes/Base/base1.b3d")
						EntityFX(secfloorModelBase,1)
						secRoofpoint = FindChild(secfloorModel,"roof")
						'Roof Coords
						secRoofX = EntityX(secRoofpoint)
						secRoofY = EntityY(secRoofpoint)
						secRoofZ = EntityZ(secRoofpoint)
						X = EntityX(BaseModelBase)
						Y = EntityY(Roofpoint)
						Z = EntityZ(BaseModelBase)
						ScaleX = 1
						ScaleY = 1
						ScaleZ = 1
						'RoofY = RoofY + 20
		     			PositionEntity secfloorModel,X,Y,Z
						PositionMesh secfloorModelBase,X,Y,Z
						
					   For wincount = 7 To 12
						counter = counter + 1 
						WinChild[wincount]= FindChild(secfloorModel,"window"+ counter)
							If WinChild[wincount]Then
								WindowsX[wincount] = EntityX(WinChild[wincount],True)
								WindowsY[wincount] = EntityY(WinChild[wincount],True)
								WindowsZ[wincount] = EntityZ(WinChild[wincount],True)
								FreeEntity WinChild[wincount]
							Else
								Exit
							End If	
						Next
						
						
						'ScaleEntity BaseModel,ScaleX,ScaleY,ScaleZ
						'ScaleEntity BaseModelBase,ScaleX,ScaleY,ScaleZ
						FreeEntity secfloorModel
						FreeEntity secRoofpoint
						For i = 7 To 12
						FreeEntity WinChild[i]
						Next
		    Case 1
				secbasefile$ = "Meshes/Base/baseL.b3d"
				If secfloorModelBase > 0 Then FreeEntity secfloorModelBase 
						secfloorModel = LoadAnimMesh("Meshes/Baseext/baseL.b3d")
						secfloorModelBase = LoadMesh("Meshes/Base/baseL.b3d")
						EntityFX(secfloorModelBase,1)
						secRoofpoint = FindChild(secfloorModel ,"roof")
						'Roof Coords
						secRoofX = EntityX(secRoofpoint)
						secRoofY = EntityY(secRoofpoint)
						secRoofZ = EntityZ(secRoofpoint)
						X = EntityX(BaseModelBase)
						Y = EntityY(Roofpoint)
						Z = EntityZ(BaseModelBase)
						ScaleX = 1
						ScaleY = 1
						ScaleZ = 1
						'RoofY = RoofY + 20
		     			PositionEntity secfloorModel,X,Y,Z
						PositionMesh secfloorModelBase,X,Y,Z
						
					   For wincount = 7 To 12
						counter = counter + 1 
						WinChild[wincount]= FindChild(secfloorModel,"window"+counter)
							If WinChild[wincount]Then
								WindowsX[wincount] = EntityX(WinChild[wincount],True)
								WindowsY[wincount] = EntityY(WinChild[wincount],True)
								WindowsZ[wincount] = EntityZ(WinChild[wincount],True)
								FreeEntity WinChild[wincount]
							Else
								Exit
							End If	
						Next
						
						
						'ScaleEntity BaseModel,ScaleX,ScaleY,ScaleZ
						'ScaleEntity BaseModelBase,ScaleX,ScaleY,ScaleZ
						FreeEntity secfloorModel
						FreeEntity secRoofpoint
						FreeEntity WinChild[1]
		Case 2
			secbasefile$ = "Meshes/Base/baseRound.b3d"
				If secfloorModelBase > 0 Then FreeEntity secfloorModelBase 		
						secfloorModel = LoadAnimMesh("Meshes/Baseext/baseRound.b3d")
						secfloorModelBase = LoadMesh("Meshes/Base/baseRound.b3d")
						EntityFX(secfloorModelBase,1)
						secRoofpoint = FindChild(secfloorModel ,"roof")
						'Roof Coords
						secRoofX = EntityX(secRoofpoint)
						secRoofY = EntityY(secRoofpoint)
						secRoofZ = EntityZ(secRoofpoint)
						X = EntityX(BaseModelBase)
						Y = EntityY(Roofpoint)
						Z = EntityZ(BaseModelBase)
						ScaleX = 1
						ScaleY = 1
						ScaleZ = 1
						'RoofY = RoofY + 20
		     			PositionEntity secfloorModel,X,Y,Z
						PositionMesh secfloorModelBase,X,Y,Z
						
					   For wincount = 7 To 12
						counter = counter + 1 
						WinChild[wincount]= FindChild(secfloorModel,"window"+counter)
							If WinChild[wincount]Then
								WindowsX[wincount] = EntityX(WinChild[wincount],True)
								WindowsY[wincount] = EntityY(WinChild[wincount],True)
								WindowsZ[wincount] = EntityZ(WinChild[wincount],True)
								FreeEntity WinChild[wincount]
							Else
								Exit
							End If	
						Next
						
						
						'ScaleEntity BaseModel,ScaleX,ScaleY,ScaleZ
						'ScaleEntity BaseModelBase,ScaleX,ScaleY,ScaleZ
						FreeEntity secfloorModel
						FreeEntity secRoofpoint
						FreeEntity WinChild[1]
						
		Case 3
			secbasefile$ = "Meshes/Base/baseU.b3d"
					If secfloorModelBase > 0 Then FreeEntity secfloorModelBase 	
						secfloorModel = LoadAnimMesh("Meshes/Baseext/baseU.b3d")
						secfloorModelBase = LoadMesh("Meshes/Base/baseU.b3d")
						EntityFX(secfloorModelBase,1)
						secRoofpoint = FindChild(secfloorModel ,"roof")
						'Roof Coords
						secRoofX = EntityX(secRoofpoint)
						secRoofY = EntityY(secRoofpoint)
						secRoofZ = EntityZ(secRoofpoint)
						X = EntityX(BaseModelBase)
						Y = EntityY(Roofpoint)
						Z = EntityZ(BaseModelBase)
						ScaleX = 1
						ScaleY = 1
						ScaleZ = 1
						'RoofY = RoofY + 20
		     			PositionEntity secfloorModel,X,Y,Z
						PositionMesh secfloorModelBase,X,Y,Z
						
					   For wincount = 7 To 12
						counter = counter + 1 
						WinChild[wincount]= FindChild(secfloorModel,"window"+counter)
							If WinChild[wincount]Then
								WindowsX[wincount] = EntityX(WinChild[wincount],True)
								WindowsY[wincount] = EntityY(WinChild[wincount],True)
								WindowsZ[wincount] = EntityZ(WinChild[wincount],True)
								FreeEntity WinChild[wincount]
							Else
								Exit
							End If	
						Next
						
						
						'ScaleEntity BaseModel,ScaleX,ScaleY,ScaleZ
						'ScaleEntity BaseModelBase,ScaleX,ScaleY,ScaleZ
						FreeEntity secfloorModel
						FreeEntity secRoofpoint
						FreeEntity WinChild[1]		
		
			End Select

Else
	Notify "you need to place a base model first"
	
EndIf

End Function 


Function LoadRoof(roofs:Int)
SaveFile_RoofModel = roofs
roofScaleX=1
roofScaleY=1
roofScaleZ=1
	Select roofs
		Case 0
			rooffile$ = "Meshes/roofs/roof1.b3d"
			If RoofModel > 0 Then FreeEntity RoofModel 
				RoofModel = LoadMesh("Meshes/roofs/roof1.b3d")
				EntityFX(RoofModel,1)
				If secondlevel  = True Then
					X = secRoofX
					Y = secRoofY '+ RoofY + 2
					Z = secRoofZ
				Else
					X = RoofX
					Y = RoofY 
					Z = RoofZ	
				EndIf	


				PositionEntity RoofModel,X,Y,Z
				
		Case 1
			rooffile$ = "Meshes/roofs/roofround.b3d"
			If RoofModel > 0 Then FreeEntity RoofModel 
				RoofModel = LoadMesh("Meshes/roofs/roofround.b3d")
				EntityFX(RoofModel,1)
				If secondlevel  = True Then
					X = secRoofX
					Y = secRoofY '+ RoofY + 2
					Z = secRoofZ
				Else
					X = RoofX
					Y = RoofY
					Z = RoofZ	
				EndIf	
				ScaleX = 1
				ScaleY = 1
				ScaleZ = 1
				PositionEntity RoofModel,X,Y,Z				

		Case 2
			rooffile$ = "Meshes/roofs/roofL.b3d"
			If RoofModel > 0 Then FreeEntity RoofModel 
				RoofModel = LoadMesh("Meshes/roofs/roofL.b3d")
				EntityFX(RoofModel,1)
				If secondlevel  = True Then
					X = secRoofX
					Y = secRoofY '+ RoofY + 2
					Z = secRoofZ
				Else
					X = RoofX
					Y = RoofY
					Z = RoofZ	
				EndIf	
				ScaleX = 1
				ScaleY = 1
				ScaleZ = 1
				PositionEntity RoofModel,X,Y,Z				
				
		Case 3
			rooffile$ = "Meshes/roofs/roof3.b3d"
			If RoofModel > 0 Then FreeEntity RoofModel 
				RoofModel = LoadMesh("Meshes/roofs/roof3.b3d")
				EntityFX(RoofModel,1)
				If secondlevel  = True Then
					X = secRoofX
					Y = secRoofY '+ RoofY + 2
					Z = secRoofZ
				Else
					X = RoofX
					Y = RoofY
					Z = RoofZ	
				EndIf	
				ScaleX = 1
				ScaleY = 1
				ScaleZ = 1
				PositionEntity RoofModel,X,Y,Z				
				
		Case 4
			rooffile$ = "Meshes/roofs/roofU.b3d"
			If RoofModel > 0 Then FreeEntity RoofModel 
				RoofModel = LoadMesh("Meshes/roofs/roofU.b3d")
				EntityFX(RoofModel,1)
				If secondlevel  = True Then
					X = secRoofX
					Y = secRoofY '+ RoofY + 2
					Z = secRoofZ
				Else
					X = RoofX
					Y = RoofY
					Z = RoofZ	
				EndIf	

				ScaleX = 1
				ScaleY = 1
				ScaleZ = 1
				PositionEntity RoofModel,X,Y,Z					
				
	End Select
	
		

End Function

Function loaddoors(doors:Int)
SaveFile_DoorModel = doors
	Select doors
		Case 0
			If DoorModel > 0 Then FreeEntity DoorModel 
				DoorModel = LoadMesh("Meshes/Doors/Door.b3d")
				X = DoorX 
				Y = DoorY 
				Z = DoorZ
				ScaleX = 1
				ScaleY = 1
				ScaleZ = 1
				PositionMesh DoorModel,DoorX,DoorY,DoorZ
			
				
End Select

End Function

Function Notify2( text$, title$ = "", serious% = False )
	Local saved$ = AppTitle
	AppTitle = title
	Notify text, serious
	AppTitle = saved
End Function