Import sidesign.minib3d
Global WinChild[10],WindowsX[10],WindowsY[10],WindowsZ[10]

SaveFile$ = RequestFile("Save As","bmf",True,"meshes/Base")
File:TStream = WriteStream(SaveFile)

BasePrim$ = RequestFile("Pick Base Primitive","b3d")

WriteLine File,BasePrim

BaseExt$ = RequestFile("Pick Base Extended","b3d")
WriteLine File,BaseExt 

BaseModel = LoadAnimMesh(BaseExt)

Roofpoint = FindChild(BaseModel,"roof")
DoorPoint = FindChild(BaseModel,"door")


'Roof Coords
RoofX = EntityX(Roofpoint)
RoofY = EntityY(Roofpoint)
RoofZ = EntityZ(Roofpoint)
WriteLine File,RoofX
WriteLine File,RoofY
WriteLine File,RoofZ

'Door Coords
DoorX = EntityX(DoorPoint)
DoorY = EntityY(DoorPoint)
DoorZ = EntityZ(DoorPoint)
WriteLine File,DoorX
WriteLine File,DoorY
WriteLine File,DoorZ

For wincount = 1 To 6
		WinChild[wincount]=  FindChild(BaseModel,"window"+wincount)
		WindowsX[wincount] = EntityX(WinChild[wincount])
		WindowsY[wincount] = EntityY(WinChild[wincount])
		WindowsZ[wincount] = EntityZ(WinChild[wincount])
		WriteLine File,WindowsX[wincount]
		WriteLine File,WindowsY[wincount]
		WriteLine File,WindowsZ[wincount]		
Next

CloseStream(File)
End