'This BMX file was edited with BLIde ( http://www.blide.org )
Function createEnvironment()
	sky = createSkybox(_VECTOR3DF(1, 1, 1), Null, loadTexture("./media/sky/irrlicht2_up.jpg"),  ..
		loadTexture("./media/sky/irrlicht2_dn.jpg"), loadTexture("./media/sky/irrlicht2_lf.jpg"),  ..
		loadTexture("./media/sky/irrlicht2_rt.jpg"), loadTexture("./media/sky/irrlicht2_ft.jpg"),  ..
		loadTexture("./media/sky/irrlicht2_bk.jpg"))
		
	Local terrain:ITerrainSceneNode = Scene.Manager.addTerrainSceneNodeFromFile( ..
	"./media/terrain/heightmap.png",  ..
	Null, ..									'  parent node
	-1, ..										'  node id
	_VECTOR3DF(-5000, -100, -5000),  ..			'  position
	_VECTOR3DF(0.0, 0.0, 0.0), ..				'  rotation
	_VECTOR3DF(10, 0, 10),  ..					'  scale
	_SCOLOR( 255, 255, 255, 255 ), ..			'  vertexColor,
	5, ..										'  maxLOD
	ETPS_17, ..									'  patchSize
	4 ..										'  smoothFactor
	)

TERRAIN.setMaterialFlag(EMF_LIGHTING, False)

terrain.setMaterialTexture(0, loadTexture("./media/terrain/texture.jpg"))

terrain.setMaterialTexture(1, loadTexture("./media/terrain/detail.jpg"))

TERRAIN.setMaterialType(EMT_DETAIL_MAP)

terrain.ScaleTexture(1.0, 20.0)

selector = Scene.Manager.createTerrainTriangleSelector(TERRAIN, 0) 'required for creating pologygonal collisions
terrain.setTriangleSelector(selector) 'assigning it to the terrain
p3EventReceiver(p3Graphics.receiver).setBox(terrain) 'add the mesh to the reciever
End Function

Function createGui()
	Local skin:IGUISkin = GUI.env.getSkin()
	Local font:IGUIFont = GUI.env.getFont("media/font/courier.bmp")
	If (font) Then skin.setFont(font)
	controls = GUI.env.addStaticText("F1 - release mouse", _RECTI(15, 550, 165, 565))
	controls.setOverrideColor(_SCOLOR(255, 255, 255, 255))
	GUI.env.addStaticText("F2 - cycle animations", _RECTI(15, 575, 185, 590)).setOverrideColor(_SCOLOR(255, 255, 255, 255))
	Local menu:IGUIContextMenu = GUI.env.addMenu()
	menu.addItem("World", -1, True, True)
	menu.getSubMenu(0).addItem("Add NPC", 1001)
	menu.getSubMenu(0).addItem("Add Light", 1005)
	menu.getSubMenu(0).addItem("Add Village", 1006)
	menu.getSubMenu(0).addItem("Add Custom...", 1007)
	menu.getSubMenu(0).addItem("Invert Lights", 1002)
	menu.getSubMenu(0).addItem("Toggle Skybox", 1003)
	menu.getSubMenu(0).addItem("Wireframe", 1004)
	
	menu.getSubMenu(0).addItem("Quit",2001)
	fpstext = GUI.env.addStaticText("fps here", _RECTI(400, 4, 670, 23), False, False)
End Function

Function createWorld()
	Local npc1:ISceneNode = createNPC(_VECTOR3DF(500, -100, 50))
	createLight(_VECTOR3DF(), npc1)
	
	createCastle(_VECTOR3DF(-2057, -100, 2057))
	createCastle(_VECTOR3DF(3205, -100, 2049))
	createCastle(_VECTOR3DF(-3054, -100, -3095))
	createCastle(_VECTOR3DF(1405, -100, -1049))
	
	
End Function

Function createCastle:ISceneNode(pos:Vector3df)
	SeedRnd(MilliSecs())
	Local castleNode:ISceneNode = Scene.Manager.addEmptySceneNode()
	castleNode.setPosition(pos)
	addMesh(Scene.Manager.addSphereMesh("Castle")).setParent(castleNode)'hide once done
	
	Local wallMesh:IMesh = loadMesh("media/castle/Builds/Build_A.b3d")'walls/Wall_a.b3d")
	
	Local rightWall:ISceneNode = addMesh(wallMesh)
	Local leftWall:ISceneNode = addMesh(wallMesh)
	Local backWall:ISceneNode = addMesh(wallMesh)
	Local frontWall:IScenenode = addMesh(wallMesh)
	
	rightWall.setParent(castleNode)
	leftWall.setParent(castleNode)
	backWall.setParent(castleNode)
	frontWall.setParent(castleNode)
	
	rightWall.setMaterialFlag(EMF_LIGHTING, False)
	leftWall.setMaterialFlag(EMF_LIGHTING, False)
	frontWall.setMaterialFlag(EMF_LIGHTING, False)
	backWall.setMaterialFlag(EMF_LIGHTING, False)
	
	setRelativePosition(rightWall, _VECTOR3DF(Rnd(440, 600), 0, Rnd(440, 600)))
	setRelativePosition(leftWall, _VECTOR3DF(Rnd(-440, -600), 0, Rnd(440, 600)))
	setRelativePosition(backWall, _VECTOR3DF(Rnd(440, 600), 0, Rnd(-440, -600)))
	setRelativePosition(frontWall, _VECTOR3DF(Rnd(-440, -600), 0, Rnd(-440, -600)))
	
	leftWall.SetRotation(_VECTOR3DF(0, Rnd(0, 359), 0))
	rightWall.SetRotation(_VECTOR3DF(0, Rnd(0, 359), 0))
	frontWall.SetRotation(_VECTOR3DF(0, Rnd(0, 359), 0))
	
	newSelector = Scene.Manager.createTriangleSelector(wallMesh, rightWall) 'required for creating pologygonal collisions
	rightWall.setTriangleSelector(selector)
	Scene.cam.addAnimator(Scene.Manager.createCollisionResponseAnimator(newSelector, Scene.cam, _VECTOR3DF(30, 40, 30), _VECTOR3DF(0, 0, 0), _VECTOR3DF(0, 10, 0)))
	
	newSelector = Scene.Manager.createTriangleSelector(wallMesh, leftWall)
	leftWall.setTriangleSelector(selector)
	Scene.cam.addAnimator(Scene.Manager.createCollisionResponseAnimator(newSelector, Scene.cam, _VECTOR3DF(30, 40, 30), _VECTOR3DF(0, 0, 0), _VECTOR3DF(0, 10, 0)))
	
	
	newSelector = Scene.Manager.createTriangleSelector(wallMesh, frontWall)
	frontWall.setTriangleSelector(selector)
	Scene.cam.addAnimator(Scene.Manager.createCollisionResponseAnimator(newSelector, Scene.cam, _VECTOR3DF(30, 40, 30), _VECTOR3DF(0, 0, 0), _VECTOR3DF(0, 10, 0)))
	
	
	newSelector = Scene.Manager.createTriangleSelector(wallMesh, backWall)
	backWall.setTriangleSelector(selector) 'assigning it to the terrain
	Scene.cam.addAnimator(Scene.Manager.createCollisionResponseAnimator(newSelector, Scene.cam, _VECTOR3DF(30, 40, 30), _VECTOR3DF(0, 0, 0), _VECTOR3DF(0, 10, 0)))
	
	
	p3EventReceiver(p3Graphics.receiver).setBox(rightWall)
	p3EventReceiver(p3Graphics.receiver).setBox(leftWall)
	p3EventReceiver(p3Graphics.receiver).setBox(frontWall)
	p3EventReceiver(p3Graphics.receiver).setBox(backWall)
	
	
	
	
	
	Return castleNode
End Function

Function createLight:ILightSceneNode(pos:Vector3df = Null, parent:ISceneNode = Null)
	Local light:ILightSceneNode = Scene.Manager.addLightSceneNode(parent, pos, _SCOLORF(.6, .6, .6))
	Local lightnode:IAnimatedMeshSceneNode = addAnimMesh(Scene.Manager.addSphereMesh("", 5))
	lightnode.setParent(LIGHT)
	lightnode.setMaterialFlag(EMF_LIGHTING, False)
	Local anim:ISceneNodeAnimator = Scene.Manager.createFlyCircleAnimator(_VECTOR3DF(0, 70, 0), 70, .001)
	light.addAnimator(anim)
	anim.drop()
	p3EventReceiver(p3Graphics.receiver).setBox(lightnode) 'add the mesh to the reciever
	Return LIGHT
End Function

Function createNPC:ISceneNode(pos:Vector3df, parent:ISceneNode = Null)
	Local NPC1:IAnimatedMeshSceneNode = addAnimMesh(loadAnimMesh("./media/npc/npc.b3d"))
	If Not parent = Null
		npc1.setParent(parent)
		setRelativePosition(NPC1, pos)
	Else
		NPC1.setPosition(pos) '
	End If
	NPC1.SetScale(_VECTOR3DF(1, 1, 1))
	NPC1.setFrameLoop(192, 295) 'for animation
	setTexture(NPC1, loadTexture("./media/npc/body.png"), 0, 0)
	setTexture(NPC1, loadTexture("./media/npc/head.png"), 1, 0)
	p3EventReceiver(p3Graphics.receiver).setBox(NPC1) 'add the mesh to the reciever
	ListAddLast(animMeshList, NPC1)
	Return npc1
End Function

Function createPlayer()
	Scene.Cam = createFPSCamera(False, 100.0, .5, Null, _VECTOR3DF(50, 0, 0))
	p3Graphics.renderDevice.getCursorControl().setVisible(False)
	Scene.cam.setFarValue(42000.0)
	Scene.cam.addAnimator(Scene.Manager.createCollisionResponseAnimator(selector, Scene.cam, _VECTOR3DF(30, 40, 30), _VECTOR3DF(0, -10, 0), _VECTOR3DF(0, 50, 0)))'collide the camera with the polygons of the terrain
	Scene.cam.setTarget(_VECTOR3DF(500, -100, 50))
End Function