
Type p3EventReceiver Extends IEventReceiver

	Field box:TList = New TList
	Global mouseState = True

	Method setBox(b:ISceneNode)
		ListAddLast(box, b)
	EndMethod

	Method OnEvent:Int(event:SEvent)
		If event.getEventType() = EET_GUI_EVENT
			Select event.getGUIEventType()
				Case EGET_MENU_ITEM_SELECTED
					Local menu:IGUIContextMenu = IGUIContextMenu(event.getGUIEventCaller())
					Local id = menu.getItemCommandId(menu.getSelectedItem())
					
					Select id
						Case 1001
							Local newNPC:ISceneNode = createNPC(_vector3df(0, -90, 0), Scene.cam)
							
							setRelativePosition(newNPC, _VECTOR3DF(0, -70, 200))
							removeParent(newNPC)
						Case 1007
							editMode = True
							editCam = createCamera(_VECTOR3DF(0, 500, 0))
							editCam.setTarget(_VECTOR3DF(0,0,0))
							Scene.Manager.setActiveCamera(editCam)
							window = CreateWindow("Drag and Drop Model in this window...", 0, 0, 400, 300, Null, WINDOW_CENTER + WINDOW_TITLEBAR + WINDOW_TOOL + WINDOW_ACCEPTFILES)
							CreateLabel("Drag and Drop your model here", 200, 150, 150, 15, window, LABEL_CENTER)
						Case 1006
							Local newTown:ISceneNode = createCastle(_vector3df(0, -100, 0))
							
							setRelativePosition(newTown, _VECTOR3DF(0, -100, 700))
							removeParent(newTown)
						Case 1005
							Local light:ILightSceneNode = Scene.Manager.addLightSceneNode(Scene.cam, Null, _SCOLORF(.6, .6, .6))
							Local lightnode:IAnimatedMeshSceneNode = addAnimMesh(Scene.Manager.addSphereMesh("", 5))
							lightnode.setParent(LIGHT)
							lightnode.setMaterialFlag(EMF_LIGHTING, False)
							setRelativePosition(light, _VECTOR3DF(0, 0, 200))
							Local endPos:Vector3df = getRelativePosition(light)
							
							Local anim:ISceneNodeAnimator = Scene.Manager.createFlyStraightAnimator(Scene.cam.getPosition(), endPos, 300)
							light.addAnimator(anim)
							anim.drop()
							
							removeParent(light)
							
						Case 1002
							For Local i:ISceneNode = EachIn box
								If Not i = Null
									i.setMaterialFlag(EMF_LIGHTING, Not i.getMaterial(0).getFlag(EMF_LIGHTING))
								End If
							Next
						Case 1003
							Local value = sky.isVisible()
							sky.setVisible(Not value)
						Case 1004
							For Local b:ISceneNode = EachIn box
								If Not b = Null
									b.setMaterialFlag(EMF_WIREFRAME, b.getMaterial(0).getWireframe() = False)
								EndIf
							Next
						Case 2001
							p3Graphics.renderDevice.closeDevice()
					End Select
			End Select
			
		End If
		If event.getEventType() = EET_KEY_INPUT_EVENT And event.getKeyPressedDown() = False
		
			Local key:Int=event.getKeyInputKey()

			Select key
				Case EKEY_KEY_W
					For Local b:ISceneNode = EachIn box
						If Not b = Null
							b.setMaterialFlag(EMF_WIREFRAME, b.getMaterial(0).getWireframe() = False)
						EndIf
					Next
					Return True
				Case EKEY_ESCAPE
					p3Graphics.renderDevice.closeDevice()
					Return True
				Case EKEY_F1
					If mouseState = True
						Scene.cam.setInputReceiverEnabled(False)
						p3Graphics.renderDevice.getCursorControl().setVisible(True)
						controls.setText("F1 - capture mouse")
					Else
						Scene.cam.setInputReceiverEnabled(True)
						p3Graphics.renderDevice.getCursorControl().setVisible(False)
						controls.setText("F1 - release mouse")
					EndIf
					mouseState = Not mouseState
					Return True
				Case EKEY_F2
					For Local b:IAnimatedMeshSceneNode = EachIn animMeshList
						If Not b = Null
							Local animnum = Rand(0, 4)
							Select animnum
								Case 0
									b.setFrameLoop(0, 20)'walk
								Case 1
									b.setFrameLoop(755, 807)'shoot arrow
								Case 2
									b.setFrameLoop(30, 54)'jump
								Case 3
									b.setFrameLoop(650, 685)'unarmed punch
								Case 4
									b.setFrameLoop(296, 351)'attack1
							End Select
							
						EndIf
					Next
					Return True
			End Select
		EndIf
		
		Return False
	EndMethod

	' we must override the generate function in order for instantiation to work properly
	' must return IEventReceiver
	Function generate:IEventReceiver()
		Return New p3EventReceiver
	EndFunction
EndType