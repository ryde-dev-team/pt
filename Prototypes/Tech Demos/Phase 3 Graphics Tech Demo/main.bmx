p3Graphics._start(p3EventReceiver.generate, Null, "Graphics Tech Demo", True)


Global fpstext:IGUIStaticText
Global controls:IGUIStaticText
'Global cam:ICameraSceneNode
Global sky:ISceneNode
Global window:TGadget
Global editCam:ICameraSceneNode

Global animMeshList:TList = New TList

Global selector:ITriangleSelector
Global newSelector:ITriangleSelector

Global editMode:Int = False
createEnvironment()
createGui()

createPlayer()
createWorld()

While p3Graphics.renderDevice.run()
'	If p3Graphics.renderDevice.isWindowActive() = False
'		cam.setInputReceiverEnabled(False)
'		p3Graphics.renderDevice.getCursorControl().setVisible(True)
'		p3Graphics.renderDevice.yield()
'	Else
'		cam.setInputReceiverEnabled(True)
'		p3Graphics.renderDevice.getCursorControl().setVisible(False)
'	EndIf
	
	Local str:String = "FPS: "
	str:+p3Graphics.videoDriver.getFPS()
	str:+" Tris: "
	str:+p3Graphics.videodriver.getPrimitiveCountDrawn()
	str:+" rotY: "
	str:+Scene.cam.GetRotation().getY()
	fpstext.setText(str)
	
	renderWorld()
	updateEvents()
	
	If EventSource() = window And Not window = Null And editMode = True
		ActivateGadget(window)
		If Not EventExtra() = Null
			Local lmesh:ISceneNode = addMesh(loadMesh(EventExtra().ToString()))
			lmesh.setParent(Scene.cam)
			setRelativePosition(lmesh, _VECTOR3DF(0, -100, 500))
			removeParent(lmesh)
			PollEvent()
			HideGadget(window)
			DisableGadget(window)
			window.Free
			editMode = False
		End If
		Select EventID()
			Case EVENT_WINDOWCLOSE
				HideGadget(window)
				DisableGadget(window)
				window.Free
				editMode = False
		End Select
	End If
Wend

p3Graphics._deinit()
End