
Global version$ = "0.00.1"
Global HomePath.s = GetCurrentDirectory()
Global DebugFile$ = GetEnvironmentVariable("PhaseIIIPMLOG")
;DebugFile$ = "C:\Program Files (x86)\SolStar Games\Realm Crafter 1\Project_Manager.log"

Enumeration
	
	#DbugFile
	
EndEnumeration

Global PrefUpdateNeeded = #False
Global PMWindowX;	PM* Project Manager Window
Global PMWindowY
Global PNWindowX;	PN* Project Notes Window
Global PNWindowY
Global PNWindowW
Global PNWindowH
Global ABWindowX;	AB* Abilities Window
Global ABWindowY
Global ABWindowW
Global ABWindowH
Global ACWindowX;	AC* Actors Window
Global ACWindowY
Global ACWindowW
Global ACWindowH
Global ANWindowX;	AN* Animations Window
Global ANWindowY
Global ANWindowW
Global ANWindowH
Global ATWindowX;	AT* Attributes Window
Global ATWindowY
Global ATWindowW
Global ATWindowH
Global COWindowX;	CO* Combat Window
Global COWindowY
Global COWindowW
Global COWindowH
Global DSWindowX;	DS* DaySeason Window
Global DSWindowY
Global DSWindowW
Global DSWindowH
Global FAWindowX;	FA* Factions Window
Global FAWindowY
Global FAWindowW
Global FAWindowH
Global GOWindowX;	GO* General Options Window
Global GOWindowY
Global GOWindowW
Global GOWindowH
Global INWindowX;	IN* Interface Window
Global INWindowY
Global INWindowW
Global INWindowH
Global ITWindowX;	IT* Items Window
Global ITWindowY
Global ITWindowW
Global ITWindowH
Global MEWindowX;	ME* Media Window
Global MEWindowY
Global MEWindowW
Global MEWindowH
Global PAWindowX;	PA* Particles Window
Global PAWindowY
Global PAWindowW
Global PAWindowH
Global PRWindowX;	PR* Projectiles Window - 461, 95, 585, 400
Global PRWindowY
Global PRWindowW
Global PRWindowH
Global SCWindowX;	SC* Scripting Window
Global SCWindowY
Global SCWindowW
Global SCWindowH;
Global ZOWindowX;	ZO* Zone Window
Global ZOWindowY
Global ZOWindowW
Global ZOWindowH

;	Create/Load/Save preferences file
Procedure DoPreferences(Which.i)
	
	If (PrefUpdateNeeded <> #True And Which = 2) : ProcedureReturn : EndIf
	
	Select Which
			
		Case 0; Create Preferences file
			If FileSize(HomePath + "PhaseIII.pref") < 1
				CreatePreferences(HomePath + "PhaseIII.pref", #PB_Preference_NoSpace)
					WritePreferenceLong("PMWindowX", 422)
					WritePreferenceLong("PMWindowY", 22)
					WritePreferenceString("DefaultProject", "")
					WritePreferenceLong("PNWindowX", 0)
					WritePreferenceLong("PNWindowY", 0)
					WritePreferenceLong("PNWindowW", 400)
					WritePreferenceLong("PNWindowH", 400)
					WritePreferenceLong("ABWindowX", 0)
					WritePreferenceLong("ABWindowY", 0)
					WritePreferenceLong("ABWindowW", 400)
					WritePreferenceLong("ABWindowH", 400)
					WritePreferenceLong("ACWindowX", 0)
					WritePreferenceLong("ACWindowY", 0)
					WritePreferenceLong("ACWindowW", 400)
					WritePreferenceLong("ACWindowH", 400)
					WritePreferenceLong("ANWindowX", 0)
					WritePreferenceLong("ANWindowY", 0)
					WritePreferenceLong("ANWindowW", 400)
					WritePreferenceLong("ANWindowH", 400)
					WritePreferenceLong("ATWindowX", 0)
					WritePreferenceLong("ATWindowY", 0)
					WritePreferenceLong("ATWindowW", 400)
					WritePreferenceLong("ATWindowH", 400)
					WritePreferenceLong("COWindowX", 431);	Combat Window Set Dimensions
					WritePreferenceLong("COWindowY", 28)
					WritePreferenceLong("COWindowW", 420)
					WritePreferenceLong("COWindowH", 480)
					WritePreferenceLong("DSWindowX", 0)
					WritePreferenceLong("DSWindowY", 0)
					WritePreferenceLong("DSWindowW", 400)
					WritePreferenceLong("DSWindowH", 400)
					WritePreferenceLong("FAWindowX", 0)
					WritePreferenceLong("FAWindowY", 0)
					WritePreferenceLong("FAWindowW", 400)
					WritePreferenceLong("FAWindowH", 400)
					WritePreferenceLong("GOWindowX", 0)
					WritePreferenceLong("GOWindowY", 0)
					WritePreferenceLong("GOWindowW", 400)
					WritePreferenceLong("GOWindowH", 400)
					WritePreferenceLong("INWindowX", 0)
					WritePreferenceLong("INWindowY", 0)
					WritePreferenceLong("INWindowW", 400)
					WritePreferenceLong("INWindowH", 400)
					WritePreferenceLong("ITWindowX", 0)
					WritePreferenceLong("ITWindowY", 0)
					WritePreferenceLong("ITWindowW", 400)
					WritePreferenceLong("ITWindowH", 400)
					WritePreferenceLong("MEWindowX", 0)
					WritePreferenceLong("MEWindowY", 0)
					WritePreferenceLong("MEWindowW", 400)
					WritePreferenceLong("MEWindowH", 400)
					WritePreferenceLong("PAWindowX", 0)
					WritePreferenceLong("PAWindowY", 0)
					WritePreferenceLong("PAWindowW", 400)
					WritePreferenceLong("PAWindowH", 400)
					WritePreferenceLong("PRWindowX", 461)
					WritePreferenceLong("PRWindowY", 95)
					WritePreferenceLong("PRWindowW", 585)
					WritePreferenceLong("PRWindowH", 400)
					WritePreferenceLong("SCWindowX", 0)
					WritePreferenceLong("SCWindowY", 0)
					WritePreferenceLong("SCWindowW", 400)
					WritePreferenceLong("SCWindowH", 400)
					WritePreferenceLong("ZOWindowX", 0)
					WritePreferenceLong("ZOWindowY", 0)
					WritePreferenceLong("ZOWindowW", 400)
					WritePreferenceLong("ZOWindowH", 400)
			
			EndIf
				
		Case 1; Load Preferences file
			
			OpenPreferences(HomePath + "PhaseIII.pref")
				PMWindowX 	= ReadPreferenceLong("PMWindowX", 422)
				PMWindowY 	= ReadPreferenceLong("PMWindowY", 22)
				TEMP$ 		= ReadPreferenceString("DefaultProject", "");	CONNECT THIS UP
				PNWindowX 	= ReadPreferenceLong("PNWindowX", 0)
				PNWindowY 	= ReadPreferenceLong("PNWindowY", 0)
				PNWindowW 	= ReadPreferenceLong("PNWindowW", 400)
				PNWindowH 	= ReadPreferenceLong("PNWindowH", 400)
				ABWindowX 	= ReadPreferenceLong("ABWindowX", 0)
				ABWindowY 	= ReadPreferenceLong("ABWindowY", 0)
				ABWindowW 	= ReadPreferenceLong("ABWindowW", 400)
				ABWindowH 	= ReadPreferenceLong("ABWindowH", 400)
				ACWindowX 	= ReadPreferenceLong("ACWindowX", 0)
				ACWindowY 	= ReadPreferenceLong("ACWindowY", 0)
				ACWindowW 	= ReadPreferenceLong("ACWindowW", 400)
				ACWindowH 	= ReadPreferenceLong("ACWindowH", 400)
				ANWindowX 	= ReadPreferenceLong("ANWindowX", 0)
				ANWindowY 	= ReadPreferenceLong("ANWindowY", 0)
				ANWindowW 	= ReadPreferenceLong("ANWindowW", 400)
				ANWindowH 	= ReadPreferenceLong("ANWindowH", 400)
				ATWindowX 	= ReadPreferenceLong("ATWindowX", 0)
				ATWindowY 	= ReadPreferenceLong("ATWindowY", 0)
				ATWindowW 	= ReadPreferenceLong("ATWindowW", 400)
				ATWindowH 	= ReadPreferenceLong("ATWindowH", 400)
				COWindowX 	= ReadPreferenceLong("COWindowX", 431)
				COWindowY 	= ReadPreferenceLong("COWindowY", 28)
				COWindowW 	= ReadPreferenceLong("COWindowW", 420)
				COWindowH 	= ReadPreferenceLong("COWindowH", 480)
				DSWindowX 	= ReadPreferenceLong("DSWindowX", 0)
				DSWindowY 	= ReadPreferenceLong("DSWindowY", 0)
				DSWindowW 	= ReadPreferenceLong("DSWindowW", 400)
				DSWindowH 	= ReadPreferenceLong("DSWindowH", 400)
				FAWindowX 	= ReadPreferenceLong("FAWindowX", 0)
				FAWindowY 	= ReadPreferenceLong("FAWindowY", 0)
				FAWindowW 	= ReadPreferenceLong("FAWindowW", 400)
				FAWindowH 	= ReadPreferenceLong("FAWindowH", 400)
				GOWindowX 	= ReadPreferenceLong("GOWindowX", 0)
				GOWindowY 	= ReadPreferenceLong("GOWindowY", 0)
				GOWindowW 	= ReadPreferenceLong("GOWindowW", 400)
				GOWindowH 	= ReadPreferenceLong("GOWindowH", 400)
				INWindowX 	= ReadPreferenceLong("INWindowX", 0)
				INWindowY 	= ReadPreferenceLong("INWindowY", 0)
				INWindowW 	= ReadPreferenceLong("INWindowW", 400)
				INWindowH 	= ReadPreferenceLong("INWindowH", 400)
				ITWindowX 	= ReadPreferenceLong("ITWindowX", 0)
				ITWindowY 	= ReadPreferenceLong("ITWindowY", 0)
				ITWindowW 	= ReadPreferenceLong("ITWindowW", 400)
				ITWindowH 	= ReadPreferenceLong("ITWindowH", 400)
				MEWindowX 	= ReadPreferenceLong("MEWindowX", 0)
				MEWindowY 	= ReadPreferenceLong("MEWindowY", 0)
				MEWindowW 	= ReadPreferenceLong("MEWindowW", 400)
				MEWindowH 	= ReadPreferenceLong("MEWindowH", 400)
				PAWindowX 	= ReadPreferenceLong("PAWindowX", 0)
				PAWindowY 	= ReadPreferenceLong("PAWindowY", 0)
				PAWindowW 	= ReadPreferenceLong("PAWindowW", 400)
				PAWindowH 	= ReadPreferenceLong("PAWindowH", 400)
				PRWindowX 	= ReadPreferenceLong("PRWindowX", 461)
				PRWindowY 	= ReadPreferenceLong("PRWindowY", 95)
				PRWindowW 	= ReadPreferenceLong("PRWindowW", 585)
				PRWindowH 	= ReadPreferenceLong("PRWindowH", 400)
				SCWindowX 	= ReadPreferenceLong("SCWindowX", 0)
				SCWindowY 	= ReadPreferenceLong("SCWindowY", 0)
				SCWindowW 	= ReadPreferenceLong("SCWindowW", 400)
				SCWindowH 	= ReadPreferenceLong("SCWindowH", 400)
				ZOWindowX 	= ReadPreferenceLong("ZOWindowX", 0)
				ZOWindowY 	= ReadPreferenceLong("ZOWindowY", 0)
				ZOWindowW 	= ReadPreferenceLong("ZOWindowW", 400)
				ZOWindowH 	= ReadPreferenceLong("ZOWindowH", 400)
			
			
		Case 2; Save preferences file
				
			CreatePreferences(HomePath + "PhaseIII.pref", #PB_Preference_NoSpace)
			
				WritePreferenceLong("PMWindowX", PMWindowX)
				WritePreferenceLong("PMWindowY", PMWindowY)
				WritePreferenceString("DefaultProject", "");	CONNECT THIS UP
				WritePreferenceLong("PNWindowX", PNWindowX)
				WritePreferenceLong("PNWindowY", PNWindowY)
				WritePreferenceLong("PNWindowW", PNWindowW)
				WritePreferenceLong("PNWindowH", PNWindowH)
				WritePreferenceLong("ABWindowX", ABWindowX)
				WritePreferenceLong("ABWindowY", ABWindowY)
				WritePreferenceLong("ABWindowW", ABWindowW)
				WritePreferenceLong("ABWindowH", ABWindowH)
				WritePreferenceLong("ACWindowX", ACWindowX)
				WritePreferenceLong("ACWindowY", ACWindowY)
				WritePreferenceLong("ACWindowW", ACWindowW)
				WritePreferenceLong("ACWindowH", ACWindowH)
				WritePreferenceLong("ANWindowX", ANWindowX)
				WritePreferenceLong("ANWindowY", ANWindowY)
				WritePreferenceLong("ANWindowW", ANWindowW)
				WritePreferenceLong("ANWindowH", ANWindowH)
				WritePreferenceLong("ATWindowX", ATWindowX)
				WritePreferenceLong("ATWindowY", ATWindowY)
				WritePreferenceLong("ATWindowW", ATWindowW)
				WritePreferenceLong("ATWindowH", ATWindowH)
				WritePreferenceLong("COWindowX", COWindowX)
				WritePreferenceLong("COWindowY", COWindowY)
				WritePreferenceLong("COWindowW", COWindowW)
				WritePreferenceLong("COWindowH", COWindowH)
				WritePreferenceLong("DSWindowX", DSWindowX)
				WritePreferenceLong("DSWindowY", DSWindowY)
				WritePreferenceLong("DSWindowW", DSWindowW)
				WritePreferenceLong("DSWindowH", DSWindowH)
				WritePreferenceLong("FAWindowX", FAWindowX)
				WritePreferenceLong("FAWindowY", FAWindowY)
				WritePreferenceLong("FAWindowW", FAWindowW)
				WritePreferenceLong("FAWindowH", FAWindowH)
				WritePreferenceLong("GOWindowX", GOWindowX)
				WritePreferenceLong("GOWindowY", GOWindowY)
				WritePreferenceLong("GOWindowW", GOWindowW)
				WritePreferenceLong("GOWindowH", GOWindowH)
				WritePreferenceLong("INWindowX", INWindowX)
				WritePreferenceLong("INWindowY", INWindowY)
				WritePreferenceLong("INWindowW", INWindowW)
				WritePreferenceLong("INWindowH", INWindowH)
				WritePreferenceLong("ITWindowX", ITWindowX)
				WritePreferenceLong("ITWindowY", ITWindowY)
				WritePreferenceLong("ITWindowW", ITWindowW)
				WritePreferenceLong("ITWindowH", ITWindowH)
				WritePreferenceLong("MEWindowX", MEWindowX)
				WritePreferenceLong("MEWindowY", MEWindowY)
				WritePreferenceLong("MEWindowW", MEWindowW)
				WritePreferenceLong("MEWindowH", MEWindowH)
				WritePreferenceLong("PAWindowX", PAWindowX)
				WritePreferenceLong("PAWindowY", PAWindowY)
				WritePreferenceLong("PAWindowW", PAWindowW)
				WritePreferenceLong("PAWindowH", PAWindowH)
				WritePreferenceLong("PRWindowX", PRWindowX)
				WritePreferenceLong("PRWindowY", PRWindowY)
				WritePreferenceLong("PRWindowW", PRWindowW)
				WritePreferenceLong("PRWindowH", PRWindowH)
				WritePreferenceLong("SCWindowX", SCWindowX)
				WritePreferenceLong("SCWindowY", SCWindowY)
				WritePreferenceLong("SCWindowW", SCWindowW)
				WritePreferenceLong("SCWindowH", SCWindowH)
				WritePreferenceLong("ZOWindowX", ZOWindowX)
				WritePreferenceLong("ZOWindowY", ZOWindowY)
				WritePreferenceLong("ZOWindowW", ZOWindowW)
				WritePreferenceLong("ZOWindowH", ZOWindowH)
			
			PrefUpdateNeeded = #False
			
	EndSelect
	
	ClosePreferences()
	
EndProcedure



;{ 	Log file ( Open values = [0 = write data | 1 = create new log and open it | 2 = Close log ] )

Procedure.s LogFile(Instring.s, PutDate, AddSpace)
	
	OpenFile(#DbugFile, DebugFile$, #PB_File_Append|#PB_File_SharedRead|#PB_File_SharedWrite)
		If PutDate = 1
			If AddSpace = 1
				WriteStringN(#DbugFile, FormatDate("%mm/%dd/%yyyy", Date()) + " : ] " + Instring)
				WriteStringN(#DbugFile, "")
			Else
				WriteStringN(#DbugFile, FormatDate("%mm/%dd/%yyyy", Date()) + " : ] " + Instring)
			EndIf
		Else
			If AddSpace = 1
				WriteStringN(#DbugFile, FormatDate("%hh:%ii:%ss", Date()) + " : ] " + Instring)
				WriteStringN(#DbugFile, "")
			Else
				WriteStringN(#DbugFile, FormatDate("%hh:%ii:%ss", Date()) + " : ] " + Instring)
			EndIf
		EndIf
	CloseFile(#DbugFile)
	
EndProcedure



Global ProjPath.s = ProgramParameter()









; IDE Options = PureBasic 5.21 LTS (Windows - x64)
; CursorPosition = 15
; Folding = -
; EnableUser
; DisableDebugger
; CurrentDirectory = C:\Program Files\PureBasic\
; EnableBuildCount = 0