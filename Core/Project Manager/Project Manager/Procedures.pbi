;====================================================================================================================
;		Project Manager - Phase III Engine Main utility launcher/Project Management tool [Include File]
;		This work is copyright(c)2014 By:Robert S. Quackenbush
;		It is colectively part of the Phase III Engine
;		Phase III Engine Copyright(c)2014 by Cory R. Dean and Robert S. Quackenbush
;		All Rights Reserved
;=====================================================================================================================


; Create tooltip
Procedure GadgetBalloonToolTip(WindowNumber.l, GadgetNumber.l, Text.s)
	
	Protected Tooltip.l, Balloon.TOOLINFO
	Tooltip = CreateWindowEx_(0, "ToolTips_Class32", "", #WS_POPUP | #TTS_NOPREFIX | #TTS_BALLOON, 0, 0, 0, 0, WindowID(WindowNumber), 0, GetModuleHandle_(0), 0)
	SendMessage_(Tooltip, #TTM_SETTIPTEXTCOLOR, RGB(255,0,0), 0)
	SendMessage_(Tooltip, #TTM_SETTIPBKCOLOR, RGB(255,255,0), 0)
;	SendMessage_(Tooltip, #TTM_SETTIPTEXTCOLOR, GetSysColor_(#COLOR_INFOTEXT), 0)
;	SendMessage_(Tooltip, #TTM_SETTIPBKCOLOR, GetSysColor_(#COLOR_INFOBK), 0)
	SendMessage_(Tooltip, #TTM_SETMAXTIPWIDTH, 0, 200)
	SendMessage_(Tooltip, #TTM_SETDELAYTIME, 20000, 0)
	Balloon\cbSize = SizeOf(TOOLINFO)
	Balloon\uFlags = #TTF_IDISHWND | #TTF_SUBCLASS
	If IsGadget(GadgetNumber)
		Balloon\hwnd = GadgetID(GadgetNumber)
		Balloon\uId = GadgetID(GadgetNumber)
	Else
		Balloon\hwnd = GadgetNumber
		Balloon\uId = GadgetNumber
	EndIf
	Balloon\lpszText = @Text
	SendMessage_(Tooltip, #TTM_ADDTOOL, 0, @Balloon)
	
  ProcedureReturn Tooltip
  
EndProcedure


; Change tooltip text
Procedure ChangeGadgetBalloonToolTip(Tooltip.l, GadgetNumber.l, Text.s)
	
	Protected Balloon.TOOLINFO
	Balloon\cbSize = SizeOf(TOOLINFO)
	Balloon\uFlags = #TTF_IDISHWND | #TTF_SUBCLASS
	If IsGadget(GadgetNumber)
		Balloon\hwnd = GadgetID(GadgetNumber)
		Balloon\uId = GadgetID(GadgetNumber)
	Else
		Balloon\hwnd = GadgetNumber
		Balloon\uId = GadgetNumber
	EndIf
	Balloon\lpszText = @Text
	SendMessage_(Tooltip, #TTM_ADDTOOL, 0, @Balloon)
  
EndProcedure


; Free tooltip
Procedure FreeBalloonTooltip(Tooltip.l)
	
	DestroyWindow_(Tooltip.l)
	
EndProcedure


;	Play the splash screen video
Procedure PlaySplash();						WORKING 100%
	
	InitMovie()
	If OpenWindow(#MovieWindow, (DesktopWidth / 2) - 320, (DesktopHeight / 2) - 240, 640, 480, "", #PB_Window_Invisible|#PB_Window_WindowCentered|#PB_Window_BorderLess)
		StickyWindow(#MovieWindow, 1)
		HideWindow(#MovieWindow, 0)  ; Show the window once all toolbar/menus has been created.
		HideWindow(#MainWindow, 1)
		If LoadMovie(#MovieWindow, HomePath + "Media\Splash.avi")
		Else
			LogFile("#####[ERROR] Splash screen movie is missing", 1, 1)
			MessageRequester("ERROR", "Splash screen is missing.", #PB_MessageRequester_Ok)
			CloseWindow(#MovieWindow)
			Delay(5000)
			ProcedureReturn
		EndIf
		PlayMovie(#MovieWindow, WindowID(#MovieWindow))
		Playing = 1
		While Playing > 0
			Playing = MovieStatus(#MovieWindow)
		Wend
		Delay(5000)
		CloseWindow(#MovieWindow)
		HideWindow(#MainWindow, 0)
		UseGadgetList(WindowID(#MainWindow))
		ImageGadget(#BannerImage_IMG, 0, 0, 400, 88, ImageID(BannerImage))
		ProcedureReturn
	EndIf
	
EndProcedure


;	Opens the project notes window
Procedure OpenWindow_NotesWindow(SelectedProjectName$);	WORKING 100%
	
	OpenWindow(#NotesWindow_Main,PNWindowX,PNWindowY,PNWindowW, PNWindowH, SelectedProjectName$ + " Notes", #PB_Window_MaximizeGadget|#PB_Window_MinimizeGadget|#PB_Window_SizeGadget)
	EditorGadget(#NotesEditor, PNWindowX, PNWindowY, PNWindowW - 1, PNWindowH - 50, #PB_Editor_WordWrap)
	ButtonGadget(#BTN_CancelNotes,PNWindowW - 100, PNWindowH - 30, 45, 20, "Cancel")
	ButtonGadget(#BTN_SaveNotes, PNWindowW - 50, PNWindowH - 30, 45, 20, "Save")
	
EndProcedure


;	Create the main application window and gadgets
Procedure OpenWindow_MainWindow();				WORKING 100%
	
	Banner = 88
	If OpenWindow(#MainWindow, PMWindowX, PMWindowY, 544, 523, AppName$ + " " + Version$, #PB_Window_TitleBar|#PB_Window_SystemMenu|#PB_Window_MinimizeGadget)
		SmartWindowRefresh(#MainWindow, #True)
		If CreateMenu(#Menu_MainWindow, WindowID(#MainWindow))
			MenuTitle("File")
			MenuTitle("Preferences")
			MenuTitle("About")
		EndIf
		ImageGadget(#BannerImage_IMG, 0, 0, 400, 88, ImageID(BannerImage))
		FrameGadget(#PB_Any, 5, 5 + Banner, 535, 210, "Tool box")
		FrameGadget(#PB_Any, 5, 225 + Banner, 535, 135, "Project Data")
		ButtonGadget(#BTN_BuildServer, 140, 365 + Banner, 95, 20, "Build Server", #PB_Button_Default)
		ButtonGadget(#BTN_BuildClient, 140, 385 + Banner, 95, 20, "Build Client", #PB_Button_Default)
		ButtonGadget(#BTN_NewProject, 20, 365 + Banner, 95, 20, "New Project", #PB_Button_Default)
		ButtonGadget(#BTN_DeleteProject, 20, 385 + Banner, 95, 20, "Delete Project", #PB_Button_Default)
		ButtonGadget(#BTN_LaunchTestServer, 260, 365 + Banner, 95, 20, "Run Test Server", #PB_Button_Default)
		ButtonGadget(#BTN_LaunchTestClient, 260, 385 + Banner, 95, 20, "Run Test Client", #PB_Button_Default)
		ButtonGadget(#BTN_Launch_PNotes, 375, 365 + Banner, 50, 45, "Project Notes", #PB_Button_MultiLine)
		BTN_Launch_TeamChat_Tooltip = GadgetBalloonToolTip(#MainWindow, #BTN_Launch_PNotes, "Project Notepad")
		ButtonGadget(#BTN_CleanProject, 430, 365 + Banner, 50, 45, "Clean Project", #PB_Button_MultiLine)
		ButtonGadget(#Button_47, 485, 365 + Banner, 50, 45, "Edit Global Docs", #PB_Button_MultiLine)
		ContainerGadget(#Container_0, 10, 20 + Banner, 525, 190)
			ButtonImageGadget(#BTN_1_Launch, 19, 5, 55, 55, ImageID(BTN_ImageID(1)), #BS_FLAT)
			BTN_Launch_MediaManager_Tooltip = GadgetBalloonToolTip(#MainWindow, #BTN_1_Launch, BTN_Plugin_Tooltips.s(1))
			ButtonImageGadget(#BTN_2_Launch, 19, 65, 55, 55, ImageID(BTN_ImageID(2)), #BS_FLAT)
			BTN_Launch_ScriptManager_Tooltip = GadgetBalloonToolTip(#MainWindow, #BTN_2_Launch, BTN_Plugin_Tooltips.s(2))
			ButtonImageGadget(#BTN_3_Launch, 20, 125, 55, 55, ImageID(BTN_ImageID(3)), #BS_FLAT)
			BTN_Launch_FactionsManager_Tooltip = GadgetBalloonToolTip(#MainWindow, #BTN_3_Launch, BTN_Plugin_Tooltips.s(3))
			ButtonImageGadget(#BTN_4_Launch, 104, 4, 55, 55, ImageID(BTN_ImageID(4)), #BS_FLAT)
			BTN_Launch_EmitterManager_Tooltip = GadgetBalloonToolTip(#MainWindow, #BTN_4_Launch, BTN_Plugin_Tooltips.s(4))
			ButtonImageGadget(#BTN_5_Launch, 105, 65, 55, 55, ImageID(BTN_ImageID(5)), #BS_FLAT)
			BTN_Launch_ProjectilesManager_Tooltip = GadgetBalloonToolTip(#MainWindow, #BTN_5_Launch, BTN_Plugin_Tooltips.s(5))
			ButtonImageGadget(#BTN_6_Launch, 105, 125, 55, 55, ImageID(BTN_ImageID(6)), #BS_FLAT)
			BTN_Launch_ItemsManager_Tooltip = GadgetBalloonToolTip(#MainWindow, #BTN_6_Launch, BTN_Plugin_Tooltips.s(6))
			ButtonImageGadget(#BTN_7_Launch, 189, 4, 55, 55, ImageID(BTN_ImageID(7)), #BS_FLAT)
			BTN_Launch_CombatManager_Tooltip = GadgetBalloonToolTip(#MainWindow, #BTN_7_Launch, BTN_Plugin_Tooltips.s(7))
			ButtonImageGadget(#BTN_8_Launch, 190, 65, 55, 55, ImageID(BTN_ImageID(8)), #BS_FLAT)
			BTN_Launch_AttributesManager_Tooltip = GadgetBalloonToolTip(#MainWindow, #BTN_8_Launch, BTN_Plugin_Tooltips.s(8))
			ButtonImageGadget(#BTN_9_Launch, 190, 125, 55, 55, ImageID(BTN_ImageID(9)), #BS_FLAT)
			BTN_Launch_AbilitiesManager_Tooltip = GadgetBalloonToolTip(#MainWindow, #BTN_9_Launch, BTN_Plugin_Tooltips.s(9))
			ButtonImageGadget(#BTN_10_Launch, 274, 4, 55, 55, ImageID(BTN_ImageID(10)), #BS_FLAT)
			BTN_Launch_ActorsManager_Tooltip = GadgetBalloonToolTip(#MainWindow, #BTN_10_Launch, BTN_Plugin_Tooltips.s(10))
			ButtonImageGadget(#BTN_11_Launch, 275, 65, 55, 55, ImageID(BTN_ImageID(11)), #BS_FLAT)
			BTN_Launch_AnimationsManager_Tooltip = GadgetBalloonToolTip(#MainWindow, #BTN_11_Launch, BTN_Plugin_Tooltips.s(11))
			ButtonImageGadget(#BTN_12_Launch, 275, 125, 55, 55, ImageID(BTN_ImageID(12)), #BS_FLAT)
			BTN_Launch_DaysSeasonsManager_Tooltip = GadgetBalloonToolTip(#MainWindow, #BTN_12_Launch, BTN_Plugin_Tooltips.s(12))
			ButtonImageGadget(#BTN_13_Launch, 359, 4, 55, 55, ImageID(BTN_ImageID(13)), #BS_FLAT)
			BTN_Launch_ZonesManager_Tooltip = GadgetBalloonToolTip(#MainWindow, #BTN_13_Launch, BTN_Plugin_Tooltips.s(13))
			ButtonImageGadget(#BTN_14_Launch, 360, 65, 55, 55, ImageID(BTN_ImageID(14)), #BS_FLAT)
			BTN_Launch_InterfaceManager_Tooltip = GadgetBalloonToolTip(#MainWindow, #BTN_14_Launch, BTN_Plugin_Tooltips.s(14))
			ButtonImageGadget(#BTN_15_Launch, 360, 125, 55, 55, ImageID(BTN_ImageID(15)), #BS_FLAT)
			BTN_Launch_GeneralOptionsManager_Tooltip = GadgetBalloonToolTip(#MainWindow, #BTN_15_Launch, BTN_Plugin_Tooltips.s(15))
			ButtonImageGadget(#BTN_16_Launch, 444, 4, 55, 55, ImageID(BTN_ImageID(16)), #BS_FLAT)
			BTN_Launch_GeneralOptionsManager_Tooltip = GadgetBalloonToolTip(#MainWindow, #BTN_16_Launch, BTN_Plugin_Tooltips.s(16))
			ButtonImageGadget(#BTN_17_Launch, 445, 65, 55, 55, ImageID(BTN_ImageID(17)), #BS_FLAT)
			BTN_Launch_GeneralOptionsManager_Tooltip = GadgetBalloonToolTip(#MainWindow, #BTN_17_Launch, BTN_Plugin_Tooltips.s(17))
			ButtonImageGadget(#BTN_18_Launch, 445, 125, 55, 55, ImageID(BTN_ImageID(18)), #BS_FLAT)
			BTN_Launch_GeneralOptionsManager_Tooltip = GadgetBalloonToolTip(#MainWindow, #BTN_18_Launch, BTN_Plugin_Tooltips.s(18))
		CloseGadgetList()
		PanelGadget(#Projects_Panel, 10, 240 + Banner, 525, 110)
		
      ; Local Projects
		AddGadgetItem(#Projects_Panel, -1, "Local Projects")
		ListIconGadget(#LocalProjectsView, 3, 3, 515, 80, "", 0, #PB_ListIcon_GridLines)
		SendMessage_(GadgetID(#LocalProjectsView), #LVM_SETCOLUMNWIDTH, 0, 0)
		AddGadgetColumn(#LocalProjectsView, 0, "Project Name", 170)
		AddGadgetColumn(#LocalProjectsView, 1, "Version", 70)
		AddGadgetColumn(#LocalProjectsView, 2, "Size", 65)
		AddGadgetColumn(#LocalProjectsView, 3, "File Count", 70)
		AddGadgetColumn(#LocalProjectsView, 4, "Client Build", 65)
		AddGadgetColumn(#LocalProjectsView, 5, "Server Build", 70)
      ; Team Projects
;		AddGadgetItem(#Projects_Panel, -1, "Team Projects")
;		ListViewGadget(#TeamProjectsView, 3, 3, 515, 80, #PB_ListView_ClickSelect)
		CloseGadgetList()
	EndIf

EndProcedure


;	Create the 'Name input' window and gadgets for creating a new project
Procedure OpenWindow_NewProject_Window();			WORKING 100%
	
	If OpenWindow(#NewProject_Window, 517, 211, 260, 100, "New Project", #PB_Window_SystemMenu|#PB_Window_TitleBar|#PB_Window_ScreenCentered)
		FrameGadget(#PB_Any, 6, 10, 250, 50, "Project Name")
		StringGadget(#IPB_New_Project_Name, 10, 28, 241, 25, "", #WS_BORDER)
		ButtonGadget(#BTN_Create_New_Project, 30, 67, 80, 25, "Create Project")
		ButtonGadget(#BTN_Create_New_Project_Cancel, 111, 67, 80, 25, "Cancel")
	EndIf
	
EndProcedure


;	Determine the size in bytes of a project and set the valu of ProjSize.f to that value
Procedure.s GetDirectorySize(ProjectPath$);			WORKING 100%
	
	ScanHandle = ExamineDirectory(#PB_Any, ProjectPath$, "")
		While NextDirectoryEntry(ScanHandle)
			If DirectoryEntryType(ScanHandle) = #PB_DirectoryEntry_Directory
				If DirectoryEntryName(ScanHandle) = "." Or DirectoryEntryName(ScanHandle) = ".."
				Else
					GetDirectorySize(ProjectPath$ + "\" + DirectoryEntryName(ScanHandle))
				EndIf
			Else
				ProjSize + DirectoryEntrySize(ScanHandle)
				FileCount +1
			EndIf
		Wend
	FinishDirectory(ScanHandle)
	
EndProcedure


;	Load Project Manager data files
Procedure LoadProjects()
	
	LogFile("[Loading Projects]==========", 0, 0)
	ClearGadgetItems(#LocalProjectsView)
	For i = 1 to 255
		Project$(i) = Space(50)
		ProjectVersion$(i) = Space(7)
		ProjectTotalFiles(i) = 0
		ProjectSize(i) = 0
		ClientBuildCount(i) = 0
		ServerBuildCount(i) = 0
	Next i
	If FileSize(HomePath + "PM_Pinfo.dat") > 0
		FH = ReadFile(#PB_Any, HomePath + "PM_Pinfo.dat", #PB_File_SharedRead)
			TotalProjects.a = ReadAsciiCharacter(FH)
			If TotalProjects > 0
				i = 0
				Counter = 1
				Repeat
					i +1
					For j = Counter to 255
						FileSeek(FH, 1 + ((j - 1) * 8))
						DataStart.q = ReadQuad(FH)
						FileSeek(FH, DataStart)
						Project$(i) + ReadString(FH, #PB_Ascii, 50)
						Project$(i) = Trim(Project$(i))
						LogFile(Project$(i) + " Loaded", 0, 0)
						ProjectVersion$(i) = ReadString(FH, #PB_Ascii,7)
						ProjectVersion$(i) = Trim(ProjectVersion$(i))
						ProjectTotalFiles(i) = ReadLong(FH)
						ProjectSize(i) = ReadQuad(FH)
						ClientBuildCount(i) = ReadLong(FH)
						ServerBuildCount(i) = ReadLong(FH)
						ProjSize = ProjectSize(i)
						If Trim(Project$(i)) <> ""
							While ProjSize > 1024
								Stepup +1
								ProjSize /1024
							Wend
							Select Stepup
			
								Case 1
			
									PS$ = Strf(ProjSize, 0) + " Kb"
			
								Case 2
			
									PS$ = Strf(ProjSize, 2) + " MB"
			
								Case 3
			
									PS$ = Strf(ProjSize, 2) + " GB"
			
							EndSelect
							AddGadgetItem(#LocalProjectsView, -1, Project$(i) + Chr(10) + ProjectVersion$(i) + Chr(10) + PS$ + Chr(10) + Str(ProjectTotalFiles(i)) + Chr(10) + Str(ClientBuildCount(i)) + Chr(10) + Str(ServerBuildCount(i)))
							Counter = j + 1
							Break
						EndIf
					Next j
				Until i = TotalProjects
				If i = 1
					SelectedProjectID = 0
					SetActiveGadget(#LocalProjectsView)
					SetGadgetState(#LocalProjectsView, SelectedProjectID)
					SelectedProjectName$ = Project$(i)
				EndIf
				Stepup = 0
				ProjSize = 0
			EndIf
		CloseFile(FH)
	EndIf
	LogFile("[Loading Projects] COMPLETED", 0, 1)
	
EndProcedure


;	Create Project Manager PM_Pinfo.dat file.
Procedure CreatePMPinfodat()
	
	If FileSize(HomePath + "PM_Pinfo.dat") > 0 : Return : EndIf
		FH = CreateFile(#PB_Any, HomePath + "PM_Pinfo.dat")
			WriteAsciiCharacter(FH, 0)
			For i = 1 to 255
				FileSeek(FH, 1 + ((i - 1) * 8))
				WriteQuad(FH, 2043 + ((i - 1) * 69)); first available is 2043, increment 69 per record
			Next i
			For i = 1 to 255
				WriteString(FH, Space(50))
				WriteString(FH, Space(7))
				WriteLong(FH, 0)
				WriteQuad(FH, 0)
				WriteLong(FH, 0)
				WriteLong(FH, 0)
			Next i
		CloseFile(FH)
	
EndProcedure


;	Update Project Manager PM_Pinfo.dat file with new project info Add Project
Procedure UpdatePM_PinfodatAdd(TotalProjects)

	FH = OpenFile(#PB_Any, HomePath + "PM_Pinfo.dat")
		
		WriteAsciiCharacter(FH, TotalProjects)
		Repeat
			Count +1
			pntr = (Count - 1) * 8
			FileSeek(FH, 1 + pntr)
			pntr2.q = ReadQuad(FH)
			FileSeek(FH, pntr2)
			junk$ = ReadString(FH, #PB_Ascii, 50)
			If Trim(junk$) = ""
				FileSeek(FH, 1 + pntr)
				WriteQuad(FH, 2043 + (pntr * 69)); first available is 2043, increment 69 per record
				FileSeek(FH, 2043 + (pntr * 69))
				WriteString(FH, Project$(TotalProjects) + Space(50 - Len(Project$(TotalProjects))))
				WriteString(FH, ProjectVersion$(TotalProjects) + Space(7 - Len(ProjectVersion$(TotalProjects))))
				WriteLong(FH, ProjectTotalFiles(TotalProjects))
				WriteQuad(FH, ProjectSize(TotalProjects))
				WriteLong(FH, ClientBuildCount(TotalProjects))
				WriteLong(FH, ServerBuildCount(TotalProjects))
				Break
			EndIf
		Until pntr >= 2042
	CloseFile(FH)
	
EndProcedure


;	Update Project Manager PM_Pinfo.dat file with new project info Delete Project
Procedure UpdatePM_PinfodatDelete(TotalProjectsID)

	FH = OpenFile(#PB_Any, HomePath + "PM_Pinfo.dat")
		
		WriteAsciiCharacter(FH, TotalProjects - 1)
		TotalProjects -1
		FileSeek(FH, 1 + (TotalProjectsID  * 8))
		WriteQuad(FH, 2043 + (TotalProjectsID * 69)); first available is 2043, increment 69 per record
		FileSeek(FH, 2043 + (TotalProjectsID * 69))
		WriteString(FH, "")
		WriteString(FH, "")
		WriteLong(FH, 0)
		WriteQuad(FH, 0)
		WriteLong(FH, 0)
		WriteLong(FH, 0)
	CloseFile(FH)
	
EndProcedure


;	Create/Load/Save preferences file
Procedure DoPreferences(Which.i)
	
	If (PrefUpdateNeeded <> #True And Which = 2) : ProcedureReturn : EndIf
	
	Select Which
			
		Case 0; Create Preferences file
			If FileSize(HomePath + "PhaseIII.pref") < 1
				CreatePreferences(HomePath + "PhaseIII.pref", #PB_Preference_NoSpace)
					WritePreferenceLong("PMWindowX", 422)
					WritePreferenceLong("PMWindowY", 22)
					WritePreferenceString("DefaultProject", "")
					WritePreferenceLong("PNWindowX", 0)
					WritePreferenceLong("PNWindowY", 0)
					WritePreferenceLong("PNWindowW", 400)
					WritePreferenceLong("PNWindowH", 400)
					WritePreferenceLong("ABWindowX", 0)
					WritePreferenceLong("ABWindowY", 0)
					WritePreferenceLong("ABWindowW", 400)
					WritePreferenceLong("ABWindowH", 400)
					WritePreferenceLong("ACWindowX", 0)
					WritePreferenceLong("ACWindowY", 0)
					WritePreferenceLong("ACWindowW", 400)
					WritePreferenceLong("ACWindowH", 400)
					WritePreferenceLong("ANWindowX", 0)
					WritePreferenceLong("ANWindowY", 0)
					WritePreferenceLong("ANWindowW", 400)
					WritePreferenceLong("ANWindowH", 400)
					WritePreferenceLong("ATWindowX", 0)
					WritePreferenceLong("ATWindowY", 0)
					WritePreferenceLong("ATWindowW", 400)
					WritePreferenceLong("ATWindowH", 400)
					WritePreferenceLong("COWindowX", 431);	Combat Window Set Dimensions
					WritePreferenceLong("COWindowY", 28)
					WritePreferenceLong("COWindowW", 420)
					WritePreferenceLong("COWindowH", 480)
					WritePreferenceLong("DSWindowX", 0)
					WritePreferenceLong("DSWindowY", 0)
					WritePreferenceLong("DSWindowW", 400)
					WritePreferenceLong("DSWindowH", 400)
					WritePreferenceLong("FAWindowX", 0)
					WritePreferenceLong("FAWindowY", 0)
					WritePreferenceLong("FAWindowW", 400)
					WritePreferenceLong("FAWindowH", 400)
					WritePreferenceLong("GOWindowX", 0)
					WritePreferenceLong("GOWindowY", 0)
					WritePreferenceLong("GOWindowW", 400)
					WritePreferenceLong("GOWindowH", 400)
					WritePreferenceLong("INWindowX", 0)
					WritePreferenceLong("INWindowY", 0)
					WritePreferenceLong("INWindowW", 400)
					WritePreferenceLong("INWindowH", 400)
					WritePreferenceLong("ITWindowX", 0)
					WritePreferenceLong("ITWindowY", 0)
					WritePreferenceLong("ITWindowW", 400)
					WritePreferenceLong("ITWindowH", 400)
					WritePreferenceLong("MEWindowX", 0)
					WritePreferenceLong("MEWindowY", 0)
					WritePreferenceLong("MEWindowW", 400)
					WritePreferenceLong("MEWindowH", 400)
					WritePreferenceLong("PAWindowX", 0)
					WritePreferenceLong("PAWindowY", 0)
					WritePreferenceLong("PAWindowW", 400)
					WritePreferenceLong("PAWindowH", 400)
					WritePreferenceLong("PRWindowX", 0)
					WritePreferenceLong("PRWindowY", 0)
					WritePreferenceLong("PRWindowW", 400)
					WritePreferenceLong("PRWindowH", 400)
					WritePreferenceLong("SCWindowX", 0)
					WritePreferenceLong("SCWindowY", 0)
					WritePreferenceLong("SCWindowW", 400)
					WritePreferenceLong("SCWindowH", 400)
					WritePreferenceLong("ZOWindowX", 0)
					WritePreferenceLong("ZOWindowY", 0)
					WritePreferenceLong("ZOWindowW", 400)
					WritePreferenceLong("ZOWindowH", 400)
			
			EndIf
				
		Case 1; Load Preferences file
			
			OpenPreferences(HomePath + "PhaseIII.pref")
				PMWindowX 	= ReadPreferenceLong("PMWindowX", 422)
				PMWindowY 	= ReadPreferenceLong("PMWindowY", 22)
				TEMP$ 		= ReadPreferenceString("DefaultProject", "");	CONNECT THIS UP
				PNWindowX 	= ReadPreferenceLong("PNWindowX", 0)
				PNWindowY 	= ReadPreferenceLong("PNWindowY", 0)
				PNWindowW 	= ReadPreferenceLong("PNWindowW", 400)
				PNWindowH 	= ReadPreferenceLong("PNWindowH", 400)
				ABWindowX 	= ReadPreferenceLong("ABWindowX", 0)
				ABWindowY 	= ReadPreferenceLong("ABWindowY", 0)
				ABWindowW 	= ReadPreferenceLong("ABWindowW", 400)
				ABWindowH 	= ReadPreferenceLong("ABWindowH", 400)
				ACWindowX 	= ReadPreferenceLong("ACWindowX", 0)
				ACWindowY 	= ReadPreferenceLong("ACWindowY", 0)
				ACWindowW 	= ReadPreferenceLong("ACWindowW", 400)
				ACWindowH 	= ReadPreferenceLong("ACWindowH", 400)
				ANWindowX 	= ReadPreferenceLong("ANWindowX", 0)
				ANWindowY 	= ReadPreferenceLong("ANWindowY", 0)
				ANWindowW 	= ReadPreferenceLong("ANWindowW", 400)
				ANWindowH 	= ReadPreferenceLong("ANWindowH", 400)
				ATWindowX 	= ReadPreferenceLong("ATWindowX", 0)
				ATWindowY 	= ReadPreferenceLong("ATWindowY", 0)
				ATWindowW 	= ReadPreferenceLong("ATWindowW", 400)
				ATWindowH 	= ReadPreferenceLong("ATWindowH", 400)
				COWindowX 	= ReadPreferenceLong("COWindowX", 0)
				COWindowY 	= ReadPreferenceLong("COWindowY", 0)
				COWindowW 	= ReadPreferenceLong("COWindowW", 420)
				COWindowH 	= ReadPreferenceLong("COWindowH", 480)
				DSWindowX 	= ReadPreferenceLong("DSWindowX", 0)
				DSWindowY 	= ReadPreferenceLong("DSWindowY", 0)
				DSWindowW 	= ReadPreferenceLong("DSWindowW", 400)
				DSWindowH 	= ReadPreferenceLong("DSWindowH", 400)
				FAWindowX 	= ReadPreferenceLong("FAWindowX", 0)
				FAWindowY 	= ReadPreferenceLong("FAWindowY", 0)
				FAWindowW 	= ReadPreferenceLong("FAWindowW", 400)
				FAWindowH 	= ReadPreferenceLong("FAWindowH", 400)
				GOWindowX 	= ReadPreferenceLong("GOWindowX", 0)
				GOWindowY 	= ReadPreferenceLong("GOWindowY", 0)
				GOWindowW 	= ReadPreferenceLong("GOWindowW", 400)
				GOWindowH 	= ReadPreferenceLong("GOWindowH", 400)
				INWindowX 	= ReadPreferenceLong("INWindowX", 0)
				INWindowY 	= ReadPreferenceLong("INWindowY", 0)
				INWindowW 	= ReadPreferenceLong("INWindowW", 400)
				INWindowH 	= ReadPreferenceLong("INWindowH", 400)
				ITWindowX 	= ReadPreferenceLong("ITWindowX", 0)
				ITWindowY 	= ReadPreferenceLong("ITWindowY", 0)
				ITWindowW 	= ReadPreferenceLong("ITWindowW", 400)
				ITWindowH 	= ReadPreferenceLong("ITWindowH", 400)
				MEWindowX 	= ReadPreferenceLong("MEWindowX", 0)
				MEWindowY 	= ReadPreferenceLong("MEWindowY", 0)
				MEWindowW 	= ReadPreferenceLong("MEWindowW", 400)
				MEWindowH 	= ReadPreferenceLong("MEWindowH", 400)
				PAWindowX 	= ReadPreferenceLong("PAWindowX", 0)
				PAWindowY 	= ReadPreferenceLong("PAWindowY", 0)
				PAWindowW 	= ReadPreferenceLong("PAWindowW", 400)
				PAWindowH 	= ReadPreferenceLong("PAWindowH", 400)
				PRWindowX 	= ReadPreferenceLong("PRWindowX", 0)
				PRWindowY 	= ReadPreferenceLong("PRWindowY", 0)
				PRWindowW 	= ReadPreferenceLong("PRWindowW", 400)
				PRWindowH 	= ReadPreferenceLong("PRWindowH", 400)
				SCWindowX 	= ReadPreferenceLong("SCWindowX", 0)
				SCWindowY 	= ReadPreferenceLong("SCWindowY", 0)
				SCWindowW 	= ReadPreferenceLong("SCWindowW", 400)
				SCWindowH 	= ReadPreferenceLong("SCWindowH", 400)
				ZOWindowX 	= ReadPreferenceLong("ZOWindowX", 0)
				ZOWindowY 	= ReadPreferenceLong("ZOWindowY", 0)
				ZOWindowW 	= ReadPreferenceLong("ZOWindowW", 400)
				ZOWindowH 	= ReadPreferenceLong("ZOWindowH", 400)
			
			
		Case 2; Save preferences file
				
			CreatePreferences(HomePath + "PhaseIII.pref", #PB_Preference_NoSpace)
			
				WritePreferenceLong("PMWindowX", PMWindowX)
				WritePreferenceLong("PMWindowY", PMWindowY)
				WritePreferenceString("DefaultProject", "");	CONNECT THIS UP
				WritePreferenceLong("PNWindowX", PNWindowX)
				WritePreferenceLong("PNWindowY", PNWindowY)
				WritePreferenceLong("PNWindowW", PNWindowW)
				WritePreferenceLong("PNWindowH", PNWindowH)
				WritePreferenceLong("ABWindowX", ABWindowX)
				WritePreferenceLong("ABWindowY", ABWindowY)
				WritePreferenceLong("ABWindowW", ABWindowW)
				WritePreferenceLong("ABWindowH", ABWindowH)
				WritePreferenceLong("ACWindowX", ACWindowX)
				WritePreferenceLong("ACWindowY", ACWindowY)
				WritePreferenceLong("ACWindowW", ACWindowW)
				WritePreferenceLong("ACWindowH", ACWindowH)
				WritePreferenceLong("ANWindowX", ANWindowX)
				WritePreferenceLong("ANWindowY", ANWindowY)
				WritePreferenceLong("ANWindowW", ANWindowW)
				WritePreferenceLong("ANWindowH", ANWindowH)
				WritePreferenceLong("ATWindowX", ATWindowX)
				WritePreferenceLong("ATWindowY", ATWindowY)
				WritePreferenceLong("ATWindowW", ATWindowW)
				WritePreferenceLong("ATWindowH", ATWindowH)
				WritePreferenceLong("COWindowX", COWindowX)
				WritePreferenceLong("COWindowY", COWindowY)
				WritePreferenceLong("COWindowW", COWindowW)
				WritePreferenceLong("COWindowH", COWindowH)
				WritePreferenceLong("DSWindowX", DSWindowX)
				WritePreferenceLong("DSWindowY", DSWindowY)
				WritePreferenceLong("DSWindowW", DSWindowW)
				WritePreferenceLong("DSWindowH", DSWindowH)
				WritePreferenceLong("FAWindowX", FAWindowX)
				WritePreferenceLong("FAWindowY", FAWindowY)
				WritePreferenceLong("FAWindowW", FAWindowW)
				WritePreferenceLong("FAWindowH", FAWindowH)
				WritePreferenceLong("GOWindowX", GOWindowX)
				WritePreferenceLong("GOWindowY", GOWindowY)
				WritePreferenceLong("GOWindowW", GOWindowW)
				WritePreferenceLong("GOWindowH", GOWindowH)
				WritePreferenceLong("INWindowX", INWindowX)
				WritePreferenceLong("INWindowY", INWindowY)
				WritePreferenceLong("INWindowW", INWindowW)
				WritePreferenceLong("INWindowH", INWindowH)
				WritePreferenceLong("ITWindowX", ITWindowX)
				WritePreferenceLong("ITWindowY", ITWindowY)
				WritePreferenceLong("ITWindowW", ITWindowW)
				WritePreferenceLong("ITWindowH", ITWindowH)
				WritePreferenceLong("MEWindowX", MEWindowX)
				WritePreferenceLong("MEWindowY", MEWindowY)
				WritePreferenceLong("MEWindowW", MEWindowW)
				WritePreferenceLong("MEWindowH", MEWindowH)
				WritePreferenceLong("PAWindowX", PAWindowX)
				WritePreferenceLong("PAWindowY", PAWindowY)
				WritePreferenceLong("PAWindowW", PAWindowW)
				WritePreferenceLong("PAWindowH", PAWindowH)
				WritePreferenceLong("PRWindowX", PRWindowX)
				WritePreferenceLong("PRWindowY", PRWindowY)
				WritePreferenceLong("PRWindowW", PRWindowW)
				WritePreferenceLong("PRWindowH", PRWindowH)
				WritePreferenceLong("SCWindowX", SCWindowX)
				WritePreferenceLong("SCWindowY", SCWindowY)
				WritePreferenceLong("SCWindowW", SCWindowW)
				WritePreferenceLong("SCWindowH", SCWindowH)
				WritePreferenceLong("ZOWindowX", ZOWindowX)
				WritePreferenceLong("ZOWindowY", ZOWindowY)
				WritePreferenceLong("ZOWindowW", ZOWindowW)
				WritePreferenceLong("ZOWindowH", ZOWindowH)
			
			PrefUpdateNeeded = #False
			
	EndSelect
	
	ClosePreferences()
	
EndProcedure



; IDE Options = PureBasic 5.21 LTS (Windows - x64)
; CursorPosition = 380
; FirstLine = 50
; Folding = AA-
; EnableUser
; Executable = RCCE_Project Manager.exe
; CurrentDirectory = C:\Program Files\PureBasic\
; EnableBuildCount = 0