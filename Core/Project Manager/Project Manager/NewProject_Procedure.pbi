Procedure CreateProject(Name$)
	
	Local FileCounter = 0
	Local QTY
	Structure Files
		name.s
		oldpath.s
		newpath.s
		size.l
	EndStructure
	
;{	Build Server
	
	Restore RequiredDirectoriesServer
	Read.i QTY
		CreateDirectory(Name$)
	For i = 1 to QTY
		Read.s Path$
		CreateDirectory(Name$ + "\" + Path$)
	Next i
	Restore RequiredFilesServer
	Read.i QTY
		CreateDirectory(Name$)
	For i = 1 to QTY
		Read.s Path$
		CreateDirectory(Name$ + "\" + Path$)
	Next i
	
;}
	
;{	Build Client

	Restore RequiredDirectoriesClient
	Read.i QTY
	For i = 1 to QTY
		Read.s Path$
		CreateDirectory(Name$ + "\" + Path$)
	Next i
	
	Restore RequiredFilesClient
	Read.i QTY
	For i = 1 to QTY
		Read.s Path$
		CopyFile(Name$ + "\" + Path$)
	Next i
		
;}
	
	
	
EndProcedure


DataSection
	RequiredDirectoriesServer:
	data.i 7
	data.s "Server"
	data.s "Server\DataFiles"
	data.s "Server\Scripts"
	data.s "Server\Scripts\ScriptData"
	data.s "Server\Accounts"
	data.s "Server\Logs"
	data.s "Server\ServerAreas"
	
	
	RequiredDirectoriesClient:
	data.i 28
	
	data.s "Client"
	data.s "Client\Logs"
	data.s "Client\Data"
	data.s "Client\Data\UI"
	data.s "Client\Data\Accounts"
	data.s "Client\Data\FX"
	data.s "Client\Data\Media"
	data.s "Client\Data\Media\Icons"
	data.s "Client\Data\Media\Icons\SpellIcons"
	data.s "Client\Data\Media\Icons\AbilityIcons"
	data.s "Client\Data\Media\Icons\ItemIcons"
	data.s "Client\Data\Media\Models"
	data.s "Client\Data\Media\Models\Actors"
	data.s "Client\Data\Media\Models\Structures"
	data.s "Client\Data\Media\Models\Scenery"
	data.s "Client\Data\Media\Models\Items"
	data.s "Client\Data\Media\TexturePool\Actors"
	data.s "Client\Data\Media\TexturePool\Structures"
	data.s "Client\Data\Media\TexturePool\Scenery"
	data.s "Client\Data\Media\TexturePool\Items"
	data.s "Client\Data\Media\Audio"
	data.s "Client\Data\Media\Audio\Voices"
	data.s "Client\Data\Media\Audio\Voices\Female"
	data.s "Client\Data\Media\Audio\Voices\Male"
	data.s "Client\Data\Media\Audio\Voices\Other"
	data.s "Client\Data\Media\Audio\Environment"
	data.s "Client\Data\Media\Audio\Sounds"
	data.s "Client\Data\Media\Audio\Music"
	
	
	
	
	RequiredFilesServer:
	
	data.i 1
	
	Data.s "Server.exe"
	
	
	
	
	RequiredFilesClient:
	
	data.i 2
	
	Data.s "Client.exe","Patcher.exe"
	
	
	
EndDataSection
; IDE Options = PureBasic 5.21 LTS (Windows - x64)
; CursorPosition = 10
; Folding = -
; EnableUser
; CurrentDirectory = C:\Program Files\PureBasic\
; EnableBuildCount = 0