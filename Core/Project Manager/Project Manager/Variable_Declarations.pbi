;====================================================================================================================
;		Project Manager - Phase III Engine Main utility launcher/Project Management tool [Include File]
;		This work is copyright(c)2014 By:Robert S. Quackenbush
;		It is colectively part of the Phase III Engine
;		Phase III Engine Copyright(c)2014 by Cory R. Dean and Robert S. Quackenbush
;		All Rights Reserved
;=====================================================================================================================


;Global ProjectPath$ = ProjPath
Global AppName$ = "Phase III"
Global version$ = "0.00.1"
Global HomePath.s = GetCurrentDirectory()
Global DebugFile$ = "Project_Manager.log"

;{	Global Veriables (Preferences)


Global PrefUpdateNeeded = #False
Global PMWindowX;	PM* Project Manager Window
Global PMWindowY
Global PNWindowX;	PN* Project Notes Window
Global PNWindowY
Global PNWindowW
Global PNWindowH
Global ABWindowX;	AB* Abilities Window
Global ABWindowY
Global ABWindowW
Global ABWindowH
Global ACWindowX;	AC* Actors Window
Global ACWindowY
Global ACWindowW
Global ACWindowH
Global ANWindowX;	AN* Animations Window
Global ANWindowY
Global ANWindowW
Global ANWindowH
Global ATWindowX;	AT* Attributes Window
Global ATWindowY
Global ATWindowW
Global ATWindowH
Global COWindowX;	CO* Combat Window
Global COWindowY
Global COWindowW
Global COWindowH
Global DSWindowX;	DS* DaySeason Window
Global DSWindowY
Global DSWindowW
Global DSWindowH
Global FAWindowX;	FA* Factions Window
Global FAWindowY
Global FAWindowW
Global FAWindowH
Global GOWindowX;	GO* General Options Window
Global GOWindowY
Global GOWindowW
Global GOWindowH
Global INWindowX;	IN* Interface Window
Global INWindowY
Global INWindowW
Global INWindowH
Global ITWindowX;	IT* Items Window
Global ITWindowY
Global ITWindowW
Global ITWindowH
Global MEWindowX;	ME* Media Window
Global MEWindowY
Global MEWindowW
Global MEWindowH
Global PAWindowX;	PA* Particles Window
Global PAWindowY
Global PAWindowW
Global PAWindowH
Global PRWindowX;	PR* Projectiles Window
Global PRWindowY
Global PRWindowW
Global PRWindowH
Global SCWindowX;	SC* Scripting Window
Global SCWindowY
Global SCWindowW
Global SCWindowH;
Global ZOWindowX;	ZO* Zone Window
Global ZOWindowY
Global ZOWindowW
Global ZOWindowH

;}

;HomePath = "E:\Old Computer\PureBasic\_backup\"
;HomePath = "C:\Program Files (x86)\SolStar Games\Realm Crafter 1\"
;	Special Procedures

;{ 	Remove quotes from a string

Procedure.s DequoteString(Instring.s)
	
	Instring = Trim(Instring, " ")
	Instring = Trim(Instring, Chr(9))
	If Left(Instring, 1) = Chr(34)
		Instring = Right(Instring, Len(Instring) - 1)
	EndIf
	If Right(Instring, 1) = Chr(34)
		Instring = Left(Instring, Len(Instring) - 1)
	EndIf
	
	ProcedureReturn(Instring)
	
EndProcedure
;}

;{	Remove preceeding and trailing white space from a string

Procedure.s Dewhitespace(Instring.s)
	
	Instring = Trim(Instring, " ")
	Instring = Trim(Instring, Chr(9))
	
	ProcedureReturn(Instring)
	
EndProcedure
;}

;{	Remove Quotes and Whitespaces from a string

Procedure.s StripString(Instring.s)
	
	Instring.s = DequoteString(Instring.s)
	Instring.s = Dewhitespace(Instring.s)
	ProcedureReturn(Instring)
	
EndProcedure
;}


;{ Main Window button Tooltip variables

Global BTN_Launch_MediaManager_Tooltip
Global BTN_Launch_ScriptManager_Tooltip
Global BTN_Launch_FactionsManager_Tooltip
Global BTN_Launch_EmitterManager_Tooltip
Global BTN_Launch_ProjectilesManager_Tooltip
Global BTN_Launch_ItemsManager_Tooltip
Global BTN_Launch_CombatManager_Tooltip
Global BTN_Launch_AttributesManager_Tooltip
Global BTN_Launch_AbilitiesManager_Tooltip
Global BTN_Launch_ActorsManager_Tooltip
Global BTN_Launch_AnimationsManager_Tooltip
Global BTN_Launch_DaysSeasonsManager_Tooltip
Global BTN_Launch_ZonesManager_Tooltip
Global BTN_Launch_InterfaceManager_Tooltip
Global BTN_Launch_GeneralOptionsManager_Tooltip
Global BTN_Launch_TeamChat_Tooltip
Global Dim BTN_Plugin_Tooltips.s(18)

Restore TipsData
For i = 1 to 18
	Read.s BTN_Plugin_Tooltips(i)
Next i


;}


;{ Image Data

Global Dim BTN_ImageID(18)
BTN_ImageID(1)	= CatchImage(#PB_Any, ?BTN_Image_Media)
BTN_ImageID(2) 	= CatchImage(#PB_Any, ?BTN_Image_Scripting)
BTN_ImageID(3) 	= CatchImage(#PB_Any, ?BTN_Image_Faction)
BTN_ImageID(4) 	= CatchImage(#PB_Any, ?BTN_Image_Particles)
BTN_ImageID(5) 	= CatchImage(#PB_Any, ?BTN_Image_Projectiles)
BTN_ImageID(6)	= CatchImage(#PB_Any, ?BTN_Image_Items)
BTN_ImageID(7) 	= CatchImage(#PB_Any, ?BTN_Image_Combat)
BTN_ImageID(8) 	= CatchImage(#PB_Any, ?BTN_Image_Attributes)
BTN_ImageID(9) 	= CatchImage(#PB_Any, ?BTN_Image_Abilities)
BTN_ImageID(10) 	= CatchImage(#PB_Any, ?BTN_Image_Actors)
BTN_ImageID(11) 	= CatchImage(#PB_Any, ?BTN_Image_Animations)
BTN_ImageID(12) 	= CatchImage(#PB_Any, ?BTN_Image_DaySeason)
BTN_ImageID(13) 	= CatchImage(#PB_Any, ?BTN_Image_Zone)
BTN_ImageID(14) 	= CatchImage(#PB_Any, ?BTN_Image_Interface)
BTN_ImageID(15)	= CatchImage(#PB_Any, ?BTN_Image_General)
BTN_ImageID(16)	= CatchImage(#PB_Any, ?BTN_Image_Expansion)
BTN_ImageID(17)	= BTN_ImageID(16)
BTN_ImageID(18)	= BTN_ImageID(16)
Global BannerImage 	= CatchImage(#PB_Any, ?BanImage)
Global MissingPlugImage = CatchImage(#PB_Any, ?PlugImageMissing)


;}


;{ Plugin Variables

Global PluginsPath.s = "Plugins\";	Path to the plugins directory

Structure Plugs
	name.s
	desc.s
	entry.s
	type.s
	dir.s
	iconID.i
EndStructure

Global NewList Plugins.Plugs()

;}

Global ProjSize.q = 0
Global FileCount.l = 0
Global TotalProjects.a = 0
Global Dim ProcRunning(18)

; 	Log file
Procedure.s LogFile(Instring.s, PutDate, AddSpace)
	
	If FileSize(DebugFile$) < 1
		CreateFile(#DbugFile, DebugFile$)
	EndIf
	OpenFile(#DbugFile, DebugFile$, #PB_File_Append|#PB_File_SharedRead|#PB_File_SharedWrite)
	If FileSize(DebugFile$) < 1
		WriteStringN(#DbugFile, "=================")
		WriteStringN(#DbugFile, " Log file started on: " + FormatDate("%mm/%dd/%yyyy", Date()))
		WriteStringN(#DbugFile, "=================")
	EndIf
	Instring = "@" + Chr(9) + Instring
	If PutDate = 1
		If AddSpace = 1
			WriteStringN(#DbugFile, FormatDate("%mm/%dd/%yyyy", Date()) + ":] " + Instring)
			WriteStringN(#DbugFile, "")
		Else
			WriteStringN(#DbugFile, FormatDate("%mm/%dd/%yyyy", Date()) + ":] " + Instring)
		EndIf
	Else
		If AddSpace = 1
			WriteStringN(#DbugFile, FormatDate("%hh:%ii:%ss", Date()) + ":] " + Instring)
			WriteStringN(#DbugFile, "")
		Else
			WriteStringN(#DbugFile, FormatDate("%hh:%ii:%ss", Date()) + ":] " + Instring)
		EndIf
	EndIf
	CloseFile(#DbugFile)
	
EndProcedure



;Load any plugins that are installed.
Procedure LoadPlugins();			WORKING 100%		??????????
	LogFile("===[Loading Plugins]==============================", 1, 1)
	If ExamineDirectory(#Directory, HomePath + PluginsPath, "*.Plugin")
		While NextDirectoryEntry(#Directory)
			If DirectoryEntryType(#Directory) = #PB_DirectoryEntry_Directory
				AddElement(Plugins())
				Plugins()\dir = DirectoryEntryName(#Directory)
			EndIf
		Wend
		FinishDirectory(#Directory)
	EndIf
	ForEach Plugins()
		Count = 0
		If FileSize(HomePath + PluginsPath + Plugins()\dir + "\plugin.data\Plugin.dat") > 0
			FH = OpenFile(#PB_Any, HomePath + PluginsPath + Plugins()\dir + "\plugin.data\Plugin.dat")
			Repeat
				Repeat
					Junk$ = Trim(ReadString(FH))
				Until Left(Junk$, 1) <> ";"
				Select Trim(LCase(Left(Junk$, 5)))
					
					Case "name", "name=", "name ", "name	"
					
						pntr = FindString(Junk$, "=", 1)
						Junk$ = Right(Junk$, Len(Junk$) - pntr)
						Plugins()\name = StripString(Junk$)
						Count +1
					
					Case "desc", "desc=", "desc ", "desc	"
					
						pntr = FindString(Junk$, "=", 1)
						Junk$ = Right(Junk$, Len(Junk$) - pntr)
						Plugins()\desc = StripString(Junk$)
						Count +2
					
					Case "entry"
					
						pntr = FindString(Junk$, "=", 1)
						Junk$ = Right(Junk$, Len(Junk$) - pntr)
						Plugins()\entry = StripString(Junk$)
						Count +4
					
					Case "type", "type=", "type ", "type	"
					
						pntr = FindString(Junk$, "=", 1)
						Junk$ = Right(Junk$, Len(Junk$) - pntr)
						Plugins()\type = StripString(Junk$)
						If Val(Plugins()\type) > 0
							BTN_Plugin_Tooltips.s(Val(Plugins()\type)) = Plugins()\desc
						EndIf
						Count +8

				EndSelect

			Until EoF(FH) = 1
			If Count <> 15
				
				LogFile("Failed to load Plugin : " + HomePath + PluginsPath + Plugins()\dir, 0, 1)
				
				Select Count
						
					Case 14
						
						LogFile("Missing [name = plugin name] parameter in Plugin.dat file", 0, 1)
						
					Case 13
						
						LogFile("Missing [desc = plugin description] parameter in Plugin.dat file", 0, 1)
						
					Case 12
						
						LogFile("Missing [name = plugin name] parameter in Plugin.dat file", 0, 1)
						LogFile("Missing [desc = plugin description] parameter in Plugin.dat file", 0, 1)
						
					Case 11
						
						LogFile("Missing [entry = plugin entry] parameter in Plugin.dat file", 0, 1)
						
					Case 10
						
						LogFile("Missing [name = plugin name] parameter in Plugin.dat file", 0, 1)
						LogFile("Missing [entry = plugin entry] parameter in Plugin.dat file", 0, 1)
						
					Case 9
						
						LogFile("Missing [desc = plugin description] parameter in Plugin.dat file", 0, 1)
						LogFile("Missing [entry = plugin entry] parameter in Plugin.dat file", 0, 1)
						
					Case 8
						
						LogFile("Missing [name = plugin name] parameter in Plugin.dat file", 0, 1)
						LogFile("Missing [desc = plugin description] parameter in Plugin.dat file", 0, 1)
						LogFile("Missing [entry = plugin entry] parameter in Plugin.dat file", 0, 1)
						
					Case 7
						
						LogFile("Missing [type = plugin type #] parameter in Plugin.dat file", 0, 1)
						
					Case 6
						
						LogFile("Missing [name = plugin name] parameter in Plugin.dat file", 0, 1)
						LogFile("Missing [type = plugin type #] parameter in Plugin.dat file", 0, 1)
						
					Case 5
						
						LogFile("Missing [desc = plugin description] parameter in Plugin.dat file", 0, 1)
						LogFile("Missing [type = plugin type #] parameter in Plugin.dat file", 0, 1)
						
					Case 4
						
						LogFile("Missing [name = plugin name] parameter in Plugin.dat file", 0, 1)
						LogFile("Missing [desc = plugin description] parameter in Plugin.dat file", 0, 1)
						LogFile("Missing [type = plugin type #] parameter in Plugin.dat file", 0, 1)
						
					Case 3
						
						LogFile("Missing [entry = plugin entry] parameter in Plugin.dat file", 0, 1)
						LogFile("Missing [type = plugin type #] parameter in Plugin.dat file", 0, 1)
						
					Case 2
						
						LogFile("Missing [name = plugin name] parameter in Plugin.dat file", 0, 1)
						LogFile("Missing [entry = plugin entry] parameter in Plugin.dat file", 0, 1)
						LogFile("Missing [type = plugin type #] parameter in Plugin.dat file", 0, 1)
						
					Case 1
						
						LogFile("Missing [desc = plugin description] parameter in Plugin.dat file", 0, 1)
						LogFile("Missing [entry = plugin entry] parameter in Plugin.dat file", 0, 1)
						LogFile("Missing [type = plugin type #] parameter in Plugin.dat file", 0, 1)
						
				EndSelect
				
			Else
				If FileSize(HomePath + PluginsPath + Plugins()\dir + "\Plugin.Data" + "\Icon.png") > 0
					Plugins()\iconID = LoadImage(#PB_Any, HomePath + PluginsPath + Plugins()\dir + "\Plugin.Data" + "\Icon.png")
					BTN_ImageID(Val(Plugins()\type)) = Plugins()\iconID
				ElseIf FileSize(HomePath + PluginsPath + Plugins()\dir + "\Plugin.Data" + "\Icon.jpg") > 0
					Plugins()\iconID = LoadImage(#PB_Any, HomePath + PluginsPath + Plugins()\dir + "\Plugin.Data" + "\Icon.jpg")
					BTN_ImageID(Val(Plugins()\type)) = Plugins()\iconID
				Else
					LogFile("Plugin [" + Plugins()\dir + "] Missing file [Icon.  (png or jpg) format]", 0, 1)
					BTN_ImageID(Val(Plugins()\type)) = MissingPlugImage
					
				EndIf	
				LogFile("Loaded plugin [" + Plugins()\dir + "] successfully.", 0, 1)
			EndIf
		EndIf
	Next
	LogFile("===[Plugins Loaded]==============================", 0, 1)
	
EndProcedure


Global Dim Project$(255)
Global Dim ProjectVersion$(255)
Global Dim ProjectTotalFiles(255)
Global Dim ProjectSize(255)
Global Dim ClientBuildCount(255)
Global Dim ServerBuildCount(255)

Define.l Event, EventWindow, EventGadget, EventType, EventMenu




; IDE Options = PureBasic 5.21 LTS (Windows - x64)
; CursorPosition = 17
; FirstLine = 15
; Folding = VF-
; EnableUser
; CurrentDirectory = C:\Program Files\PureBasic\
; EnableBuildCount = 0