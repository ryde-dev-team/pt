;====================================================================================================================
;		Project Manager - Phase III Engine Main utility launcher/Project Management tool [Include File]
;		This work is copyright(c)2014 By:Robert S. Quackenbush.
;		It is colectively part of the Phase III Engine
;		Phase III Engine Copyright(c)2014 by Cory R. Dean and Robert S. Quackenbush. All Rights Reserved
;
;=====================================================================================================================






;{- Enumerations / DataSections

;{ Windows
Enumeration
	
	#MainWindow
	#NewProject_Window
	#NotesWindow_Main
	#NoteEditorWidth = 399
	#NoteEditorHeight = 550
	#MovieWindow
	#Movie
	
EndEnumeration
;}

;{ Menu bars
Enumeration
	
	#Menu_MainWindow
	
EndEnumeration

;}

;{ Menu/Toolbar items

Enumeration
	
EndEnumeration

;}

;{ Gadgets
Enumeration
	
	;	BUTTON Gadgets
	
	#BTN_1_Launch		:	#BTN_2_Launch		:	#BTN_3_Launch		:	#BTN_4_Launch
	#BTN_5_Launch		:	#BTN_6_Launch		:	#BTN_7_Launch		:	#BTN_8_Launch
	#BTN_9_Launch		:	#BTN_10_Launch	:	#BTN_11_Launch	:	#BTN_12_Launch
	#BTN_13_Launch	:	#BTN_14_Launch	:	#BTN_15_Launch	:	#BTN_16_Launch
	#BTN_17_Launch	:	#BTN_18_Launch	:	#BTN_BuildServer	:	#BTN_BuildClient
	#BTN_NewProject	:	#BTN_DeleteProject	:	#BTN_LaunchTestServer:	#BTN_LaunchTestClient
	#BTN_Launch_PNotes	:	#BTN_CleanProject	:	#Button_47		:	#BTN_Create_New_Project
	#BTN_Create_New_Project_Cancel:	#BTN_SaveNotes:	#BTN_CancelNotes

	
	;	ICONVIEW Gadgets
	
	#TeamProjectsView
	#LocalProjectsView
	
	;	CONTAINER Gadgets
	
	#Container_0
	
	;	PANEL Gadgets
	
	#Projects_Panel
	
	;	Input Box Gadgets
	
	#IPB_New_Project_Name
	
	;	Image Gadgets
	
	#BannerImage_IMG
	
	;	Other Gadgets
	
	#NotesEditor
	#DbugFile
	
EndEnumeration

;}

;{ Item Constants

Enumeration
	
	#Directory
	
EndEnumeration

;}

; IDE Options = PureBasic 5.21 LTS (Windows - x64)
; CursorPosition = 86
; FirstLine = 43
; Folding = z-
; EnableUser
; CurrentDirectory = C:\Program Files\PureBasic\
; EnableBuildCount = 0