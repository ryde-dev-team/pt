;====================================================================================================================
;		ProjectilesManager - Phase III Engine utility
;		This work is copyright(c)2014 By:Robert S. Quackenbush
;		It is colectively part of the Phase III Engine
;		Phase III Engine Copyright(c)2014 by Cory R. Dean and Robert S. Quackenbush
;		All Rights Reserved
;=====================================================================================================================





;{- Enumerations / DataSections
;{ Windows
Enumeration
	
	#Window_ProjectilesManager_Main
	#Window_Preview_Main
	#Window_CreateNewProjectiles
	
EndEnumeration
;}

;{ Gadgets

Enumeration
	
	#LIB_Projectiles_List
	#BTN_AddProjectile
	#BTN_RemoveProjectile
	#BTN_EditProjectile
	#BTN_SAVE
	#BTN_QUIT
	#SPN_ProjectileSpeed
	#SPN_PhysWeight
	#SPN_PhysFric
	#CBX_Has_Physics
	#CMB_ProjectileMeshTexture
	#CMB_Emitter1Select
	#CMB_Emitter2Select
	#CMB_ProjectileMESH
	#CMB_ProjectileName
	#CBX_TrackTarget
	#CMB_ProjectileScript
	#OPT_ProjectileState_SOLID
	#OPT_ProjectileState_LIQUID
	#OPT_ProjectileState_GAS
	#OPT_ProjectileState_MAGIC
	#IPB_DamageInformation_MINDMG
	#IPB_DamageInformation_MAXDMG
	#CMB_DamageInformation_TYPE
	#BTN_SAVEProjectiles
	#BTN_DELETEProjectiles
	#BTN_QUITProjectiles
	
EndEnumeration

;}

;}


;{	Tooltip Global Variables

Global LIB_Projectiles_List_Tooltip
Global BTN_AddProjectile_Tooltip
Global BTN_RemoveProjectile_Tooltip
Global BTN_EditProjectile_Tooltip
Global Button_SAVE_Tooltip
Global Button_QUIT_Tooltip
Global SPN_ProjectileSpeed_Tooltip
Global SPN_PhysWeight_Tooltip
Global SPN_PhysFric_Tooltip
Global CBX_Has_Physics_Tooltip
Global CMB_ProjectileMeshTexture_Tooltip
Global CMB_ProjectileMeshTexture_E_Tooltip
Global CMB_Emitter1Select_Tooltip
Global CMB_Emitter1Select_E_Tooltip
Global CMB_Emitter2Select_Tooltip
Global CMB_Emitter2Select_E_Tooltip
Global CMB_ProjectileMESH_Tooltip
Global CMB_ProjectileMESH_E_Tooltip
Global CMB_ProjectileName_Tooltip
Global CMB_ProjectileName_E_Tooltip
Global CBX_TrackTarget_Tooltip
Global CMB_ProjectileScript_Tooltip
Global CMB_ProjectileScript_E_Tooltip
Global OPT_ProjectileState_SOLID_Tooltip
Global OPT_ProjectileState_LIQUID_Tooltip
Global OPT_ProjectileState_GAS_Tooltip
Global OPT_ProjectileState_MAGIC_Tooltip
Global IPB_DamageInformation_MINDMG_Tooltip
Global IPB_DamageInformation_MAXDMG_Tooltip
Global CMB_DamageInformation_TYPE_Tooltip
Global BTN_SAVEProjectiles_Tooltip
Global BTN_DELETEProjectiles_Tooltip
Global BTN_QUITProjectiles_Tooltip

;}

; Create tooltip
Procedure GadgetBalloonToolTip(WindowNumber.l, GadgetNumber.l, Text.s)
	
	Protected Tooltip.l, Balloon.TOOLINFO
	Tooltip = CreateWindowEx_(0, "ToolTips_Class32", "", #WS_POPUP | #TTS_NOPREFIX | #TTS_BALLOON, 0, 0, 0, 0, WindowID(WindowNumber), 0, GetModuleHandle_(0), 0)
	SendMessage_(Tooltip, #TTM_SETTIPTEXTCOLOR, RGB(255,0,0), 0)
	SendMessage_(Tooltip, #TTM_SETTIPBKCOLOR, RGB(255,255,0), 0)
	;	SendMessage_(Tooltip, #TTM_SETTIPTEXTCOLOR, GetSysColor_(#COLOR_INFOTEXT), 0)
	;	SendMessage_(Tooltip, #TTM_SETTIPBKCOLOR, GetSysColor_(#COLOR_INFOBK), 0)
	SendMessage_(Tooltip, #TTM_SETMAXTIPWIDTH, 0, 200)
	SendMessage_(Tooltip, #TTM_SETDELAYTIME, 20000, 0)
	Balloon\cbSize = SizeOf(TOOLINFO)
	Balloon\uFlags = #TTF_IDISHWND | #TTF_SUBCLASS
	If IsGadget(GadgetNumber)
		Balloon\hwnd = GadgetID(GadgetNumber)
		Balloon\uId = GadgetID(GadgetNumber)
	Else
		Balloon\hwnd = GadgetNumber
		Balloon\uId = GadgetNumber
	Endif
	Balloon\lpszText = @Text
	SendMessage_(Tooltip, #TTM_ADDTOOL, 0, @Balloon)
	ProcedureReturn Tooltip
	
EndProcedure

; Change tooltip text
Procedure ChangeGadgetBalloonToolTip(Tooltip.l, GadgetNumber.l, Text.s)
	
	Protected Balloon.TOOLINFO
	Balloon\cbSize = SizeOf(TOOLINFO)
	Balloon\uFlags = #TTF_IDISHWND | #TTF_SUBCLASS
	If IsGadget(GadgetNumber)
		Balloon\hwnd = GadgetID(GadgetNumber)
		Balloon\uId = GadgetID(GadgetNumber)
	Else
		Balloon\hwnd = GadgetNumber
		Balloon\uId = GadgetNumber
	Endif
	Balloon\lpszText = @Text
	SendMessage_(Tooltip, #TTM_ADDTOOL, 0, @Balloon)
	
EndProcedure

; Free tooltip
Procedure FreeBalloonTooltip(Tooltip.l)
	
	DestroyWindow_(Tooltip.l)
	
EndProcedure 
;

;{	Structured Variables

;	Object to hold Damage Types
Structure DMGTypes
	name.s
	type.s
	typeID.w
EndStructure
Global NewCombat.w = 1
Global NewList DMGType.DMGTypes()

Structure Projectiles
	
	ProID.a
	ProName.s[20]
	ProType.s[20]
	ProjState.a
	MeshID.l
	TexID.l
	Emit1ID.l
	Emit2ID.l
	Speed.a
	Track.a
	PhysWeight.f
	PhysFriction.f
	Script.s[30]
	DMGType.DMGTypes
	
EndStructure

;}

XIncludeFile "Phase3\Manager_GeneralInclude\General_Includes.pbi"
;DebugFile$ = "C:\Program Files (x86)\SolStar Games\Realm Crafter 1\Project_Manager.log"

LogFile("=====[Projectiles Manager Started]", 1, 1)


;{	Global Variables

Global NewList Projectile.Projectiles()
Global CommandLineData$ = ProgramParameter()

Global TotalProjectiles = 0
Global ValidateCount = 0

x = CountProgramParameters()

For i = 0 to x - 1
	Junk$ = ProgramParameter(i)
	CommandLineData$ = CommandLineData$ + Junk$
Next i
If GetEnvironmentVariable("PhaseIII") <> version$
	RunProgram(Chr(34) + "Phase III-PM.exe" + Chr(34))
	End
EndIf
Global WinX = Val(GetEnvironmentVariable("Plug7X"))
Global WinY = Val(GetEnvironmentVariable("Plug7Y"))
Global WinW = Val(GetEnvironmentVariable("Plug7W"))
Global WinH = Val(GetEnvironmentVariable("Plug7H"))
Junk$ = CommandLineData$

;Junk$ = "C:\Program Files (x86)\SolStar Games\Realm Crafter 1\Projects\Test 2"
;CommandLineData$ = "C:\Program Files (x86)\SolStar Games\Realm Crafter 1\Projects\Test 2"

If Right(Junk$,1) = "\"
	Junk$ = Left(Junk$,Len(Junk$) - 1)
EndIf
pntr2 = 1:pntr = 0

Repeat
	
	pntr = FindString(Junk$, "\", pntr2)
	If pntr > 0
		pntr2 = pntr + 1
	EndIf
	
Until pntr < 1

Global ProjectName$ = Mid(Junk$, pntr2, Len(Junk$))

Procedure OpenWindow_AddProjectiles()
	
	
	
	
EndProcedure


;{	Procedure to load or build the combat.dat data file.
Procedure GetDMGTypes()
	
	LogFile("Loading Combat.dat file",0,0)
	If FileSize(CommandLineData$ + "\Combat.dat") > 0
		FH = OpenFile(#PB_Any, CommandLineData$ + "\Combat.dat", #PB_File_SharedRead)
			ScriptedCombat 	= ReadAsciiCharacter(FH)
			BaseAttackDelay	= ReadUnicodeCharacter(FH)
			ScriptedSpells 	= ReadAsciiCharacter(FH)
			ScriptedLoot 		= ReadAsciiCharacter(FH)
			EquipmentDamaged 	= ReadAsciiCharacter(FH)
			NewCombat		= ReadWord(FH)
			ValidateCount = 1
		While Not EoF(FH)
			AddElement(DMGType())
			DMGType()\name = ReadString(FH, #PB_Ascii, 20)
			DMGType()\type = UCase(ReadString(FH, #PB_Ascii, 20))
			DMGType()\typeID = ReadWord(FH)
			ValidateCount +1
		Wend
		LogFile("Loaded [" + Str(ValidateCount) + "] Damage Types",0,0)
		CloseFile(FH)
		If NewCombat <> ValidateCount
			LogFile("##### - ERROR Loading Combat.dat file",0,0)
			MessageRequester("ERROR", "Incorrect values in Combat.dat file.", #PB_MessageRequester_Ok)
		EndIf
	Else
		LogFile("Creating file [Combat.dat]",0,0)
		FH = CreateFile(#PB_Any, CommandLineData$ + "\Combat.dat")
			WriteAsciiCharacter(FH, ScriptedCombat)
			WriteUnicodeCharacter(FH, BaseAttackDelay)
			WriteAsciiCharacter(FH, ScriptedSpells)
			WriteAsciiCharacter(FH, ScriptedLoot)
			WriteAsciiCharacter(FH, EquipmentDamaged)
			WriteWord(FH, NewCombat)
		CloseFile(FH)
	EndIf
	
EndProcedure
;}


Procedure ProcessProjectiles(Which.i)
	
	Select Which
			
		Case 0;	Create the projectiles.dat file
			If FileSize(CommandLineData$ + "\Projectiles.dat") = 0
				FH = CreateFile(#PB_Any, CommandLineData$ + "\Projectiles.dat")
				WriteWord(FH, 0)
				CloseFile(FH)
			EndIf
			
		Case 1;	Load the projectiles.dat file
			
			If FileSize(CommandLineData$ + "\Projectiles.dat") > 0
				FH = OpenFile(#PB_Any, CommandLineData$ + "\Projectiles.dat", #PB_File_SharedRead)
				TotalProjectiles 	= ReadWord(FH)
				ValidateCount = 0
				While Not EoF(FH)
					AddElement(Projectile())
					Projectile()\ProName		= ReadString(FH, Ascii, 20)
					Projectile()\ProType		= ReadString(FH, Ascii, 20)
					Projectile()\ProjState	= ReadAsciiCharacter(FH)
					Projectile()\MeshID		= ReadLong(FH)
					Projectile()\TexID		= ReadLong(FH)
					Projectile()\Emit1ID		= ReadLong(FH)
					Projectile()\Emit2ID		= ReadLong(FH)
					Projectile()\Speed		= ReadAsciiCharacter(FH)
					Projectile()\Track		= ReadAsciiCharacter(FH)
					Projectile()\PhysWeight	= ReadFloat(FH)
					Projectile()\PhysFriction	= ReadFloat(FH)
					Projectile()\Script		= ReadString(FH, Ascii, 30)
					ValidateCount +1
				Wend
				CloseFile(FH)
			EndIf
			
			If TotalProjectiles <> ValidateCount
				MessageRequester("ERROR", "Projectiles.dat file is corrupted.", #PB_MessageRequester_Ok)
			EndIf
			
		Case 2;	Save the projectiles.dat file
			
			If FileSize(CommandLineData$ + "\Projectiles.dat") > 0
				FH = OpenFile(#PB_Any, CommandLineData$ + "\Projectiles.dat", #PB_File_SharedRead)
				WriteWord(FH, TotalProjectiles)
				ForEach Projectile()
					WriteString(FH, Trim(Projectile()\ProName) + Space(20 - Len(Trim(Projectile()\ProName))))
					WriteString(FH, Trim(Projectile()\ProType) + Space(20 - Len(Trim(Projectile()\ProType))))
					WriteLong(FH, Projectile()\MeshID)
					WriteLong(FH, Projectile()\TexID)
					WriteLong(FH, Projectile()\Emit1ID)
					WriteLong(FH, Projectile()\Emit2ID)
					WriteAsciiCharacter(FH, Projectile()\Speed)
					WriteAsciiCharacter(FH, Projectile()\Track)
					WriteFloat(FH, Projectile()\PhysWeight)
					WriteFloat(FH, Projectile()\PhysFriction)
					WriteString(FH, Trim(Projectile()\Script) + Space(30 - Len(Trim(Projectile()\Script))))
				Next
				CloseFile(FH)
			EndIf
			
			
			
	EndSelect
	
	
	
EndProcedure

Define.l Event, EventWindow, EventGadget, EventType, EventMenu
;}

Procedure OpenWindow_Window_CreateNewProjectiles()
	
	If OpenWindow(#Window_CreateNewProjectiles, 461, 95, 585, 400, "Create New Projectile [" + ProjectName$ + "]", #PB_Window_SystemMenu|#PB_Window_MinimizeGadget|#PB_Window_TitleBar)
		ContainerGadget(#PB_Any, 0, 0, 475, 402, #PB_Container_Raised)
			FrameGadget(#PB_Any, 0, 0, 470, 397, "Projectile Properties")
			ContainerGadget(#PB_Any, 5, 20, 203, 271)
				ComboBoxGadget(#CMB_ProjectileMeshTexture, 3, 231, 190, 20, #PB_ComboBox_Editable)
				CMB_ProjectileMeshTexture_Tooltip = GadgetBalloonToolTip(#Window_CreateNewProjectiles, #CMB_ProjectileMeshTexture, "Select the texture used for the projectile mesh")
				CMB_ProjectileMeshTexture_E_Tooltip = GadgetBalloonToolTip(#Window_CreateNewProjectiles, FindWindowEx_(GadgetID(#CMB_ProjectileMeshTexture), 0, "Edit", 0), "Select the texture used for the projectile mesh")
				TextGadget(#PB_Any, 3, 212, 190, 16, "Mesh Texture", #PB_Text_Border|#PB_Text_Center)
				ComboBoxGadget(#CMB_Emitter1Select, 3, 37, 190, 20, #PB_ComboBox_Editable)
				CMB_Emitter1Select_Tooltip = GadgetBalloonToolTip(#Window_CreateNewProjectiles, #CMB_Emitter1Select, "Select Emitter #1")
				CMB_Emitter1Select_E_Tooltip = GadgetBalloonToolTip(#Window_CreateNewProjectiles, FindWindowEx_(GadgetID(#CMB_Emitter1Select), 0, "Edit", 0), "Select Emitter #1")
				ComboBoxGadget(#CMB_Emitter2Select, 3, 86, 190, 20, #PB_ComboBox_Editable)
				CMB_Emitter2Select_Tooltip = GadgetBalloonToolTip(#Window_CreateNewProjectiles, #CMB_Emitter2Select, "Select Emitter #2")
				CMB_Emitter2Select_E_Tooltip = GadgetBalloonToolTip(#Window_CreateNewProjectiles, FindWindowEx_(GadgetID(#CMB_Emitter2Select), 0, "Edit", 0), "Select Emitter #2")
				ComboBoxGadget(#CMB_ProjectileMESH, 3, 180, 190, 20, #PB_ComboBox_Editable)
				CMB_ProjectileMESH_Tooltip = GadgetBalloonToolTip(#Window_CreateNewProjectiles, #CMB_ProjectileMESH, "Select the mesh used by the projectile")
				CMB_ProjectileMESH_E_Tooltip = GadgetBalloonToolTip(#Window_CreateNewProjectiles, FindWindowEx_(GadgetID(#CMB_ProjectileMESH), 0, "Edit", 0), "Select the mesh used by the projectile")
				TextGadget(#PB_Any, 3, 161, 190, 16, "Mesh", #PB_Text_Border|#PB_Text_Center)
				TextGadget(#PB_Any, 3, 65, 190, 17, "Emitter 2", #PB_Text_Border|#PB_Text_Center)
				TextGadget(#PB_Any, 3, 17, 190, 17, "Emitter 1", #PB_Text_Border|#PB_Text_Center)
				FrameGadget(#PB_Any, 0, 0, 198, 121, "Emitters")
				FrameGadget(#PB_Any, 0, 127, 198, 138, "Projectile Mesh / Texture")
			CloseGadgetList()
			ContainerGadget(#PB_Any, 219, 20, 110, 224)
				SpinGadget(#SPN_ProjectileSpeed, 5, 185, 90, 25, 0, 100, #PB_Spin_Numeric)
				SPN_ProjectileSpeed_Tooltip = GadgetBalloonToolTip(#Window_CreateNewProjectiles, #SPN_ProjectileSpeed, "This sets the speed at which the projectile will initially start at")
				TextGadget(#PB_Any, 5, 165, 90, 17, "Projectile Speed", #PB_Text_Border|#PB_Text_Center)
				SpinGadget(#SPN_PhysWeight, 5, 74, 90, 25, 0, 100, #PB_Spin_Numeric)
				SPN_PhysWeight_Tooltip = GadgetBalloonToolTip(#Window_CreateNewProjectiles, #SPN_PhysWeight, "This option represents the actual physics weight of the projectile")
				SpinGadget(#SPN_PhysFric, 5, 130, 90, 25, 0, 100, #PB_Spin_Numeric)
				SPN_PhysFric_Tooltip = GadgetBalloonToolTip(#Window_CreateNewProjectiles, #SPN_PhysFric, "This option sets the amount of friction force is applied to the projectile")
				CheckBoxGadget(#CBX_Has_Physics, 5, 26, 78, 15, "Has Physics")
				SetGadgetState(#CBX_Has_Physics, 1)
				CBX_Has_Physics_Tooltip = GadgetBalloonToolTip(#Window_CreateNewProjectiles, #CBX_Has_Physics, "Selecting this option will allow physics to apply to the projectile")
				TextGadget(#PB_Any, 5, 55, 90, 17, "Weight", #PB_Text_Border|#PB_Text_Center)
				TextGadget(#PB_Any, 5, 110, 90, 17, "Friction", #PB_Text_Border|#PB_Text_Center)
				FrameGadget(#PB_Any, 0, 0, 105, 219, "Physics Info")
			CloseGadgetList()
			ContainerGadget(#PB_Any, 219, 245, 110, 145)
				ComboBoxGadget(#CMB_ProjectileName, 5, 65, 90, 20, #PB_ComboBox_Editable)
				CMB_ProjectileName_Tooltip = GadgetBalloonToolTip(#Window_CreateNewProjectiles, #CMB_ProjectileName, "Enter projectile name for new projectile or select created projectile to edit or delete")
				CMB_ProjectileName_E_Tooltip = GadgetBalloonToolTip(#Window_CreateNewProjectiles, FindWindowEx_(GadgetID(#CMB_ProjectileName), 0, "Edit", 0), "Enter projectile name for new projectile or select created projectile to edit or delete")
				TextGadget(#PB_Any, 5, 45, 90, 17, "Projectile Name", #PB_Text_Border|#PB_Text_Center)
				CheckBoxGadget(#CBX_TrackTarget, 5, 20, 87, 17, "Tracks Target")
				CBX_TrackTarget_Tooltip = GadgetBalloonToolTip(#Window_CreateNewProjectiles, #CBX_TrackTarget, "This will cause the projectile to always head towards the target even if the target is moving")
				ComboBoxGadget(#CMB_ProjectileScript, 5, 113, 90, 20, #PB_ComboBox_Editable)
				CMB_ProjectileScript_Tooltip = GadgetBalloonToolTip(#Window_CreateNewProjectiles, #CMB_ProjectileScript, "Select a script to be run when the projectile reaches it's destination or its maximum distance")
				CMB_ProjectileScript_E_Tooltip = GadgetBalloonToolTip(#Window_CreateNewProjectiles, FindWindowEx_(GadgetID(#CMB_ProjectileScript), 0, "Edit", 0), "Select a script to be run when the projectile reaches it's destination or its maximum distance")
				TextGadget(#PB_Any, 5, 93, 90, 17, "Destination Script", #PB_Text_Border|#PB_Text_Center)
				FrameGadget(#PB_Any, 0, 0, 105, 140, "Various Options")
			CloseGadgetList()
			ContainerGadget(#PB_Any, 342, 20, 120, 86)
				FrameGadget(#PB_Any, 0, 0, 114, 80, "Projectile State")
				OptionGadget(#OPT_ProjectileState_SOLID, 5, 15, 43, 15, "Solid")
				OPT_ProjectileState_SOLID_Tooltip = GadgetBalloonToolTip(#Window_CreateNewProjectiles, #OPT_ProjectileState_SOLID, "the state in which a substance has no tendency to flow under moderate stress; resists forces (such as compression) that tend to deform it; and retains a definite size and shape")
				SetGadgetState(#OPT_ProjectileState_SOLID, 1)
				OptionGadget(#OPT_ProjectileState_LIQUID, 5, 30, 48, 15, "Liquid")
				OPT_ProjectileState_LIQUID_Tooltip = GadgetBalloonToolTip(#Window_CreateNewProjectiles, #OPT_ProjectileState_LIQUID, "the state in which a substance exhibits a characteristic readiness to flow with little or no tendency to disperse and relatively high incompressibility")
				OptionGadget(#OPT_ProjectileState_GAS, 5, 45, 39, 15, "Gas")
				OPT_ProjectileState_GAS_Tooltip = GadgetBalloonToolTip(#Window_CreateNewProjectiles, #OPT_ProjectileState_GAS, "the state of matter distinguished from the solid and liquid states by: relatively low density and viscosity; relatively great expansion and contraction with changes in pressure and temperature; the ability to diffuse readily; and the spontaneous tendency to become distributed uniformly throughout any container ")
				OptionGadget(#OPT_ProjectileState_MAGIC, 5, 60, 49, 15, "Magic")
				OPT_ProjectileState_MAGIC_Tooltip = GadgetBalloonToolTip(#Window_CreateNewProjectiles, #OPT_ProjectileState_MAGIC, "If the projectile state does not fall in to one of the other 3 categories then select this one.")
			CloseGadgetList()
			ContainerGadget(#PB_Any, 5, 292, 203, 95)
				TextGadget(#PB_Any, 12, 63, 47, 17, "Type", #PB_Text_Border|#PB_Text_Center)
				StringGadget(#IPB_DamageInformation_MINDMG, 60, 16, 125, 18, "", #PB_String_Numeric|#WS_BORDER)
				IPB_DamageInformation_MINDMG_Tooltip = GadgetBalloonToolTip(#Window_CreateNewProjectiles, #IPB_DamageInformation_MINDMG, "This should be set to the minimum damage amount the projectile will do on a successful hit")
				StringGadget(#IPB_DamageInformation_MAXDMG, 60, 39, 125, 18, "", #PB_String_Numeric|#WS_BORDER)
				IPB_DamageInformation_MAXDMG_Tooltip = GadgetBalloonToolTip(#Window_CreateNewProjectiles, #IPB_DamageInformation_MAXDMG, "This should be set to the maximum damage amount the projectile will do on a successful hit")
				TextGadget(#PB_Any, 13, 17, 46, 17, "Minimum", #PB_Text_Border|#PB_Text_Right)
				TextGadget(#PB_Any, 12, 40, 47, 17, "Maximum", #PB_Text_Border|#PB_Text_Right)
				FrameGadget(#PB_Any, 0, 0, 199, 91, "Damage Information")
				ComboBoxGadget(#CMB_DamageInformation_TYPE, 60, 62, 125, 20)
				CMB_DamageInformation_TYPE_Tooltip = GadgetBalloonToolTip(#Window_CreateNewProjectiles, #CMB_DamageInformation_TYPE, "This is the damage type this projectile will do if it successfully deals damage")
			CloseGadgetList()
		CloseGadgetList()
		ButtonGadget(#BTN_SAVEProjectiles, 481, 15, 100, 25, "Save Projectiles")
		BTN_SAVEProjectiles_Tooltip = GadgetBalloonToolTip(#Window_CreateNewProjectiles, #BTN_SAVEProjectiles, "This saves the projectiles to the projectiles datafile")
		ButtonGadget(#BTN_DELETEProjectiles, 480, 45, 100, 25, "Delete Projectile")
		BTN_DELETEProjectiles_Tooltip = GadgetBalloonToolTip(#Window_CreateNewProjectiles, #BTN_DELETEProjectiles, "Delete the currently displayed projectile")
		ButtonGadget(#BTN_QUITProjectiles, 481, 75, 100, 25, "Quit")
		BTN_QUITProjectiles_Tooltip = GadgetBalloonToolTip(#Window_CreateNewProjectiles, #BTN_QUITProjectiles, "This exits the projectile manager without saving the currently created projectiles")
	EndIf

EndProcedure



Procedure OpenWindow_Window_ProjectilesManager_Main()
	
	If OpenWindow(#Window_ProjectilesManager_Main, 461, 30, 402, 565, "Projectiles Manager [" + ProjectName$ + "]", #PB_Window_SystemMenu|#PB_Window_TitleBar)
		FrameGadget(#PB_Any, 0, 10, 400, 470, "Current Projectiles")
		ListIconGadget(#LIB_Projectiles_List, 4, 25, 391, 450, "Name", 100, #PB_ListIcon_AlwaysShowSelection|#PB_ListIcon_GridLines|#PB_ListIcon_FullRowSelect)
		SendMessage_(GadgetID(#LIB_Projectiles_List), #LVM_SETCOLUMNWIDTH, 0, #LVSCW_AUTOSIZE_USEHEADER)
		LIB_Projectiles_List_Tooltip = GadgetBalloonToolTip(#Window_ProjectilesManager_Main, #LIB_Projectiles_List, "This lists all the currently created projectiles")
		ButtonGadget(#BTN_AddProjectile, 10, 495, 95, 30, "Add Projectile")
		BTN_AddProjectile_Tooltip = GadgetBalloonToolTip(#Window_ProjectilesManager_Main, #BTN_AddProjectile, "Create a new projectile")
		ButtonGadget(#BTN_RemoveProjectile, 200, 495, 95, 30, "Remove Projectile")
		BTN_RemoveProjectile_Tooltip = GadgetBalloonToolTip(#Window_ProjectilesManager_Main, #BTN_RemoveProjectile, "Delete the selected projectile from the projectiles list")
		ButtonGadget(#BTN_EditProjectile, 105, 495, 95, 30, "Edit Projectile")
		BTN_EditProjectile_Tooltip = GadgetBalloonToolTip(#Window_ProjectilesManager_Main, #BTN_EditProjectile, "Edit the properties of the selected projectile")
		ButtonGadget(#BTN_SAVE, 295, 495, 95, 30, "Save Projectiles")
		Button_SAVE_Tooltip = GadgetBalloonToolTip(#Window_ProjectilesManager_Main, #BTN_SAVE, "Saves the current projectiles to the projectiles data file")
		ButtonGadget(#BTN_QUIT, 10, 528, 380, 25, "Q U I T")
		Button_QUIT_Tooltip = GadgetBalloonToolTip(#Window_ProjectilesManager_Main, #BTN_QUIT, "Exit the Projectiles Manager without saving the current session to the projectiles data file")
	EndIf
	
EndProcedure

ProcessProjectiles(0)
GetDMGTypes()
OpenWindow_Window_ProjectilesManager_Main()
direction = 2
;{- Event loop
Repeat
	
	Repeat
		
		Event = WindowEvent()
		Select Event
			
			Case #PB_Event_Gadget
			
				EventGadget = EventGadget()
				EventType = EventType()
				If EventGadget = #LIB_Projectiles_List
					
				ElseIf EventGadget = #BTN_AddProjectile
					
					OpenWindow_Window_CreateNewProjectiles()
					ForEach DMGType()
						DamageData$ = UCase(DMGType()\name)
						AddGadgetItem(#CMB_DamageInformation_TYPE, -1, DamageData$)
						NewCombat +1
					Next
					If NewCombat < 50
						SetGadgetText(#CMB_DamageInformation_TYPE, "No Damage Types Found")
					EndIf
					
				ElseIf EventGadget = #BTN_RemoveProjectile
				
				ElseIf EventGadget = #BTN_EditProjectile
				
				ElseIf EventGadget = #BTN_SAVE
				
					ProcessProjectiles(2)
				
				ElseIf EventGadget = #BTN_QUIT
				
					CloseWindow(#Window_ProjectilesManager_Main)
;					CloseWindow(#Window_Preview_Main)
					End
					
				ElseIf EventGadget = #SPN_ProjectileSpeed
				ElseIf EventGadget = #SPN_PhysWeight
				ElseIf EventGadget = #SPN_PhysFric
				ElseIf EventGadget = #CBX_Has_Physics
				ElseIf EventGadget = #CMB_ProjectileMeshTexture
				ElseIf EventGadget = #CMB_Emitter1Select
				ElseIf EventGadget = #CMB_Emitter2Select
				ElseIf EventGadget = #CMB_ProjectileMESH
				ElseIf EventGadget = #CMB_ProjectileName
				ElseIf EventGadget = #CBX_TrackTarget
				ElseIf EventGadget = #CMB_ProjectileScript
				ElseIf EventGadget = #OPT_ProjectileState_SOLID
				ElseIf EventGadget = #OPT_ProjectileState_LIQUID
				ElseIf EventGadget = #OPT_ProjectileState_GAS
				ElseIf EventGadget = #OPT_ProjectileState_MAGIC
				ElseIf EventGadget = #IPB_DamageInformation_MINDMG
				ElseIf EventGadget = #IPB_DamageInformation_MAXDMG
				ElseIf EventGadget = #CMB_DamageInformation_TYPE
				ElseIf EventGadget = #BTN_SAVEProjectiles
				ElseIf EventGadget = #BTN_DELETEProjectiles
				ElseIf EventGadget = #BTN_QUITProjectiles
				
				EndIf
			
			Case #PB_Event_CloseWindow
			
				EventWindow = EventWindow()
				If EventWindow = #Window_ProjectilesManager_Main
					
					CloseWindow(#Window_ProjectilesManager_Main)
					
					If IsWindow(#Window_Preview_Main)
						
						CloseWindow(#Window_Preview_Main)
						
					EndIf
					
					If IsWindow(#Window_CreateNewProjectiles)
						
						CloseWindow(#Window_CreateNewProjectiles)
						
					EndIf
					End
				ElseIf EventWindow = #Window_Preview_Main
					
					CloseWindow(#Window_Preview_Main)
					
				ElseIf EventWindow = #Window_CreateNewProjectiles
					
					CloseWindow(#Window_CreateNewProjectiles)
					
				EndIf
			
		EndSelect
		
	Until Event = 0
	
Forever
;
;}

; IDE Options = PureBasic 5.21 LTS (Windows - x64)
; CursorPosition = 215
; Folding = H17
; EnableUser
; Executable = ProjectilesManager.exe
; CommandLine = 100,0,49,55|C:\Compile\Project1
; CurrentDirectory = C:\Program Files\PureBasic\
; EnableBuildCount = 4